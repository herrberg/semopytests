python run_semopy.py --method SUMSL --dir_name set9 --dir_out_postfix _semopy_SUMSL
python test.py --source set9 --target set9_semopy_SUMSL --out_dir set9_semopy_SUMSL
python run_semopy.py --method HUSML --dir_name set9 --dir_out_postfix _semopy_HUSML
python test.py --source set9 --target set9_semopy_HUSML --out_dir set9_semopy_HUSML
python run_semopy.py --method Adam --dir_name set9 --dir_out_postfix _semopy_Adam
python test.py --source set9 --target set9_semopy_Adam --out_dir set9_semopy_Adam
