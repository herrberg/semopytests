import numpy as np
import pandas as pd
import argparse
import pickle
import os
from tqdm import tqdm
from semopy import Model, Optimizer
from utils import MODS_FOLDER, RESULTS_FOLDER, TESTS_FOLDER


def parse_argv():
    parser = argparse.ArgumentParser(description='Test results against correct parameters.')
    parser.add_argument('--models_range', nargs=2, type=int, default=(1, 1001))
    parser.add_argument('--source', type=str)
    parser.add_argument('--target', type=str)
    parser.add_argument('--out_dir', type=str)
    parser.add_argument('--objective', type=str, default='')
    return parser.parse_args()


def params_to_table(opt):
    params = opt.params
    mod = opt.model
    t = list()
    # Beta
    op = mod.operations.REGRESSION.value
    for i in range(*mod.beta_range):
        j = i - mod.beta_range[0]
        m, n = mod.parameters['Beta'][j]
        lval, rval = mod.beta_names[0][m], mod.beta_names[1][n]
        t.append({'lhs': lval, 'op': op, 'rhs': rval,
                  'est': params[i]})
    # Lambda
    op = mod.operations.MEASUREMENT.value
    for i in range(*mod.lambda_range):
        j = i - mod.lambda_range[0]
        m, n = mod.parameters['Lambda'][j]
        lval, rval = mod.lambda_names[1][n], mod.lambda_names[0][m]
        t.append({'lhs': lval, 'op': op, 'rhs': rval,
                  'est': params[i]})
    # Psi
    op = mod.operations.COVARIANCE.value
    for i in range(*mod.psi_range):
        j = i - mod.psi_range[0]
        m, n = mod.parameters['Psi'][j]
        lval, rval = mod.psi_names[0][m], mod.psi_names[1][n]
        t.append({'lhs': lval, 'op': op, 'rhs': rval,
                  'est': params[i]})
    # Theta
    op = mod.operations.COVARIANCE.value
    for i in range(*mod.theta_range):
        j = i - mod.theta_range[0]
        m, n = mod.parameters['Theta'][j]
        lval, rval = mod.theta_names[0][m], mod.theta_names[1][n]
        t.append({'lhs': lval, 'op': op, 'rhs': rval,
                  'est': params[i]})
    return pd.DataFrame(t, columns=['lhs', 'op', 'rhs', 'est'])


def load_parameters(opt, param_table):
    mod = opt.model
    ops = mod.operations
    params = opt.params
    for ind, row in param_table.iterrows():
        lhs, op, rhs, est = row['lhs'], ops(row['op']), row['rhs'], row['est']
        if op == ops.REGRESSION:
            n, m = mod.beta_names[0].index(lhs), mod.beta_names[1].index(rhs)
            i = mod.parameters['Beta'].index((n, m)) + mod.beta_range[0]
            opt.mx_beta[n, m] = est
            params[i] = est
        elif op == ops.MEASUREMENT:
            n, m = mod.lambda_names[0].index(rhs),\
                   mod.lambda_names[1].index(lhs)
            i = mod.parameters['Lambda'].index((n, m)) + mod.lambda_range[0]
            opt.mx_lambda[n, m] = est
            params[i] = est
        elif op == ops.COVARIANCE:
            if lhs in mod.vars['Indicators']:
                n, m = mod.theta_names[0].index(lhs),\
                       mod.theta_names[1].index(rhs)
                i = mod.parameters['Theta'].index((n, m)) + mod.theta_range[0]
                opt.mx_theta[n, m] = est
                params[i] = est
            else:
                n, m = mod.psi_names[0].index(lhs), mod.psi_names[1].index(rhs)
                try:
                    i = mod.parameters['Psi'].index((n, m)) + mod.psi_range[0]
                except ValueError:
                    try:
                        i = mod.parameters['Psi'].index((m, n)) + mod.psi_range[0]
                    except ValueError:
                        # ULS has variables for exogenous variables?!
                            continue
                opt.mx_psi[n, m] = est
                opt.mx_psi[m, n] = est
                params[i] = est


def norm(x):
    return x['est'].abs().sum(numeric_only=True).sum()


def cmp_params(a, b, cutoff=0.3):
    a, b = a[a.op != '~~'], b[b.op != '~~']
    cols = ['op', 'lhs', 'rhs']
    a, b = a.sort_values(by=cols)['est'].values, b.sort_values(by=cols)['est'].values
    diff = np.mean(np.abs((a - b) / b))
    return diff, diff > cutoff


if __name__ == '__main__':
    args = parse_argv()
    dir_target = '{}/{}'.format(RESULTS_FOLDER, args.target)
    dir_source = '{}/{}'.format(MODS_FOLDER, args.source)
    dir_output = '{}/{}'.format(TESTS_FOLDER, args.out_dir)
    if not os.path.exists(TESTS_FOLDER):
        os.makedirs(TESTS_FOLDER)
    if not os.path.exists(dir_output):
        os.makedirs(dir_output)
    obj = args.objective
    if obj == 'ULS' or (not obj and 'ULS' in args.target):
        obj = 'ULS'
    elif obj == 'GLS' or (not obj and 'GLS' in args.target):
        obj = 'GLS'
    else:
        obj = 'MLW'
    mods = list()
    errors = list()
    nonconv = list()
    of_vals = list()
    filename_info = dir_target + '/info.txt'
    with open(filename_info, 'r') as f:
        time = float(f.readline())
    for i in tqdm(range(*args.models_range)):
        filename_data = dir_source + '/data{}.txt'.format(i)
        filename_model = dir_source + '/mod{}.txt'.format(i)
        filename_params = dir_source + '/params{}.txt'.format(i)
        filename_result = dir_target + '/result{}.txt'.format(i)

        params = pd.read_csv(filename_params, sep=',', index_col=None)
        data = pd.read_csv(filename_data, sep=',', index_col=None)
        with open(filename_model) as f:
            mod = f.read()
        result = pd.read_csv(filename_result, sep=',', index_col=None)
        mod = Model(mod)
        mod.load_dataset(data)
        opt = Optimizer(mod)
        load_parameters(opt, params)
        if obj == 'ULS':
            of_val = opt.unweighted_least_squares(opt.params)
        elif obj == 'GLS':
            of_val = opt.general_least_squares(opt.params)
        else:
            of_val = opt.ml_wishart(opt.params)
        of_vals.append(of_val)
        diff, conv = cmp_params(result, params)
        errors.append(diff)
        nonconv.append(conv | np.isnan(of_val) | np.isnan(diff))
        mods.append(i)
    nonconv = np.array(nonconv)
    mods = np.array(mods)
    errors = np.array(errors)
    of_vals = np.array(of_vals)
    d_p = {'mods': mods, 'nonconv': nonconv, 'errors': errors,
           'of_vals': of_vals, 'objective': obj, 'time': time}
    with open('{}/test_data.pickle'.format(dir_output), 'wb') as f:
        pickle.dump(d_p, f)
    '''
    mean_error = np.mean(errors); median_error = np.median(errors)
    mean_of = np.mean(of_vals); median_of = np.median(of_vals)
    num_nonconv = np.count_nonzero(nonconv)
    nonconv_inds = mods[nonconv]
    with open('{}/table.txt'.format(dir_output), 'w') as f:
        f.write("N_nonconv| Mean error| Median error| Mean OF value| Median OF value\n")
        f.write("{:>10}| {:>10}| {:>10}| {:>10}| {:>10}\n".format('N_nonconv', 'Mean error', 'Median error', 'Mean OF', 'Median OF'))
        f.write('-'*60, '\n')
        f.write("{:>10}| {:>10}| {:>10}| {:>10}| {:>10}\n".format(num_nonconv, mean_error, median_error, mean_of, median_of))
    with open('{}/nonconvs.txt'.format(dir_output), 'w') as f:
        f.write(nonconv_inds)'''