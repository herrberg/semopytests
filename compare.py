from itertools import combinations
from utils import TESTS_FOLDER
import pandas as pd
import numpy as np
import argparse
import pickle


def parse_argv():
    parser = argparse.ArgumentParser(description='Compare test results.')
    parser.add_argument('--dirs', nargs='+', type=str)
    parser.add_argument('--out', type=str, default='cmp.txt')
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_argv()
    files = ['{}/{}/test_data.pickle'.format(TESTS_FOLDER, d)
             for d in args.dirs]
    names = [''.join(d.split('_')[-2:]) for d in args.dirs]
    nonconvs, errors, ofs, mods, objs = list(), list(), list(), list(), list()
    times = list()
    sets_nonconvs = dict()
    for file in files:
        with open(file, 'rb') as f:
            d = pickle.load(f)
            nonconvs.append(d['nonconv'])
            errors.append(d['errors'])
            ofs.append(d['of_vals'])
            mods.append(d['mods'])
            objs.append(d['objective'])
            times.append(d['time'])
    nonconv = nonconvs[-1]
    sets_nonconvs[names[-1]] = nonconvs[-1]
    for i in range(len(mods) - 1):
        if np.any(mods[i] != mods[i + 1]):
            raise Exception("Incompatible test sets.")
        sets_nonconvs[names[i]] = nonconvs[i]
        nonconv = nonconv | nonconvs[i]
    conv = ~nonconv
    n_nonconv = np.count_nonzero(conv)
    n_nonconvs = [np.count_nonzero(nconv) for nconv in nonconvs]
    mean_errors = [np.mean(error[conv]) for error in errors]
    median_errors = [np.median(error[conv]) for error in errors]
    mean_ofs = [np.mean(of[conv]) for of in ofs]
    median_ofs = [np.median(of[conv]) for of in ofs]
    t = zip(names, n_nonconvs, mean_ofs, median_ofs, mean_errors,
            median_errors, objs, times)
    sz = max((len(name) for name in names))
    fmt = '{:>' + str(sz) + '}| {:>10}| {:>10}| {:>10}| {:>10}| {:>10}| {:>5}\n'
    with open('{}/{}'.format(TESTS_FOLDER, args.out), 'w') as f:
        header = fmt.format('#(Method)', 'N_nonconv', 'Mean OF', 'Median OF',
                            'Mean error', 'Median error', 'Time')
        f.write(header)
#        line = "-" * (62 + sz) + '\n'
#        row = "{:>" + str(sz) + "}| {:10}| {:10.4f}| {:10.4f}| {:10.4f}| {:10.4f}\n"
        row = "{}&{:10}&{:.4f}&{:.4f}&{:.4f}&{:.4f}&{} \\\ " + '\\' + 'hline\n'
        for name, n, mean_of, median_of, mean_e, median_e, obj, time in t:
#            f.write(line)
            if 'semopy' in name:
                t = name.split('semopy')
                t[0] = 'semopy'
            else:
                t = name.split('lav')
                t[0] = 'lavaan'
            name = '\\textbf{' + t[0] + '}/' + t[1]
            f.write(row.format(name, n, mean_of, median_of, mean_e, median_e, int(time)))
            #print(mods[i][nonconvs[i]])
        f.write('\n\nDiff of nonconv-sets:\n')
        t = list()
#        df = pd.DataFrame(data=np.zeros((len(names), len(names))),
#                          index=names, columns=names)
        mx = np.zeros((len(names), len(names)))
        
        for a, b in combinations(sets_nonconvs.keys(), 2):
            alen, blen = np.count_nonzero(sets_nonconvs[a]), np.count_nonzero(sets_nonconvs[b])
            set1 = sets_nonconvs[a] & (~sets_nonconvs[b])
            set2 = sets_nonconvs[b] & (~sets_nonconvs[a])
            ilen1 = np.count_nonzero(set1)
            ilen2 = np.count_nonzero(set2)
            i, j = names.index(a), names.index(b)
            mx[i, i] = alen
            mx[j, j] = blen
            mx[i, j] = ilen1
            mx[j, i] = ilen2
            f.write("{:20}({:3})|{:20}({:3}) : {:3}/{:3}\n".format(a, alen, b, blen, ilen1, ilen2))
            t.append((a, b, set1, set2))
        pd.set_option('display.max_rows', 500)
        pd.set_option('display.max_columns', 500)
        pd.set_option('display.width', 1000)
        f.write(str(pd.DataFrame(data=mx, index=names, columns=names)))
        f.write("\n\n\n\n")
        for key, item in sets_nonconvs.items():
            f.write("{}:\n{}\n".format(key, np.where(item)[0]))
        f.write("\n")
        for a, b, set1, set2 in t:
            f.write("Did converge for {:15}, but didn't for {:15}:\n".format(b, a))
            f.write(str(np.where(set1)[0]))
            f.write("\nDid converge for {:15}, but didn't for {:15}:\n".format(a, b))
            f.write(str(np.where(set2)[0]))