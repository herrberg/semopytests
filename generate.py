from semopy.model_generator import generate_model
from utils import MODS_FOLDER
import numpy.random as rnd
from tqdm import tqdm
import argparse
import os


def parse_argv():
    parser = argparse.ArgumentParser(description="Generate random models.")
    parser.add_argument("--num_models", type=int, default=1000)
    parser.add_argument('--num_latents', nargs=2, type=int, default=(0, 3))
    parser.add_argument('--num_obs', nargs=2, type=int, default=(2, 6))
    parser.add_argument('--num_inds', nargs=2, type=int, default=(2, 5))
    parser.add_argument('--p_inds', type=float, default=0.0)
    parser.add_argument('--num_cycles', nargs=2, type=int, default=(0, 1))
    parser.add_argument('--num_samples', nargs=2, type=int,
                        default=(100, 500))
    parser.add_argument('--param_scale', nargs=2, type=float, default=(1, 1))
    parser.add_argument('--param_shift', type=float, default=0.0)
    parser.add_argument('--output_dir', type=str, default='mods')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_argv()
    dir_name = '{}/'.format(MODS_FOLDER) + args.output_dir
    if not os.path.exists(MODS_FOLDER):
        os.makedirs(MODS_FOLDER)
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    scale = rnd.uniform(*args.param_scale)
    with open('{}/info.txt'.format(dir_name), 'w') as f:
        f.write(str(args))
    for i in tqdm(range(1, args.num_models + 1)):
        n_latents = rnd.randint(*args.num_latents)
        n_obs = rnd.randint(*args.num_obs)
        n_inds = args.num_inds
        n_samples = rnd.randint(*args.num_samples)
        n_cycles = rnd.randint(*args.num_cycles)
        p_inds = args.p_inds
        model, params_df, data = generate_model(n_obs, n_latents, n_inds,
                                                p_inds, n_cycles, scale,
                                                n_samples)
        data.to_csv('{}/data{}.txt'.format(dir_name, i), sep=',', index=False)
        params_df.to_csv('{}/params{}.txt'.format(dir_name, i), sep=',',
                         index=False)
        with open('{}/mod{}.txt'.format(dir_name, i), 'w') as f:
            f.write(model)
        with open('{}/mod{}_info.txt'.format(dir_name, i), 'w') as f:
            s = 'n_latents = {}, n_obs = {}, n_samples = {}, n_cycles = {}'
            f.write(s.format(n_latents, n_obs, n_samples, n_cycles))