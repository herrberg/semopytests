  library(lavaan)
  library(progress)
  args = commandArgs(trailingOnly=TRUE)
  method <- args[2]
  dir_models <- paste0('mods/', args[1])
  dir_out <- paste0('results/', args[1], '_lav_', method)
  num_models <- args[3]
  dir.create(dir_out, showWarnings = FALSE)
  tt = Sys.time()
  if (method == 'MLW')
  {
   method <- 'ML'
  }
  pb <- progress_bar$new(format="[:bar] :current/:total (:percent), eta: :eta, elapsed: :elapsed", total = num_models)
  for(i in 1:num_models){
    filename_mod <- paste0(dir_models, '/mod', i, '.txt')
    filename_data <- paste0(dir_models, '/data', i, '.txt')
    filename_mlw_out <- paste0(dir_out, '/result', i, '.txt')
    
    mod <- readLines(filename_mod)
    data <- read.csv(filename_data, header = TRUE)
    fit <- sem(mod, data=data, estimator=method)
    t <- inspect(fit, what="list")
    t <- t[!t$free==0, ]
    t <- t[, c('lhs', 'op', 'rhs', 'est')]
    write.csv(t, file = filename_mlw_out, row.names=FALSE)
    pb$tick()
  }
  time_lav = as.numeric(Sys.time() - tt, units = "secs")
  write(c(time_lav, args), paste0(dir_out, '/info.txt'))