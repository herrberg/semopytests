import pandas as pd
from utils import MODS_FOLDER, RESULTS_FOLDER, TESTS_FOLDER
import os

dirs = [MODS_FOLDER, RESULTS_FOLDER, TESTS_FOLDER]
# MLW here stands for SLSQP
optims = ['MLW', 'L-BFGS-B', 'SMSNO', 'SUMSL', 'HUSML', 'Adam']
optim_set = None
for d in dirs:
    if not os.path.isdir(d):
        os.makedirs(d)
sets = pd.read_csv('sets.csv', index_col=None)
sets_methods = dict()
with open('generate.sh', 'w') as f:
    s = 'python generate.py --num_latents {} --num_obs {} --num_inds {} --p_inds {} --num_cycles {} --num_samples {} --param_scale {} --output_dir {}\n'
    for _, row in sets.iterrows():
        line = s.format(row['n_lat'], row['n_obs'], row['n_manif'], row['p_manif'],
                        row['n_cycles'], row['n_samples'], row['scale'],
                        row['Set ID'])
        f.write(line)
        sets_methods[row['Set ID']] = row['methods'].split(' ')
    optim_set = row['Set ID']
# Instead of last Set we fix it to another.
optim_set = 'set9'



with open('run_semopy.sh', 'w') as f, open('test_semopy.sh', 'w') as f2:
    s1 = 'python run_semopy.py --objective {} --dir_name {} --dir_out_postfix _semopy_{}\n'
    s2 = 'python test.py --source {} --target {}_semopy_{} --out_dir {}_semopy_{}\n'
    for name, methods in sets_methods.items():
        for method in methods:
            ts1 = s1.format(method, name, method)
            ts2 = s2.format(name, name, method, name, method)
            f.write(ts1)
            f.write(ts2)
            f2.write(ts2)

with open('run_semopy_opt.sh', 'w') as f, open('test_semopy_opt.sh', 'w') as f2:
    s1 = 'python run_semopy.py --method {} --dir_name {} --dir_out_postfix _semopy_{}\n'
    s2 = 'python test.py --source {} --target {}_semopy_{} --out_dir {}_semopy_{}\n'
    for method in optims:
        ts1 = s1.format(method, optim_set, method)
        ts2 = s2.format(name, optim_set, method, optim_set, method)
        f.write(ts1)
        f.write(ts2)
        f2.write(ts2)
with open('compare_opt.sh', 'w') as f:
    s = 'python compare.py --dir {} --out {}\n'
    d_so = ' '.join('{}_semopy_{}'.format(optim_set, opt) for opt in optims)
    f.write(s.format(d_so, 'opts.txt'))

with open('run_lavaan.sh', 'w') as f, open('test_lavaan.sh', 'w') as f2:
    s1 = 'Rscript run_lavaan.r {} {} 1000\n'
    s2 = 'python test.py --source {} --target {}_lav_{} --out_dir {}_lav_{}\n'
    for name, methods in sets_methods.items():
        for method in methods:
            ts1 = s1.format(name, method)
            ts2 = s2.format(name, name, method, name, method)
            f.write(ts1)
            f.write(ts2)
            f2.write(ts2)

with open('compare.sh', 'w') as f:
    s = 'python compare.py --dir {} --out {}\n'
    d_so = '{}_semopy_{}'
    d_lav = '{}_lav_{}'
    f_out = '{}_{}.txt'
    for name, methods in sets_methods.items():
        for method in methods:
            d1 = d_so.format(name, method)
            d2 = d_lav.format(name, method)
            d = '{} {}'.format(d1, d2)
            out = f_out.format(name, method)
            f.write(s.format(d, out))