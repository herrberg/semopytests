from utils import TESTS_FOLDER
from os import listdir
from os.path import isfile, join
from collections import defaultdict
import argparse

groups_filename = 'graphgroups.txt'
#First goes lavaan - GLS - MLW - ULS, then goes semopy
sets_of_styles = [('red', 'square'), ('blue', 'o')]
template_start = '''\\begin{figure}[H]
\\centerfloat
\\begin{adjustbox}{max width=\\textwidth}
\\begin{tabular}{ccc}
'''

# color, mark, legend entry, coordinates (tuples)
template_start_addplot = '''\\begin{tikzpicture}
    \\begin{scope}[scale=0.46, transform shape]
	\\pgfkeys{{/pgfplots/legend entry/.code=\\addlegendentry{#1}}}
	\\begin{axis}[
	xlabel={Set},
	legend pos=north east,
	ymajorgrids=true,
	grid style=dashed,
	]
    '''
template_start_addplot_leftest = '''\\begin{tikzpicture}
    \\begin{scope}[scale=0.46, transform shape]
	\\pgfkeys{{/pgfplots/legend entry/.code=\\addlegendentry{#1}}}
	\\begin{axis}[
	xlabel={Set},
	ylabel={$N$, number of non-convergent models},
	legend pos=north east,
	ymajorgrids=true,
	grid style=dashed,
	]
    '''
template_addplot = '''\\addplot[
	color={},
	mark={},
	legend entry={},
	only marks
	]
	coordinates {{
		{}
	}};
    '''
template_addplot_end = '''\\end{axis}
    \\end{scope}
    \\end{tikzpicture}'''
# Captions for figures
template_end = '''\\\\ {} & {} & {}
    \\end{{tabular}}
    \\end{{adjustbox}}
	\\caption{{{}}}
	\\label{{{}}}
\end{{figure}}
'''

def parse_argv():
    parser = argparse.ArgumentParser(description='Create latex tables.')
    parser.add_argument('--range', nargs=2, type=int, default=(0, 15))
    parser.add_argument('--methods', type=str, default='MLW,ULS,GLS')
    return parser.parse_args()


args = parse_argv()
start, end = args.range
methods = args.methods.split(',')
head = '\\begin{{table}}[H] \n \\label{{set{}}} \n \\caption{{Set {}}}\n \\begin{{adjustbox}}{{max width=\\textwidth}} \n \\begin{{tabular}}{{|c|c|c|c|c|c|c|c|}}\n'
head += '\\hline Pkg/OF & N & Mean-OF & Median OF & Mean $\\epsilon$ & Median $\\epsilon$ & Time (s) \\\\ \\hline\n'
tail = '\\end{tabular} \n \n \\end{adjustbox} \n \\end{table}'
for i in range(start, end):
    s = head.format(i + 1, i + 1)
    for method in methods:
        filename = TESTS_FOLDER + '/set{}_{}.txt'.format(i, method)
        with open(filename, 'r') as f:
            f.readline()
            line = f.readline()
            while line.strip():
                s += line
                line = f.readline()
    s += tail
    with open(TESTS_FOLDER + '/table{}.txt'.format(i), 'w') as f:
        f.write(s)
        
with open(groups_filename, 'r') as f:
    groups = [l.strip().split(' ') for l in f.readlines() if len(l.strip())]

for group in groups:
    s = str()
    for i in group:
        with open(TESTS_FOLDER + '/table{}.txt'.format(i), 'r') as f:
            s += f.read() + '\n'
    with open(TESTS_FOLDER + '/tables{}-{}.txt'.format(group[0], group[-1]), 'w') as f:
        f.write(s)
times = defaultdict(lambda: defaultdict(list))
for i, group in enumerate(groups):
    gr = defaultdict(lambda: defaultdict(list))
    table_filename = '{}/table{}.txt'.format(TESTS_FOLDER, '{}')
    for table in group:
        with open(table_filename.format(table), 'r') as f:
            lines = f.readlines()
            tl = list()
            for line in lines:
                if line.startswith('\\text'):
                    t = line.split('&')
                    kt = t[:2]
                    kt.append(t[-1].split(' ')[0])
                    tl.append(kt)
#            tl = [l.split('&')[:2] for l in f.readlines() if l.startswith('\\text')]
        for name, val, time in tl:
            a = name.index('{')
            b = name.index('}')
            package = name[a + 1 : b]
            method = name[b + 2:]
            val = int(val)
            gr[method][package].append(val)
            times[method][package].append(float(time))
    content = template_start
    plots = list()
    leftest = True
    for method in methods:
        if leftest:
            plot = template_start_addplot_leftest
            leftest = False
        else:
            plot = template_start_addplot
        for package, style in zip(('lavaan', 'semopy'), sets_of_styles):
            items = ''
            for k, val in zip(group, gr[method][package]):
                items += '({},{})'.format(int(k) + 1, val)
            plot += template_addplot.format(*style, package, items)
        plot += template_addplot_end
        plots.append(plot)
    content += '\n&\n'.join(plots)
    content += template_end.format(*methods, 'Sets {}-{}'.format(group[0], group[-1]),
                                   'graph{}'.format(i + 1))
    with open('{}/graphgroup{}.txt'.format(TESTS_FOLDER, i), 'w') as f:
        f.write(content)

template_start = '''\\begin{figure}[H]
\\centering
\\begin{adjustbox}{max width=\\textwidth}
\\begin{tabular}{ccc}
'''
template_start_addplot_leftmost ='''\\begin{tikzpicture}
    \\begin{scope}[scale=0.48, transform shape]
    \\begin{axis}[
	ybar,
	y axis line style = { opacity = 0 },
    x axis line style = { opacity = 0 },
	xtick style={draw=none},
	ymode=log,
	legend pos=north west,
	enlarge x limits={abs=0.5},
	xtick={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15},
	bar width=3,
    xlabel={Set},
	ylabel={Time (s)},
	ytick=\\empty,
	nodes near coords,
    every node near coord/.append style={font=\\tiny, /pgf/number format/.cd,fixed,precision=1}
	]
'''
template_start_addplot ='''\\begin{tikzpicture}
    \\begin{scope}[scale=0.48, transform shape]
    \\begin{axis}[
	ybar,
	y axis line style = { opacity = 0 },
    axis y line = none,
    x axis line style = { opacity = 0 },
	xtick style={draw=none},
	ymode=log,
	enlarge x limits={abs=0.5},
	xtick={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15},
	bar width=3,
    xlabel={Set},
	ylabel={Time (s)},
	ytick=data,
	nodes near coords,
    every node near coord/.append style={font=\\tiny, /pgf/number format/.cd,fixed,precision=1}
	]
'''
template_addplot = '''\\addplot coordinates{{(1, {}) (2, {}) (3, {}) (4, {}) (5, {}) (6, {}) (7, {}) (8, {}) (9, {}) (10, {}) (11, {}) (12, {}) (13, {}) (14, {}) (15, {})}};
\\addplot coordinates{{(1, {}) (2, {}) (3, {}) (4, {}) (5, {}) (6, {}) (7, {}) (8, {}) (9, {}) (10, {}) (11, {}) (12, {}) (13, {}) (14, {}) (15, {})}};
'''

template_addplot_end = '''\\end{axis}
    \\end{scope}
    \\end{tikzpicture}'''

template_addplot_leftmost_end = '''\\legend{\\textbf{semopy}, \\textbf{lavaan}}
    \\end{axis}
    \\end{scope}
    \\end{tikzpicture}'''
template_end = '''
    \\end{tabular}
    \\end{adjustbox}
	\\caption{Performance benchmark, logarithmic scale}
	\\label{timebench}
\\end{figure}
'''


so_mlw = times['MLW']['semopy']
so_uls = times['ULS']['semopy']
so_gls = times['GLS']['semopy']
la_mlw = times['MLW']['lavaan']
la_uls = times['ULS']['lavaan']
la_gls = times['GLS']['lavaan']
content = template_start
content += template_start_addplot_leftmost
content += template_addplot.format(*so_mlw, *la_mlw)
content += template_addplot_leftmost_end
content += '\n & \n'

content += template_start_addplot
content += template_addplot.format(*so_uls, *la_uls)
content += template_addplot_end
content += '\n & \n'

content += template_start_addplot
content += template_addplot.format(*so_gls, *la_gls)
content += template_addplot_end
content += '\n \\\\ \n MLW & ULS & GLS'

content += template_end

#for i in range(1, 10):
#    if (i - 1) % 3 == 0:
#        content += template_start_addplot_leftmost
#    else:
#        content += template_start_addplot
#    so = times['MLW']['semopy']
#    k = i - 1
#    content += template_addplot.format(so_mlw[k], so_uls[k], so_gls[k], la_mlw[k], la_uls[k], la_gls[k])
#    content += template_addplot_end
#    if i % 3 == 0:
#        content += '\n\\\\ Set {} & Set {} & Set {} \\\\\n'.format(i-2, i-1, i)
#    else:
#        content += '\n&\n'
#content += template_end
#
#content += '\n\n' + template_start
#for i in range(10, 16):
#    if (i - 1) % 3 == 0:
#        content += template_start_addplot_leftmost
#    else:
#        content += template_start_addplot
#    so = times['MLW']['semopy']
#    k = i - 1
#    content += template_addplot.format(so_mlw[k], so_uls[k], so_gls[k], la_mlw[k], la_uls[k], la_gls[k])
#    content += template_addplot_end
#    if i % 3 == 0:
#        content += '\n\\\\ Set {} & Set {} & Set {} \\\\\n'.format(i-2, i-1, i)
#    else:
#        content += '\n&\n'
#content += template_end

with open('{}/perfgraph.txt'.format(TESTS_FOLDER), 'w') as f:
    f.write(content)