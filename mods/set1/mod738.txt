eta1 =~ y1 + y2
eta2 =~ y3 + y4
x5 ~ x2 + x3 + x4
eta1 ~ x5
eta2 ~ eta1
x1 ~ eta2
