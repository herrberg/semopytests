eta1 =~ y1 + y2
eta2 =~ y3 + y4
x2 ~ x4
x5 ~ x1 + x2
x1 ~ x3
eta2 ~ eta1 + x1
