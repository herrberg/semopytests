eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x1 ~ x2
eta2 ~ x1 + x4 + x5
eta1 ~ x1
x5 ~ x3
