eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x5 ~ x2 + x4
x3 ~ eta1 + x5
x1 ~ x2
eta2 ~ x4
