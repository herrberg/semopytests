eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x3 ~ eta2 + x2 + x5
eta1 ~ x4 + x5
x1 ~ eta1
