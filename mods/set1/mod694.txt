eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta1 ~ x4
x3 ~ eta1
x5 ~ x1 + x3
eta2 ~ x4
x2 ~ eta1
