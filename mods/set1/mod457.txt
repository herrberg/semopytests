eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x1 ~ x3 + x4
x5 ~ x3
x2 ~ eta2 + x5
eta1 ~ x4
