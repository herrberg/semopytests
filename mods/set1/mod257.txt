eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x2 ~ x1
x4 ~ x2
eta2 ~ x4
x3 ~ x1
eta1 ~ x4
x5 ~ x4
