eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x2 ~ x3
eta1 ~ x2
x5 ~ eta2 + x3 + x4
x4 ~ x1
