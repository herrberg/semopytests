eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x5 ~ x1
eta1 ~ x2 + x5
x2 ~ eta2
x4 ~ x5
x3 ~ x2
