eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x1 ~ eta1
x5 ~ x1
x3 ~ x5
eta2 ~ x3
x2 ~ eta2
x4 ~ x2
