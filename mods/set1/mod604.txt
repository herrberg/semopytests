eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta2 ~ x1
x4 ~ eta2
x5 ~ eta2
x2 ~ eta1 + eta2
eta1 ~ x3
