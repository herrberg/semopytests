eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta1 ~ eta2 + x4
x1 ~ eta1 + x5
x2 ~ eta1
x5 ~ x3
