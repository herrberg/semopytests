eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta2 ~ x2 + x5
eta1 ~ eta2
x2 ~ x4
x1 ~ x5
x3 ~ x4
