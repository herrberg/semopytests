eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta1 ~ x1 + x3
x1 ~ eta2
x5 ~ eta2 + x2
x4 ~ x3
