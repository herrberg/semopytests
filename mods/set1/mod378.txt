eta1 =~ y1 + y2
eta2 =~ y3 + y4
x1 ~ x5
eta2 ~ x1
x3 ~ eta2 + x2
x4 ~ x1
eta1 ~ eta2
