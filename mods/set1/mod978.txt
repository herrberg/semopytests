eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x2 ~ eta1 + x3
x5 ~ x2
x4 ~ x5
x1 ~ x4
eta1 ~ eta2
