eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x5 ~ eta1 + x1 + x4
eta2 ~ x5
x3 ~ eta1
x2 ~ eta1
