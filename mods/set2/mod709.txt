eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta1 ~ eta2 + x3 + x4 + x5
eta2 ~ x2
x3 ~ x1
