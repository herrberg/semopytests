eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta2 ~ eta1
x4 ~ eta2 + x3
x1 ~ x2
x3 ~ x1 + x5
