eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x3 ~ x4
x2 ~ x3
eta2 ~ x2
x1 ~ eta2
x5 ~ x1
eta1 ~ x3
