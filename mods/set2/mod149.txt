eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x2 ~ eta1 + x1 + x5
eta2 ~ x2
x4 ~ eta2
x3 ~ x4
