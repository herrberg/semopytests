eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta1 ~ eta2 + x2
x4 ~ eta2 + x1 + x3
x5 ~ x3
