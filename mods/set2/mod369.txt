eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x1 ~ x5
x4 ~ x1
x2 ~ eta2 + x4
x3 ~ x5
eta1 ~ x5
