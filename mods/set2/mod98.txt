eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta2 ~ x2
x3 ~ eta2
x4 ~ x3
x5 ~ x4
x1 ~ eta2
eta1 ~ x3
