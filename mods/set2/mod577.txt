eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x5 ~ eta1 + eta2
x4 ~ x5
x2 ~ eta2
x3 ~ x2
x1 ~ eta2
