eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta2 ~ x1
eta1 ~ eta2
x3 ~ eta1 + x4
x4 ~ x2
x5 ~ x4
