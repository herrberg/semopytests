eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta1 ~ x1
x4 ~ x1
x2 ~ x4
x3 ~ x2
eta2 ~ x1
x5 ~ x1
