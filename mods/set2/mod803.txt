eta1 =~ y1 + y2
eta2 =~ y3 + y4
x4 ~ x2
eta1 ~ x2
x3 ~ eta1 + x1
eta2 ~ x1
x5 ~ eta2
