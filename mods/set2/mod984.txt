eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta2 ~ eta1
x1 ~ eta2 + x2 + x4
x2 ~ x5
x3 ~ x5
