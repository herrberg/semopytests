eta1 =~ y2 + y9
eta2 =~ y3 + y4 + y5
eta3 =~ y22 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19 + y20
eta8 =~ y21 + y22
x6 ~ x1 + x5
x10 ~ x6
x8 ~ x10 + x9
x4 ~ x7 + x8
eta5 ~ eta3 + eta4 + x10
eta1 ~ eta8
eta3 ~ eta1
eta2 ~ x10
eta4 ~ eta7
x2 ~ eta3
x3 ~ eta6 + x5
