eta1 =~ y1 + y2 + y3
eta2 =~ y11 + y4 + y6
eta3 =~ y13 + y8 + y9
eta4 =~ y10 + y11 + y12
eta5 =~ y13 + y14 + y15
eta6 =~ y16 + y17 + y18
eta7 =~ y19 + y20 + y21
eta8 =~ y22 + y23 + y24
x9 ~ eta3
x4 ~ x5 + x9
x7 ~ x1 + x4
eta8 ~ eta2 + x9
x10 ~ eta8
eta1 ~ x4
x2 ~ eta1
eta5 ~ eta8 + x3
eta7 ~ eta5
x8 ~ x9
eta4 ~ x9
eta6 ~ eta3 + x6
