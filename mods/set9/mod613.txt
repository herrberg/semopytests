eta1 =~ y1 + y11
eta2 =~ y4 + y5
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21
eta1 ~ x3 + x8
x10 ~ eta1
eta3 ~ x10
x6 ~ eta1 + x5
x7 ~ x6
x2 ~ x1 + x10
x1 ~ eta4
eta8 ~ x6
eta2 ~ x8
eta7 ~ x3 + x4
x4 ~ eta6
eta5 ~ eta6 + x9
