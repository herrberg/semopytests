eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y17 + y6
eta8 =~ y18 + y19
x1 ~ eta2
eta8 ~ x1
eta5 ~ eta1 + eta8 + x10
x6 ~ eta5 + eta6
x7 ~ eta5
eta6 ~ x8
x2 ~ eta6
x9 ~ eta5
eta4 ~ x9
x5 ~ eta8 + x3
eta3 ~ x5
x4 ~ eta5
eta7 ~ eta2
