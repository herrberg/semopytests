eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y5
eta7 =~ y18 + y20
eta8 =~ y20 + y21
eta4 ~ eta1
x4 ~ eta4 + x6 + x7
x5 ~ x1 + x4
eta5 ~ x10 + x5
x9 ~ eta5
x3 ~ x9
x2 ~ eta3 + eta5
eta8 ~ eta5
eta7 ~ x4
x8 ~ x1
eta2 ~ eta3
x6 ~ eta6
