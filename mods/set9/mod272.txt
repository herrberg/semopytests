eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y22
eta7 =~ y18 + y19 + y20
eta8 =~ y18 + y22
x6 ~ eta1 + eta8
x2 ~ x6
x5 ~ eta2 + eta6 + x6
x9 ~ eta1 + x4
x10 ~ x8 + x9
x8 ~ eta5
eta3 ~ x8
x7 ~ eta6
eta7 ~ x7
x1 ~ eta8
eta4 ~ eta2
x3 ~ x4
