eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y10 + y11
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19
x10 ~ x2
x9 ~ eta7 + x10 + x5
x6 ~ eta2 + x1 + x9
eta1 ~ x5
eta3 ~ x10
eta4 ~ eta3
x1 ~ eta6
eta5 ~ eta7
x7 ~ eta3
x4 ~ eta2
x8 ~ eta3 + eta8
x3 ~ x1
