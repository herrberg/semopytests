eta1 =~ y1 + y2
eta2 =~ y10 + y3 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y16 + y17 + y9
eta8 =~ y18 + y19 + y20
x5 ~ x7 + x9
x8 ~ eta7 + x3 + x4 + x5
eta3 ~ x8
eta1 ~ x6
x4 ~ eta1
eta7 ~ eta5
eta2 ~ eta7 + x1
eta4 ~ eta2
eta6 ~ x2 + x9
x10 ~ x4
eta8 ~ eta1
