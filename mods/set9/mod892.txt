eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y4 + y8
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
x10 ~ x5 + x7
x6 ~ x10
eta4 ~ eta2 + x6
x9 ~ eta4
eta6 ~ x3 + x6
x1 ~ eta6
x8 ~ x6
x2 ~ eta5 + x3
eta7 ~ x2
eta8 ~ x4 + x5
eta1 ~ eta6
eta3 ~ eta5
