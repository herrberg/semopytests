eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y12 + y13 + y9
eta6 =~ y14 + y15
eta7 =~ y16 + y17 + y9
eta8 =~ y19 + y20 + y21
eta1 ~ x2
x10 ~ eta1
eta8 ~ eta3 + x10
x6 ~ eta2 + x1 + x10
eta6 ~ x6
x4 ~ x1 + x9
eta4 ~ x4
eta2 ~ eta7
x5 ~ x10 + x3
eta5 ~ x5
eta3 ~ x7
x8 ~ eta2
