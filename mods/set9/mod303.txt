eta1 =~ y10 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y9
eta6 =~ y11 + y12
eta7 =~ y13 + y14
eta8 =~ y15 + y16 + y17
x9 ~ eta8 + x10
eta3 ~ x5 + x9
x4 ~ eta8 + x3
x2 ~ eta8 + x8
eta2 ~ x9
eta4 ~ eta2
eta5 ~ eta4 + x7
eta7 ~ eta5
eta6 ~ eta2 + x1
eta1 ~ eta8
x6 ~ x9
