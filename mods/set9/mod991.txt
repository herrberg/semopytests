eta1 =~ y1 + y2 + y3
eta2 =~ y20 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
eta5 =~ y13 + y14 + y15
eta6 =~ y16 + y17
eta7 =~ y18 + y19 + y20
eta8 =~ y11 + y21 + y23
eta5 ~ x7
x3 ~ eta5
x1 ~ x10 + x3 + x9
x8 ~ eta7 + x1 + x5
x5 ~ eta1 + x4
x10 ~ eta3
eta4 ~ x1
x9 ~ eta6 + x6
eta8 ~ eta7
eta2 ~ x2 + x3
