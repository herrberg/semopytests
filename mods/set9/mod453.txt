eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y10 + y11 + y17
eta8 =~ y19 + y20 + y21
eta7 ~ eta6 + x5 + x9
eta2 ~ eta7
eta6 ~ eta3
x1 ~ x5
eta5 ~ eta6
x4 ~ eta5 + x3
x6 ~ x4
x2 ~ eta4 + x8 + x9
eta4 ~ x7
x10 ~ x3
eta1 ~ x9
eta8 ~ x3
