eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y13 + y14 + y7
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
eta6 ~ eta2 + x8 + x9
eta5 ~ eta6
eta1 ~ eta7 + x7 + x8
x7 ~ x5
x1 ~ x8
x3 ~ eta4 + x7
x4 ~ eta4
x9 ~ eta3 + eta8 + x10
x6 ~ eta7
x2 ~ x10
