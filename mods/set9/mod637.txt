eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y11 + y6
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13
eta7 =~ y14 + y15
eta8 =~ y16 + y17 + y18
eta2 ~ eta3 + eta6 + eta8 + x10
eta7 ~ eta2 + x8
eta5 ~ eta2
x9 ~ eta3
x2 ~ x6 + x9
x5 ~ eta3 + eta4
x1 ~ x4 + x5
eta1 ~ eta8
x6 ~ x3
x7 ~ x3
