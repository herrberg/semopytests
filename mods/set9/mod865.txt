eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y2
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
x2 ~ eta2 + x4
x1 ~ x2
x3 ~ x1
eta7 ~ eta1 + eta2 + eta3 + eta8
eta8 ~ x6
x5 ~ eta5 + eta8 + x8
x7 ~ x5
eta3 ~ eta4 + x9
eta4 ~ x10
eta6 ~ x5
