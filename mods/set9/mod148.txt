eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y10 + y7 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13
eta6 =~ y14 + y16 + y5
eta7 =~ y17 + y18
eta8 =~ y19 + y20 + y21
x3 ~ eta3 + x5
x10 ~ x1 + x3 + x9
eta1 ~ x5
eta7 ~ eta8 + x3 + x7
x6 ~ eta4 + x4 + x5
x1 ~ x2
x8 ~ eta2 + eta5 + eta8
eta6 ~ eta4
