eta1 =~ y2 + y9
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y7
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21
eta2 ~ x1 + x3
eta5 ~ eta2 + eta7 + x7 + x8
x7 ~ x2
x5 ~ x8
eta4 ~ x4
eta7 ~ eta4
x10 ~ eta8 + x1
x9 ~ eta2
x6 ~ x8
eta6 ~ x1
eta1 ~ eta8
eta3 ~ x1
