eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y11 + y9
eta6 =~ y12 + y13
eta7 =~ y14 + y15 + y16
eta8 =~ y15 + y18
x9 ~ eta6 + x7
eta1 ~ x9
x8 ~ eta1
x5 ~ eta4 + x4 + x7
eta8 ~ x5
x1 ~ x9
eta7 ~ x1 + x10
eta4 ~ eta5
eta3 ~ x5
x2 ~ eta3
eta2 ~ x3 + x9
x6 ~ eta4
