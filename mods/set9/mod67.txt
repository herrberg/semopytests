eta1 =~ y14 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13
eta7 =~ y14 + y15 + y16
eta8 =~ y17 + y18 + y19
x4 ~ eta8
eta3 ~ eta1 + eta7 + x4 + x8
x5 ~ x4
x1 ~ x5
eta2 ~ eta6 + x4
x3 ~ eta2
eta4 ~ eta1
eta5 ~ eta7
x7 ~ eta5 + x6
x9 ~ eta2 + x10
x2 ~ x6
