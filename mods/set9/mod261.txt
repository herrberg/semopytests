eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y13 + y18
eta6 =~ y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y13 + y20
eta5 ~ eta1 + x3
x9 ~ eta5 + x5
x4 ~ x9
eta3 ~ eta1 + eta6 + x10
eta7 ~ eta3 + x7
eta4 ~ eta5 + x1
x6 ~ eta6
eta2 ~ x6
x2 ~ eta3
eta8 ~ x10
x8 ~ eta5
