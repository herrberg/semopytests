eta1 =~ y1 + y2
eta2 =~ y4 + y9
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y14
eta7 =~ y14 + y15 + y16
eta8 =~ y18 + y19 + y20
eta7 ~ x8
eta2 ~ eta1 + eta7 + x4
eta6 ~ eta7 + x3 + x7
x6 ~ eta6
x1 ~ x8
x7 ~ eta3 + x10
x9 ~ x8
eta5 ~ x9
eta8 ~ x5 + x8
x2 ~ eta7
eta1 ~ eta4
