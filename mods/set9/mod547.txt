eta1 =~ y10 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y14 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19 + y20
eta6 ~ x2
x6 ~ eta6 + eta7 + eta8 + x5
eta3 ~ eta5
eta8 ~ eta3 + eta4 + x4
x9 ~ x5
x3 ~ x9
x7 ~ x3
eta1 ~ eta8
x8 ~ eta1
x1 ~ x2
x10 ~ eta7
eta2 ~ x3
