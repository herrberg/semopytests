eta1 =~ y1 + y2
eta2 =~ y2 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
x7 ~ x5
x10 ~ x7
eta4 ~ x10
x9 ~ eta4
eta3 ~ x7
x4 ~ eta8 + x7
eta6 ~ x4
x3 ~ x2 + x7
x8 ~ x1 + x10
eta2 ~ x5
x6 ~ x10
eta1 ~ eta7 + x6
eta5 ~ x6
