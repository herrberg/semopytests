eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y14
eta7 =~ y15 + y16
eta8 =~ y18 + y8
x5 ~ eta3
x9 ~ x3 + x5
x10 ~ x8 + x9
x7 ~ eta7 + x1 + x10
x2 ~ x7
eta2 ~ eta4
x3 ~ eta2
eta6 ~ x4 + x9
eta1 ~ eta2
x6 ~ eta8 + x1
eta5 ~ x1
