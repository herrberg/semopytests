eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y16
eta6 =~ y13 + y15 + y20
eta7 =~ y16 + y17
eta8 =~ y18 + y19 + y20
eta7 ~ eta2 + eta3 + x4 + x6
eta8 ~ eta7
eta5 ~ eta8 + x9
eta1 ~ eta5
x10 ~ eta7 + x1
eta4 ~ x5 + x6
eta6 ~ eta4
x8 ~ eta6
x3 ~ eta6 + x7
x2 ~ eta4
