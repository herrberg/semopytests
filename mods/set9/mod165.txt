eta1 =~ y1 + y2
eta2 =~ y4 + y9
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
x4 ~ eta5
eta1 ~ x4
eta4 ~ eta1
x5 ~ eta4
x2 ~ x5 + x7
x8 ~ eta1
x1 ~ eta8 + x8
eta3 ~ eta1 + eta6
x9 ~ eta3
x6 ~ eta7 + x8
eta6 ~ x10
x3 ~ x8
eta2 ~ eta6
