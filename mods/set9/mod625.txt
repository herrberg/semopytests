eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y14 + y5 + y6
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19
x10 ~ eta2
eta1 ~ x10
x2 ~ eta1 + x5
eta4 ~ eta8 + x2 + x4
x9 ~ eta1
x7 ~ eta3 + x9
eta6 ~ x7
x5 ~ eta5
x6 ~ x5
eta7 ~ x1 + x3 + x5
x8 ~ x7
