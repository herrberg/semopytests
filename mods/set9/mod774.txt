eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y1 + y6 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y10 + y19
eta8 =~ y20 + y21
x7 ~ eta2
eta3 ~ eta8 + x10 + x3 + x6 + x7
eta4 ~ x7
x8 ~ eta4
eta7 ~ x3
eta5 ~ eta7
eta6 ~ x4
x10 ~ eta6
eta8 ~ x9
x1 ~ x2 + x7
x5 ~ eta4
eta1 ~ x10
