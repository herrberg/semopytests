eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y9
eta7 =~ y14 + y15 + y16
eta8 =~ y17 + y18 + y19
eta1 ~ x8
x9 ~ eta1 + x2 + x7
x5 ~ x10 + x3 + x6 + x9
x1 ~ x9
eta6 ~ x1
x2 ~ eta5 + eta7
x4 ~ x9
x3 ~ eta8
eta4 ~ x10
eta3 ~ eta2 + x1
