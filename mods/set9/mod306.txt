eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y12 + y8
eta5 =~ y13 + y14
eta6 =~ y10 + y15 + y16
eta7 =~ y18 + y19
eta8 =~ y20 + y21 + y22
x3 ~ x7
eta6 ~ eta4 + x3
eta8 ~ eta2 + eta6 + x6 + x8
x4 ~ eta8 + x9
x5 ~ x3
eta1 ~ x9
eta7 ~ eta1
x10 ~ eta6
x2 ~ x10
eta5 ~ x2
eta3 ~ eta6
x1 ~ x10
