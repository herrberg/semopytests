eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y20 + y7 + y8
eta4 =~ y10 + y11 + y12
eta5 =~ y13 + y14
eta6 =~ y16 + y18
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21 + y22
eta8 ~ eta1
x6 ~ eta8
x4 ~ eta1 + eta2 + eta3 + x8
eta7 ~ x3 + x4
x9 ~ eta1 + x7
x7 ~ eta4 + x5
x1 ~ eta1
x10 ~ eta2
eta6 ~ eta8
x2 ~ x4
eta5 ~ x5
