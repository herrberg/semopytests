eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y7
eta6 =~ y13 + y15 + y2
eta7 =~ y16 + y17
eta8 =~ y18 + y19 + y20
x1 ~ eta1 + eta6 + x4
eta5 ~ x1
eta3 ~ x4
eta8 ~ eta3
eta4 ~ eta6 + x3
eta7 ~ x4
x3 ~ x8
x2 ~ eta6
x10 ~ eta1
x9 ~ eta2 + x4
x6 ~ x1
x5 ~ x6
x7 ~ x1
