eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y20
eta7 =~ y17 + y18 + y20
eta8 =~ y19 + y20
eta4 ~ x1
x7 ~ eta4
x3 ~ x1 + x2 + x4
x6 ~ x3 + x9
x5 ~ eta6 + x6
eta1 ~ x5
eta7 ~ eta3 + x4
eta6 ~ x8
x9 ~ eta8
x10 ~ eta3
eta5 ~ eta4
eta2 ~ eta5
