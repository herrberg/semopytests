eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y11 + y15
eta8 =~ y17 + y18
eta7 ~ x6
eta3 ~ eta7 + x3 + x4
x4 ~ eta6
eta4 ~ x2 + x6
x1 ~ eta2 + eta7
x5 ~ eta7
eta5 ~ x6
eta1 ~ eta8
x3 ~ eta1 + x10 + x8
x7 ~ x4
x9 ~ x3
