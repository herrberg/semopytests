eta1 =~ y1 + y10 + y2
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y1 + y11
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20 + y21
x6 ~ eta6 + x5
eta2 ~ eta3 + x4 + x6
x1 ~ eta2
eta5 ~ eta6
x10 ~ eta5 + x9
eta4 ~ eta2
x7 ~ eta6 + eta7
eta1 ~ x5
x8 ~ eta5
eta8 ~ x4
eta7 ~ x3
x2 ~ eta3
