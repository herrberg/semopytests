eta1 =~ y1 + y2
eta2 =~ y10 + y3
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y18 + y19
eta8 =~ y19 + y20 + y21
x9 ~ x10 + x3
eta3 ~ eta5 + x9
x7 ~ eta3
x8 ~ x10 + x4
x6 ~ x8
eta4 ~ x6
eta8 ~ x9
x3 ~ eta6
eta1 ~ x9
x2 ~ x10
x1 ~ x8
x4 ~ eta7
x5 ~ x6
eta2 ~ eta6
