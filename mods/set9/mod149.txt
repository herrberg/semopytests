eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y13 + y20
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18
eta8 =~ y11 + y20
eta4 ~ eta7
x10 ~ eta4 + x1
x8 ~ eta3 + x10
eta8 ~ eta2 + x8
eta1 ~ x8
x7 ~ x10 + x3
x2 ~ eta6 + x7
x6 ~ x2
x5 ~ eta4
x9 ~ x8
x4 ~ eta7
eta5 ~ x2
