eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y17 + y18
eta8 =~ y18 + y19
eta6 ~ x7
eta1 ~ x10 + x7
x9 ~ eta1 + x4
x8 ~ eta1 + eta2 + x6
eta3 ~ x8
x1 ~ eta4
x4 ~ eta7 + x1
x2 ~ x1
eta5 ~ eta8 + x1
x3 ~ x1 + x5
