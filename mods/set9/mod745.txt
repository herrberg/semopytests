eta1 =~ y1 + y2
eta2 =~ y3 + y5 + y8
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y20
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18
eta8 =~ y19 + y20
x3 ~ eta7
x7 ~ x3
x10 ~ x5 + x7 + x8
x1 ~ eta1 + x10
eta2 ~ x10 + x2 + x4
x2 ~ eta3
eta5 ~ x10
x9 ~ eta5
eta6 ~ x4
x8 ~ eta4
eta8 ~ x3 + x6
