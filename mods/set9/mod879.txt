eta1 =~ y1 + y2
eta2 =~ y4 + y5 + y6
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19
eta8 =~ y10 + y20 + y22
eta4 ~ x9
eta2 ~ eta4 + x1
x3 ~ eta2 + x6
eta7 ~ eta1 + x3 + x8
eta8 ~ eta7
x4 ~ eta4
x10 ~ x4
eta3 ~ eta4
x2 ~ eta5 + x4
x1 ~ x7
x8 ~ eta6
x5 ~ x1
