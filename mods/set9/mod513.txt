eta1 =~ y2 + y3 + y8
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
eta2 ~ eta1
x6 ~ eta2 + x4 + x9
eta8 ~ eta6 + x1 + x5 + x6
eta3 ~ eta8
x5 ~ eta7
x1 ~ eta4
x10 ~ x6
eta5 ~ eta7 + x2 + x3
x4 ~ x8
x7 ~ eta2
