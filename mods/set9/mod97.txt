eta1 =~ y2 + y3 + y9
eta2 =~ y20 + y4 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
eta5 =~ y13 + y14 + y15
eta6 =~ y16 + y17
eta7 =~ y18 + y19
eta8 =~ y20 + y21
eta4 ~ x7 + x8
eta8 ~ eta3 + eta4 + eta5
x9 ~ eta6 + eta8 + x4 + x5
x2 ~ x9
eta7 ~ eta2 + eta4
x10 ~ eta7 + x6
x1 ~ eta1 + x8
x3 ~ x8
