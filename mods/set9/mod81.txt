eta1 =~ y1 + y2
eta2 =~ y13 + y3 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y16 + y6
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21 + y22
x3 ~ x7
eta2 ~ eta6 + eta7 + x3 + x8 + x9
x8 ~ x2
x9 ~ x5
x10 ~ x8
x1 ~ x10
x4 ~ eta6
eta7 ~ eta8
eta5 ~ x9
eta4 ~ eta7
x6 ~ x5
eta3 ~ eta6
eta1 ~ x3
