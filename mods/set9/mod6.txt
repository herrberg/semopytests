eta1 =~ y10 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y9
eta6 =~ y11 + y12
eta7 =~ y13 + y14
eta8 =~ y15 + y16
x7 ~ x5
x3 ~ x7
x6 ~ x3
eta1 ~ x1 + x6
eta8 ~ eta1 + x10
x1 ~ eta4
x2 ~ eta1 + eta3
eta2 ~ x2
x10 ~ x8
eta6 ~ x3
eta5 ~ eta3
x9 ~ eta5
eta7 ~ x2
x4 ~ x10
