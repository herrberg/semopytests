eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y9
eta6 =~ y12 + y4
eta7 =~ y13 + y14 + y15
eta8 =~ y16 + y17 + y18
eta4 ~ x10 + x6
eta2 ~ eta4
x1 ~ eta2
x4 ~ x1
x9 ~ x1
eta5 ~ x1
x7 ~ eta2 + eta6 + x2
eta3 ~ eta2 + eta7
x5 ~ eta1 + eta4 + x8
x8 ~ eta8
x3 ~ x6
