eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y13 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y9
eta6 =~ y11 + y12 + y13
eta7 =~ y14 + y15
eta8 =~ y16 + y17 + y18
x3 ~ eta3 + eta5 + eta8
eta6 ~ x3
x4 ~ eta1 + eta6
x7 ~ x4
eta2 ~ x3
x2 ~ eta2
x9 ~ x2
eta1 ~ x5
x1 ~ eta6
x8 ~ eta4 + x1
eta5 ~ x10
eta7 ~ eta4
x6 ~ eta4
