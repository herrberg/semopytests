eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
eta5 =~ y13 + y18
eta6 =~ y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y22 + y8
eta2 ~ x7
x1 ~ eta2 + x5
x9 ~ x1 + x10 + x2
eta5 ~ x9
eta1 ~ eta5
x6 ~ eta1 + x4
x10 ~ eta3 + x8
eta4 ~ x10
eta7 ~ x3 + x9
eta8 ~ x2
eta6 ~ eta3
