eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y13 + y14 + y8
eta6 =~ y16 + y17 + y5
eta7 =~ y18 + y19 + y20
eta8 =~ y21 + y22
eta1 ~ x7
x2 ~ eta1 + x10
x8 ~ x2
eta5 ~ x8 + x9
eta4 ~ x9
eta3 ~ eta4
x6 ~ eta3
x3 ~ eta1
x1 ~ x3
x4 ~ x1
eta6 ~ eta1
eta7 ~ x8
x5 ~ eta7
eta8 ~ x5
eta2 ~ x8
