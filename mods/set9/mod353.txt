eta1 =~ y1 + y10 + y3
eta2 =~ y4 + y5
eta3 =~ y14 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20
eta8 ~ eta6
eta2 ~ eta5 + eta8 + x8
x9 ~ eta2 + x1
x6 ~ x9
x2 ~ eta2 + x3
x7 ~ x10 + x2
x5 ~ x2
eta7 ~ eta4 + x2
eta3 ~ x2
eta1 ~ eta8
x4 ~ eta8
