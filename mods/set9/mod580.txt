eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y4
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
eta5 ~ eta7 + x7
eta1 ~ eta4 + x7
x10 ~ eta1 + x9
eta7 ~ x6 + x8
eta8 ~ eta1
eta6 ~ eta1
x6 ~ eta2 + x4 + x5
x3 ~ x7
x1 ~ eta1
eta3 ~ x1
x2 ~ x1
