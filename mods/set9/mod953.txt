eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y19 + y8
x7 ~ x8
x6 ~ eta1 + x7
x3 ~ x6
eta7 ~ eta5 + x2 + x7
eta5 ~ eta4 + x5
eta6 ~ eta8
x2 ~ eta6
x4 ~ eta5
x9 ~ x5
x1 ~ x9
x10 ~ eta4
eta2 ~ eta5
eta3 ~ eta5
