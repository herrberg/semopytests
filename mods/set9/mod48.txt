eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y13 + y5
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11 + y5
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19 + y20
x8 ~ x3
x1 ~ x8
eta5 ~ x6 + x8
x7 ~ eta5 + x10
x4 ~ x2 + x7
eta8 ~ x4
eta3 ~ x3
eta2 ~ eta3 + x9
eta1 ~ x8
x6 ~ x5
eta6 ~ eta5
eta7 ~ x4
eta4 ~ x4
