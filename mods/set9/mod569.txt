eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y13 + y18 + y20
eta8 =~ y13 + y22
x3 ~ x1
eta2 ~ x3
x4 ~ eta2 + eta8
x10 ~ x4 + x5
eta5 ~ x10
x8 ~ eta4 + x4
x6 ~ x8
eta1 ~ x3 + x9
x2 ~ eta1
x7 ~ x4
eta7 ~ x4
eta6 ~ eta7
eta8 ~ eta3
