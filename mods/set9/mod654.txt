eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y7 + y9
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18
eta8 =~ y19 + y8
eta1 ~ eta8 + x2 + x3
eta3 ~ eta1
eta5 ~ x5
x7 ~ eta5
x4 ~ x7
x3 ~ x4
eta7 ~ x4
eta4 ~ eta1
eta2 ~ eta4
eta6 ~ x10 + x7
x9 ~ eta1
x8 ~ eta8
x1 ~ x8
x6 ~ x8
