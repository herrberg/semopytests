eta1 =~ y1 + y2
eta2 =~ y4 + y5 + y9
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y18 + y4
eta8 =~ y19 + y20 + y21
eta2 ~ eta1
x2 ~ eta2 + eta5
x1 ~ x2
x7 ~ x1 + x9
x3 ~ x7
x4 ~ x1
x6 ~ eta4 + eta7
x9 ~ x6
x5 ~ x2
x10 ~ x5
eta8 ~ x9
x8 ~ eta8
eta3 ~ eta6 + x2
