eta1 =~ y1 + y2
eta2 =~ y19 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y16 + y6
eta7 =~ y17 + y18
eta8 =~ y19 + y20 + y21
x3 ~ x9
eta1 ~ eta7 + x3
eta8 ~ eta1 + x1 + x10
x8 ~ eta8
eta6 ~ x3
x4 ~ eta2 + eta5 + x6
x1 ~ x4
x5 ~ eta4 + eta8
x7 ~ eta8
eta7 ~ x2
eta3 ~ eta7
