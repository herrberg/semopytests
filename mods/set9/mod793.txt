eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y1 + y10 + y11
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y18 + y4
eta8 =~ y19 + y20 + y21
x7 ~ x5
x2 ~ eta5 + x7
eta4 ~ x2
eta7 ~ eta4
eta1 ~ eta7
x8 ~ eta1
eta2 ~ x8
eta8 ~ x2 + x6
x1 ~ eta8
x3 ~ x1 + x4
eta6 ~ x5
x9 ~ eta7
eta3 ~ x6
x10 ~ x2
