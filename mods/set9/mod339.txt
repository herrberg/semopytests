eta1 =~ y1 + y2 + y9
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
eta5 =~ y13 + y14 + y15
eta6 =~ y16 + y17
eta7 =~ y18 + y19
eta8 =~ y20 + y21 + y9
x7 ~ x10
x4 ~ x7
x3 ~ eta6 + x4 + x9
eta3 ~ x1 + x4
eta1 ~ eta6
eta7 ~ eta1
eta8 ~ x7
x6 ~ eta8
eta4 ~ x6
eta5 ~ x9
x8 ~ eta5
x2 ~ eta6
x5 ~ x2
eta2 ~ x2
