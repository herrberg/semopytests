eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y13 + y14 + y2
eta6 =~ y16 + y5
eta7 =~ y17 + y18
eta8 =~ y19 + y20
x2 ~ eta5 + eta7 + x3 + x4
x5 ~ x2
eta2 ~ x2
x1 ~ eta1
eta7 ~ x1
x8 ~ x2
x9 ~ eta4 + eta7 + x10
eta6 ~ x1
x4 ~ eta3
eta8 ~ eta1
x6 ~ eta5
x7 ~ x3
