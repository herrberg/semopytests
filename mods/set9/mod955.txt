eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y7 + y9
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y14 + y18
eta8 =~ y19 + y20
eta4 ~ eta1 + eta3
x7 ~ eta4 + x2
x3 ~ eta6 + x1 + x7
x9 ~ eta8 + x3 + x4
eta5 ~ x6 + x9
eta7 ~ eta5
x5 ~ x3
x8 ~ eta2 + x5
x10 ~ x6
