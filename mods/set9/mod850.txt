eta1 =~ y10 + y18
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19 + y20
eta6 ~ x3 + x5
eta4 ~ eta6 + eta8 + x8
eta1 ~ x7
x3 ~ eta1 + x9
x1 ~ x5
eta5 ~ x1
x6 ~ eta7 + x3
eta2 ~ eta1 + eta3
x4 ~ x5
x10 ~ x7
x2 ~ eta1
