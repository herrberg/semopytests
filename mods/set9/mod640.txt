eta1 =~ y1 + y8
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
x5 ~ x9
x8 ~ eta1 + x9
eta5 ~ x1
eta2 ~ eta5
eta1 ~ eta2
eta7 ~ eta2 + eta3
x2 ~ eta7 + x3
x4 ~ eta8 + x9
x7 ~ eta2
eta6 ~ eta5
eta4 ~ eta7
x3 ~ x10
x6 ~ x10
