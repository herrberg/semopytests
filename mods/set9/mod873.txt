eta1 =~ y1 + y2
eta2 =~ y4 + y9
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y14
eta7 =~ y11 + y15 + y17
eta8 =~ y18 + y19 + y20
x10 ~ eta2 + eta6
x6 ~ x10
x4 ~ x6 + x9
eta5 ~ eta4 + x1 + x9
x1 ~ eta8
eta3 ~ x10 + x8
x2 ~ x1
eta7 ~ x6
x3 ~ x9
x7 ~ x9
eta1 ~ x7
x5 ~ x7
