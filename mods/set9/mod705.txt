eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
eta5 =~ y13 + y14 + y15
eta6 =~ y16 + y5
eta7 =~ y18 + y19 + y20
eta8 =~ y20 + y21
eta8 ~ x7
eta7 ~ eta1 + eta3 + eta4 + eta8
x10 ~ eta2 + eta7 + x6 + x8
eta3 ~ x2
x4 ~ eta8 + x3
x3 ~ x9
x8 ~ eta5
eta6 ~ x6
x5 ~ eta5
x1 ~ eta8
