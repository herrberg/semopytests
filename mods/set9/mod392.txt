eta1 =~ y1 + y3 + y8
eta2 =~ y4 + y5
eta3 =~ y7 + y8
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20 + y21
x7 ~ eta7
x9 ~ eta8 + x7
x5 ~ x9
eta2 ~ x7
x4 ~ eta2
eta4 ~ x4 + x8
eta1 ~ eta4
eta5 ~ x7
eta3 ~ eta6 + eta7
x1 ~ eta3
x3 ~ x8
x10 ~ x3
x2 ~ x8
x6 ~ x4
