eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y13 + y21
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18
eta8 =~ y14 + y20 + y21
eta8 ~ x2
eta2 ~ eta8
x10 ~ eta2 + eta6
x5 ~ x10
eta7 ~ x1 + x5
x9 ~ eta2
x3 ~ x2 + x6
eta3 ~ x2
x4 ~ eta8
x8 ~ x4
x7 ~ eta4 + x2
eta1 ~ x1
eta5 ~ x1
