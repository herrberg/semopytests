eta1 =~ y2 + y4
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
x10 ~ eta3 + eta5
eta7 ~ x10 + x8
eta8 ~ x10 + x5
x3 ~ eta8
x1 ~ x3
eta2 ~ x1
x2 ~ x10
eta1 ~ eta6 + x2
eta4 ~ eta1
x9 ~ x2 + x6
x7 ~ x5
x4 ~ x2
