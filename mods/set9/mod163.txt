eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y8
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y2 + y20
eta7 ~ eta5
eta8 ~ eta7 + x3
eta4 ~ eta8
eta2 ~ eta3 + eta4 + x4 + x6
x9 ~ eta2
x3 ~ x8
x1 ~ eta1 + eta3
eta6 ~ eta4 + x5
x2 ~ x3
x7 ~ x10 + x3
