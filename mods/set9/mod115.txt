eta1 =~ y18 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y18
eta8 =~ y18 + y19 + y20
eta6 ~ x3
x6 ~ eta6
x4 ~ x6
x2 ~ eta6 + x1
x5 ~ x2
eta3 ~ eta6
eta7 ~ eta3
x8 ~ x6
eta1 ~ x6
x9 ~ eta5 + x10 + x6
eta5 ~ eta4
x10 ~ eta2
x7 ~ x2
eta8 ~ x6
