eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y11 + y9
eta6 =~ y13 + y14 + y5
eta7 =~ y15 + y16
eta8 =~ y17 + y18
x9 ~ x3
eta7 ~ eta1 + eta6 + x6 + x9
eta3 ~ eta2 + eta7 + x7
x8 ~ eta7
x4 ~ eta5 + x8
eta6 ~ x10
x1 ~ eta7
eta8 ~ x6
eta4 ~ eta1
x5 ~ x6
x2 ~ eta5
