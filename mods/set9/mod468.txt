eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y11 + y4
eta8 =~ y19 + y20 + y21
x4 ~ eta2 + eta8 + x3
x7 ~ eta4 + x4 + x6
eta5 ~ x7
eta3 ~ eta8 + x8
x9 ~ eta3
x2 ~ eta2
x8 ~ eta6
x5 ~ eta3
x10 ~ eta3
x1 ~ x10
eta1 ~ eta8
eta7 ~ x3
