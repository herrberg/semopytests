eta1 =~ y1 + y2
eta2 =~ y4 + y7
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y20 + y9
eta4 ~ eta8 + x10 + x5
x7 ~ eta4
x3 ~ x7
eta5 ~ x3
eta3 ~ eta4
x2 ~ eta6 + x3
eta1 ~ x7
x9 ~ eta1 + x6
x1 ~ eta1
x4 ~ eta1
eta7 ~ eta8
x8 ~ x3
eta2 ~ x10
