eta1 =~ y18 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y10 + y18 + y20
x1 ~ eta3 + eta7 + x4
x10 ~ x1
eta6 ~ x10
x9 ~ eta6
x6 ~ x9
eta1 ~ x6
x8 ~ x1
x4 ~ x7
x5 ~ eta5 + x4
x3 ~ x5
eta8 ~ x10
eta4 ~ eta8
eta2 ~ x10
x2 ~ x9
