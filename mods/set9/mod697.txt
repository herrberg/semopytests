eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13 + y14
eta6 =~ y13 + y15 + y16
eta7 =~ y18 + y19
eta8 =~ y2 + y20
eta5 ~ eta2
x10 ~ eta5
x3 ~ eta1 + eta2
x5 ~ eta2 + eta3 + eta8
x8 ~ x5
x2 ~ x8
x6 ~ eta2 + x4
eta4 ~ x6
eta8 ~ eta7
x7 ~ x1 + x5
x9 ~ x5
eta6 ~ eta8
