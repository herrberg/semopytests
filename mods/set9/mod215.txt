eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y15 + y2
eta7 =~ y16 + y17 + y18
eta8 =~ y20 + y7
x1 ~ x3
x8 ~ x1
eta6 ~ x2 + x8
eta2 ~ eta6
eta3 ~ eta2 + x6
eta7 ~ eta3
x4 ~ eta5 + x6
eta1 ~ x4
eta5 ~ eta4
x10 ~ eta2
eta8 ~ eta5
x5 ~ eta2
x9 ~ eta4
x7 ~ eta4
