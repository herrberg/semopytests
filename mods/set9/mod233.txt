eta1 =~ y1 + y4
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y21
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19 + y20
eta8 =~ y21 + y22 + y23
eta5 ~ eta1 + eta6 + eta7 + eta8
x1 ~ x5
x2 ~ x1 + x8
x9 ~ x2
eta6 ~ x9
eta4 ~ eta8
eta2 ~ eta4
x3 ~ eta2
eta3 ~ eta6 + x4 + x7
x6 ~ eta3
x10 ~ x9
