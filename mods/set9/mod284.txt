eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y9
eta6 =~ y10 + y11 + y13
eta7 =~ y14 + y15
eta8 =~ y16 + y17 + y18
eta5 ~ x4 + x8
x10 ~ eta3 + eta5
eta2 ~ x10 + x3 + x7
x9 ~ eta2
eta4 ~ eta5
x6 ~ eta4
eta8 ~ eta6 + x2
x3 ~ eta1 + eta8
x5 ~ eta2
eta1 ~ eta7
x1 ~ x3
