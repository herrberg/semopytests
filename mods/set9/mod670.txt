eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y16 + y20
eta7 =~ y17 + y18 + y19
eta8 =~ y10 + y20
eta4 ~ x10
x6 ~ eta2 + eta4 + x5
x1 ~ eta4
eta5 ~ x1 + x2
eta7 ~ x1
eta1 ~ eta7
eta3 ~ eta4
x3 ~ eta4 + eta8 + x4
x8 ~ x10
eta2 ~ eta6
x9 ~ eta2
x7 ~ eta8
