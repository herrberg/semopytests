eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y18 + y19 + y2
x7 ~ x4
eta6 ~ x7
eta1 ~ eta4 + eta6 + eta8 + x3
eta5 ~ eta1
eta7 ~ eta2 + eta6 + x2
x8 ~ x2
x9 ~ eta1
x5 ~ x3
x10 ~ x5
x6 ~ x7
x1 ~ eta2
eta3 ~ eta8
