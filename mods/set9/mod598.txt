eta1 =~ y1 + y8
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
eta4 ~ x7
eta2 ~ eta4 + x4
x2 ~ eta1 + eta4 + eta6 + x3
x5 ~ x2
eta3 ~ x1 + x3
x6 ~ eta3 + eta5
x9 ~ eta4
eta5 ~ eta8 + x8
eta7 ~ eta5
x10 ~ eta5
