eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y18
eta7 =~ y16 + y17 + y18
eta8 =~ y18 + y19 + y20
eta3 ~ x4 + x8
x1 ~ eta3
x2 ~ x1 + x10
eta7 ~ x2
x5 ~ eta8 + x10
eta4 ~ eta6 + x5
eta2 ~ eta4
x7 ~ eta2
x6 ~ x10
x3 ~ x4
eta1 ~ x3
x9 ~ eta5 + x4
