eta1 =~ y1 + y2 + y5
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y4
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20
eta4 ~ eta5
x3 ~ eta2 + eta4 + x6
eta1 ~ x3
x8 ~ eta1
x9 ~ x8
x5 ~ eta4
x4 ~ eta3 + eta4
x2 ~ x4 + x7
eta2 ~ eta6
x10 ~ x7
x1 ~ x10
eta8 ~ x3
eta7 ~ x8
