eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y2
eta7 =~ y16 + y17
eta8 =~ y18 + y19
x1 ~ eta7 + eta8 + x5
x3 ~ eta4 + eta6 + x1
eta3 ~ x3
x9 ~ eta2 + eta3
x7 ~ x6 + x9
x8 ~ eta3 + x2
eta5 ~ eta2
x10 ~ x9
x4 ~ eta3
eta1 ~ eta7
