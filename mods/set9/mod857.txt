eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y2 + y8 + y9
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19
eta8 =~ y20 + y21 + y22
x10 ~ eta1
eta4 ~ x10
x9 ~ eta4 + eta5
x6 ~ eta3 + x9
x3 ~ x6
eta7 ~ eta8 + x7
eta3 ~ eta7 + x1
eta6 ~ x4 + x9
x8 ~ eta4
eta2 ~ x8
x5 ~ eta4
x7 ~ x2
