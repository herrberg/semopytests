eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y4 + y5
eta8 =~ y19 + y20
x2 ~ eta3 + eta4
eta7 ~ eta6 + x2
x10 ~ x2 + x6
eta1 ~ x10
x5 ~ eta5
x6 ~ x5
x1 ~ eta6 + x8
x7 ~ eta6 + x3
eta4 ~ x4
eta2 ~ x8
eta8 ~ eta2
x9 ~ eta3
