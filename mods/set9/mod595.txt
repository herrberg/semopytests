eta1 =~ y1 + y3 + y4
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y4
eta5 =~ y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19 + y20
eta8 =~ y21 + y22
eta1 ~ eta3 + eta4 + x1
eta8 ~ eta1 + eta7 + x4 + x5
x10 ~ eta1 + x3
eta6 ~ x10 + x8
x1 ~ x6
x9 ~ x5 + x7
eta2 ~ eta3
x2 ~ eta3
eta5 ~ x5
