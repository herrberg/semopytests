eta1 =~ y1 + y3 + y9
eta2 =~ y4 + y5
eta3 =~ y13 + y6
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19 + y20
eta1 ~ eta7
x5 ~ eta1
x4 ~ x3 + x5
eta4 ~ x4
x1 ~ eta4 + eta8
eta2 ~ x1
eta6 ~ x4
x6 ~ eta6
eta5 ~ x6
eta3 ~ x5
x9 ~ eta3
x8 ~ x3
x10 ~ eta8
x2 ~ x10
x7 ~ eta3
