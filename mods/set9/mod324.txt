eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y8
eta7 =~ y17 + y3
eta8 =~ y18 + y19 + y20
x9 ~ eta7 + x2
x4 ~ x9
x10 ~ x4
x6 ~ eta7
eta2 ~ x6
eta5 ~ x5 + x9
eta3 ~ x6
eta8 ~ eta3 + eta4
eta6 ~ x2
x8 ~ x6
x3 ~ x1 + x6
x7 ~ eta3
eta1 ~ x1
