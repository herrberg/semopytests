eta1 =~ y1 + y2
eta2 =~ y10 + y3 + y4
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13
eta7 =~ y14 + y15 + y16
eta8 =~ y17 + y18
x8 ~ eta7 + x10
x2 ~ x8 + x9
x1 ~ eta2 + x2 + x5
eta8 ~ eta3 + x8
eta1 ~ x8
x4 ~ eta1 + x6
eta4 ~ eta5 + x4
x3 ~ x10
x7 ~ eta1
eta6 ~ x7
