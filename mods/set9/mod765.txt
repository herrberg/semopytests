eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y10 + y16 + y17
eta8 =~ y14 + y20
eta7 ~ x10 + x7
x1 ~ eta7
x9 ~ x1
eta6 ~ eta5 + x9
x2 ~ eta6
eta4 ~ x9
x3 ~ eta4 + eta8
x8 ~ x3
x10 ~ eta1
eta2 ~ eta7
x5 ~ eta3 + x7
x6 ~ x10
x4 ~ x6
