eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y9
eta7 =~ y16 + y17
eta8 =~ y18 + y19
x3 ~ x6
eta4 ~ eta6 + eta7 + x3
x5 ~ eta8
x7 ~ eta2 + x10 + x2 + x5
eta6 ~ x7
eta5 ~ eta6
x4 ~ x3 + x8
x9 ~ x4
eta3 ~ x4
x1 ~ x5
eta1 ~ x6
