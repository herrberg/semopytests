eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y4 + y9
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20 + y21
x7 ~ x10 + x3 + x5
eta7 ~ eta1 + x7
eta3 ~ eta7
eta1 ~ eta2
eta4 ~ eta7
x6 ~ x7
x2 ~ eta2 + eta8
x8 ~ eta7 + x4
x1 ~ x5
eta5 ~ x5
x9 ~ x5
eta6 ~ x10
