eta1 =~ y1 + y2
eta2 =~ y4 + y9
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y9
eta6 =~ y11 + y12 + y13
eta7 =~ y14 + y15 + y16
eta8 =~ y17 + y18 + y19
eta3 ~ eta6 + x1
x4 ~ eta3 + x6 + x7 + x8
x10 ~ x4
x9 ~ x1
x8 ~ eta5
x5 ~ x1
x7 ~ eta2
eta4 ~ x1
eta1 ~ x8
eta6 ~ x2 + x3
eta8 ~ x6
eta7 ~ x6
