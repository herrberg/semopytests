eta1 =~ y1 + y2 + y8
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y18
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20
eta5 ~ x4
x3 ~ eta5 + eta6 + eta8 + x10
x2 ~ eta1 + x3
x5 ~ eta5
x6 ~ x5
x10 ~ eta7
eta3 ~ eta2 + eta4 + x3
x8 ~ x3
x1 ~ x4
x7 ~ x5
x9 ~ eta2
