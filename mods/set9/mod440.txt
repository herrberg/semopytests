eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13
eta7 =~ y14 + y16 + y6
eta8 =~ y17 + y18
x2 ~ x9
eta4 ~ x2
eta5 ~ eta4
x3 ~ eta4
x10 ~ x3
x6 ~ eta8 + x2
x8 ~ x6
eta2 ~ x9
x1 ~ x2
eta6 ~ x1
eta8 ~ eta1
x5 ~ x2
x7 ~ eta7 + x9
x4 ~ eta7
eta3 ~ x4
