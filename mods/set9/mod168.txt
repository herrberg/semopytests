eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y2 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15
eta7 =~ y17 + y8
eta8 =~ y18 + y19 + y20
x10 ~ eta4
eta2 ~ x10
x9 ~ eta2 + eta7
x2 ~ eta5 + x10
x6 ~ x2 + x3
eta3 ~ x2
eta7 ~ eta8
x3 ~ x7
x5 ~ x2 + x8
eta6 ~ x5
x1 ~ eta2
x4 ~ x7
x8 ~ eta1
