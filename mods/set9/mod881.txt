eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y14 + y15 + y16
eta8 =~ y18 + y20 + y9
x4 ~ eta1 + x7
eta8 ~ eta7 + x3 + x4
x8 ~ eta8
eta6 ~ eta3 + eta7
x1 ~ eta7 + x10
eta5 ~ x1
x10 ~ x6
x9 ~ eta4 + eta8
x5 ~ x10
x2 ~ x7
eta2 ~ eta8
