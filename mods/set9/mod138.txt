eta1 =~ y2 + y20 + y4
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18
eta8 =~ y19 + y20
eta3 ~ eta2
x7 ~ eta3 + x2
eta5 ~ x5 + x6 + x7
x8 ~ x4
x2 ~ x8
eta7 ~ eta3 + x1
x9 ~ eta7
eta1 ~ eta4
x5 ~ eta1
eta6 ~ eta2
eta8 ~ x8
x3 ~ eta2
x10 ~ x2
