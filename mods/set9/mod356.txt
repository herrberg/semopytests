eta1 =~ y13 + y2
eta2 =~ y3 + y4
eta3 =~ y13 + y5 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19 + y20
x7 ~ eta1 + eta4 + x10
x9 ~ eta2 + eta7 + x1 + x7
x4 ~ x9
x3 ~ x2
eta7 ~ x3 + x8
eta2 ~ eta3
eta6 ~ x7
x5 ~ eta7
eta5 ~ x3
eta8 ~ x9
x6 ~ x1
