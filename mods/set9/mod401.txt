eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y13 + y7 + y8
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
x6 ~ eta8 + x8
eta7 ~ eta1 + eta4 + x6
eta5 ~ x8
eta3 ~ x6
x5 ~ eta3 + eta6 + x7
x10 ~ x6
eta2 ~ eta3
x4 ~ eta2
x1 ~ x6
x2 ~ x8
x3 ~ x6
x9 ~ eta1
