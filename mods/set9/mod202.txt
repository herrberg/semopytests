eta1 =~ y1 + y13 + y2
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
x6 ~ eta7 + x5 + x7
eta7 ~ eta1
x5 ~ eta3 + x10 + x8
eta5 ~ x7
x4 ~ eta7
eta4 ~ x3
x8 ~ eta4
x1 ~ x5
x10 ~ eta6
eta2 ~ x5
x2 ~ eta3
eta8 ~ eta3
x9 ~ eta1
