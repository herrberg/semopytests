eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y16 + y4
eta8 =~ y17 + y18
x10 ~ eta2 + x7 + x8
eta1 ~ x10
x9 ~ eta2
x8 ~ eta6 + x1
eta8 ~ x10 + x6
eta5 ~ eta2 + eta4
eta4 ~ x4
eta6 ~ x3
x5 ~ x8
eta3 ~ x1
x2 ~ eta3
eta7 ~ x7
