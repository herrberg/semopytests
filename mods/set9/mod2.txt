eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y8
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
x3 ~ x10
x9 ~ eta6 + x3
eta5 ~ x1 + x4 + x9
eta6 ~ eta8
eta1 ~ x4
eta2 ~ eta1 + eta4
x5 ~ x3
x1 ~ x8
x7 ~ x1
eta7 ~ x3
x2 ~ eta1
x6 ~ x3
eta3 ~ x10
