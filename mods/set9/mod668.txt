eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y14
eta7 =~ y1 + y15 + y17
eta8 =~ y18 + y19
eta1 ~ eta4
eta8 ~ eta4 + x6
x8 ~ eta6 + eta8
x9 ~ x8
x1 ~ eta5 + x2
x6 ~ eta3 + x1
eta7 ~ x6
x3 ~ x10 + x6
eta2 ~ x3
x7 ~ eta2
x4 ~ eta2
x5 ~ eta4
