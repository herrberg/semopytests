eta1 =~ y1 + y2 + y3
eta2 =~ y11 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y20
eta7 =~ y18 + y19
eta8 =~ y20 + y21 + y22
eta5 ~ eta1 + eta6 + eta8 + x8
x7 ~ eta5
eta6 ~ x2
x9 ~ eta6 + x4
x5 ~ x9
x6 ~ eta3 + x9
eta2 ~ x8
x10 ~ eta2
x1 ~ x10
eta7 ~ x8
eta4 ~ eta5
x3 ~ eta2
