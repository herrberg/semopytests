eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y12 + y13
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19
eta4 ~ x1 + x3
eta5 ~ eta4 + eta7
x10 ~ eta4
eta6 ~ x10
x2 ~ eta6 + x6
x4 ~ x2
eta3 ~ x10
x8 ~ eta4
eta8 ~ x8
eta1 ~ eta8
x7 ~ eta6
x1 ~ x5
x9 ~ eta2 + x3
