eta1 =~ y1 + y2 + y3
eta2 =~ y11 + y4 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y12 + y18
eta8 =~ y19 + y20 + y21
eta6 ~ x7
x10 ~ eta6 + x6
eta1 ~ x10
x1 ~ eta1 + eta2
eta2 ~ x4
x9 ~ eta1
x2 ~ eta5 + x8 + x9
x6 ~ x3
eta7 ~ x10
eta4 ~ eta2
x5 ~ eta4
eta8 ~ x4
eta3 ~ x9
