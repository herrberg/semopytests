eta1 =~ y1 + y18 + y2
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13 + y14
eta6 =~ y10 + y16
eta7 =~ y17 + y18
eta8 =~ y19 + y20
x6 ~ x2
eta5 ~ eta3 + eta7 + x6
x5 ~ eta2 + eta5 + x3
eta7 ~ eta8
x7 ~ eta7 + x4
x8 ~ x1
x3 ~ x8
eta1 ~ x3
eta2 ~ x10
eta3 ~ eta4
eta6 ~ eta3
x9 ~ eta6
