eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y6
x10 ~ eta5
eta6 ~ x10
x4 ~ eta6
eta3 ~ x4
eta2 ~ eta3
x1 ~ x10
x8 ~ eta4 + x1
eta8 ~ eta6
x2 ~ eta8
x6 ~ eta6
eta1 ~ x6 + x7
x9 ~ x1 + x5
x3 ~ x1
x7 ~ eta7
