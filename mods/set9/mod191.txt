eta1 =~ y2 + y7
eta2 =~ y10 + y3 + y4
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20 + y21
x3 ~ eta3 + x1 + x10 + x7
eta7 ~ x3
x2 ~ eta7
x4 ~ eta5 + x2 + x5
eta6 ~ x7
eta1 ~ x7 + x8
eta8 ~ eta1
x5 ~ x6
eta2 ~ x8
eta4 ~ eta2
x9 ~ eta1
