eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y5
eta6 ~ eta1 + eta2 + x1 + x3 + x8
x9 ~ eta6
x7 ~ x10 + x4 + x6
x3 ~ x7
eta7 ~ x8
eta8 ~ eta5 + x2 + x6
eta3 ~ x4
x5 ~ x2
eta4 ~ x2
