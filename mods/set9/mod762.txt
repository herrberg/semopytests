eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y13 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18
eta6 ~ eta4 + x7 + x8
x1 ~ eta6
x2 ~ eta2 + x1
x3 ~ x10 + x2
eta1 ~ eta5 + x7
x5 ~ eta1
x9 ~ x2
x8 ~ x4
eta8 ~ eta6 + x6
eta3 ~ x7
eta7 ~ eta3
