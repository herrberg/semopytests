eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16
eta7 =~ y10 + y18 + y19
eta8 =~ y21 + y9
eta1 ~ x8
x4 ~ eta1 + eta2
eta3 ~ x4
eta6 ~ x10 + x8 + x9
eta5 ~ eta1
x7 ~ eta5
eta8 ~ eta2 + x2 + x6
x1 ~ eta8
x3 ~ x6
eta7 ~ x3
eta4 ~ x9
x5 ~ x6
