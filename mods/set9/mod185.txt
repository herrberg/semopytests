eta1 =~ y10 + y2
eta2 =~ y10 + y3
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20
x1 ~ eta3 + x10 + x3
x4 ~ x1 + x5 + x7
x10 ~ eta1
eta2 ~ x1 + x2
eta3 ~ x8
eta8 ~ eta7 + x10
eta4 ~ eta1
eta5 ~ x7
eta6 ~ x1
x9 ~ x3
x6 ~ x5
