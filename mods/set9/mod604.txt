eta1 =~ y1 + y9
eta2 =~ y20 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21
eta4 ~ x1 + x8
x9 ~ eta4 + x6
x6 ~ x7
eta7 ~ eta2 + eta3 + eta4
x2 ~ eta7
eta3 ~ x10 + x5
eta6 ~ x3 + x6
eta8 ~ eta3
eta1 ~ x6
eta5 ~ x4 + x8
