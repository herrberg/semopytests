eta1 =~ y1 + y2
eta2 =~ y15 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y20 + y6
eta7 ~ x10 + x8
x8 ~ eta2 + eta3 + x4 + x5 + x7
x3 ~ eta2
x6 ~ eta5 + x1 + x10
x9 ~ x5
eta6 ~ x4
eta8 ~ eta2
x2 ~ x7
eta1 ~ x2
eta4 ~ x5
