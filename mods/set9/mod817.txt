eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y13 + y6
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
eta4 ~ eta2
x2 ~ eta1 + eta4 + x1
x3 ~ x2
x7 ~ x3
eta5 ~ x8
eta1 ~ eta5
x9 ~ eta5
x4 ~ eta3 + eta4
eta7 ~ x4
eta8 ~ eta5
eta6 ~ eta2
x5 ~ x8
x6 ~ eta2
x10 ~ x1
