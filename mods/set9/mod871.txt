eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y12 + y8
eta5 =~ y13 + y14 + y15
eta6 =~ y16 + y17
eta7 =~ y18 + y19 + y20
eta8 =~ y21 + y8
eta3 ~ eta8 + x4 + x5
eta7 ~ eta3 + x2
x10 ~ eta7
x2 ~ eta5
x6 ~ x2
eta6 ~ x6 + x8
x1 ~ eta6
x3 ~ eta3
eta2 ~ x3 + x9
eta1 ~ x6
eta4 ~ x3
x7 ~ x4
