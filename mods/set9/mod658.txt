eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y9
eta7 =~ y15 + y17 + y2
eta8 =~ y18 + y19 + y20
eta4 ~ x6 + x9
x7 ~ eta4
eta1 ~ x7
eta5 ~ x4
x9 ~ eta5 + eta6
x3 ~ eta7 + x9
x1 ~ x3
eta8 ~ eta3 + x6
x10 ~ eta2 + x2 + x9
x5 ~ x9
x8 ~ eta4
