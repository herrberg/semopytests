eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y4
eta5 ~ eta8 + x4 + x6
x3 ~ eta5
x2 ~ x3
x10 ~ eta5
x9 ~ x10
eta4 ~ x9
eta2 ~ eta8
eta7 ~ eta2 + x8
eta1 ~ eta5 + x7
x5 ~ eta1
eta3 ~ x5
x1 ~ eta6 + x10
