eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y2
eta6 =~ y13 + y14
eta7 =~ y16 + y17 + y5
eta8 =~ y18 + y19 + y20
eta4 ~ eta1 + eta6 + x2 + x8
eta7 ~ eta4
x4 ~ eta2 + eta7 + x3
x9 ~ x6
x5 ~ eta8 + x9
eta6 ~ x5
eta5 ~ x8
x7 ~ eta5
eta3 ~ eta1
eta8 ~ x10
x1 ~ eta8
