eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y11 + y13 + y14
eta7 =~ y10 + y16 + y17
eta8 =~ y19 + y20
x9 ~ x10
x8 ~ x1 + x9
eta7 ~ x2 + x8
eta4 ~ eta7
x3 ~ eta4
x2 ~ x7
x4 ~ x2
eta5 ~ eta6 + x4
eta8 ~ eta7 + x6
x5 ~ eta8
eta3 ~ eta8
x6 ~ eta1 + eta2
