eta1 =~ y1 + y2
eta2 =~ y3 + y9
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19
x3 ~ eta4
x8 ~ x3
eta5 ~ x4 + x8
eta8 ~ eta1 + eta5
x10 ~ eta8 + x6
x4 ~ x5
eta6 ~ x5
x1 ~ eta6
x2 ~ x5
eta2 ~ eta5
eta3 ~ eta2
x7 ~ x4
eta7 ~ x4
x9 ~ x6
