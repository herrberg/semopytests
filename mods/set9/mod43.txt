eta1 =~ y1 + y20 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y20
eta7 ~ eta3 + x6 + x9
eta6 ~ eta7
eta3 ~ eta8
x1 ~ eta3 + x2
x8 ~ x1 + x3
x4 ~ x1 + x5
eta1 ~ x4
eta4 ~ eta1
x3 ~ x10
x7 ~ x3
x5 ~ eta2
eta5 ~ eta1
