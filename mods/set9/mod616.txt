eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y6 + y9
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20 + y21
x3 ~ eta3
x5 ~ x3
eta7 ~ eta6 + x5
eta8 ~ eta7 + x6
x6 ~ x1
x7 ~ x4 + x5
x4 ~ eta5 + x9
x10 ~ eta1 + eta7
x8 ~ x1
eta2 ~ eta7
eta1 ~ eta4
x2 ~ x9
