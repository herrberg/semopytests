eta1 =~ y1 + y2
eta2 =~ y13 + y3 + y4
eta3 =~ y2 + y6 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19 + y20
x8 ~ eta3 + x1 + x7
eta4 ~ x8
eta6 ~ x1 + x10 + x9
x5 ~ eta6
eta8 ~ x5
x3 ~ eta8
eta2 ~ x8
x4 ~ eta2
eta7 ~ eta6
x2 ~ eta5 + eta6
x6 ~ x10
eta1 ~ x6
