eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y16 + y8
eta8 =~ y17 + y18 + y19
eta3 ~ x10 + x6
eta4 ~ eta3
eta2 ~ eta4
eta1 ~ eta2 + x4
x8 ~ eta8
x6 ~ x1 + x8
eta7 ~ eta3 + eta5
x3 ~ x10 + x5
x7 ~ x8
x4 ~ eta6
x2 ~ x6
x9 ~ x4
