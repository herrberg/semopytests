eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y13 + y20
eta6 =~ y14 + y15 + y16
eta7 =~ y18 + y19 + y2
eta8 =~ y20 + y21 + y22
x3 ~ eta3 + eta5
x8 ~ x3
eta2 ~ eta1 + eta6 + x8
eta4 ~ eta2 + eta8
x7 ~ eta4
x10 ~ x7
x9 ~ eta2
x1 ~ eta7
x5 ~ x1
eta1 ~ x5
x6 ~ x8
x2 ~ x6
x4 ~ eta5
