eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y8
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17
eta8 =~ y13 + y18 + y20
eta6 ~ x10
eta5 ~ eta6 + x8
eta3 ~ eta5 + x7
x3 ~ eta3
x5 ~ x10
eta2 ~ eta1 + x5
x4 ~ eta2
x6 ~ x4
eta7 ~ eta5
x2 ~ x5
x9 ~ x2
eta4 ~ eta3
x8 ~ eta8 + x1
