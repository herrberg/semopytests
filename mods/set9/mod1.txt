eta1 =~ y1 + y2 + y6
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y13 + y20
eta6 =~ y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20 + y21
x6 ~ eta4 + x7
eta8 ~ x6
x1 ~ eta8 + x10
eta7 ~ x1
x10 ~ eta2 + x9
eta6 ~ eta8
eta5 ~ x10 + x2
x5 ~ eta5
x3 ~ x5
x4 ~ x3
eta1 ~ x1
eta3 ~ x6
x8 ~ eta4
