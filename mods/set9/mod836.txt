eta1 =~ y1 + y10
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13
eta7 =~ y14 + y15
eta8 =~ y16 + y17 + y18
x2 ~ x3
eta2 ~ x2
x1 ~ eta2 + eta3 + x9
x7 ~ x1
x8 ~ eta1 + x7
eta4 ~ x8
x10 ~ eta4
eta5 ~ eta7 + x7
eta8 ~ x7
x4 ~ eta1
x6 ~ eta2
x5 ~ x6
eta6 ~ eta7
