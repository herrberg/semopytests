eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y4 + y7 + y8
eta4 =~ y10 + y11
eta5 =~ y13 + y14 + y17
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19
eta8 =~ y20 + y21 + y22
x7 ~ x2
eta1 ~ eta2 + x7
x1 ~ eta1 + x10
x6 ~ x1
eta3 ~ eta4 + eta7 + x6
x10 ~ eta5
x3 ~ x10 + x5
eta8 ~ x6
x8 ~ x1
eta2 ~ x4
x9 ~ x10
eta6 ~ eta5
