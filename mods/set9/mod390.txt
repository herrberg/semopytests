eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y14
eta7 =~ y16 + y18
eta8 =~ y17 + y18
x10 ~ eta3
x1 ~ eta5 + x10 + x5 + x7 + x9
eta4 ~ x1
eta2 ~ eta4
x9 ~ x8
x6 ~ x2 + x9
eta7 ~ eta1 + x6
eta6 ~ x1
x4 ~ x2
x3 ~ eta3
eta8 ~ x3
