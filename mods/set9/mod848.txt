eta1 =~ y13 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y4
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19 + y20
eta5 ~ eta7 + x6
x9 ~ eta5 + x1 + x8
eta8 ~ x9
x2 ~ eta5
x3 ~ x2
eta1 ~ x3
x5 ~ eta2 + x1
x4 ~ x5
eta6 ~ x2
eta3 ~ eta6
eta4 ~ eta5
x7 ~ x10 + x2
