eta1 =~ y1 + y2
eta2 =~ y4 + y8
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y11 + y9
eta6 =~ y12 + y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18
eta6 ~ x10 + x6 + x8
x2 ~ eta6 + x5
x9 ~ x2
eta3 ~ x9
x5 ~ eta2
x10 ~ eta1 + x4
eta7 ~ eta6
x1 ~ eta8 + x8
eta5 ~ x2
eta8 ~ x7
x3 ~ x2
eta4 ~ x6
