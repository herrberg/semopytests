eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y16
eta7 =~ y14 + y18
eta8 =~ y19 + y20 + y21
eta8 ~ eta3 + x6
eta7 ~ eta2 + eta8 + x3
x8 ~ eta7 + x7
eta1 ~ x1 + x8
eta5 ~ eta1
x9 ~ eta5
x7 ~ eta6
eta4 ~ x8
x4 ~ eta8 + x2 + x5
x10 ~ eta1
