eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11
eta6 =~ y11 + y13 + y14
eta7 =~ y15 + y17 + y8
eta8 =~ y18 + y19 + y20
x10 ~ eta7
x7 ~ eta5 + eta6 + x10 + x6
x9 ~ eta2 + x7
x5 ~ x9
x1 ~ eta4 + x7 + x8
x6 ~ eta1
eta3 ~ x2 + x7
x2 ~ eta8
x8 ~ x4
x3 ~ x10
