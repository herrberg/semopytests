eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y9
eta6 =~ y11 + y6
eta7 =~ y13 + y14 + y15
eta8 =~ y16 + y17 + y18
x1 ~ eta4
x5 ~ x1 + x2 + x9
x6 ~ x5
x3 ~ x6
x7 ~ x3
eta7 ~ eta6 + x5
eta5 ~ eta7
eta8 ~ eta3 + eta4
eta1 ~ x1
eta2 ~ x9
x4 ~ eta3
x8 ~ x9
x10 ~ x2
