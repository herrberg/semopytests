eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y9
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y16 + y9
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21
eta1 ~ eta3 + x6
x1 ~ eta1 + eta5
x8 ~ x1
eta7 ~ x8
eta4 ~ eta3 + x3
eta5 ~ x2 + x5
x10 ~ eta3 + x7
eta2 ~ eta1
x9 ~ eta2
eta8 ~ eta1
x3 ~ x4
eta6 ~ eta1
