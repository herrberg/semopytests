eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y18 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y17 + y2
eta8 =~ y18 + y19 + y20
eta3 ~ x1
x5 ~ eta3 + x2
eta8 ~ x5
eta4 ~ eta6
x2 ~ eta4
eta7 ~ x5
eta1 ~ eta7
x6 ~ eta1
x3 ~ x6
x8 ~ eta1 + eta2
x10 ~ eta1
eta5 ~ x5
x9 ~ x2
x7 ~ eta4 + x4
