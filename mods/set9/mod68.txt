eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y6
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y17 + y7
eta7 =~ y18 + y19
eta8 =~ y20 + y21
x6 ~ eta7
x1 ~ x4 + x6
x7 ~ x9
x3 ~ x7
eta5 ~ eta4 + x3
x4 ~ eta5
eta3 ~ x10 + x4
eta6 ~ eta3
eta1 ~ eta5
x2 ~ x6
x10 ~ eta2
x8 ~ eta8 + x4
x5 ~ eta8
