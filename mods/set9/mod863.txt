eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y9
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19
eta8 ~ eta1 + x9
x3 ~ eta8
x4 ~ x2 + x3 + x7
eta3 ~ eta8
x2 ~ x1
x10 ~ x2
x8 ~ x10
x5 ~ x1
eta4 ~ eta2 + x3
eta7 ~ eta6 + x3
eta2 ~ eta5
x6 ~ x3
