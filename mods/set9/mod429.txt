eta1 =~ y2 + y3 + y9
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14
eta7 =~ y10 + y15 + y16
eta8 =~ y18 + y19 + y20
x10 ~ x3
x5 ~ x10
x8 ~ eta3 + eta5 + eta8 + x5
eta2 ~ x2 + x5
eta7 ~ x6
x2 ~ eta7
x1 ~ x10
eta1 ~ x1
x7 ~ eta1 + x4
eta6 ~ x2
x9 ~ eta1
eta4 ~ x6
