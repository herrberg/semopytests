eta1 =~ y10 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18
eta7 ~ x1
eta2 ~ eta1 + eta6 + eta7 + eta8
x7 ~ eta2 + x4 + x5
x4 ~ x6
eta3 ~ eta2
x2 ~ eta3
x3 ~ eta5 + x2
eta4 ~ x3
x8 ~ eta5
x10 ~ eta5
x9 ~ x4
