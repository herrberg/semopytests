eta1 =~ y1 + y13
eta2 =~ y13 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19 + y20
eta5 ~ x2
x9 ~ eta5 + x10 + x7
x8 ~ x9
eta3 ~ x8
x10 ~ eta6 + x5
eta1 ~ x10 + x6
eta4 ~ eta1
x4 ~ eta7 + x7
eta2 ~ x4
x6 ~ x3
eta8 ~ eta7
x1 ~ eta8
