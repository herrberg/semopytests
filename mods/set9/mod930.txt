eta1 =~ y2 + y6
eta2 =~ y20 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19
eta8 =~ y20 + y21 + y22
x1 ~ eta8 + x9
x3 ~ eta7 + x1 + x10
x2 ~ eta1 + eta3 + x3
eta2 ~ x2
eta5 ~ eta2 + x5
x8 ~ eta5
x6 ~ eta2
eta6 ~ x4
eta3 ~ eta6
x7 ~ eta3
eta4 ~ x9
