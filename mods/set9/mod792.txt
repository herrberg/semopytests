eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y10 + y19
eta8 =~ y18 + y19 + y20
x6 ~ eta1
x4 ~ eta4 + x6
x3 ~ eta6 + x4
eta7 ~ x3
x10 ~ x4
eta2 ~ eta5 + x10
eta3 ~ eta2
x2 ~ eta3
x7 ~ eta6 + x9
x1 ~ x10
x5 ~ eta2
eta8 ~ eta6
x8 ~ eta8
