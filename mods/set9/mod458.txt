eta1 =~ y1 + y2
eta2 =~ y19 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
eta4 ~ x5
x10 ~ eta4
x4 ~ x10
eta7 ~ eta3 + x4
eta6 ~ eta3
eta5 ~ eta6
x6 ~ eta6
eta8 ~ x10
x1 ~ eta8
eta1 ~ eta6
x8 ~ eta1
x7 ~ eta3 + x9
x2 ~ x10 + x3
eta2 ~ eta4
