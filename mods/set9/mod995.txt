eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y7
x7 ~ x6
eta5 ~ x1 + x3 + x7 + x9
eta3 ~ eta5
x8 ~ eta5
eta6 ~ eta5
x4 ~ eta7 + eta8 + x6
eta1 ~ x4
eta2 ~ eta1
x3 ~ eta4 + x5
x10 ~ x1
x2 ~ x6
