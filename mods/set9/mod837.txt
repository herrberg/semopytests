eta1 =~ y10 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y9
eta6 =~ y11 + y12
eta7 =~ y13 + y14
eta8 =~ y15 + y16 + y17
x4 ~ eta1 + eta4
eta4 ~ eta6 + eta7 + x3
eta3 ~ eta4
x6 ~ eta1
eta5 ~ x6
x5 ~ eta7
eta2 ~ eta7
eta8 ~ eta2 + x10 + x8
x7 ~ x1
x3 ~ x7
x9 ~ x3
x2 ~ x7
