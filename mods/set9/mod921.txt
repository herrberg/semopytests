eta1 =~ y10 + y2 + y9
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19 + y20
eta8 =~ y21 + y22 + y23
x5 ~ x1 + x4 + x6
eta7 ~ eta6 + x5
x10 ~ eta7
x9 ~ x10
eta1 ~ eta4
x4 ~ eta1
x3 ~ x5
eta3 ~ x6 + x7
eta8 ~ x6
x8 ~ eta7
eta5 ~ eta7
eta2 ~ x2 + x6
