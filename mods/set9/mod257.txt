eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16
eta7 =~ y17 + y20
eta8 =~ y20
x10 ~ eta4 + x2
eta5 ~ eta3 + x10
x7 ~ eta5
eta3 ~ x4 + x9
x1 ~ x2
eta1 ~ eta7 + eta8 + x1
x5 ~ x9
x6 ~ eta6 + x5
eta2 ~ x6
x8 ~ x4
x3 ~ eta4
