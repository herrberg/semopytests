eta1 =~ y1 + y2
eta2 =~ y3 + y8
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y8
x8 ~ eta5 + eta6 + x6
x10 ~ x8
eta7 ~ x10
eta4 ~ eta7 + x1
eta1 ~ eta4
x4 ~ eta7
eta8 ~ eta2 + eta3 + eta7
x5 ~ x10
eta5 ~ x2
x3 ~ eta3
x9 ~ x10
x7 ~ x10
