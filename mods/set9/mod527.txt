eta1 =~ y1 + y10 + y2
eta2 =~ y5 + y6 + y8
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19 + y20
eta8 =~ y21 + y22
x3 ~ eta1
x5 ~ x3
eta7 ~ x10 + x5 + x8
x1 ~ eta8
eta6 ~ x1
x10 ~ eta6
eta5 ~ x10 + x2 + x9
eta4 ~ eta1 + x7
x4 ~ eta4
eta2 ~ eta4
eta3 ~ x5
x8 ~ x6
