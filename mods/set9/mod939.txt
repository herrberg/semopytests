eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y17 + y3
eta8 =~ y18 + y19
eta8 ~ x3
eta3 ~ eta8
eta1 ~ eta3 + eta7 + x10 + x9
x1 ~ eta1
x5 ~ x1
x6 ~ eta1
x8 ~ eta2 + eta6 + eta8 + x7
x2 ~ eta1 + eta4
x9 ~ x4
eta5 ~ eta3
