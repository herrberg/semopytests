eta1 =~ y1 + y2
eta2 =~ y11 + y3 + y4
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y19
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20 + y21
x4 ~ eta1 + eta7 + x6
x1 ~ x4
x10 ~ x6
x8 ~ x10 + x5
eta2 ~ eta4 + eta6 + x6
x7 ~ eta2
x2 ~ x7
eta6 ~ eta3
x9 ~ eta5 + x6
x3 ~ eta7
eta8 ~ eta4
