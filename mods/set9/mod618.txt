eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y15
eta7 =~ y15 + y16
eta8 =~ y18 + y19
x3 ~ x6
eta2 ~ x3 + x5
x4 ~ eta2
eta6 ~ x4
eta3 ~ eta6
x7 ~ eta2 + eta8
x8 ~ x4
x2 ~ x4
eta5 ~ x2 + x9
eta7 ~ x3
eta4 ~ x2
eta1 ~ eta4
eta8 ~ x10
x1 ~ eta6
