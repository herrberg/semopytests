eta1 =~ y1 + y2
eta2 =~ y4 + y9
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y12 + y13 + y4
eta6 =~ y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20
eta2 ~ x10 + x9
eta1 ~ eta2 + eta8 + x2
x5 ~ eta1
x2 ~ x4
eta3 ~ eta2
x3 ~ eta3 + eta4
x8 ~ x3
eta8 ~ eta5
x7 ~ x9
x6 ~ x9
eta6 ~ x2
x1 ~ x2
eta7 ~ x3
