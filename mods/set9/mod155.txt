eta1 =~ y1 + y9
eta2 =~ y3 + y4
eta3 =~ y5 + y7 + y9
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19 + y20
x2 ~ eta7 + x4
x1 ~ x2
eta6 ~ eta7
x7 ~ eta6
x3 ~ eta7 + x6
x9 ~ eta2
x6 ~ x5 + x9
eta8 ~ eta5 + x2 + x8
eta3 ~ eta8
eta1 ~ x6
eta4 ~ eta7
x10 ~ x2
