eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y2
eta8 =~ y18 + y19
eta7 ~ eta2 + eta4 + eta5 + eta8
eta2 ~ x9
eta1 ~ eta4
x10 ~ eta4
eta5 ~ eta6
x3 ~ eta5 + x4
x1 ~ x3
x5 ~ eta2 + eta3
eta3 ~ x8
x6 ~ x9
x7 ~ x6
x4 ~ x2
