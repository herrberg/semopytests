eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
eta6 =~ y15 + y9
eta7 =~ y15 + y17 + y18
eta8 =~ y19 + y20 + y21
x3 ~ x10 + x4 + x6
eta1 ~ eta6 + x3
eta3 ~ eta1
x1 ~ eta2 + eta5 + x6 + x8
x2 ~ x6
x7 ~ eta8 + x3
eta7 ~ x4
eta4 ~ x10 + x5
x8 ~ x9
