eta1 =~ y1 + y13 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
eta5 =~ y13 + y14
eta6 =~ y15 + y17 + y21
eta7 =~ y18 + y19
eta8 =~ y20 + y21
x4 ~ eta8 + x2 + x5
x6 ~ eta2 + eta5 + x1 + x4
x7 ~ eta3 + x4
x10 ~ eta7 + x3 + x7
eta6 ~ eta2
eta1 ~ eta6
eta4 ~ x7
x9 ~ x1
x8 ~ eta7
