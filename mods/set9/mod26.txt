eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14
eta7 =~ y12 + y15 + y17
eta8 =~ y18 + y19
eta2 ~ x1 + x2 + x7
x10 ~ eta2 + x8
eta5 ~ eta1
x8 ~ eta5 + eta6
x7 ~ x6
eta7 ~ eta2
x1 ~ x5
x4 ~ x1
x9 ~ eta2
eta8 ~ x9
x3 ~ x2
eta4 ~ x2
eta3 ~ x6
