eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y2 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y13 + y2
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21
eta3 ~ eta2 + eta4 + eta5 + x7
x10 ~ eta1 + eta3 + x1
eta1 ~ eta8
eta2 ~ x3
x9 ~ eta2
x1 ~ eta7
x5 ~ x7
eta6 ~ eta2
x2 ~ eta8
x4 ~ eta4
x6 ~ eta5
x8 ~ eta1
