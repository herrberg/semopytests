eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y13
eta6 =~ y11 + y12
eta7 =~ y13 + y14 + y15
eta8 =~ y16 + y17
eta4 ~ eta5
eta7 ~ eta4 + x1 + x3 + x6
eta8 ~ x3 + x8
x8 ~ eta2
x4 ~ eta3 + x1
eta1 ~ eta6 + x4
x2 ~ x8
x5 ~ eta3
x7 ~ eta3
x9 ~ x3
x10 ~ x9
