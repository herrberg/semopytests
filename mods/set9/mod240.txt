eta1 =~ y1 + y2
eta2 =~ y11 + y4
eta3 =~ y4 + y5 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20 + y21
x3 ~ x10
eta3 ~ eta8 + x1 + x3 + x5
x4 ~ eta3
x6 ~ x4
x5 ~ eta2
eta7 ~ eta4 + eta5 + x10
x2 ~ eta3
eta6 ~ eta1 + x3
x9 ~ x4
x7 ~ x9
x8 ~ eta2
