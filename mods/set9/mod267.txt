eta1 =~ y18 + y2
eta2 =~ y2 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18
eta8 =~ y19 + y20
eta3 ~ eta1
eta8 ~ eta3 + eta5 + x1 + x8
x9 ~ eta8 + x4
x5 ~ eta7 + x10 + x9
eta4 ~ x5
x8 ~ eta6
x3 ~ x9
x10 ~ x6
eta7 ~ x7
x2 ~ x1
eta2 ~ eta5
