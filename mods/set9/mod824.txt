eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y13 + y6 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y1 + y18
eta8 =~ y19 + y20 + y21
x7 ~ eta2
x1 ~ x7 + x8
eta5 ~ eta7 + x5 + x7 + x9
x2 ~ x8
x3 ~ x8
eta7 ~ x6
eta4 ~ eta1 + x9
eta3 ~ eta4 + eta6
x4 ~ x7
eta8 ~ eta7
x10 ~ eta8
