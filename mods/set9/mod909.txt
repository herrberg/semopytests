eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y4 + y5 + y6
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20 + y4
x7 ~ eta6 + eta7 + x6
x3 ~ x5 + x7
x10 ~ x3
eta1 ~ x7
eta3 ~ eta1
x4 ~ x3
eta4 ~ x6
eta2 ~ x7
x8 ~ x6
x1 ~ x8
eta5 ~ x1
eta8 ~ x7
eta7 ~ x9
x5 ~ x2
