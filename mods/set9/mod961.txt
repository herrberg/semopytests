eta1 =~ y2 + y21
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18 + y7
eta8 =~ y20 + y21
x2 ~ eta7 + x6
x8 ~ x2
eta4 ~ x8
x3 ~ eta3 + eta4
eta1 ~ eta8 + x3
x10 ~ x8 + x9
eta6 ~ x10
x4 ~ x8
eta5 ~ x4
eta2 ~ eta5
x7 ~ eta5
eta8 ~ x5
x1 ~ x3
