eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y4 + y7 + y8
eta4 =~ y10 + y11 + y12
eta5 =~ y13 + y14 + y15
eta6 =~ y16 + y17
eta7 =~ y18 + y4
eta8 =~ y20 + y21 + y22
eta6 ~ x7
x10 ~ eta6 + x4
eta1 ~ x10
x8 ~ eta1
eta8 ~ eta6 + x9
x3 ~ eta3 + eta7 + eta8
x4 ~ x1
eta2 ~ eta4 + x4
eta5 ~ eta8
eta4 ~ x6
eta3 ~ x5
x2 ~ x4
