eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y11 + y9
eta6 =~ y12 + y13
eta7 =~ y14 + y15 + y16
eta8 =~ y13 + y17 + y19
x1 ~ eta2 + x9
eta1 ~ x1 + x5 + x8
eta3 ~ eta1 + eta5 + eta6
x5 ~ x2
x6 ~ x9
x7 ~ x10 + x5
eta6 ~ eta7
eta8 ~ eta7
x3 ~ eta8
eta4 ~ x9
x4 ~ eta2
