eta1 =~ y1 + y2
eta2 =~ y2 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y10 + y16 + y17
eta8 =~ y19 + y20 + y21
x5 ~ x4
x10 ~ x1 + x5
eta7 ~ x10 + x2
eta3 ~ x2
x1 ~ x6
eta6 ~ x10
eta1 ~ x5
x3 ~ eta1
x7 ~ x1
eta8 ~ eta5 + x6
x8 ~ eta4 + x6
eta5 ~ eta2
x9 ~ x4
