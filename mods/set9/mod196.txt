eta1 =~ y1 + y11
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y4
x10 ~ eta2 + eta8
x3 ~ eta6 + x10
eta1 ~ eta4 + x3 + x8
x6 ~ eta1
x5 ~ x6
eta3 ~ eta5
eta4 ~ eta3
x4 ~ eta2
eta7 ~ x4
x2 ~ x3
x1 ~ x6
x9 ~ x1
x7 ~ x9
