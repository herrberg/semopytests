eta1 =~ y1 + y2
eta2 =~ y10 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y18 + y19
eta8 =~ y19 + y20 + y21
x5 ~ x3
eta3 ~ eta1 + x1 + x5
x9 ~ x5
eta5 ~ x4 + x9
x6 ~ eta5
eta4 ~ x3
eta6 ~ eta4
x8 ~ x1
eta2 ~ x8
x2 ~ eta2
eta7 ~ x5
x10 ~ eta4
eta8 ~ eta5
x7 ~ x9
