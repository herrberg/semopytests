eta1 =~ y11 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
eta6 ~ eta1 + eta3 + eta4
x2 ~ eta6
eta4 ~ x4 + x7 + x9
x10 ~ eta1 + eta5
x8 ~ x10 + x3
x6 ~ x1
eta7 ~ x6
eta3 ~ eta7 + x5
x3 ~ eta2
eta8 ~ eta4
