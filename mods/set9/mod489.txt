eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y22 + y5 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y13 + y21
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21 + y22
x8 ~ x10 + x2 + x5 + x7
eta3 ~ x8
eta4 ~ x8
eta5 ~ eta4
x3 ~ x2
x4 ~ x6
x7 ~ x4
eta2 ~ x2
eta1 ~ eta2
x5 ~ x9
eta8 ~ x5
x1 ~ eta7 + eta8
eta6 ~ eta2
