eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14
eta7 =~ y16 + y17 + y5
eta8 =~ y11 + y18 + y20
x7 ~ eta4
eta6 ~ x2 + x7
x3 ~ eta6 + x10
x6 ~ x3
eta2 ~ eta1 + x7 + x8
x1 ~ eta6
eta8 ~ x1
x5 ~ eta3 + x7
x9 ~ x4
eta1 ~ x9
eta7 ~ eta6
eta5 ~ eta6
