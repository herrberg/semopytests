eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y17 + y18 + y9
eta8 =~ y1 + y20 + y21
eta4 ~ eta3 + eta8 + x3 + x4
eta5 ~ eta4
eta8 ~ eta2 + x10 + x7
eta6 ~ eta1 + eta8
x6 ~ eta3
x9 ~ x2 + x6
x8 ~ x3
x1 ~ x6
eta7 ~ x6
x5 ~ eta1
