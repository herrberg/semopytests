eta1 =~ y1 + y2
eta2 =~ y13 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15
eta7 =~ y17 + y18 + y2
eta8 =~ y19 + y20 + y21
x10 ~ x1
x8 ~ eta3 + x10
eta7 ~ eta1 + x7 + x8
x2 ~ eta7 + x6
eta1 ~ x9
x5 ~ x6
eta8 ~ x3 + x6
eta2 ~ eta5 + eta8
x4 ~ x9
eta5 ~ eta6
eta4 ~ x10
