eta1 =~ y14 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
eta4 ~ eta3 + eta7 + eta8
x8 ~ eta4 + eta6
eta7 ~ x2
x3 ~ eta3
x5 ~ eta5 + x3
x7 ~ eta2
eta6 ~ x6 + x7
x6 ~ eta1
x10 ~ x3
x4 ~ eta5
x9 ~ eta2
x1 ~ x7
