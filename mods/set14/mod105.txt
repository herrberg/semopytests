eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y17
eta7 =~ y14 + y15
eta8 =~ y16 + y17
eta7 ~ eta2 + eta8
x1 ~ eta7 + x6
eta3 ~ x1 + x4
eta8 ~ eta3
x9 ~ x2
x6 ~ x9
eta5 ~ eta3
x10 ~ eta5
x4 ~ x7
x8 ~ x9
x5 ~ eta4 + x9
eta6 ~ x9
x3 ~ x7
eta1 ~ eta2
