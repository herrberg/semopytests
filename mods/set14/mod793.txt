eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y18
eta8 =~ y18 + y19
x2 ~ eta7 + x6
eta8 ~ x2
x4 ~ eta3 + eta8
eta6 ~ eta5 + x4
eta4 ~ eta6
eta2 ~ eta4
x5 ~ eta2
x3 ~ eta8
x9 ~ x3
x8 ~ x2
x1 ~ eta6
eta1 ~ eta8 + x7
eta7 ~ eta8
x10 ~ eta3
