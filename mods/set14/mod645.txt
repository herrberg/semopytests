eta1 =~ y1 + y3 + y7
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y10 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18
eta8 =~ y19 + y20
x10 ~ x1 + x3 + x4 + x6 + x7
x2 ~ eta4 + x10
x1 ~ x2 + x5 + x9
eta7 ~ x3
eta6 ~ x3
eta2 ~ eta6
eta5 ~ eta8 + x4
eta3 ~ eta5
eta1 ~ x10
x9 ~ x8
