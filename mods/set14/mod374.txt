eta1 =~ y1 + y2
eta2 =~ y4 + y5 + y6
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
eta7 ~ eta4 + eta8 + x6
x2 ~ eta7
x6 ~ eta1 + eta3 + x10 + x2 + x4
eta1 ~ eta5
eta6 ~ x4
x1 ~ eta8 + x3 + x8
x7 ~ eta1
eta2 ~ eta5
eta4 ~ x5
x9 ~ eta8
