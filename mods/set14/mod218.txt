eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y16 + y8
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
eta6 ~ x10 + x6
x2 ~ eta6 + eta7
eta8 ~ eta2 + x10
x5 ~ eta8 + x8
x9 ~ x5
eta4 ~ x10
eta5 ~ x10
eta3 ~ x3 + x7 + x8
eta1 ~ eta3
x3 ~ eta1
x4 ~ eta8
eta7 ~ x1
