eta1 =~ y1 + y11 + y2
eta2 =~ y4 + y5 + y6
eta3 =~ y12 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21 + y22
x9 ~ eta2 + eta5 + x2
eta1 ~ eta4 + x6 + x8
eta3 ~ eta1
eta5 ~ eta3
x7 ~ x10 + x2
x4 ~ x2
x3 ~ x2
eta7 ~ eta5
x5 ~ eta3
x6 ~ eta3
eta6 ~ eta2 + x1
eta8 ~ x8
