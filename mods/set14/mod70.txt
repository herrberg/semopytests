eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y16 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y11 + y9
eta6 =~ y12 + y13
eta7 =~ y14 + y15
eta8 =~ y16 + y17 + y18
x8 ~ eta5 + x7
x9 ~ x6 + x8
eta5 ~ x9
eta7 ~ eta5
x3 ~ eta7 + x1
eta8 ~ x9
eta1 ~ eta5 + x4
x2 ~ eta1
x5 ~ eta5
eta4 ~ eta2 + eta3 + x6
x10 ~ x4
eta6 ~ x1
