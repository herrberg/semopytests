eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y16 + y5 + y6
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13
eta7 =~ y14 + y15 + y16
eta8 =~ y17 + y18 + y19
eta7 ~ eta3
x1 ~ eta2 + eta7
x4 ~ eta7
x7 ~ eta4 + x4 + x5
eta6 ~ x10 + x8
eta4 ~ eta6 + eta8 + x3
x8 ~ eta4
x6 ~ eta7 + x2
x3 ~ eta5
eta1 ~ eta4
x9 ~ x5
