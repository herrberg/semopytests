eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y8 + y9
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19 + y2
eta1 ~ eta5 + x8
x10 ~ eta1 + eta6
eta7 ~ x10
x8 ~ eta7
x7 ~ eta1 + x6
eta8 ~ x5
eta6 ~ eta8 + x3
x4 ~ eta1
x2 ~ eta2 + eta4 + x4
x1 ~ eta6
eta2 ~ x9
x3 ~ eta3
