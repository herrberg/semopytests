eta1 =~ y1 + y16 + y2
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y15 + y8
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19 + y20
x3 ~ eta3 + eta8
x10 ~ eta1 + x3
x2 ~ eta5 + x10
x7 ~ x2
x6 ~ x4 + x7
x5 ~ x6
x8 ~ x7
eta4 ~ x8
eta1 ~ x2
x1 ~ eta3
x9 ~ eta2 + eta5 + eta6
eta7 ~ x6
