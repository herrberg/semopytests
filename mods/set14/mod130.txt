eta1 =~ y1 + y13 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21
eta7 ~ eta2 + eta8 + x5
x1 ~ eta7 + x10
eta6 ~ eta4 + eta5 + x1 + x4
x10 ~ eta6
x9 ~ x10
eta3 ~ eta6
x6 ~ eta6
eta8 ~ eta1
x7 ~ x5
x8 ~ x2 + x7
x3 ~ x5
