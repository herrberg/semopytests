eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y9
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19
x7 ~ eta3 + eta4 + x9
eta4 ~ x2
x4 ~ eta2 + eta4
x3 ~ eta6 + x1 + x9
x8 ~ x6 + x9
x1 ~ x10
eta6 ~ eta7
eta8 ~ x1
eta5 ~ x2
x5 ~ x9
eta1 ~ eta3
