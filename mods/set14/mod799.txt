eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y13 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y8
x7 ~ eta7 + x10
eta2 ~ x7
x4 ~ eta2 + eta6 + x1
x9 ~ eta5 + x4
eta8 ~ x3
x1 ~ eta8
eta1 ~ x4
eta3 ~ eta5 + x5
eta4 ~ eta3
x5 ~ eta4 + x2 + x6 + x8
