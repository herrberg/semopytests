eta1 =~ y2 + y9
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y11 + y18
eta8 =~ y19 + y20
x5 ~ x7
x1 ~ x5
x2 ~ x1 + x10
x9 ~ eta1 + eta2 + eta4 + x7
eta8 ~ x9
eta4 ~ eta8 + x4
eta1 ~ eta5 + eta7 + x6
eta6 ~ eta1
x3 ~ eta2
eta3 ~ x5 + x8
