eta1 =~ y1 + y2 + y3
eta2 =~ y15 + y4 + y5
eta3 =~ y7 + y8
eta4 =~ y11 + y18 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20
x3 ~ eta4 + x4
x8 ~ x3 + x5
eta2 ~ x8
eta5 ~ eta1 + eta4
eta7 ~ eta5 + x7
x4 ~ x2
eta6 ~ x4 + x9
eta8 ~ eta6
eta3 ~ eta5 + x1
x6 ~ eta5
eta1 ~ x6
x10 ~ x2
