eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y19 + y7
eta4 =~ y10 + y19 + y8
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19 + y20
eta1 ~ x5
x6 ~ eta1 + eta8 + x1
eta2 ~ x4 + x6
eta7 ~ eta2
x3 ~ eta7 + x2 + x9
eta6 ~ x3
x7 ~ eta5 + x6
x8 ~ eta2 + eta4
x4 ~ x10 + x3
eta3 ~ x4
