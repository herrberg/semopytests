eta1 =~ y1 + y2
eta2 =~ y14 + y3 + y4
eta3 =~ y6 + y7
eta4 =~ y10 + y14 + y8
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20
x8 ~ eta5
eta4 ~ eta3 + x10 + x8
x5 ~ x3
x6 ~ eta1 + x5
eta2 ~ eta8 + x6
eta3 ~ eta2 + eta7
eta1 ~ eta2
x10 ~ x1
x4 ~ x8
x9 ~ x5
x7 ~ x3
eta6 ~ eta2
x2 ~ x6
