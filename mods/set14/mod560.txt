eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y4
eta7 =~ y18 + y19
eta8 =~ y21 + y5
eta3 ~ eta2
eta5 ~ eta3 + x8
x2 ~ x5 + x7 + x8
x4 ~ x2 + x9
eta8 ~ x8
eta6 ~ x8
eta4 ~ eta6
eta1 ~ eta3
x10 ~ x7
x3 ~ x10
eta7 ~ x6 + x7
x1 ~ eta6
