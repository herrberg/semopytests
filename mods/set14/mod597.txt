eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y16 + y9
eta6 =~ y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20 + y21
x9 ~ eta2
eta5 ~ x1 + x4 + x9
eta3 ~ eta5
x4 ~ eta7
eta8 ~ eta5 + x2
eta6 ~ eta8
x2 ~ eta6
eta1 ~ x6 + x8
x1 ~ eta1
x7 ~ x4
eta4 ~ x7
x5 ~ x10 + x7
x3 ~ eta7
