eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y19 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19
x7 ~ x10 + x2
eta7 ~ eta5 + x3 + x7
eta1 ~ eta7
x10 ~ eta7 + x6
eta6 ~ x10
eta3 ~ eta7
eta8 ~ eta3
x6 ~ eta4
x3 ~ x4
eta2 ~ x3 + x8
x5 ~ x6
x9 ~ x4
x1 ~ eta3
