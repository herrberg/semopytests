eta1 =~ y1 + y10 + y2
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y10 + y13 + y14
eta6 =~ y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21 + y22
x6 ~ x10
x2 ~ x6 + x7 + x8
eta8 ~ eta6 + x2
x5 ~ eta3 + x2
x7 ~ x5
x9 ~ eta5 + x6
eta5 ~ eta7
x3 ~ eta1 + eta4 + x1
eta6 ~ x3 + x4
eta2 ~ eta7
