eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y9
eta6 =~ y14 + y15
eta7 =~ y16 + y18 + y9
eta8 =~ y19 + y20 + y21
x1 ~ eta6 + eta7 + x5
eta5 ~ x1
x5 ~ eta5 + x9
x10 ~ eta3 + x1 + x3
x4 ~ eta7
x7 ~ eta3
eta2 ~ eta7
eta4 ~ eta7 + x8
eta8 ~ eta6 + x2
x6 ~ eta8
x2 ~ eta1
