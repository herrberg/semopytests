eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y10 + y14
eta7 =~ y16 + y17
eta8 =~ y18 + y19
x9 ~ eta8
x6 ~ x9
eta3 ~ x6
eta4 ~ eta5 + eta8
eta5 ~ eta6 + x7
eta6 ~ x10 + x8
x2 ~ eta5
x10 ~ x2
x1 ~ eta6 + x4
eta2 ~ x5 + x8
x3 ~ eta2 + eta7
eta1 ~ x2
