eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y19
eta7 =~ y17 + y18 + y19
eta8 =~ y18 + y20 + y22
eta8 ~ eta3 + x10
x5 ~ eta6 + eta8
x4 ~ x5
eta2 ~ eta7 + x4
eta5 ~ eta2 + x2 + x9
x10 ~ x5
x3 ~ x10
x1 ~ eta1 + eta8
eta1 ~ x7
x9 ~ x6
eta4 ~ x5
x8 ~ eta2
