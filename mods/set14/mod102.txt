eta1 =~ y1 + y2
eta2 =~ y3 + y9
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13
eta7 =~ y14 + y15 + y16
eta8 =~ y17 + y18 + y19
x9 ~ x6 + x8
eta8 ~ eta1 + eta2 + x2 + x9
x4 ~ eta8 + x3
x7 ~ eta7 + x4
x3 ~ x1 + x10
eta3 ~ eta8
x5 ~ x8
eta4 ~ x3
eta2 ~ x4
eta6 ~ eta1 + eta5
