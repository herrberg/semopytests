eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y8
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
eta5 =~ y10 + y13 + y14
eta6 =~ y16 + y17
eta7 =~ y18 + y19 + y20
eta8 =~ y21 + y22 + y23
x9 ~ eta3 + eta5 + eta7 + x1 + x3 + x6 + x7
x2 ~ x9
x7 ~ x2
eta8 ~ eta2 + x8
eta5 ~ eta8 + x10
eta1 ~ x2
x4 ~ eta5
x5 ~ x10
eta6 ~ eta3
eta4 ~ eta3
