eta1 =~ y1 + y2 + y3
eta2 =~ y15 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
eta6 ~ x9
x10 ~ eta6
eta8 ~ x10 + x3 + x7 + x8
x8 ~ eta5
x6 ~ x8
eta7 ~ x6
eta4 ~ eta6
x5 ~ eta4 + x1
x2 ~ x5
eta3 ~ x2
eta2 ~ eta1 + x3
eta1 ~ x4
x1 ~ x2
