eta1 =~ y18 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y11 + y18
eta8 =~ y19 + y20
x4 ~ x5
x2 ~ eta3 + x10 + x4
eta1 ~ x2
eta3 ~ x3 + x6
eta7 ~ eta3 + eta8
eta6 ~ eta7
eta4 ~ eta6
x1 ~ x4
eta5 ~ eta6
x7 ~ eta5
eta2 ~ x2
x6 ~ eta2
x10 ~ x9
x8 ~ eta5
