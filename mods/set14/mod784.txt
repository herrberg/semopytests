eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y16 + y18
eta7 =~ y14 + y18 + y19
eta8 =~ y20 + y21
eta6 ~ eta1
x4 ~ eta6 + x8
x9 ~ eta2 + x4
x6 ~ eta8 + x9
eta7 ~ eta4 + x4
x10 ~ eta7
eta4 ~ x10
x7 ~ eta5 + x4
x3 ~ x7
eta3 ~ x9
x1 ~ x4
x5 ~ x7
x8 ~ x2
