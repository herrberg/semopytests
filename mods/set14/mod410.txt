eta1 =~ y1 + y2 + y3
eta2 =~ y18 + y4 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
eta5 =~ y13 + y14 + y15
eta6 =~ y16 + y17 + y18
eta7 =~ y19 + y20 + y22
eta8 =~ y22 + y23
x2 ~ x3 + x4 + x5
x6 ~ x2
x3 ~ eta2 + x6 + x7
eta3 ~ eta1 + x3 + x9
eta5 ~ x3
eta6 ~ eta1
x8 ~ x5
x1 ~ x8
x10 ~ x2
eta8 ~ eta1
eta4 ~ x8
eta7 ~ eta2
