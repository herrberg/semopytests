eta1 =~ y1 + y2 + y3
eta2 =~ y11 + y4 + y5
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
eta5 =~ y14 + y15 + y17
eta6 =~ y16 + y17 + y18
eta7 =~ y19 + y20
eta8 =~ y21 + y22 + y23
x2 ~ eta8 + x5 + x6
x5 ~ x8
eta8 ~ eta4 + x7
eta5 ~ x7
x3 ~ x5
eta7 ~ eta2 + x7
x1 ~ eta7 + x10
x10 ~ x9
eta2 ~ x1
eta3 ~ eta1 + x4 + x6
eta6 ~ x10
