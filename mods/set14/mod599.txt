eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y5
eta6 =~ y14 + y15 + y18
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21
x8 ~ eta1
eta3 ~ x2 + x7 + x8 + x9
x1 ~ eta3
x4 ~ eta5 + x1
eta2 ~ x4
x5 ~ x1
x9 ~ x6
x10 ~ eta3
x2 ~ x10
eta5 ~ eta8
x3 ~ x4
eta4 ~ x3
eta6 ~ eta7 + x1
