eta1 =~ y1 + y2 + y8
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
eta5 =~ y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19 + y8
eta8 =~ y21 + y22 + y23
x5 ~ eta5
x10 ~ eta6 + x1 + x4 + x5
eta1 ~ x10 + x2
eta2 ~ eta1 + x3
eta8 ~ eta2 + x8
eta4 ~ x5 + x9
x7 ~ eta4
x4 ~ eta3
x8 ~ x6
eta6 ~ eta2
x1 ~ eta7
