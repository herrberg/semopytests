eta1 =~ y1 + y2 + y8
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y16 + y21 + y22
x4 ~ x10 + x9
x7 ~ x4
x5 ~ eta8
x8 ~ x5
x10 ~ eta7 + x8
eta4 ~ x4
eta6 ~ x2 + x9
eta5 ~ eta6 + x6
x2 ~ eta5
x3 ~ x8
eta2 ~ eta3 + x1 + x2
eta1 ~ eta7
