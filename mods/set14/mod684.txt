eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y14 + y15 + y6
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21
eta7 ~ eta5 + x5 + x8
x4 ~ eta7
x5 ~ x1 + x4
x1 ~ eta6 + eta8 + x3
eta2 ~ x1
eta1 ~ x1 + x9
x7 ~ eta3 + eta6
x2 ~ x7
x10 ~ eta8
eta4 ~ x10
x6 ~ x3
