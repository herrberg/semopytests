eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y18 + y6
eta8 =~ y15 + y19
x8 ~ eta4
x3 ~ x8
x2 ~ x3
eta2 ~ x2
x7 ~ eta1 + eta2 + eta5
eta8 ~ x7 + x9
eta1 ~ eta8 + x6
x1 ~ eta1
eta3 ~ eta7 + x3
x5 ~ x3
eta6 ~ x10
eta7 ~ eta6
x4 ~ x10
