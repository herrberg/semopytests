eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y16 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
x8 ~ eta4 + eta6 + eta8
eta1 ~ x7 + x8
eta3 ~ eta1
x10 ~ eta3
eta5 ~ x10
x2 ~ eta8 + x3
eta2 ~ x8
eta6 ~ eta2
x9 ~ eta4
x5 ~ x9
x4 ~ eta1
x1 ~ x9
x6 ~ eta8
eta7 ~ x9
