eta1 =~ y1 + y12 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18
eta2 ~ eta3 + x10
x9 ~ eta2
eta3 ~ eta8 + x9
x3 ~ eta3
eta8 ~ x8
x1 ~ eta3
x4 ~ x10
eta5 ~ x4
x2 ~ eta2 + eta7 + x7
x5 ~ eta4 + x8
eta6 ~ x9
eta4 ~ x6
eta7 ~ eta1
