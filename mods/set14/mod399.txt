eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y5
eta6 =~ y14 + y15
eta7 =~ y12 + y16 + y18
eta8 =~ y19 + y20
x5 ~ x7
eta1 ~ eta2 + x5
eta3 ~ eta1 + eta8
eta5 ~ eta3
eta8 ~ x10 + x2
x3 ~ x6 + x7
eta2 ~ x4 + x9
x1 ~ eta6 + eta8
x2 ~ x1 + x8
eta7 ~ x6
eta4 ~ x7
