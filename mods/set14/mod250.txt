eta1 =~ y1 + y10 + y2
eta2 =~ y19 + y4
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19 + y20
eta8 =~ y21 + y22 + y23
x3 ~ eta7 + eta8
x2 ~ eta3 + x10 + x3
eta6 ~ x2
eta5 ~ x2 + x4
x1 ~ eta5
eta3 ~ eta4 + x6 + x8
x7 ~ x2
eta4 ~ x2
x5 ~ x6 + x9
x8 ~ eta2
eta1 ~ x6
