eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y9
eta6 =~ y12 + y13 + y14
eta7 =~ y14 + y15 + y16
eta8 =~ y18 + y19 + y20
x5 ~ eta8 + x1
x10 ~ x3 + x5
x1 ~ x10 + x7
eta5 ~ x1
eta1 ~ x10
x4 ~ eta1 + x2 + x9
x6 ~ eta1 + eta4
eta7 ~ x10
eta3 ~ eta7
eta6 ~ eta2 + x10 + x8
