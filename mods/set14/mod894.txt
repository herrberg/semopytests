eta1 =~ y1 + y2
eta2 =~ y17 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y12 + y13 + y15
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18
eta8 =~ y19 + y20
eta5 ~ eta2 + x9
eta3 ~ eta5 + x10
eta7 ~ eta3
eta1 ~ x9
eta4 ~ eta1
x2 ~ x9
x1 ~ x2
eta6 ~ eta1 + x7
x4 ~ x6
x10 ~ x4
x5 ~ x10
eta8 ~ x4
x8 ~ x10
eta2 ~ eta3
x3 ~ eta2
