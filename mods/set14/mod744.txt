eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y14
eta5 =~ y12 + y13 + y14
eta6 =~ y16 + y18
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21 + y22
x2 ~ eta2 + x5
x6 ~ x2 + x3 + x8
x7 ~ eta1 + x5
eta5 ~ x1 + x7
x4 ~ x7 + x9
eta1 ~ x4
x8 ~ eta8
x10 ~ x2
eta3 ~ x10
eta6 ~ x5
eta4 ~ eta6
eta7 ~ x7
