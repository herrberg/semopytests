eta1 =~ y1 + y2 + y3
eta2 =~ y11 + y14 + y5
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
eta5 =~ y13 + y14
eta6 =~ y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21 + y22
eta7 ~ x9
x4 ~ eta4 + eta7 + x1 + x5
eta5 ~ eta4 + x6
x6 ~ eta3 + eta6
eta1 ~ x5
x10 ~ eta1 + eta8
x7 ~ eta7 + x3
eta6 ~ eta2
x1 ~ x2
x8 ~ x1
