eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y18
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
x8 ~ eta8 + x9
x3 ~ eta2 + eta7 + x10 + x8
x4 ~ x8
eta4 ~ eta8 + x5
eta5 ~ eta4
x5 ~ eta5
eta7 ~ x6
x7 ~ x1
x9 ~ x7
eta6 ~ x10 + x2
eta3 ~ eta1 + x10
