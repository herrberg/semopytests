eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y19
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
x2 ~ eta6 + x10
x1 ~ x2
x5 ~ x1
eta3 ~ eta5 + x5
eta2 ~ eta3
x9 ~ x1 + x4 + x6
eta7 ~ x2 + x3 + x8
eta8 ~ eta7
x7 ~ x5
eta4 ~ x2
eta1 ~ x4
eta6 ~ eta3
