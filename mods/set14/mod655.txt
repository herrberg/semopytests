eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y11 + y8
eta4 =~ y10 + y11 + y18
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19 + y20
eta8 =~ y21 + y22
eta4 ~ x6
eta1 ~ eta4 + x8
eta7 ~ eta1
x9 ~ eta3 + eta7 + x3
eta3 ~ x5
x4 ~ eta3 + x1
x10 ~ eta2 + eta3
eta8 ~ eta6 + x10
eta2 ~ eta8
x2 ~ eta7
x7 ~ eta4
eta5 ~ eta2
