eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y10 + y5
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
x1 ~ x4 + x7
eta4 ~ x1 + x3 + x5
x2 ~ x4
x6 ~ eta1 + x10 + x3
eta6 ~ x6
eta2 ~ x4
x8 ~ eta1 + eta7
eta3 ~ x4
eta5 ~ x5
eta8 ~ eta7
x9 ~ eta8
