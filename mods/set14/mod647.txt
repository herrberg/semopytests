eta1 =~ y2 + y21 + y3
eta2 =~ y14 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19 + y20
eta8 =~ y21 + y22 + y23
x7 ~ eta6
eta7 ~ x3 + x7
eta2 ~ eta6 + x8
eta4 ~ eta2 + x10 + x4 + x5
x2 ~ eta4
x4 ~ x2
eta1 ~ x8
x5 ~ x9
x6 ~ x5
eta8 ~ eta6
x1 ~ x8
eta5 ~ eta2
eta3 ~ eta6
