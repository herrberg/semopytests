eta1 =~ y1 + y2 + y3
eta2 =~ y11 + y4
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13
eta7 =~ y14 + y15
eta8 =~ y16 + y17
x2 ~ x10 + x8
x3 ~ eta6 + x2
eta8 ~ eta7
x10 ~ eta8 + x1
x1 ~ eta3 + x2
eta5 ~ x1
x6 ~ x8
eta4 ~ x6
eta2 ~ x10
x5 ~ x8
eta1 ~ eta8
x7 ~ x8
x9 ~ x7
x4 ~ x8
