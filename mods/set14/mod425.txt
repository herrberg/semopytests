eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y11 + y6
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
x1 ~ x4
x6 ~ eta8 + x1 + x5
x9 ~ x6
x5 ~ x9
eta8 ~ eta5 + x8
x7 ~ eta1 + eta8
x3 ~ eta7 + x8
eta5 ~ eta2
eta3 ~ eta8
eta7 ~ eta6 + x10
eta1 ~ eta4
x2 ~ x4
