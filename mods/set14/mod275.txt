eta1 =~ y1 + y2 + y3
eta2 =~ y10 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y10 + y14 + y15
eta7 =~ y17 + y18
eta8 =~ y19 + y20 + y21
x7 ~ x5
eta7 ~ x4 + x7
x6 ~ eta7
x10 ~ eta1 + x7
eta3 ~ x10 + x8
eta2 ~ eta4
x4 ~ eta2
eta1 ~ eta3
eta5 ~ x2 + x7 + x9
x9 ~ x1
eta6 ~ x5
x3 ~ eta3
eta8 ~ eta1
