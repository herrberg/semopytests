eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y15 + y5 + y6
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18
eta6 ~ eta5 + eta7
eta1 ~ eta7
x2 ~ eta1 + x5
x9 ~ x2 + x8
x5 ~ x9
x4 ~ x5
x1 ~ eta7
x3 ~ x1
x10 ~ eta2 + x2 + x6
eta2 ~ eta3 + x7
eta3 ~ eta4
eta8 ~ x2
