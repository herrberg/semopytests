eta1 =~ y1 + y2
eta2 =~ y16 + y4 + y5
eta3 =~ y5 + y6 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21
x8 ~ eta1 + eta3
x3 ~ eta7 + x5 + x8
eta3 ~ eta8 + x3 + x9
eta2 ~ x3
eta6 ~ eta2
x2 ~ eta6
eta5 ~ x2
x7 ~ eta2
eta4 ~ x3
x10 ~ eta1
x4 ~ x5
x1 ~ x4
x6 ~ eta8
