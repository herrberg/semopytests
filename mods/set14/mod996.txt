eta1 =~ y1 + y18 + y2
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y10 + y18
eta8 =~ y19 + y20
x8 ~ eta8 + x10 + x2
x3 ~ x8
eta5 ~ x8
eta2 ~ eta5
eta7 ~ x8 + x9
eta3 ~ eta7
x1 ~ x4 + x8
x2 ~ eta4 + eta6 + x1
eta1 ~ x2
x6 ~ x1 + x7
x5 ~ x10
