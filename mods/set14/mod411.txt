eta1 =~ y1 + y9
eta2 =~ y10 + y3
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18
eta8 =~ y19 + y20 + y21
x3 ~ eta6 + x6
eta2 ~ x3
eta6 ~ eta2
x7 ~ eta1 + eta6
x5 ~ x2 + x7
x4 ~ eta5 + x7 + x8 + x9
x10 ~ x4
eta3 ~ x10
eta7 ~ x7
eta5 ~ x1
eta1 ~ eta8
eta4 ~ x1
