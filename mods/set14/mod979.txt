eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y9
eta6 =~ y14 + y15
eta7 =~ y16 + y18 + y19
eta8 =~ y19 + y20 + y21
eta5 ~ eta6 + x3 + x8 + x9
x1 ~ eta5 + eta7 + x4
x9 ~ x1
x2 ~ x9
x10 ~ x2
eta7 ~ x5
eta8 ~ x1
x8 ~ x6
eta4 ~ eta2 + eta5
x4 ~ eta3
eta1 ~ x4
x7 ~ x6
