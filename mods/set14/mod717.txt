eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y10 + y15
eta7 =~ y18 + y19 + y5
eta8 =~ y20 + y21 + y22
eta3 ~ eta2 + eta6
x4 ~ eta3 + x5
x10 ~ eta1 + x4 + x9
eta7 ~ x10 + x3
eta6 ~ eta7 + x7
eta4 ~ x5
x6 ~ eta4
x8 ~ eta4
eta1 ~ x2
eta8 ~ x5
x1 ~ eta8
eta5 ~ eta4
