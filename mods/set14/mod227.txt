eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y9
eta6 =~ y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y18 + y19 + y20
x10 ~ eta4
eta6 ~ x10
x2 ~ eta2 + eta6
eta2 ~ eta5 + eta7
x1 ~ x10 + x9
x6 ~ x1
x8 ~ eta6
x3 ~ x8
eta8 ~ x10
x4 ~ eta2
x7 ~ x4
eta3 ~ x1
eta1 ~ eta6
x5 ~ x4
eta5 ~ x4
