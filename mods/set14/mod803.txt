eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y11 + y9
eta6 =~ y13 + y16
eta7 =~ y14 + y15 + y16
eta8 =~ y17 + y18 + y19
x3 ~ x1
x6 ~ eta1 + x3
x10 ~ x2 + x6
eta1 ~ x10
eta3 ~ x10 + x7
x8 ~ eta3
eta6 ~ x8
x7 ~ eta8 + x4
x5 ~ x7
eta4 ~ x5
eta5 ~ x7
x9 ~ x4
eta2 ~ x10
eta7 ~ eta2
