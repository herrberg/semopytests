eta1 =~ y11 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y9
eta6 =~ y11 + y12 + y13
eta7 =~ y14 + y15 + y16
eta8 =~ y17 + y18 + y19
x3 ~ x8
x6 ~ eta5 + x3
x1 ~ eta4 + x10 + x4 + x6
x2 ~ eta6 + x1
eta1 ~ eta8 + x2
eta2 ~ x1
eta5 ~ x2
eta7 ~ x4
x9 ~ eta7
x7 ~ x2
eta3 ~ x6
x5 ~ eta8
