eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14
eta7 =~ y10 + y16
eta8 =~ y17 + y18
x5 ~ eta3 + eta7
x10 ~ x5 + x6
x9 ~ x10
eta6 ~ x9
x4 ~ eta6
eta1 ~ eta6 + x3
eta2 ~ eta1 + x8
eta4 ~ eta6
eta5 ~ x10
x6 ~ eta5
x7 ~ x6
x2 ~ eta7 + x1
eta8 ~ x3
