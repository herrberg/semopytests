eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y12 + y16
eta8 =~ y12 + y18 + y19
x5 ~ x6 + x7 + x9
eta6 ~ x10 + x2 + x5
x6 ~ eta1 + eta6 + eta8
eta5 ~ eta3 + x6
x4 ~ eta5
eta1 ~ eta4
eta7 ~ x6
x3 ~ eta2 + eta7
x8 ~ eta1
x1 ~ eta4
