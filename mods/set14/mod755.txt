eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y10 + y6 + y7
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y10 + y14 + y15
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21
x1 ~ x9
eta5 ~ eta7 + x1
eta8 ~ eta2 + eta5 + x7
eta1 ~ eta8
x2 ~ eta3 + eta4
x7 ~ x2
eta7 ~ x10
x6 ~ eta5
eta2 ~ x3
eta6 ~ x1
x5 ~ eta5
x4 ~ eta7
eta3 ~ x7
x8 ~ eta4
