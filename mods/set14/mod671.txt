eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y5 + y6 + y8
eta4 =~ y10 + y9
eta5 =~ y12 + y13 + y15
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18
eta8 =~ y19 + y20
x7 ~ x5
x6 ~ eta1 + x1 + x7
x3 ~ x6
x2 ~ eta6 + x3
x1 ~ eta8
x4 ~ x1 + x10
eta3 ~ x4 + x9
eta7 ~ eta3
x9 ~ eta7
eta5 ~ x3
eta2 ~ x3
eta4 ~ eta1
x8 ~ x10
