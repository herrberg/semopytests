eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y10 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y11 + y18 + y19
eta8 =~ y21 + y22
x3 ~ eta3 + eta6
eta1 ~ x3 + x4
eta6 ~ eta1
x1 ~ eta6 + x6
x2 ~ x3 + x5 + x8
eta7 ~ eta8 + x2
eta5 ~ eta7 + x7
x10 ~ eta6
eta4 ~ x4
x7 ~ eta2
x9 ~ x8
