eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y4 + y6 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y21 + y22 + y8
eta5 ~ eta2 + x4
eta4 ~ eta5 + x1 + x9
eta2 ~ eta4 + x10
eta3 ~ eta2 + x6
x10 ~ eta7 + x5 + x7
eta8 ~ x10
x3 ~ x10
x8 ~ x3
eta6 ~ x8
eta1 ~ eta5
x2 ~ eta1
