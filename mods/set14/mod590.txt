eta1 =~ y1 + y2
eta2 =~ y19 + y3
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13
eta7 =~ y14 + y15 + y16
eta8 =~ y17 + y18 + y19
x7 ~ eta1 + eta4 + eta5 + x1
x5 ~ eta3 + x10 + x7
x9 ~ x4 + x5
eta3 ~ eta7 + x8 + x9
x2 ~ x7
x6 ~ x1
eta8 ~ x6
x10 ~ eta6
eta4 ~ x3
eta7 ~ eta2
