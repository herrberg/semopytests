eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y9
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
eta2 ~ x6 + x9
x4 ~ eta1 + eta2
x7 ~ eta3 + x4
eta1 ~ x3 + x7
eta8 ~ eta2 + x2
eta6 ~ eta2
x10 ~ eta1 + x5
eta4 ~ eta2
eta5 ~ eta4
x5 ~ x8
x1 ~ x9
eta7 ~ x1
