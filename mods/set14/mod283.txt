eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y10 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y18 + y4
eta8 =~ y19 + y20
eta4 ~ x2 + x6 + x9
eta1 ~ eta4 + eta5 + eta7 + x1
x8 ~ eta1
x2 ~ x8
eta5 ~ x5
x7 ~ eta5 + x4
eta6 ~ x7
eta2 ~ eta5
eta3 ~ x9
eta7 ~ x3
x4 ~ eta8
x10 ~ x4
