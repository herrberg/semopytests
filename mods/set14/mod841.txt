eta1 =~ y12 + y9
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18
eta8 =~ y19 + y20 + y21
x10 ~ eta2 + x4
x5 ~ eta1 + x10
eta5 ~ x5 + x8
eta1 ~ eta3 + eta5
x1 ~ eta8 + x10
eta6 ~ eta1
x9 ~ eta3
x7 ~ x9
x6 ~ eta1 + x3
x2 ~ x4
eta2 ~ eta4
eta7 ~ x9
