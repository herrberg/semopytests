eta1 =~ y1 + y12 + y2
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19
x3 ~ x10 + x2
eta3 ~ x3
x2 ~ eta3 + eta4
eta1 ~ eta2 + eta3 + eta7
x8 ~ x7
eta2 ~ x8 + x9
x9 ~ eta8
x4 ~ eta2
eta6 ~ x1 + x9
x1 ~ x6
x5 ~ x1
eta5 ~ x3
