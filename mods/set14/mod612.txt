eta1 =~ y1 + y15 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y13 + y14 + y16
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19
eta8 =~ y20 + y21 + y22
x5 ~ eta2
x9 ~ eta4 + eta7 + x5 + x8
x2 ~ x9
x8 ~ x2
eta3 ~ eta2 + x1
x1 ~ eta6
x4 ~ eta7 + x10
eta8 ~ x4
eta5 ~ eta2
x3 ~ x7
eta4 ~ x3
x6 ~ x9
eta1 ~ eta7
