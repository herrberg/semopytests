eta1 =~ y1 + y18 + y2
eta2 =~ y4 + y5 + y6
eta3 =~ y12 + y7 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18
eta8 =~ y19 + y20
eta7 ~ x5
eta5 ~ eta7 + x8
eta3 ~ eta5
eta6 ~ eta1 + eta3 + x10
eta8 ~ eta6 + x1 + x2
x8 ~ eta8 + x4 + x9
eta1 ~ eta2
x10 ~ x6
x3 ~ x10
x1 ~ eta4
x7 ~ eta5
