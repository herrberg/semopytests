eta1 =~ y1 + y2 + y3
eta2 =~ y14 + y4
eta3 =~ y6 + y7
eta4 =~ y10 + y14 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19 + y20
eta8 ~ x4
eta2 ~ eta1 + eta8 + x8
eta1 ~ eta6 + x2 + x6 + x7
eta7 ~ eta1
x7 ~ eta4 + eta7
x3 ~ eta8 + x1 + x10 + x5
eta3 ~ x4
x9 ~ eta4 + eta5
