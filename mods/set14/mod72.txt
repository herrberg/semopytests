eta1 =~ y1 + y18 + y2
eta2 =~ y18 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20
x1 ~ x6 + x9
eta6 ~ x1 + x3
x6 ~ eta6
eta8 ~ eta3 + x2 + x6
x10 ~ eta6 + eta7 + x7
eta4 ~ eta6 + x8
eta5 ~ eta4
x5 ~ x9
x8 ~ eta2
x4 ~ eta6
eta1 ~ x9
