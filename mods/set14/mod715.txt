eta1 =~ y16 + y2 + y3
eta2 =~ y15 + y4 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20
x2 ~ x5
x4 ~ x2 + x8
x8 ~ eta2 + eta4
eta3 ~ eta6 + x2 + x6 + x9
x3 ~ eta3
eta1 ~ x3
x7 ~ eta5 + x2
x1 ~ x10 + x2
eta8 ~ x3
x9 ~ eta8
eta7 ~ x8
