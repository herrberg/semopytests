eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y12 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y15 + y20
x10 ~ eta6 + x7
eta4 ~ x1 + x10 + x8
x3 ~ eta4
x7 ~ x3
x2 ~ eta6
eta5 ~ x2
eta7 ~ eta5
x9 ~ eta7
eta1 ~ eta8 + x10
x5 ~ eta1
x4 ~ eta3
eta8 ~ x4
x6 ~ eta6
eta2 ~ eta4
