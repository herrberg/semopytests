eta1 =~ y14 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y14 + y16
eta6 =~ y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21
eta1 ~ eta8
eta6 ~ eta1 + x4
x2 ~ eta6
x9 ~ x2
x5 ~ x9
x10 ~ eta7 + x5 + x7
eta5 ~ x9
eta4 ~ x2
x4 ~ eta4
x7 ~ x1 + x6
eta3 ~ x2
eta2 ~ x2
x3 ~ eta1
x8 ~ x1
