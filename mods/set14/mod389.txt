eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y10 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
x10 ~ eta8 + x6
x2 ~ eta5 + x10
x8 ~ eta4 + x2
eta8 ~ eta7 + x8
x5 ~ eta5
eta6 ~ x5
eta2 ~ x8
x9 ~ x8
x7 ~ x3 + x9
eta7 ~ x4
eta3 ~ x5
x1 ~ eta3
eta1 ~ x9
