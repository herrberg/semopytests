eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19 + y20
eta8 =~ y14 + y19 + y21
x5 ~ x10 + x6 + x8
eta5 ~ x5
eta6 ~ eta5
eta3 ~ eta4 + x5
x6 ~ eta5 + eta8
x7 ~ eta1 + x6 + x9
x4 ~ x7
eta4 ~ eta2
eta7 ~ eta5
x3 ~ x5
x1 ~ eta8
x2 ~ eta8
