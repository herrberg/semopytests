eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y10 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
x9 ~ x7
eta7 ~ x9
eta5 ~ eta7 + x10 + x2
x2 ~ eta3 + eta8 + x8
x6 ~ eta1 + eta6 + eta7
x1 ~ x5
x8 ~ eta2 + eta4 + x1
eta4 ~ x2
x3 ~ x9
x4 ~ x5
