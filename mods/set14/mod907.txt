eta1 =~ y18 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19 + y7
eta2 ~ eta3 + x1 + x4
x9 ~ eta2 + x2
eta7 ~ x9
x5 ~ eta2 + eta8 + x8
x1 ~ x5
eta6 ~ eta3
eta4 ~ eta6
x6 ~ x8
x10 ~ x9
x7 ~ x2
x3 ~ x4
eta1 ~ x2
eta5 ~ eta3
