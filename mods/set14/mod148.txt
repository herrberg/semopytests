eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y5 + y6
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
x7 ~ eta7 + x2 + x8
eta6 ~ x4 + x7
x2 ~ eta6 + x10
x1 ~ eta2 + x6 + x7
x4 ~ x5
x6 ~ x3
eta1 ~ eta4 + eta5 + x4
x9 ~ x7
eta3 ~ eta4
eta8 ~ eta6
