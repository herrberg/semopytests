eta1 =~ y1 + y2
eta2 =~ y14 + y19 + y4
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21 + y22
x1 ~ x6
eta8 ~ eta4 + eta5 + x1 + x3 + x7
eta1 ~ eta6 + x1
x9 ~ x6
x5 ~ x9
eta3 ~ eta6
eta7 ~ eta3
eta2 ~ eta6
x2 ~ x3
x10 ~ x1 + x4
x8 ~ eta3
