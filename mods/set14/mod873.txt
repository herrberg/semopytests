eta1 =~ y1 + y10
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18
x4 ~ eta6 + x1 + x5 + x6
x3 ~ x10 + x7
x1 ~ x3
eta8 ~ eta4 + eta6
x8 ~ eta8
eta4 ~ eta7 + x8
x2 ~ x3
eta3 ~ eta6
eta1 ~ x7
x9 ~ x1
eta2 ~ x6
eta5 ~ eta8
