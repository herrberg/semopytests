eta1 =~ y14 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
x2 ~ eta1 + eta6 + x4 + x7
eta4 ~ x2
eta7 ~ eta4
x1 ~ x4
x9 ~ x2
eta1 ~ x10 + x9
x3 ~ eta8 + x2 + x5
eta5 ~ x3 + x8
x5 ~ x6
eta8 ~ eta3
eta2 ~ eta8
