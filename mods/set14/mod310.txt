eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y10 + y7 + y8
eta4 =~ y10 + y11 + y18
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16
eta7 =~ y17 + y18
eta8 =~ y19 + y20 + y21
eta4 ~ eta8 + x6
eta1 ~ eta4 + x4 + x8
x6 ~ eta1 + eta6 + x10 + x5
eta7 ~ eta5
x8 ~ eta7
x2 ~ x3 + x4
x5 ~ eta3 + x9
x3 ~ x7
eta3 ~ x1
eta2 ~ x8
