eta1 =~ y2 + y5
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y15 + y20 + y22
eta6 ~ eta1 + x1 + x8
x5 ~ eta2 + eta6
x9 ~ eta3 + eta4 + eta6
x2 ~ eta6 + eta7 + x6
x8 ~ x2
eta3 ~ eta5
eta8 ~ x4 + x6
x7 ~ eta1
x3 ~ eta7 + x10
