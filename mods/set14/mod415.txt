eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y7 + y9
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18
x8 ~ eta1 + eta3 + x4 + x7
x3 ~ eta6 + eta8 + x10 + x8
eta3 ~ x3
eta5 ~ eta3
eta4 ~ eta1
x7 ~ x1
x5 ~ x8
x6 ~ eta7 + x4
eta2 ~ eta8
x2 ~ eta1
x9 ~ x7
