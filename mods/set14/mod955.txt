eta1 =~ y1 + y2
eta2 =~ y10 + y3 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
eta1 ~ x8
eta7 ~ eta1
eta4 ~ eta3 + eta7 + eta8 + x9
x3 ~ eta4
x7 ~ x3
eta8 ~ x6
x2 ~ eta8
x4 ~ x5 + x9
eta2 ~ x4
x5 ~ eta2
x1 ~ eta7 + x10
eta5 ~ x1
eta6 ~ eta7
