eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y14 + y9
eta7 =~ y15 + y16
eta8 =~ y17 + y18
x2 ~ eta3 + eta5
x6 ~ x2 + x4
eta4 ~ eta2 + x5 + x6
eta3 ~ eta4 + x3
x10 ~ eta3
eta1 ~ eta4
eta2 ~ eta7 + eta8
eta6 ~ eta4
x1 ~ x6 + x8 + x9
eta8 ~ x7
