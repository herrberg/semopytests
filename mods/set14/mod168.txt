eta1 =~ y1 + y2
eta2 =~ y14 + y3 + y5
eta3 =~ y12 + y6
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18
eta8 =~ y19 + y20 + y21
eta5 ~ eta4 + x1 + x2 + x3 + x5
x9 ~ eta5 + x7
x3 ~ x9
eta7 ~ x9
x1 ~ eta6
x10 ~ eta5
eta3 ~ eta2 + x2
eta8 ~ x5
x8 ~ x1
eta1 ~ x7
x6 ~ eta4
x4 ~ x1
