eta1 =~ y1 + y2 + y3
eta2 =~ y10 + y4 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19
eta2 ~ eta3 + eta7
eta8 ~ eta1 + eta2 + eta5 + x4
x6 ~ eta8
eta1 ~ x5 + x6
x4 ~ x1 + x3
x7 ~ eta2
x10 ~ eta4 + eta7
eta6 ~ eta1
x8 ~ eta6 + x2
x9 ~ x3
