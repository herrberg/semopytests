eta1 =~ y2 + y8
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
eta3 ~ x9
x6 ~ eta1 + eta3 + eta4 + x10
x1 ~ x6
eta7 ~ x5 + x6
eta1 ~ eta7
x2 ~ eta2 + eta4
eta8 ~ x2 + x3 + x4 + x7
x7 ~ eta6
eta5 ~ x2
x8 ~ x6
