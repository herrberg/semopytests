eta1 =~ y1 + y11 + y2
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y5
eta8 =~ y18 + y19 + y20
eta3 ~ x10
x3 ~ eta3 + eta6
eta4 ~ x1 + x3 + x8
eta6 ~ eta2 + eta4
x7 ~ x3
x4 ~ eta5 + eta7 + x7 + x9
x6 ~ x5
eta7 ~ x6
eta8 ~ x7
x2 ~ eta4
eta1 ~ x7
