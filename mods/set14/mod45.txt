eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y11 + y16
eta6 =~ y12 + y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
eta2 ~ eta6 + eta7 + x2 + x9
x4 ~ eta2 + x5
x7 ~ x4
x6 ~ x7
x5 ~ x10 + x6
eta5 ~ x1 + x9
eta3 ~ x2
x8 ~ eta6
eta8 ~ x4
x3 ~ x4
eta4 ~ x5
eta1 ~ x4
