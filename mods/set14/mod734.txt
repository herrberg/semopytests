eta1 =~ y18 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y4 + y6 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20
x1 ~ x4 + x7
eta8 ~ x1 + x8
x2 ~ eta2 + eta8 + x10 + x3
eta6 ~ x2
x10 ~ x9
eta7 ~ eta4 + x2
x4 ~ eta8 + x5
eta3 ~ x10 + x6
eta1 ~ eta3
eta5 ~ eta3
