eta1 =~ y1 + y3 + y7
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y13 + y18
eta6 =~ y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19 + y20
x1 ~ x10 + x6 + x8
x4 ~ x1
x10 ~ eta3 + eta4 + x3
eta3 ~ eta2
x5 ~ eta1 + x10
x7 ~ x5
eta4 ~ x7
eta8 ~ eta4
x2 ~ x6
eta1 ~ eta7
eta5 ~ eta7
x9 ~ x10
eta6 ~ x3
