eta1 =~ y1 + y13 + y2
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y21 + y5
eta8 ~ eta2 + eta6 + x3
x4 ~ eta8 + x6 + x7
x3 ~ x4 + x5
x1 ~ x3
x5 ~ eta5
eta1 ~ eta8 + x10
x2 ~ eta1 + x9
eta4 ~ eta6
eta7 ~ eta8
x8 ~ eta1
eta3 ~ x5
