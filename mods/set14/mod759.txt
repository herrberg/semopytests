eta1 =~ y1 + y2 + y5
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y16 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19
eta8 =~ y20 + y21 + y22
x9 ~ eta3 + eta8 + x3
eta7 ~ x9
eta2 ~ eta7 + x8
x3 ~ eta2
x2 ~ eta3 + eta4
x10 ~ x2
eta1 ~ x2
x4 ~ eta8
x6 ~ x4
eta4 ~ x5 + x7
x1 ~ x2
eta6 ~ eta4
eta5 ~ eta6
