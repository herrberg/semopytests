eta1 =~ y1 + y14 + y2
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19
x3 ~ eta4 + x8
eta2 ~ x3
eta3 ~ eta2
x1 ~ eta3 + x2
x6 ~ x1 + x4
x2 ~ x6
eta8 ~ eta2
eta7 ~ eta8
x9 ~ x1
eta5 ~ eta6 + x3 + x7
x5 ~ eta3 + x10
eta1 ~ x4
