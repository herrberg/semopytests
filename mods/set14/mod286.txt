eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y6 + y8
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y18
eta7 =~ y18 + y19
eta8 =~ y20 + y21 + y22
eta8 ~ x8 + x9
eta1 ~ eta8 + x1
eta6 ~ eta1 + eta2 + x5
x4 ~ eta6 + x2
x7 ~ eta1
x3 ~ eta1 + eta3
eta7 ~ eta1
x10 ~ x5
eta4 ~ x5
x8 ~ eta1
x6 ~ eta2
eta5 ~ eta8
