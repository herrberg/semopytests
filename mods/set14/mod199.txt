eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y12 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y21
eta7 =~ y18 + y19
eta8 =~ y20 + y21
x7 ~ eta4 + eta7 + x5
eta6 ~ eta7 + x1
x9 ~ eta6
x1 ~ x9
eta1 ~ eta4 + x8
x3 ~ eta1
eta3 ~ eta7
x4 ~ eta7
eta8 ~ x8
x6 ~ eta8
x10 ~ eta6 + x2
eta2 ~ x8
x2 ~ eta5
