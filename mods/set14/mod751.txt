eta1 =~ y10 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19
eta6 ~ x10 + x3
x9 ~ eta6 + x8
eta1 ~ x9
x7 ~ eta2 + eta6
eta2 ~ x2
eta7 ~ eta2 + x4
x4 ~ eta3
eta4 ~ eta6
x10 ~ eta4 + x5
x1 ~ x4
x6 ~ x1
eta8 ~ x6
eta5 ~ x8
