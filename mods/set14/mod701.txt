eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y5 + y6
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y14 + y5
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19 + y20
eta8 ~ x4
x5 ~ eta8
eta5 ~ eta2 + eta7 + x5 + x8
eta6 ~ eta5
x2 ~ eta6
eta3 ~ x3 + x5
x6 ~ eta3
x9 ~ x5
eta7 ~ eta1 + x10
eta4 ~ eta5
x1 ~ x3
x7 ~ x4
x10 ~ eta6
