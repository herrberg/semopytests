eta1 =~ y1 + y2
eta2 =~ y16 + y3 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19
eta2 ~ eta4 + x3
x5 ~ eta2 + x10 + x8
eta8 ~ x5
eta3 ~ eta8
eta6 ~ eta3
x10 ~ eta6
eta1 ~ eta2
x7 ~ eta1 + eta5
x2 ~ x7
eta7 ~ eta8
x6 ~ eta7
x1 ~ eta2
x4 ~ eta3
x9 ~ eta8
