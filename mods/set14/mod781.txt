eta1 =~ y1 + y18 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
eta7 ~ eta8
eta6 ~ eta5 + eta7 + x1 + x4
eta2 ~ eta6
x2 ~ eta2 + x10
x3 ~ x2
x5 ~ eta6
x1 ~ x5
eta3 ~ x1
x9 ~ x2
x8 ~ x9
eta1 ~ x2
x7 ~ eta6
x6 ~ eta4 + eta6
