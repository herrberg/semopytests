eta1 =~ y1 + y4
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y12 + y16
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20
x2 ~ eta4 + x10 + x7
x5 ~ x2
x1 ~ eta7 + x5
eta5 ~ x2
x10 ~ eta5
x8 ~ eta3 + x7
eta6 ~ x5 + x6
eta1 ~ eta6
x3 ~ eta5
x9 ~ x3
eta2 ~ x7
eta8 ~ eta2
x4 ~ eta4
