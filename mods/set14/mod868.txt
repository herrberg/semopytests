eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y18 + y6
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
x2 ~ eta6 + eta7
x1 ~ eta8 + x2 + x5
eta5 ~ x1 + x10 + x4
eta6 ~ eta5
x7 ~ eta6
x9 ~ x2
eta1 ~ x9
x6 ~ eta1
x5 ~ eta2
eta4 ~ x4
x3 ~ eta4
eta3 ~ x1
x8 ~ eta2
