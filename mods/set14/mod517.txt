eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16
eta7 =~ y18 + y4
eta8 =~ y18 + y19 + y21
x4 ~ eta7 + x3
x8 ~ eta1 + x4
x10 ~ eta2 + x4 + x9
eta8 ~ x10
x9 ~ eta5 + eta8 + x7
x2 ~ eta6
x7 ~ x2
x1 ~ eta7
eta3 ~ x4
eta4 ~ x2
x5 ~ eta6
x6 ~ eta6
