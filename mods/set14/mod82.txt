eta1 =~ y1 + y2 + y3
eta2 =~ y16 + y4
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y16
eta6 =~ y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19 + y20
x1 ~ eta1 + x5 + x6
eta3 ~ x1
x9 ~ eta3
x6 ~ x9
eta6 ~ eta2 + x1 + x2 + x4
x4 ~ eta8
x2 ~ x8
eta7 ~ x4
x10 ~ x4 + x7
eta5 ~ eta2
eta4 ~ eta8
eta1 ~ x3
