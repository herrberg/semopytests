eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y8 + y9
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y8
eta8 =~ y18 + y19 + y20
eta4 ~ eta6 + x2
eta7 ~ eta4 + x4
x8 ~ eta1 + eta7 + x7
eta6 ~ eta5 + x8
eta2 ~ eta4 + x1 + x9
x5 ~ x10 + x2 + x6
eta3 ~ x1
x3 ~ eta8 + x4
