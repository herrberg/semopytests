eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y6 + y9
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y10 + y21
eta2 ~ eta4 + x4
x2 ~ eta2 + eta6
eta7 ~ x2 + x8
x1 ~ eta7 + x3
eta1 ~ x1
eta5 ~ x9
x3 ~ eta5 + x7
eta4 ~ x2
eta8 ~ x5 + x7
x6 ~ eta8
x10 ~ x7
eta3 ~ x5
