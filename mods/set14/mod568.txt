eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y19 + y7 + y8
eta4 =~ y10 + y11
eta5 =~ y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y14 + y19
x9 ~ x8
x2 ~ eta8 + x5 + x7 + x9
x10 ~ x2
eta5 ~ eta1 + x10
eta7 ~ x2
eta3 ~ eta7
x5 ~ x3
eta2 ~ x5 + x6
x1 ~ eta2
x4 ~ eta6 + x1
eta4 ~ x10
x7 ~ eta7
