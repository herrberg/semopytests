eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16
eta7 =~ y18 + y19
eta8 =~ y16 + y21
x1 ~ eta6 + x4 + x8
eta8 ~ eta5 + x1 + x3
x9 ~ eta8
eta6 ~ x9
x7 ~ x1 + x2
x3 ~ eta3
x6 ~ x3
eta7 ~ x6
x5 ~ x3
eta2 ~ eta3
eta1 ~ eta3
eta4 ~ eta1
x10 ~ x6
