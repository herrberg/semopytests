eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19 + y20
eta8 =~ y14 + y22
x5 ~ eta3
x7 ~ eta5 + x3 + x5
eta8 ~ x7
x3 ~ eta2 + eta8
eta6 ~ eta1 + eta4 + eta8 + x1
x8 ~ x10 + x7
x10 ~ x2
x6 ~ x10
x4 ~ x5
x9 ~ x5
eta7 ~ x5
