eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19
eta8 =~ y14 + y21 + y8
x6 ~ x3
eta8 ~ eta3 + x6
x7 ~ eta8 + x4
eta3 ~ x2 + x5 + x7
x9 ~ eta3
x4 ~ eta4
eta7 ~ x7
x2 ~ eta2
x10 ~ eta6 + x5
eta1 ~ x10
eta5 ~ eta6 + x8
x1 ~ x6
