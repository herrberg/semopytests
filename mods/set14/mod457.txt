eta1 =~ y1 + y2 + y3
eta2 =~ y19 + y4
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19
eta5 ~ eta3 + x10
x5 ~ eta1 + eta5 + x8
x4 ~ eta2 + x3 + x5
eta1 ~ x7
eta3 ~ x5
x9 ~ x10
x1 ~ x9
eta8 ~ x6
x3 ~ eta8
eta7 ~ eta1
x2 ~ x8
eta6 ~ x9
eta4 ~ x9
