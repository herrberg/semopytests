eta1 =~ y1 + y2
eta2 =~ y12 + y3 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13
eta7 =~ y14 + y15
eta8 =~ y16 + y17 + y18
eta1 ~ eta8
eta4 ~ eta1 + eta6
eta5 ~ eta4
x2 ~ x1 + x9
x5 ~ x2
eta6 ~ x5
x3 ~ eta6
eta7 ~ eta6 + x8
x10 ~ eta4
x4 ~ x2
eta2 ~ x4
x6 ~ eta4
x1 ~ x6
x7 ~ eta8
eta3 ~ x4
