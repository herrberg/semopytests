eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y18
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
x4 ~ x5
eta8 ~ eta4 + eta5 + x4
x10 ~ eta2 + eta8
eta4 ~ x10 + x2 + x8
x8 ~ eta6
x1 ~ x10
x9 ~ x1 + x6
eta2 ~ x3
eta3 ~ x10
eta7 ~ eta8
x7 ~ x4
eta1 ~ x5
