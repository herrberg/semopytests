eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y9
eta6 =~ y12 + y13
eta7 =~ y14 + y15 + y16
eta8 =~ y17 + y18
x2 ~ eta2 + eta5 + eta6
eta8 ~ x10 + x2
eta1 ~ eta8
x10 ~ eta1 + x6
eta7 ~ eta8 + x1
x5 ~ eta7 + x3
x9 ~ eta8
eta3 ~ eta2 + eta4 + x8
x4 ~ x1
x7 ~ x1
