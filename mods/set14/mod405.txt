eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y9
eta8 =~ y18 + y19
x6 ~ eta4 + x1 + x10
eta3 ~ x6
eta7 ~ eta3 + x9
x5 ~ eta7 + x8
eta8 ~ x6
x9 ~ eta1 + eta6
eta2 ~ eta5 + eta7 + x4
eta4 ~ x3
x2 ~ x3
x7 ~ x9
eta6 ~ eta7
