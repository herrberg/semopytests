eta1 =~ y18 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19
x9 ~ x2 + x3 + x5
eta4 ~ eta3 + x7 + x9
x1 ~ x9
x2 ~ x1
eta5 ~ x9
x6 ~ eta5
eta6 ~ x7
eta1 ~ eta6
eta8 ~ eta1
x10 ~ x9
eta2 ~ eta7 + x5
x4 ~ eta2
x8 ~ x7
