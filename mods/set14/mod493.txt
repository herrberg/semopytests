eta1 =~ y1 + y2
eta2 =~ y4 + y8
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
eta4 ~ eta7 + eta8
eta1 ~ eta4 + x2
x8 ~ eta1 + x1 + x9
x4 ~ eta4 + x6
eta3 ~ eta7
eta2 ~ x6
eta6 ~ eta2
x10 ~ eta1
eta8 ~ eta1 + x5
eta5 ~ eta7
x7 ~ eta5
x3 ~ eta7
