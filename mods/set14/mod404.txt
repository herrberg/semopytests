eta1 =~ y1 + y10
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19
eta7 ~ eta3 + x2 + x5 + x9
x10 ~ x9
x3 ~ x10
x2 ~ eta6 + x1
x4 ~ x2
x5 ~ eta5
eta8 ~ eta1 + x2
eta2 ~ eta8
eta1 ~ eta2
eta4 ~ eta6
x8 ~ eta4
x7 ~ eta8
x6 ~ x10
