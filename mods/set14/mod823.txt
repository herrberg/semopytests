eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y6 + y9
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y21 + y22 + y6
x3 ~ eta1 + eta3 + eta4 + x9
eta7 ~ x3 + x5
x2 ~ eta7 + x1 + x4 + x8
x1 ~ x6
x5 ~ x7
eta8 ~ eta7
eta1 ~ eta8
eta5 ~ x3
eta2 ~ x7
eta6 ~ x8
x10 ~ x8
