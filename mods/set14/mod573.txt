eta1 =~ y1 + y2
eta2 =~ y12 + y3 + y4
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
x7 ~ eta5 + x10
x3 ~ x7
eta6 ~ x3
eta3 ~ eta6 + eta7
eta5 ~ x3 + x6
eta8 ~ x7 + x9
x1 ~ eta8
x5 ~ x3 + x4
eta2 ~ x5
x8 ~ eta2
x2 ~ x6
eta1 ~ x9
eta4 ~ x7
