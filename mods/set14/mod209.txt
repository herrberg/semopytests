eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19
eta8 =~ y12 + y20 + y9
eta4 ~ eta1 + x3 + x4 + x6 + x9
x7 ~ x9
x4 ~ eta2 + eta7
x3 ~ x5
eta1 ~ eta3
eta5 ~ x5
eta6 ~ x4
x2 ~ eta6
x8 ~ x5
x1 ~ x4
eta2 ~ x1
eta8 ~ x9
x10 ~ eta1
