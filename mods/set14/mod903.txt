eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y9
eta6 =~ y12 + y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
eta5 ~ eta1 + x2
x1 ~ eta5
x6 ~ x1
eta7 ~ eta5
x7 ~ eta7
x8 ~ x7
x4 ~ x8
eta4 ~ x1 + x3
x3 ~ x10
x9 ~ eta5
eta1 ~ x9
eta6 ~ x10
eta3 ~ eta6
x5 ~ x7
eta8 ~ eta5
eta2 ~ x8
