eta1 =~ y1 + y2 + y9
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16
eta7 =~ y17 + y18
eta8 =~ y17 + y19
eta8 ~ eta4 + x8
eta6 ~ eta8 + x5 + x9
eta1 ~ eta6
x4 ~ eta1 + eta7
x8 ~ eta6
x3 ~ x7
x9 ~ x3
eta7 ~ x2
eta2 ~ eta1 + x6
eta5 ~ x10 + x5
x1 ~ x3
eta3 ~ x5
