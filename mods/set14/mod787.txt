eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y12 + y14 + y15
eta7 =~ y16 + y17
eta8 =~ y16 + y18 + y19
eta8 ~ eta5 + x8
eta1 ~ eta8
eta5 ~ eta1 + eta2 + x7
x9 ~ eta5 + x4
x2 ~ x9
x6 ~ eta1
eta3 ~ x6
x7 ~ eta6
x5 ~ x6
x10 ~ x7
x3 ~ x6
x1 ~ eta6
eta7 ~ eta4 + eta5
