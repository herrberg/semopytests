eta1 =~ y1 + y2 + y3
eta2 =~ y10 + y4
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13
eta7 =~ y14 + y15
eta8 =~ y16 + y17
x6 ~ eta5 + x1 + x2 + x8
eta8 ~ x6
eta4 ~ eta8 + x7
eta6 ~ eta4 + x3
eta1 ~ eta8
eta2 ~ x4
x10 ~ eta2
x2 ~ x10
x5 ~ x2
eta3 ~ x7 + x9
eta7 ~ eta8
eta5 ~ eta7
