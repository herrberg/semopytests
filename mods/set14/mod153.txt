eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y10 + y18 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20 + y21
x2 ~ eta3 + eta8 + x8
x4 ~ eta4 + x2
x8 ~ eta2 + x4
x6 ~ eta5 + eta8
x9 ~ eta6 + x6
x1 ~ eta8
eta1 ~ x1
x3 ~ eta1
x5 ~ x7
eta5 ~ x5
eta7 ~ eta3
x10 ~ eta8
