eta1 =~ y1 + y2 + y7
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
eta5 =~ y13 + y14 + y15
eta6 =~ y15 + y16 + y18
eta7 =~ y19 + y20 + y21
eta8 =~ y22 + y23 + y24
x7 ~ eta6 + eta8
eta7 ~ x7
x3 ~ eta7 + x5
x1 ~ eta4 + x3
eta2 ~ x1 + x4 + x6
eta1 ~ eta2
x9 ~ eta7
x2 ~ x9
eta8 ~ x3
eta5 ~ eta2
x10 ~ eta3
eta4 ~ x10
x8 ~ x1
