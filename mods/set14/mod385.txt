eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y16 + y4 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19 + y20
x2 ~ x1 + x5
x4 ~ eta1 + x2
eta6 ~ eta2 + x4
eta7 ~ eta6
x10 ~ x4
eta1 ~ x10
x9 ~ eta1
eta5 ~ x2
x7 ~ eta5
eta4 ~ eta5
eta2 ~ x8
eta3 ~ eta5
x5 ~ x6
x3 ~ eta5
eta8 ~ x1
