eta1 =~ y19 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y19
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21 + y22
eta6 ~ eta5 + x7
x4 ~ eta6
eta7 ~ x4
x2 ~ eta1 + eta7 + x8
eta1 ~ x3
eta8 ~ eta1 + x10 + x5
x8 ~ x1
eta4 ~ eta7
eta3 ~ x8
x7 ~ x4
eta2 ~ x10
x9 ~ eta5
x6 ~ x8
