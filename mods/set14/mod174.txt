eta1 =~ y1 + y2 + y3
eta2 =~ y16 + y4
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15
eta7 =~ y13 + y16
eta8 =~ y18 + y19 + y20
x6 ~ eta2 + x5
x7 ~ x6 + x9
x4 ~ x1 + x7
x2 ~ x4
x9 ~ eta6 + x2
eta4 ~ eta7 + x9
x8 ~ x3
eta7 ~ eta8 + x8
x10 ~ x6
eta1 ~ eta3 + x7
eta5 ~ eta7
