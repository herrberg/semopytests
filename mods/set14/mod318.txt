eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y15 + y8
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19
x6 ~ x10 + x7 + x9
eta7 ~ eta4 + x6 + x8
eta3 ~ eta6 + eta7 + x3
x7 ~ eta3
eta2 ~ x6
x5 ~ eta5 + x6
x1 ~ eta7
x2 ~ eta6 + x4
eta1 ~ x2
x9 ~ eta8
