eta1 =~ y1 + y2 + y3
eta2 =~ y19 + y4
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y13 + y14 + y19
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19 + y20
x4 ~ x2
x6 ~ eta2 + eta8 + x4 + x7
eta2 ~ eta4 + x3 + x8
eta6 ~ eta2 + x10 + x5
x3 ~ eta6
eta3 ~ eta4
eta5 ~ eta3 + x1
x9 ~ eta3
eta7 ~ x8
eta1 ~ x8
