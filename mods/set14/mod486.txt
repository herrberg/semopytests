eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y14 + y19
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
x8 ~ eta3 + x3
eta1 ~ eta4 + x8
x1 ~ eta1 + x9
x7 ~ eta3 + eta7
x5 ~ x7
x6 ~ x8
x3 ~ x6
x10 ~ x3
x4 ~ x8
eta8 ~ x4
eta2 ~ eta4
x2 ~ eta2
eta5 ~ x9
eta6 ~ x4
