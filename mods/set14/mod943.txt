eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y19 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y22
eta7 =~ y18 + y19
eta8 =~ y20 + y21 + y22
eta6 ~ eta1 + x6 + x9
x4 ~ eta6 + eta7 + x10
eta8 ~ x4
eta7 ~ eta4 + eta8
x8 ~ eta7
x9 ~ eta3
x3 ~ x9
x5 ~ eta7
eta2 ~ eta5 + eta7
x1 ~ eta2
x2 ~ eta1
x7 ~ x9
