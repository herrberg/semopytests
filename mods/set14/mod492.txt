eta1 =~ y1 + y2 + y5
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y12 + y23
eta5 =~ y13 + y14 + y15
eta6 =~ y16 + y17 + y18
eta7 =~ y19 + y20 + y21
eta8 =~ y22 + y23
eta1 ~ eta4 + eta6 + eta7 + x6
x5 ~ eta1
eta3 ~ x5
x10 ~ eta3
eta6 ~ x1 + x5
x8 ~ eta1
eta8 ~ x6 + x7
x2 ~ eta8
x3 ~ eta2 + eta4 + eta5
eta7 ~ x4
x9 ~ eta5
