eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y18 + y8
eta5 =~ y10 + y11 + y12
eta6 =~ y10 + y13 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20 + y21
x6 ~ eta3 + eta4 + x1 + x5 + x7
x8 ~ x10 + x6
x9 ~ x8
x7 ~ x9
x10 ~ eta2 + eta8
x4 ~ x6
x1 ~ eta1
eta5 ~ eta4 + eta6
x2 ~ eta4
eta7 ~ x2
eta3 ~ x3
