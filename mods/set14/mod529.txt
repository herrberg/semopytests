eta1 =~ y1 + y7
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13
eta7 =~ y14 + y15
eta8 =~ y16 + y17
x5 ~ eta5 + eta6
x8 ~ x5
eta6 ~ x8
eta1 ~ eta3 + eta6
eta2 ~ x6
x9 ~ eta2
eta3 ~ eta8 + x9
x4 ~ eta3
eta4 ~ eta5 + eta7 + x3
x3 ~ x7
x1 ~ eta6
x2 ~ x1
x10 ~ eta8
