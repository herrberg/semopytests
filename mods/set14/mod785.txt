eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y16 + y6
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
x2 ~ eta1 + eta8
x9 ~ x10 + x2 + x8
eta8 ~ eta7 + x3 + x9
eta2 ~ eta8
x10 ~ eta3
eta4 ~ x9
eta7 ~ eta6
x6 ~ x2
eta5 ~ x6
x5 ~ eta7
x4 ~ x5
x7 ~ eta1
x1 ~ eta6
