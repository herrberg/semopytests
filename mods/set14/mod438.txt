eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y12 + y14
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
x2 ~ eta4
x6 ~ eta2 + eta7 + x2
x10 ~ eta8 + x3
x5 ~ x10
eta7 ~ eta5 + x4 + x5
eta1 ~ eta7
eta3 ~ x5
x8 ~ x10
eta8 ~ eta7
x1 ~ eta7
eta6 ~ x10
x7 ~ x4
x9 ~ eta5
