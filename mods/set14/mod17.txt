eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y16
eta6 =~ y12 + y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
x1 ~ eta1 + eta8 + x8
x4 ~ eta3 + x1
x5 ~ x10 + x4
x9 ~ eta7 + x1 + x3
x8 ~ x2 + x9
eta8 ~ eta5
x7 ~ eta1 + eta6
eta6 ~ eta2
eta4 ~ eta2
x6 ~ x2
