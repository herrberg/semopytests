eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y13 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13 + y14
eta6 =~ y10 + y15
eta7 =~ y17 + y18
eta8 =~ y19 + y20
x9 ~ eta2 + eta4 + x3 + x4
eta3 ~ x9
eta2 ~ eta3 + eta5
x2 ~ eta2 + x1
eta5 ~ eta6
x5 ~ eta6 + x7
eta8 ~ eta5
x8 ~ eta1 + eta8
x6 ~ eta2
eta7 ~ x6
x10 ~ x1
