eta1 =~ y1 + y2
eta2 =~ y3 + y9
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
x6 ~ eta5 + x1
x2 ~ x6 + x7
x1 ~ eta6 + x2
eta1 ~ x10 + x2
x3 ~ x4 + x6
x9 ~ x8
eta6 ~ x9
x10 ~ x5
eta4 ~ x2
eta8 ~ x2
eta2 ~ x2
eta3 ~ x10
eta7 ~ x9
