eta1 =~ y11 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19
x1 ~ x10 + x8
x5 ~ x1
eta1 ~ eta2
x10 ~ eta1 + x4
eta4 ~ x8
eta7 ~ eta4 + x2
x7 ~ eta2 + x6
eta5 ~ eta1
x3 ~ eta6 + x1
x9 ~ x10
eta3 ~ eta4
eta8 ~ x8
x4 ~ x1
