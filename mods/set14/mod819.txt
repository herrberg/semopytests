eta1 =~ y1 + y14 + y2
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
eta5 =~ y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19
eta8 =~ y13 + y21 + y22
eta2 ~ eta3 + x1 + x4
x8 ~ eta2
x7 ~ x8
eta1 ~ eta5 + x7 + x9
eta5 ~ x6
x1 ~ eta7 + x3 + x8
x5 ~ eta2
eta4 ~ eta8
x9 ~ eta4
eta6 ~ x4
x2 ~ eta6
eta7 ~ x10
