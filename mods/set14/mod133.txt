eta1 =~ y1 + y2
eta2 =~ y3 + y9
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13
eta7 =~ y14 + y15
eta8 =~ y16 + y17
x5 ~ eta3 + eta8 + x10
eta1 ~ x5 + x6 + x8
x9 ~ eta1 + eta5 + x3
x10 ~ x9
eta2 ~ eta1
eta6 ~ eta2
eta4 ~ eta1
x7 ~ x1 + x6
eta5 ~ x4
eta7 ~ eta3
x2 ~ x6
