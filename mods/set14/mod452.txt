eta1 =~ y1 + y15
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y9
eta6 =~ y11 + y12 + y13
eta7 =~ y14 + y15
eta8 =~ y16 + y17 + y18
x3 ~ x6
eta1 ~ eta4 + x2 + x3 + x8
x7 ~ eta1 + x5
x8 ~ x7
x10 ~ x8
x9 ~ x10
eta3 ~ x9
x1 ~ eta1
eta8 ~ eta5 + x3
x4 ~ x8
eta6 ~ x4
eta2 ~ eta4 + eta7
