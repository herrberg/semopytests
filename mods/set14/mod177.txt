eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y18
eta7 =~ y16 + y17
eta8 =~ y18 + y19
eta6 ~ eta1 + eta5 + eta8 + x4
eta7 ~ eta6
eta8 ~ eta7 + x10
x5 ~ eta1 + x2
x1 ~ eta6
x9 ~ eta1 + x3
x6 ~ eta1
eta2 ~ eta4 + x6
x2 ~ eta3
eta4 ~ x7
x8 ~ x4
