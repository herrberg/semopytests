eta1 =~ y1 + y9
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y9
eta6 =~ y11 + y12
eta7 =~ y13 + y14 + y15
eta8 =~ y16 + y17 + y18
x5 ~ eta3 + x10 + x6
eta5 ~ eta1 + x5
x10 ~ eta5
x7 ~ eta2 + x5
x8 ~ eta3
eta7 ~ x8
x4 ~ eta7
eta6 ~ x5
x6 ~ eta8
x9 ~ x5
x2 ~ x6
x1 ~ x8
eta4 ~ x3 + x8
