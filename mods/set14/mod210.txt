eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y15 + y8 + y9
eta4 =~ y10 + y11 + y12
eta5 =~ y13 + y14 + y15
eta6 =~ y16 + y17
eta7 =~ y18 + y19
eta8 =~ y11 + y20 + y22
eta5 ~ eta6
eta4 ~ eta5 + x2
x3 ~ eta4 + x1 + x10
x8 ~ eta5 + eta8 + x5
x7 ~ x8 + x9
eta2 ~ eta4 + x4
eta3 ~ eta4
eta8 ~ x7
eta1 ~ eta7 + x1
x6 ~ x7
