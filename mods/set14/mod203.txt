eta1 =~ y1 + y18 + y2
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y11 + y12 + y14
eta6 =~ y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21
eta1 ~ x10 + x4
x1 ~ eta1
eta4 ~ x1
x5 ~ eta4
x10 ~ eta5 + x1 + x2 + x3
x2 ~ eta8 + x9
eta3 ~ x4 + x6
x6 ~ eta2
eta7 ~ eta5
eta6 ~ x2
x7 ~ x4
x8 ~ eta1
