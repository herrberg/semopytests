eta1 =~ y1 + y3 + y8
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19
eta8 =~ y12 + y20 + y22
eta4 ~ eta2 + x2 + x5
x1 ~ eta4 + eta7
x8 ~ eta3 + eta6 + x1 + x10
x4 ~ x8
eta5 ~ x1
x6 ~ eta4
x10 ~ eta1
eta8 ~ x10
x2 ~ x1 + x3
x7 ~ eta1
x9 ~ eta4
