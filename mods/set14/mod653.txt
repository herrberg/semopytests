eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y18 + y6
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13
eta7 =~ y14 + y15 + y16
eta8 =~ y17 + y18 + y19
eta1 ~ eta4 + eta5 + eta7 + x1 + x2
x5 ~ eta1 + x3
eta5 ~ x5
x4 ~ x2 + x9
x8 ~ eta3 + x2 + x6 + x7
eta8 ~ eta1
x10 ~ x6
eta2 ~ x6
eta6 ~ x6
