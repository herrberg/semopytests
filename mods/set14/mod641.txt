eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y12 + y22 + y6
x4 ~ x1 + x2 + x3
x7 ~ eta3 + x4
x2 ~ eta5 + x7 + x9
eta1 ~ x2 + x8
eta8 ~ x1
eta2 ~ eta8
eta6 ~ x3 + x6
eta4 ~ eta6
x5 ~ x3
x10 ~ eta3
eta7 ~ x1
