eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y12 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y18 + y19 + y21
eta8 =~ y20 + y21 + y22
x7 ~ eta4
eta1 ~ x4 + x7
x10 ~ eta1
x4 ~ eta7 + x10
x1 ~ eta2 + x7
eta6 ~ x1 + x5
x8 ~ x2 + x7
x3 ~ eta3 + eta5 + eta7
eta5 ~ x6
x9 ~ eta7
eta8 ~ eta7
