eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14
eta7 =~ y10 + y16 + y17
eta8 =~ y18 + y19
x4 ~ eta2
x1 ~ x4
x9 ~ eta4 + x1
x2 ~ x9
eta1 ~ x2
x6 ~ x4
x10 ~ eta8 + x6
eta7 ~ x10
eta3 ~ x4 + x5
x3 ~ x7 + x9
eta4 ~ x3
eta6 ~ x1
eta5 ~ x4
x8 ~ x2
