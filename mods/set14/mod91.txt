eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y15 + y8
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19 + y6
x2 ~ eta3 + x10
eta4 ~ x2
eta6 ~ eta4
eta7 ~ eta1 + eta4
x3 ~ eta8 + x6
eta1 ~ x3
x8 ~ x2 + x7
x9 ~ x5 + x8
x1 ~ x9
x4 ~ x7
eta2 ~ eta3
x6 ~ eta1
eta5 ~ x5
