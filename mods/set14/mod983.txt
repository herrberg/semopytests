eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
eta6 =~ y10 + y15
eta7 =~ y16 + y17
eta8 =~ y18 + y19
eta3 ~ x7 + x8 + x9
x4 ~ eta1 + eta3
eta8 ~ x5 + x9
x6 ~ eta8 + x10
eta5 ~ x9
eta4 ~ eta5
x3 ~ x5
x8 ~ eta6
eta7 ~ x8
x2 ~ x9
eta2 ~ x10
x1 ~ eta1
