eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y3 + y8 + y9
eta4 =~ y10 + y12
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19
eta8 =~ y20 + y21 + y22
x2 ~ eta2 + eta5 + eta7 + x3 + x8
x4 ~ eta1 + eta3 + x5
eta2 ~ eta6 + x4
x7 ~ eta7
eta1 ~ eta2
eta4 ~ x6
x1 ~ eta4
eta6 ~ x1
eta8 ~ x1
x8 ~ x10
x9 ~ x4
