eta1 =~ y1 + y2 + y3
eta2 =~ y12 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y19 + y9
eta5 =~ y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20
eta2 ~ eta1 + eta4
x4 ~ eta2 + x2
x7 ~ eta2 + x6
x2 ~ x1 + x5
x9 ~ eta2 + eta8 + x10
x3 ~ x9
eta1 ~ x9
eta6 ~ eta7 + x6
eta7 ~ eta5
x5 ~ x8
eta3 ~ x6
