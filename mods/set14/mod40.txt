eta1 =~ y15 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13
eta6 =~ y14 + y15
eta7 =~ y16 + y8
eta8 =~ y18 + y19 + y20
eta8 ~ x4 + x7
x2 ~ x7
x9 ~ x2
eta7 ~ x7
x8 ~ eta7
x6 ~ eta4 + x3 + x4
eta1 ~ x10 + x6
x3 ~ eta1
x10 ~ eta6
eta3 ~ eta5 + eta6
x1 ~ eta3 + x5
eta2 ~ x4
