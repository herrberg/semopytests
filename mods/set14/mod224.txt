eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y10 + y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13 + y14
eta7 =~ y15 + y16 + y17
eta8 =~ y18 + y19
eta2 ~ eta3 + eta4 + x9
x10 ~ eta2
x2 ~ eta2 + eta8
x8 ~ x2
eta1 ~ eta2 + x6
x4 ~ eta1
x7 ~ eta3
eta6 ~ x3 + x7
eta7 ~ eta2 + eta5 + x5
x1 ~ eta2
eta4 ~ x1
