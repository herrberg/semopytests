eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y18 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18
eta8 ~ eta7
x8 ~ eta8
x9 ~ x8
eta4 ~ eta8 + x2 + x5
x6 ~ eta1 + eta8
x4 ~ x6
x10 ~ eta5 + x6
eta1 ~ x10
x3 ~ eta3 + x5
eta6 ~ eta3
x1 ~ x2 + x7
eta2 ~ eta3
