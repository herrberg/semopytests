eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y9
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y10 + y16 + y17
eta8 =~ y19 + y20 + y21
eta8 ~ x10
x2 ~ eta8 + x1
eta6 ~ eta4 + eta7 + x2
x1 ~ eta6
x9 ~ eta1 + x1
x3 ~ eta2 + x1 + x8
x5 ~ x3
eta2 ~ x7
eta5 ~ x1
x6 ~ eta2
eta3 ~ x10
x8 ~ x4
