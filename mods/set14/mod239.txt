eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12
eta6 =~ y14 + y8
eta7 =~ y15 + y16
eta8 =~ y17 + y18
x4 ~ x7 + x9
eta2 ~ x4 + x5
x10 ~ eta2 + x3
x5 ~ eta1 + eta4 + x10
x1 ~ eta6 + x5
eta4 ~ eta3
eta8 ~ x7
x2 ~ eta7 + eta8
x8 ~ x9
x6 ~ eta1
eta5 ~ x7
