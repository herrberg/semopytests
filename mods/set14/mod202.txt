eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y11 + y16
eta8 =~ y11 + y18 + y19
x3 ~ eta8 + x4 + x6
x9 ~ eta3 + x3
eta1 ~ x9
eta5 ~ eta1 + x8
x4 ~ eta4 + x10 + x7
x10 ~ x3
x5 ~ eta6 + x6
x2 ~ eta7 + x6
eta2 ~ eta6
x1 ~ x3
