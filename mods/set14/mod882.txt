eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y9
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y15 + y17
eta8 =~ y19 + y20
eta4 ~ x1 + x10 + x3
eta3 ~ eta4 + x7 + x8
x2 ~ eta7
x1 ~ x2
x6 ~ x4
x7 ~ x5 + x6
eta8 ~ x2
x9 ~ eta1 + x10
eta2 ~ x7
x5 ~ eta2 + eta6
eta5 ~ eta6
