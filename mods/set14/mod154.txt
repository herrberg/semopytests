eta1 =~ y2 + y9
eta2 =~ y3 + y4
eta3 =~ y5 + y7 + y8
eta4 =~ y8 + y9
eta5 =~ y10 + y11 + y12
eta6 =~ y13 + y14 + y15
eta7 =~ y16 + y17 + y18
eta8 =~ y19 + y20
eta5 ~ x10 + x7
eta8 ~ eta5
eta3 ~ eta8 + x2 + x9
x6 ~ eta3
x7 ~ eta8
x5 ~ eta2 + eta8
x1 ~ x2
eta1 ~ eta8 + x4
eta7 ~ eta1
x3 ~ eta6 + x8
x4 ~ x3
eta4 ~ x9
