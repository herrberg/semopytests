eta1 =~ y1 + y2 + y3
eta2 =~ y15 + y4 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y14 + y21
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19 + y20
eta8 =~ y21 + y22 + y23
x9 ~ eta7 + x8
eta5 ~ x3 + x9
x7 ~ eta5 + x5
x2 ~ x7
x10 ~ x2
x8 ~ x10
eta8 ~ x8
eta1 ~ eta5
x6 ~ eta2 + x5
eta6 ~ x5
x4 ~ eta7
eta4 ~ eta7
x1 ~ x5
x3 ~ eta3
