eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y14 + y18
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19
eta8 =~ y12 + y21
x10 ~ eta2 + x6
x9 ~ x10 + x4
eta6 ~ eta5 + x10
x2 ~ eta1 + eta6
eta2 ~ eta6
eta7 ~ eta2
x4 ~ eta8
x5 ~ x4
eta1 ~ x7
eta4 ~ eta3 + x1 + x6
x3 ~ eta2
x8 ~ x10
