eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y14 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12
eta6 =~ y13 + y14
eta7 =~ y15 + y16
eta8 =~ y17 + y18 + y19
eta1 ~ eta3 + eta4
eta6 ~ eta1 + x1 + x3
eta7 ~ eta6 + x7
eta3 ~ eta6
eta2 ~ eta3
x5 ~ eta6
x10 ~ x5
x1 ~ eta8
x2 ~ eta1
x8 ~ x3
eta5 ~ eta1 + x6
x4 ~ x1
x9 ~ x5
