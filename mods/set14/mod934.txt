eta1 =~ y15 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta5 =~ y11 + y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y21 + y6
eta3 ~ eta7 + x10 + x3 + x4 + x9
eta5 ~ eta2 + eta3 + x2
eta7 ~ eta5 + x6
eta6 ~ eta5
eta8 ~ eta6 + x8
eta4 ~ x6
x5 ~ eta4
eta2 ~ eta1
x10 ~ x1
x7 ~ x4
