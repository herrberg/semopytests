eta1 =~ y1 + y15 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y15 + y7 + y8
eta4 =~ y10 + y11
eta5 =~ y12 + y13 + y14
eta6 =~ y15 + y16 + y17
eta7 =~ y18 + y19
eta8 =~ y20 + y21 + y22
x2 ~ x10 + x9
x3 ~ x2 + x7
eta2 ~ x3
eta1 ~ eta2
x6 ~ eta1
x1 ~ x6
eta4 ~ eta5 + x2 + x4
x8 ~ eta4
x4 ~ x5
eta7 ~ eta3 + eta8
eta5 ~ eta7
x10 ~ x3
eta8 ~ eta6
