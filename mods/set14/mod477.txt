eta1 =~ y1 + y18 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y18
eta5 =~ y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18
eta8 =~ y19 + y20
x1 ~ eta3
x2 ~ x1 + x8
x7 ~ eta4 + eta6 + x2 + x5
x4 ~ eta2
eta6 ~ x4
x10 ~ eta6
x8 ~ eta1 + x3
x3 ~ x2
x9 ~ x6
eta4 ~ x9
eta7 ~ eta4
eta5 ~ x1
eta8 ~ eta4
