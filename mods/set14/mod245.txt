eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y11 + y8
eta5 =~ y10 + y9
eta6 =~ y11 + y12
eta7 =~ y13 + y14
eta8 =~ y15 + y16 + y17
eta7 ~ x5
x1 ~ eta7
eta2 ~ x1
eta8 ~ x2 + x5
eta1 ~ eta8 + x3 + x7
x2 ~ eta1
eta3 ~ x10 + x5 + x6
eta6 ~ x5 + x8
x9 ~ eta6 + x4
eta4 ~ x6
eta5 ~ x8
