eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta5 =~ y10 + y11 + y9
eta6 =~ y12 + y8
eta7 =~ y14 + y15
eta8 =~ y16 + y17
eta2 ~ eta1 + eta6
eta3 ~ eta2 + x2
x1 ~ eta3 + x6
x10 ~ eta3 + x9
eta6 ~ eta3
x8 ~ eta5 + x3
x9 ~ x8
x4 ~ eta2 + x5
x5 ~ eta7
eta8 ~ x2
eta4 ~ x2
x7 ~ x8
