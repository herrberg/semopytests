eta1 =~ y16 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y16 + y7 + y9
eta4 =~ y10 + y11
eta5 =~ y12 + y13
eta6 =~ y14 + y15 + y16
eta7 =~ y17 + y18
eta8 =~ y19 + y20
x8 ~ eta1 + eta4 + x1 + x2
x10 ~ x8
x6 ~ x10
x7 ~ x2
eta8 ~ eta7 + x7
eta3 ~ eta8
x5 ~ x8
eta4 ~ x5
eta6 ~ x2
x9 ~ x8
x3 ~ eta2
eta1 ~ x3
eta5 ~ eta1
x4 ~ eta5
