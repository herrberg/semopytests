eta1 =~ y1 + y2 + y3
eta2 =~ y14 + y4 + y5
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta5 =~ y10 + y13 + y14
eta6 =~ y15 + y16
eta7 =~ y17 + y18 + y19
eta8 =~ y20 + y21 + y22
eta8 ~ x4 + x5
x7 ~ eta8 + x3
x5 ~ eta1 + x7
eta7 ~ x1 + x2 + x5
eta5 ~ eta7
x10 ~ eta5
x1 ~ eta3
x6 ~ eta4 + x5 + x9
eta6 ~ x5
eta2 ~ eta4
x8 ~ x7
