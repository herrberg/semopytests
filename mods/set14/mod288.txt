eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta5 =~ y10 + y11
eta6 =~ y12 + y13
eta7 =~ y14 + y15 + y16
eta8 =~ y18 + y19 + y8
eta5 ~ x8 + x9
x5 ~ eta5
x1 ~ eta8 + x5
eta1 ~ x1 + x2
eta2 ~ eta1
eta6 ~ eta2
x8 ~ x5 + x6 + x7
eta3 ~ eta7 + x8
x4 ~ x1
x10 ~ x5
x3 ~ eta5
eta4 ~ x9
