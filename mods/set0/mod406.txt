eta1 =~ y1 + y2
eta2 =~ y3 + y4
x2 ~ x4
x5 ~ x2
eta2 ~ x4
x3 ~ eta2
x1 ~ eta2
eta1 ~ eta2
