eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x5 ~ eta2 + x1 + x4
x3 ~ x1
eta1 ~ x1
eta2 ~ x2
