eta1 =~ y1 + y2
eta2 =~ y3 + y4
x4 ~ eta1 + x3
x1 ~ x4
eta2 ~ eta1 + x5
x2 ~ x3
