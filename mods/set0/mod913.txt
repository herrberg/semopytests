eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x4 ~ eta1
x2 ~ eta2 + x4
x1 ~ x4
x3 ~ eta1
eta2 ~ x5
