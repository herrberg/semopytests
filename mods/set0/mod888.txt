eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta2 ~ eta1 + x4
x3 ~ eta2 + x1
x4 ~ x5
x2 ~ eta1
