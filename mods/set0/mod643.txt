eta1 =~ y1 + y2
eta2 =~ y3 + y4
x3 ~ eta2 + x2
eta1 ~ x3
x1 ~ eta1
x5 ~ x1
x4 ~ x5
