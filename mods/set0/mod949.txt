eta1 =~ y1 + y2
eta2 =~ y3 + y4
x3 ~ eta1
x4 ~ x3
eta2 ~ x4
x1 ~ eta2
x5 ~ eta2
x2 ~ x5
