eta1 =~ y1 + y2
eta2 =~ y3 + y4
x4 ~ x1 + x5
x2 ~ x4
eta2 ~ x2
x3 ~ x2
eta1 ~ x3
