eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x4 ~ x1
eta1 ~ x4
eta2 ~ eta1
x2 ~ x1 + x3
x5 ~ x4
