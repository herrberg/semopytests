eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta2 ~ x2
x1 ~ eta2
eta1 ~ x1
x5 ~ eta2
x4 ~ x5
x3 ~ x1
