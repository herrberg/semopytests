eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x1 ~ eta1
x2 ~ x1
x3 ~ x2
x4 ~ x3
x5 ~ x4
eta2 ~ x3
