eta1 =~ y1 + y2
eta2 =~ y3 + y4
x2 ~ x1
x4 ~ x2 + x3
eta2 ~ x3
x5 ~ eta2
eta1 ~ x5
