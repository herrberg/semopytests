eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x3 ~ eta1 + x4
x1 ~ eta1
eta2 ~ eta1 + x2
x5 ~ eta2
