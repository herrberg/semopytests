eta1 =~ y1 + y2
eta2 =~ y3 + y4
x2 ~ eta2 + x3 + x5
eta1 ~ eta2
x1 ~ eta2
x4 ~ x1
