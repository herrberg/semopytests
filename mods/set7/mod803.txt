eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x1 ~ x10 + x3
eta2 ~ eta1 + x3
x6 ~ eta1 + x5
x7 ~ x3
x4 ~ x10
x9 ~ x4
x2 ~ eta1
x8 ~ x10
