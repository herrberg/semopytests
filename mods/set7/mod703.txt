eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta2 ~ x2
x3 ~ eta2
x8 ~ x3
x4 ~ eta1 + eta2 + x6
x5 ~ x4
eta1 ~ x10
x9 ~ x2
x7 ~ x9
x1 ~ x4
