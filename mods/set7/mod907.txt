eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta2 ~ x2 + x4
x8 ~ eta2 + x9
x2 ~ eta1 + x3 + x6
x9 ~ x7
x1 ~ x9
x10 ~ x6
x5 ~ x6
