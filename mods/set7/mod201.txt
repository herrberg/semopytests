eta1 =~ y1 + y2
eta2 =~ y3 + y4
x5 ~ x8
x6 ~ x3 + x5
x1 ~ x6
x10 ~ x6
eta1 ~ x8
eta2 ~ x6
x2 ~ x5 + x9
x7 ~ x8
x4 ~ x8
