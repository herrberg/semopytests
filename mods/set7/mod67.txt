eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x1 ~ x5 + x7
x4 ~ eta2 + x1
x8 ~ x4 + x9
eta1 ~ x8
x10 ~ x6
x7 ~ x10
x2 ~ x7
x3 ~ x6
