eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x5 ~ eta1 + x10
x8 ~ x10
x7 ~ x10 + x4
eta2 ~ x7 + x9
x6 ~ eta2
x1 ~ x10
x2 ~ eta1 + x3
