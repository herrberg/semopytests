eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x6 ~ x2
x5 ~ x6
x7 ~ x5
x1 ~ x6 + x8
x9 ~ eta2 + x1
eta1 ~ x1
x3 ~ x1
x10 ~ x3
x4 ~ eta2
