eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x6 ~ x10 + x2 + x4
eta2 ~ eta1 + x2
x7 ~ eta2
x1 ~ x7
x3 ~ x1
x5 ~ eta1
x8 ~ x7
x9 ~ eta1
