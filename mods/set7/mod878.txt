eta1 =~ y1 + y2
eta2 =~ y3 + y4
x10 ~ x7
x5 ~ x10
x9 ~ x10 + x4
eta1 ~ x9
eta2 ~ x10
x3 ~ eta2
x4 ~ x1 + x8
x2 ~ x10
x6 ~ eta2
