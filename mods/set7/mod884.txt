eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x7 ~ x4 + x8
x6 ~ x7
eta2 ~ x3 + x6
x10 ~ eta2 + x1
x9 ~ x10
eta1 ~ x7
x2 ~ x10
x5 ~ x8
