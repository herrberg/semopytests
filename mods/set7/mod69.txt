eta1 =~ y1 + y2
eta2 =~ y3 + y4
x8 ~ x2
eta2 ~ eta1 + x8
x7 ~ eta2 + x6 + x9
x4 ~ eta2
eta1 ~ x10
x5 ~ x9
x1 ~ x5
x3 ~ x1
