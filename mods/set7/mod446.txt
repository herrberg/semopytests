eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x9 ~ x8
x4 ~ x5 + x6 + x9
x3 ~ eta2 + x8
x2 ~ x3
x10 ~ x8
x6 ~ x1
eta1 ~ x6
x7 ~ eta2
