eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta1 ~ x1 + x6
x4 ~ eta1 + x2
eta2 ~ x4
x9 ~ eta2 + x10
x5 ~ x4
x8 ~ x10 + x7
x3 ~ eta2
