eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x5 ~ x10
x1 ~ x5
x6 ~ x1 + x3
x7 ~ x6
x8 ~ x10
x9 ~ x8
x2 ~ x10
eta2 ~ x2
eta1 ~ eta2
x3 ~ x4
