eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta1 ~ x1 + x4
x9 ~ eta1
eta2 ~ x10 + x7 + x9
x2 ~ x9
x5 ~ x2
x4 ~ x8
x3 ~ x7
x6 ~ x4
