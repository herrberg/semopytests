eta1 =~ y1 + y2
eta2 =~ y3 + y4
x10 ~ x6
x5 ~ x10
x1 ~ x5
eta1 ~ x1
x3 ~ eta1
x2 ~ eta2 + x4 + x5
x4 ~ x9
x7 ~ eta1
eta2 ~ x8
