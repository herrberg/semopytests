eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x6 ~ x4
x5 ~ x6
x10 ~ x5
x2 ~ x10
eta2 ~ x2
x1 ~ eta2
x8 ~ x5
eta1 ~ x2
x7 ~ x3 + x6
x9 ~ x2
