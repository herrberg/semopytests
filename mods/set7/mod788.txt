eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x8 ~ x6
x4 ~ x5 + x8
x9 ~ eta2 + x4
x2 ~ x9
x7 ~ x2
x10 ~ eta1 + x5
x1 ~ x8
x3 ~ eta1
