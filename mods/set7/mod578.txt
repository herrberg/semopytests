eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x6 ~ eta1
x9 ~ x2 + x6
x10 ~ x2 + x7
x1 ~ x10
x8 ~ x1
x7 ~ eta2 + x5
x3 ~ x4 + x6
