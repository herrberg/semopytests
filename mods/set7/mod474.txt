eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x6 ~ x3
x10 ~ x6
x9 ~ x10
x8 ~ x9
x7 ~ x5 + x6
x5 ~ x2
x4 ~ x10
x1 ~ x9
eta2 ~ x3
eta1 ~ x9
