eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x1 ~ eta1
x7 ~ eta2 + x1 + x4
x2 ~ x7
x3 ~ eta1
x10 ~ x3 + x8
x6 ~ eta1
x9 ~ eta1
eta2 ~ x5
