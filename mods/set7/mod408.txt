eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x8 ~ x2
x3 ~ x8
x4 ~ x3
eta2 ~ x3 + x6
x7 ~ x8
x5 ~ x3
x10 ~ x3 + x9
eta1 ~ x8
x1 ~ x8
