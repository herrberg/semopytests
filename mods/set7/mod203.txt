eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x9 ~ x2
x8 ~ x9
x7 ~ x2
x4 ~ x7
x3 ~ x2
eta2 ~ x2 + x6
x6 ~ x10
x5 ~ x6
x1 ~ x5
eta1 ~ x7
