eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x6 ~ x2
x3 ~ eta2 + x10 + x6 + x8
eta1 ~ x3
x9 ~ x3 + x5
x4 ~ eta2
x7 ~ x2
x5 ~ x1
