eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x4 ~ eta1 + x1 + x10
x7 ~ eta2 + x2 + x4
eta2 ~ x5
x2 ~ x9
x6 ~ x3
eta1 ~ x6
x8 ~ x6
