eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x5 ~ eta2 + x7
x1 ~ x3 + x5 + x6
x10 ~ x4
eta1 ~ x10
x3 ~ eta1 + x2
x2 ~ x8
x9 ~ eta2
