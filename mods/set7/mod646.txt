eta1 =~ y1 + y2
eta2 =~ y3 + y4
x5 ~ x3
x7 ~ x5 + x8
eta1 ~ x7
x6 ~ eta2 + x7
x10 ~ x2 + x7
x8 ~ x4
x1 ~ x7
eta2 ~ x9
