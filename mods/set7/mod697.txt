eta1 =~ y1 + y2
eta2 =~ y3 + y4
x3 ~ x2
x1 ~ eta1 + x3
eta2 ~ x1 + x10 + x4 + x7 + x9
x5 ~ x6
x8 ~ x5
eta1 ~ x8
