eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x1 ~ x4 + x5
x9 ~ x1
x3 ~ x7 + x9
eta1 ~ x3
x6 ~ eta2 + x10 + x2
x7 ~ x6 + x8
