eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x6 ~ x8
x4 ~ eta2 + x5 + x6
x5 ~ eta1 + x2
x3 ~ x2
x9 ~ x3
x1 ~ x6
x10 ~ x1
x7 ~ eta1
