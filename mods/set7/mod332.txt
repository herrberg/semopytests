eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x7 ~ x10 + x4
x3 ~ x7
x6 ~ x10 + x2 + x8
x2 ~ x9
eta2 ~ x1 + x2
x5 ~ x8
eta1 ~ x5
