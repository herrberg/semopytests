eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x2 ~ x8
x5 ~ x2 + x9
x3 ~ x5 + x7
x4 ~ x3
x7 ~ eta1 + x1
eta2 ~ x9
x10 ~ x9
x6 ~ eta1
