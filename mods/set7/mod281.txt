eta1 =~ y1 + y2
eta2 =~ y3 + y4
x2 ~ eta1 + x3
x6 ~ x2 + x5 + x7
x1 ~ x6
eta2 ~ x3 + x4 + x9
x10 ~ eta2
x8 ~ x10
