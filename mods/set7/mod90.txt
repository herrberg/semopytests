eta1 =~ y1 + y2
eta2 =~ y3 + y4
x1 ~ x3 + x5
x4 ~ x1 + x10
x9 ~ x4
x6 ~ x9
x10 ~ eta2
x8 ~ x1
x7 ~ x1
eta1 ~ eta2
x2 ~ eta1
