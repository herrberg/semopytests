eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta2 ~ x8
x6 ~ eta2 + x3 + x5
x10 ~ eta1 + x3
x2 ~ x10
x1 ~ eta2
x9 ~ x10
x7 ~ x3
x4 ~ x3
