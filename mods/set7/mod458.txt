eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x3 ~ x5
x2 ~ x3
x6 ~ x2 + x7
x9 ~ x6
x8 ~ eta2 + x10 + x3
x7 ~ eta1
x1 ~ x10
x4 ~ x1
