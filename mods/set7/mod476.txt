eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x2 ~ x10
eta2 ~ x2
eta1 ~ eta2 + x3
x1 ~ eta1
x7 ~ x1
x6 ~ x7
x8 ~ x1
x4 ~ eta2 + x9
x5 ~ x2
