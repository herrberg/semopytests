eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x5 ~ x10 + x9
x3 ~ eta2 + x5
x4 ~ x3
x1 ~ x3
x8 ~ x1
eta1 ~ x9
x6 ~ x1 + x7
x2 ~ x9
