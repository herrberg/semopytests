eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x2 ~ x10 + x4
eta2 ~ x2 + x5
x3 ~ eta2
x9 ~ x3
x7 ~ x2
x8 ~ x1 + x2
x6 ~ eta1 + x4
