eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x9 ~ eta2 + x3 + x4
x2 ~ x9
x7 ~ x10
eta2 ~ x7
x8 ~ eta2 + x1
x4 ~ x6
x5 ~ x9
eta1 ~ x10
