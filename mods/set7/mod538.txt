eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta1 ~ x2 + x8
x7 ~ eta1 + x6
x9 ~ x7
x1 ~ x4
x2 ~ x1
x5 ~ x10 + x8
eta2 ~ x2
x3 ~ x8
