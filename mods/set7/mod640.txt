eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x9 ~ x2
x10 ~ x6 + x9
eta1 ~ x10
x1 ~ x2
x8 ~ x9
x3 ~ x8
x6 ~ eta2 + x5
x4 ~ x9
x7 ~ x4
