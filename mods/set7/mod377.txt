eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x8 ~ x10 + x5 + x6
eta1 ~ x2 + x4 + x8
eta2 ~ eta1 + x1
x9 ~ eta2
x7 ~ x5
x3 ~ eta2
