eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x8 ~ x2
x6 ~ x10 + x8 + x9
x1 ~ x6
x3 ~ x1
eta2 ~ x6
eta1 ~ eta2
x4 ~ x8
x7 ~ eta2
x5 ~ x8
