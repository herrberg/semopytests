eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x3 ~ x8
eta2 ~ x3
x5 ~ x3 + x9
x7 ~ x10 + x5
x9 ~ x6
x1 ~ x6
x4 ~ x2 + x8
eta1 ~ x10
