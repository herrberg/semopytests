eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x1 ~ x10 + x2
x8 ~ x1 + x7
x3 ~ eta1 + x4
x7 ~ x3
x9 ~ x3
eta2 ~ x9
x5 ~ x3
x6 ~ x7
