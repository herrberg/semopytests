eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x4 ~ x5
x2 ~ eta2 + x4
x9 ~ x10 + x2 + x6
x3 ~ x9
x8 ~ x3
x7 ~ x1 + x9
eta1 ~ x4
