eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x3 ~ eta1 + x2 + x9
x7 ~ x3
x10 ~ x7
x4 ~ x10
x1 ~ eta2 + x3
x6 ~ x1
x5 ~ x9
x8 ~ x10
