eta1 =~ y1 + y2
eta2 =~ y3 + y4
x8 ~ x10
eta1 ~ x5 + x8
x1 ~ eta1
x2 ~ x1
x9 ~ x4 + x8
eta2 ~ x9
x7 ~ eta2
x6 ~ x9
x3 ~ x1
