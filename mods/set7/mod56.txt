eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x5 ~ x6
x7 ~ x5 + x8
eta2 ~ x7
x9 ~ x1 + x7
x3 ~ eta1 + x7
x2 ~ x3
x10 ~ x2
x4 ~ x5
