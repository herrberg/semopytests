eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta1 ~ eta2 + x3 + x5 + x9
x10 ~ eta1 + x8
x4 ~ x1 + x10 + x6
x6 ~ x2
x5 ~ x7
