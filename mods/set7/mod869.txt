eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x10 ~ eta1 + x1 + x7 + x9
x4 ~ x10
eta2 ~ x4
x2 ~ x3
x9 ~ x2
x8 ~ eta1
x6 ~ x7
x5 ~ x4
