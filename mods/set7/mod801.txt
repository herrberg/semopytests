eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x6 ~ x2 + x4
x8 ~ x6
x5 ~ eta2 + x8
x1 ~ x5
eta2 ~ eta1
x10 ~ eta2
x9 ~ x10
x3 ~ eta2
x7 ~ eta2
