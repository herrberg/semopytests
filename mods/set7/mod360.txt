eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x1 ~ x4
x10 ~ x1 + x8
eta1 ~ x10
x5 ~ eta1
x6 ~ x5
x2 ~ eta2
x8 ~ x2
x9 ~ x2
x7 ~ x9
x3 ~ x10
