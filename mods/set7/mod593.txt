eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x6 ~ x10 + x8
x7 ~ x5 + x6
x9 ~ x3 + x7
eta2 ~ x10
eta1 ~ eta2 + x1
x4 ~ x10
x2 ~ x6
