eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x8 ~ x5
x1 ~ x8
x10 ~ x1
x3 ~ eta1 + x2 + x5
eta1 ~ x9
eta2 ~ eta1
x7 ~ eta2
x6 ~ eta1
x4 ~ x5
