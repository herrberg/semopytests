eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x3 ~ x7
x9 ~ x10 + x3
eta2 ~ x6 + x9
x5 ~ eta2
x2 ~ x7
x4 ~ x2 + x8
eta1 ~ x10
x1 ~ x8
