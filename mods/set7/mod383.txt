eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta2 ~ x6
eta1 ~ eta2
x8 ~ eta1 + x2
x7 ~ x10 + x8
x9 ~ x7
x1 ~ x9
x5 ~ x3 + x4 + x8
