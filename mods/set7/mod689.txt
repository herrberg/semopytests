eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x10 ~ x2
x4 ~ x10
eta1 ~ x4
x8 ~ eta2 + x10
x1 ~ x8
x9 ~ x1 + x7
x3 ~ x4
x5 ~ x7
x6 ~ eta2
