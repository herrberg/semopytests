eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x1 ~ x2
x7 ~ x1
eta1 ~ x7 + x8
x9 ~ eta1
x5 ~ eta2 + x3 + x9
x4 ~ x7
x8 ~ x10
eta2 ~ x6
