eta1 =~ y1 + y2
eta2 =~ y3 + y4
x3 ~ x8
x10 ~ x1 + x3
x7 ~ x3 + x4
eta1 ~ x1
x2 ~ eta2 + x1 + x6 + x9
x5 ~ eta2
