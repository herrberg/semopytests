eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x8 ~ eta1 + x6
x3 ~ x8
x6 ~ eta2 + x1
x9 ~ x2 + x4 + x8
x7 ~ x9
x5 ~ eta1
x10 ~ x9
