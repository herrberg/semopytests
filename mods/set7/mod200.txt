eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta2 ~ eta1 + x8
x7 ~ eta2
x2 ~ x7
x10 ~ x2 + x3
x6 ~ x10
x1 ~ x7
x9 ~ x1
x4 ~ x3
x5 ~ x1
