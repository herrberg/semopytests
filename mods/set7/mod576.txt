eta1 =~ y1 + y2
eta2 =~ y3 + y4
x3 ~ eta2 + x4 + x6
eta2 ~ x1 + x2 + x8
x9 ~ eta1 + x8
x10 ~ x6
x5 ~ x8
x7 ~ x5
