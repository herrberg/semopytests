eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta1 ~ eta2 + x3
x5 ~ eta1
x1 ~ x2 + x3
x6 ~ x1
x10 ~ x4 + x6
x9 ~ x10
x8 ~ x4
x7 ~ x6
