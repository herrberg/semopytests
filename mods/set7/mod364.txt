eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x7 ~ x3 + x4
x5 ~ x10 + x7
x8 ~ x5
x3 ~ x2
eta1 ~ x4 + x6
x9 ~ x6
eta2 ~ x3
x1 ~ x2
