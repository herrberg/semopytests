eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x3 ~ x2
x7 ~ x3
x10 ~ eta2 + x2 + x5 + x8 + x9
x1 ~ x8
x4 ~ x1
eta1 ~ x6 + x8
