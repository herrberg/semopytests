eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x3 ~ x5
x7 ~ x3
x1 ~ x7
x2 ~ x1
x9 ~ eta1 + x5
x6 ~ x1 + x8
x10 ~ x6
eta2 ~ x7
x4 ~ eta1
