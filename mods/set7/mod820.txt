eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x5 ~ x1 + x6 + x8
x3 ~ x5
x6 ~ x9
eta1 ~ x10 + x6
x4 ~ eta1
eta2 ~ x4
x2 ~ x9
x7 ~ x5
