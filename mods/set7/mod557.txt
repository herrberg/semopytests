eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x1 ~ x8
x5 ~ x1
x2 ~ x5
x6 ~ x2 + x3
x7 ~ eta1 + x5
x3 ~ x10
x4 ~ x3
eta2 ~ x3
x9 ~ eta1
