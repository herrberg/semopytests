eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta1 ~ eta2 + x5
x8 ~ eta1 + x3
x9 ~ eta1
x5 ~ x4
x1 ~ eta1
x6 ~ eta1
x7 ~ eta1
x2 ~ x7
x10 ~ eta1
