eta1 =~ y1 + y2
eta2 =~ y3 + y4
x3 ~ eta1
x8 ~ x3 + x6
x9 ~ x8
x1 ~ x9
x10 ~ eta2 + x6
x2 ~ x10
x7 ~ x3
x5 ~ x3
eta2 ~ x4
