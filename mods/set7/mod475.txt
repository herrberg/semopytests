eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x1 ~ eta1 + x2 + x4 + x5 + x8
eta2 ~ x6
x7 ~ eta2
x8 ~ x7
x9 ~ x8
x4 ~ x10
x3 ~ x7
