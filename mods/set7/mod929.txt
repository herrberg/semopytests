eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x2 ~ x8
eta1 ~ x2
x7 ~ eta1 + x5
x4 ~ eta2 + x7
x6 ~ x2
x1 ~ x10 + x8 + x9
x3 ~ eta2
