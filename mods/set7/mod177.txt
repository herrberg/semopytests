eta1 =~ y1 + y2
eta2 =~ y3 + y4
x2 ~ eta1 + x5
x10 ~ x2 + x6
x9 ~ eta1 + x3
x1 ~ x3 + x8
x7 ~ x1
x6 ~ x4
eta2 ~ x6
