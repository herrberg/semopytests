eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x4 ~ x1 + x10 + x8 + x9
x3 ~ eta1 + x2 + x5
x9 ~ x3
x6 ~ x9
x7 ~ eta2 + x6
