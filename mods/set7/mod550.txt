eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x7 ~ x3 + x4 + x6
eta2 ~ x7
x5 ~ eta2
x4 ~ x2
x1 ~ x7
x8 ~ x4 + x9
eta1 ~ x6
x10 ~ x6
