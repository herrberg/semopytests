eta1 =~ y1 + y2
eta2 =~ y3 + y4
x7 ~ x3
x2 ~ x7
x1 ~ eta1 + x2
x5 ~ x1 + x6 + x9
x10 ~ x1 + x4
x8 ~ x6
eta2 ~ x8
