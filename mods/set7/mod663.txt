eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x5 ~ x4
x6 ~ x5
x7 ~ eta1 + eta2 + x6
x10 ~ x7 + x9
eta2 ~ x8
x2 ~ x5
x1 ~ x6
x9 ~ x3
