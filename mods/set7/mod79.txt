eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x1 ~ x6 + x8
x3 ~ x1 + x2
x7 ~ x3
x4 ~ x6 + x9
eta2 ~ eta1 + x6
x10 ~ x2
x5 ~ x8
