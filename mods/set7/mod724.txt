eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta2 ~ x10
x9 ~ eta1 + eta2
x1 ~ x3 + x9
x8 ~ x1 + x6
x7 ~ x8
x5 ~ x10 + x2
x4 ~ x9
