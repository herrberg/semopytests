eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x8 ~ eta1 + x2
x5 ~ x8
x3 ~ eta2 + x10 + x5
eta1 ~ x9
x7 ~ x2
x1 ~ x7
x4 ~ x9
x6 ~ eta1
