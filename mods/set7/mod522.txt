eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta2 ~ eta1
x3 ~ eta2 + x6
x10 ~ x3
x1 ~ x10
x8 ~ eta2 + x2
x5 ~ x8
x4 ~ x3
x9 ~ x4
x7 ~ x9
