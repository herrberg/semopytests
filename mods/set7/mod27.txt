eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta1 ~ x10 + x7
x1 ~ eta1
x3 ~ x1
x9 ~ x3
x6 ~ x9
x7 ~ eta2 + x2
x5 ~ x10
x4 ~ x1
x8 ~ x9
