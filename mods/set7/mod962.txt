eta1 =~ y1 + y2
eta2 =~ y3 + y4
x5 ~ x4
eta2 ~ x1 + x5
x8 ~ eta2 + x9
x10 ~ eta1 + x8
x7 ~ x8
x3 ~ x5 + x6
x2 ~ x1
