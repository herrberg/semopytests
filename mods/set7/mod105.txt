eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x2 ~ x8
eta2 ~ x2
x6 ~ eta2
x9 ~ eta2
eta1 ~ x2 + x7
x3 ~ eta1
x4 ~ x5 + x7
x1 ~ eta2
x10 ~ x8
