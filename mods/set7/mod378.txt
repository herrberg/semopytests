eta1 =~ y1 + y2
eta2 =~ y3 + y4
x10 ~ x9
x7 ~ x10
x6 ~ eta1 + x4 + x7
x2 ~ x1 + x8
eta1 ~ x2 + x3
eta2 ~ x2
x5 ~ x9
