eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x7 ~ x8
x2 ~ x1 + x10 + x8
x3 ~ x8
x9 ~ eta1 + x3
eta2 ~ x9
x6 ~ eta1
x5 ~ x4 + x9
