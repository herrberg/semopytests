eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x7 ~ x1 + x2
x9 ~ eta1 + x10 + x7
eta1 ~ x4
x8 ~ x1
x6 ~ x8
eta2 ~ x8
x5 ~ eta2
x3 ~ x2
