eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x2 ~ x9
x1 ~ x2
x5 ~ eta2 + x1
eta1 ~ x9
x10 ~ x2
x6 ~ x10
x4 ~ x6
eta2 ~ x3 + x8
x7 ~ x1
