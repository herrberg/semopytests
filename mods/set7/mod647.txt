eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta2 ~ x2
x8 ~ eta2
x6 ~ x10 + x8
x4 ~ x6
x5 ~ x4
x10 ~ x7
x1 ~ x6
x3 ~ eta2 + x9
eta1 ~ x8
