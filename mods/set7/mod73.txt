eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x3 ~ eta1 + x5
x2 ~ x3
eta1 ~ x1 + x8
x6 ~ x3
x9 ~ x3
x10 ~ x9
eta2 ~ x1
x7 ~ x5
x4 ~ x1
