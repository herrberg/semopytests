eta1 =~ y1 + y2
eta2 =~ y3 + y4
x9 ~ x5 + x6
eta1 ~ x3
x5 ~ eta1 + x1
eta2 ~ x6 + x7
x7 ~ x10 + x2
x4 ~ eta1
x1 ~ x8
