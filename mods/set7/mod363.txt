eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta2 ~ x3 + x8 + x9
x5 ~ eta2
x7 ~ x5
x8 ~ x6
x2 ~ x5
x10 ~ x2
x4 ~ x5
eta1 ~ x2
x1 ~ x8
