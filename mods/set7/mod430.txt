eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x3 ~ x6
x1 ~ x3
eta2 ~ x1
x5 ~ eta2 + x4 + x8
x8 ~ eta1
x9 ~ eta2
x2 ~ x6 + x7
x10 ~ x7
