eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta2 ~ eta1
x7 ~ eta2 + x10 + x6 + x8
x8 ~ x4
x1 ~ x10 + x2 + x5
x3 ~ eta2
x9 ~ x8
