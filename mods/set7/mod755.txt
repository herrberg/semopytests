eta1 =~ y1 + y2
eta2 =~ y3 + y4
x6 ~ x4
x2 ~ x6
x3 ~ eta1 + x10 + x2
eta2 ~ x2 + x9
x1 ~ eta2
x7 ~ eta1
x8 ~ x6
x5 ~ x6
