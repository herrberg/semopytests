eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x4 ~ x10
x9 ~ x4
x8 ~ x3 + x9
eta2 ~ x8
eta1 ~ eta2
x6 ~ x10
x5 ~ eta2
x2 ~ x1 + x9
x7 ~ x4
