eta1 =~ y1 + y2
eta2 =~ y3 + y4
x2 ~ x7
x3 ~ x2 + x5
x10 ~ x3
x1 ~ x10 + x4 + x9
eta1 ~ x1
x6 ~ x3
eta2 ~ x2
x4 ~ x8
