eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x10 ~ x3
x2 ~ x10
x6 ~ x10
x7 ~ x6
x9 ~ eta1 + eta2 + x1 + x7
x5 ~ eta1 + x4
x8 ~ eta1
