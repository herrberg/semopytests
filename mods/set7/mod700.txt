eta1 =~ y1 + y2
eta2 =~ y3 + y4
x8 ~ eta2 + x2
x1 ~ x8
x9 ~ x5 + x8
x5 ~ x3
x7 ~ x10 + x8
eta1 ~ x3
x6 ~ x10
x4 ~ x5
