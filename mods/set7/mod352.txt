eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x8 ~ eta2 + x5 + x9
eta2 ~ x6
x1 ~ eta2 + x4
x10 ~ x1
eta1 ~ eta2 + x7
x9 ~ x2 + x3
