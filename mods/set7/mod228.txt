eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x6 ~ x8
x5 ~ x10 + x6
eta1 ~ x5 + x7
x10 ~ x2
eta2 ~ x6
x1 ~ x4 + x8
x9 ~ x5
x3 ~ x4
