eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x7 ~ x1 + x8
x9 ~ x6 + x7
x6 ~ x10
x4 ~ x7
x3 ~ x4
eta1 ~ x3
x2 ~ x6
eta2 ~ x8
x5 ~ x4
