eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x1 ~ x3 + x5
x6 ~ x1
eta1 ~ x10 + x5
eta2 ~ eta1
x8 ~ eta2
x10 ~ x4
x2 ~ x4 + x9
x7 ~ x2
