eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta1 ~ x10
x9 ~ eta1
x1 ~ x9
x3 ~ x1 + x2
x5 ~ x1 + x6
x8 ~ eta2 + x9
x6 ~ x4
x7 ~ x9
