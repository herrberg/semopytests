eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x5 ~ eta2 + x9
x6 ~ x5
x1 ~ eta1 + x9
x10 ~ x2 + x9
x4 ~ x3 + x9
x7 ~ x4
x8 ~ x4
