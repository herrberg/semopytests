eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x10 ~ x1 + x4
x3 ~ x10
x2 ~ x4
eta1 ~ x4 + x6 + x8
x7 ~ eta1
x5 ~ eta2 + x7
x9 ~ x8
