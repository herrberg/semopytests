eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta2 ~ x6
x10 ~ eta2 + x3
x8 ~ x10
x1 ~ x8 + x9
x3 ~ eta1 + x2
x4 ~ x2 + x5
x7 ~ x8
