eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x8 ~ x10 + x6
x3 ~ x8
x2 ~ x1
eta1 ~ x2
x10 ~ eta1
x4 ~ eta2 + x6 + x7
x9 ~ x7
x5 ~ eta2
