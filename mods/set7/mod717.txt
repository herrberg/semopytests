eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x7 ~ x2 + x6 + x9
x5 ~ x4 + x7
eta1 ~ eta2
x9 ~ eta1
x8 ~ x9
x1 ~ x8
x10 ~ x4
x3 ~ x2
