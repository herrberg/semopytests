eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x2 ~ x1 + x9
x3 ~ x10 + x2 + x5
x6 ~ x3
x4 ~ x3
x7 ~ x3
x9 ~ eta1
eta2 ~ x2
x8 ~ x1
