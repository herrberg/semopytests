eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x10 ~ eta2 + x4
x7 ~ x10
x6 ~ eta2
x2 ~ eta2 + x3
x5 ~ x4
x9 ~ eta2
x8 ~ x9
eta1 ~ x4
x3 ~ x1
