eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x6 ~ x3 + x5
eta2 ~ x6
eta1 ~ eta2
x4 ~ eta1
x10 ~ x4
x1 ~ x10
x8 ~ x6
x2 ~ x5
x9 ~ eta2
x7 ~ eta1
