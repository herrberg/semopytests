eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x3 ~ eta1 + x6 + x9
x10 ~ x3
x9 ~ x2
x5 ~ x2 + x4
x8 ~ x2
x7 ~ x9
x1 ~ x7
eta2 ~ x6
