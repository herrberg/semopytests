eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta1 ~ x5
x3 ~ eta1
x9 ~ x5
x4 ~ x9
x7 ~ x5
eta2 ~ x7
x6 ~ x2 + x5
x10 ~ eta1 + x8
x1 ~ x8
