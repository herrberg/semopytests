eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x2 ~ x10 + x3 + x4 + x6 + x8
eta1 ~ eta2
x10 ~ eta1
x9 ~ x3
x5 ~ x6
x7 ~ x6
x1 ~ x3
