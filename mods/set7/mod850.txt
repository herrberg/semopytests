eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x2 ~ x9
x6 ~ eta2 + x2
x5 ~ x6 + x7
x3 ~ x5
eta1 ~ x2
x1 ~ x2 + x4
x10 ~ x1
x8 ~ x2
