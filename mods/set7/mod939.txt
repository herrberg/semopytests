eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x5 ~ x3 + x8
x6 ~ x2 + x5
x1 ~ x5
x10 ~ eta2 + x5
x4 ~ x10
eta1 ~ x5 + x9
x2 ~ x7
