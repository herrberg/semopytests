eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x8 ~ x4
x5 ~ x10 + x8
x10 ~ x6
x9 ~ x10
eta1 ~ x8
x7 ~ x2 + x6
eta2 ~ x10
x1 ~ x4
x3 ~ x10
