eta1 =~ y1 + y2
eta2 =~ y3 + y4
x2 ~ eta1 + x6
x5 ~ x2 + x3
x1 ~ x5
x7 ~ x2 + x9
eta2 ~ x7
x8 ~ x6
x4 ~ x6
x10 ~ x6
