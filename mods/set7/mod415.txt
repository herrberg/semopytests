eta1 =~ y1 + y2
eta2 =~ y3 + y4
x10 ~ eta1 + x5
x6 ~ x10 + x2
x7 ~ x10 + x8
eta2 ~ x7
x1 ~ x10
x9 ~ x2
x4 ~ x9
x3 ~ x2
