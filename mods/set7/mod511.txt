eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x5 ~ x6
x4 ~ x5
x7 ~ x3 + x4
x10 ~ x2 + x7
x8 ~ x7
eta2 ~ x8
x2 ~ eta1 + x1
x9 ~ x7
