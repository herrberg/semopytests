eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta2 ~ eta1 + x10 + x2 + x5 + x8
x6 ~ eta2
x3 ~ x6
x4 ~ eta2 + x1
x7 ~ x8
x9 ~ x2
