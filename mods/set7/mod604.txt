eta1 =~ y1 + y2
eta2 =~ y3 + y4
x2 ~ eta2 + x10 + x5
x6 ~ x2
eta1 ~ x2
x4 ~ eta1 + x8
x7 ~ x4
x1 ~ eta1
x9 ~ eta2
x3 ~ eta2
