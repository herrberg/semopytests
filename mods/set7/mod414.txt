eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta2 ~ eta1 + x1 + x10
x7 ~ eta2 + x6
x3 ~ x2 + x7
x4 ~ x3
x8 ~ x4 + x5
x9 ~ x3
