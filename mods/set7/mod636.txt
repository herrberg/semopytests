eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x8 ~ x7
x2 ~ x8
x10 ~ x2
eta1 ~ x10
x4 ~ eta1
x9 ~ eta1 + eta2
x3 ~ x1 + x9
x6 ~ x10
x5 ~ x6
