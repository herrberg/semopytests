eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x9 ~ x6 + x8
x3 ~ x9
x10 ~ x3 + x5
x2 ~ eta1
x8 ~ x1 + x2
x7 ~ x9
x4 ~ x1
eta2 ~ x2
