eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x8 ~ x3 + x5
x2 ~ eta1 + eta2 + x5
eta2 ~ x1
x6 ~ x1
x9 ~ x5
x7 ~ x9
x10 ~ x1
x4 ~ x3
