eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x1 ~ eta1 + x10 + x2
eta2 ~ x1
x10 ~ x5
x9 ~ x1
x4 ~ x9
x7 ~ x1
x8 ~ x7
x3 ~ eta1
x6 ~ eta1
