eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x6 ~ x5
x7 ~ x1 + x6
x9 ~ x7
x8 ~ eta2 + x6
x1 ~ x3
x10 ~ x7
x4 ~ eta2
eta1 ~ x1
x2 ~ eta2
