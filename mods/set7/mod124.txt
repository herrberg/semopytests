eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta2 ~ x2 + x8
x1 ~ eta2
x4 ~ x1
x6 ~ x4 + x7
eta1 ~ x2
x3 ~ eta2 + x10
x5 ~ x8
x9 ~ x7
