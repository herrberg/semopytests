eta1 =~ y1 + y2
eta2 =~ y3 + y4
x4 ~ x3 + x6
x10 ~ x4
x7 ~ x10
eta2 ~ x2
x6 ~ eta2
x5 ~ eta1 + x3 + x9
x1 ~ eta2
x8 ~ x4
