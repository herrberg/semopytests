eta1 =~ y1 + y2
eta2 =~ y3 + y4
x10 ~ eta2 + x1
x4 ~ x10
x6 ~ x4
eta2 ~ x3
eta1 ~ x10 + x2
x9 ~ eta1
x7 ~ x9
x8 ~ eta2
x5 ~ x10
