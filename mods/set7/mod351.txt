eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x5 ~ x1 + x4
eta2 ~ x5
x2 ~ eta2
x3 ~ x1
x6 ~ x3
x8 ~ eta1 + x10 + x5
x9 ~ eta2
x4 ~ x7
