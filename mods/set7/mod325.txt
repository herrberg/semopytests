eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x9 ~ x3
x8 ~ x9
x10 ~ x6 + x8
x1 ~ x10 + x4
x7 ~ x1
x5 ~ x8
eta2 ~ x5
x2 ~ x6
eta1 ~ x2
