eta1 =~ y1 + y2
eta2 =~ y3 + y4
x3 ~ x5
eta2 ~ x3 + x7
x9 ~ eta2
x4 ~ x2 + x3 + x6
eta1 ~ x3 + x8
x1 ~ x3
x10 ~ x3
