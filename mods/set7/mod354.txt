eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x9 ~ eta1 + x4
x3 ~ x10 + x9
x7 ~ x9
x2 ~ x5
x6 ~ x2
x10 ~ x6
eta2 ~ x2
x8 ~ x10
x1 ~ eta1
