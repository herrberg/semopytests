eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x3 ~ x8
x6 ~ x3
x9 ~ x6
x1 ~ x6
x4 ~ x3
eta1 ~ x8
x7 ~ eta1
x2 ~ x3
x5 ~ x2
x10 ~ x6
eta2 ~ x8
