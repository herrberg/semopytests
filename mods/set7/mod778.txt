eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta1 ~ x8
x3 ~ eta1
eta2 ~ x3 + x7 + x9
x2 ~ eta2
x7 ~ x1 + x5
x4 ~ x3
x10 ~ x4
x6 ~ eta2
