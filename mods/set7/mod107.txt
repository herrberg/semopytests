eta1 =~ y1 + y2
eta2 =~ y3 + y4
x7 ~ x1
x3 ~ x2 + x7
x5 ~ eta1 + x1
x4 ~ x5
x10 ~ x7
eta2 ~ x10
x9 ~ x5
x8 ~ x5
x6 ~ x1
