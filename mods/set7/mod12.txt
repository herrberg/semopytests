eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x6 ~ eta1
x3 ~ x6
eta2 ~ x3
x9 ~ eta2
x2 ~ x9
x5 ~ x3
x8 ~ x3
x10 ~ x6
x1 ~ x6
x7 ~ x1
x4 ~ x7
