eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x3 ~ x2
x6 ~ x3
x4 ~ eta2 + x6
x1 ~ x10 + x4 + x9
x8 ~ x6
x5 ~ x2
eta1 ~ x2
x7 ~ x3
