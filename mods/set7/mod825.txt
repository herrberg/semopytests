eta1 =~ y1 + y2
eta2 =~ y3 + y4
x1 ~ x6 + x8
x3 ~ x6
x10 ~ x3
x5 ~ eta2 + x6
x7 ~ x5
eta2 ~ x2
x9 ~ eta1 + x6
x4 ~ eta2
