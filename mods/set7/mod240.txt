eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x7 ~ eta1 + x2 + x5
eta2 ~ x10
x2 ~ eta2
x6 ~ x5
x1 ~ x6
x8 ~ eta1
x3 ~ x6
x4 ~ eta1
x9 ~ x6
