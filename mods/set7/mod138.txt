eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x7 ~ x1
x10 ~ x7
x5 ~ x10
x9 ~ x5 + x6
x8 ~ eta1 + x7
x2 ~ x8
eta2 ~ x2 + x4
x3 ~ x10
