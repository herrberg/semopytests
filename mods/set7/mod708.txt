eta1 =~ y1 + y2
eta2 =~ y3 + y4
x8 ~ x9
x2 ~ x8
eta2 ~ x2
eta1 ~ eta2 + x7
x1 ~ eta2
x4 ~ x1
x3 ~ x4
x6 ~ x2
x7 ~ x5
x10 ~ x7
