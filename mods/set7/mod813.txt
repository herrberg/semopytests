eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x10 ~ x4 + x8
x3 ~ x10
x2 ~ x10
x6 ~ eta2 + x2
x5 ~ x10 + x7
x9 ~ x5
eta1 ~ x8
x1 ~ x4
