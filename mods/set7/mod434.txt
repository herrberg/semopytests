eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x7 ~ eta1 + x8
x6 ~ x7
x9 ~ eta2 + x2 + x5 + x6
x10 ~ x6
x1 ~ x6
x3 ~ x2 + x4
