eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x8 ~ x5
x9 ~ x4 + x8
x2 ~ x1 + x9
x6 ~ x5
eta1 ~ x6
x7 ~ eta1
x4 ~ x3
eta2 ~ eta1
x10 ~ eta1
