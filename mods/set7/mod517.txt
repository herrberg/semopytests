eta1 =~ y1 + y2
eta2 =~ y3 + y4
x1 ~ eta2 + x10 + x8
x2 ~ x1 + x6 + x7
x9 ~ x1
x4 ~ x10
x8 ~ x5
eta1 ~ x6
x3 ~ x1
