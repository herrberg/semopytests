eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x4 ~ x7
x5 ~ eta1 + x4
x9 ~ eta2 + x1 + x5
x3 ~ x2 + x6
eta2 ~ x3
x8 ~ x10
eta1 ~ x8
