eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x2 ~ x6
x10 ~ x5 + x6
eta1 ~ x10
x4 ~ eta1
x7 ~ x6
eta2 ~ x7
x9 ~ x10
x8 ~ x1 + x6
x3 ~ x5
