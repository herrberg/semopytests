eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x4 ~ x8
eta1 ~ x4
x10 ~ eta1 + x3 + x6
x5 ~ x10
x7 ~ x4
x2 ~ eta2 + x7
x1 ~ x4 + x9
