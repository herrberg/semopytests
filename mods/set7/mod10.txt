eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x10 ~ eta2
x2 ~ x10 + x3
x4 ~ x2
x7 ~ x4
x6 ~ x7
x1 ~ x4
x8 ~ x10
x9 ~ eta2 + x5
eta1 ~ x9
