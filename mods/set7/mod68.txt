eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x9 ~ x2
x7 ~ x9
x5 ~ eta1 + x7
x3 ~ x1 + x9
eta2 ~ x3
x8 ~ x3
x4 ~ x8
x6 ~ x3
x10 ~ x3
