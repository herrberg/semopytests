eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x4 ~ x2 + x7 + x9
eta1 ~ x4 + x8
x10 ~ eta1
x6 ~ x10
eta2 ~ x8
x3 ~ eta2
x1 ~ eta2
x7 ~ x5
