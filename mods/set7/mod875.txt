eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x4 ~ eta1 + x10
x6 ~ x4
x7 ~ x1 + x6
x3 ~ x7
eta1 ~ eta2
x2 ~ x4
x5 ~ eta1
x9 ~ x5
x8 ~ x6
