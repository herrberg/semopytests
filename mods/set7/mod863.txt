eta1 =~ y1 + y2
eta2 =~ y3 + y4
x8 ~ eta2 + x2 + x6
x9 ~ x8
x7 ~ x9
eta1 ~ x1 + x8
x10 ~ x5
x1 ~ x10
x3 ~ x4
x2 ~ x3
