eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x1 ~ eta1 + x7 + x8
x3 ~ eta2 + x1
x2 ~ x3
x6 ~ x10 + x2
x5 ~ x8
x4 ~ eta1
x9 ~ x10
