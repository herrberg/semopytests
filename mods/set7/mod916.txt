eta1 =~ y1 + y2
eta2 =~ y3 + y4
x3 ~ x6
x7 ~ eta1 + x2 + x3
eta1 ~ x4
x10 ~ eta1 + eta2 + x5
x5 ~ x1
x8 ~ x1
x9 ~ x5
