eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x9 ~ x1
x5 ~ x4 + x8 + x9
x3 ~ eta2
x4 ~ x3
x7 ~ x10 + x4
x2 ~ eta1 + x4
x6 ~ x8
