eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta1 ~ x1 + x2
x6 ~ eta1
x10 ~ x6
x3 ~ x10
x4 ~ x3 + x9
x9 ~ x7
x5 ~ x9
x8 ~ eta1
eta2 ~ x3
