eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x8 ~ x1 + x5
eta2 ~ eta1 + x8
x9 ~ eta2 + x10
x2 ~ x9
x7 ~ x9
x10 ~ x4
x3 ~ x8
x6 ~ x4
