eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x4 ~ x2 + x3 + x5 + x6 + x8
x1 ~ x4
eta2 ~ x1
eta1 ~ eta2
x10 ~ x4
x7 ~ x8
x9 ~ x2
