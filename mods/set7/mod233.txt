eta1 =~ y1 + y2
eta2 =~ y3 + y4
x5 ~ x3
x10 ~ x5 + x6 + x7 + x9
eta1 ~ x10
x6 ~ x4 + x8
eta2 ~ x4
x9 ~ x1
x2 ~ x3
