eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta2 ~ x3
x1 ~ eta2 + x4 + x6
x9 ~ x1
x2 ~ x1
x8 ~ x2
x5 ~ x1
x7 ~ x5
x10 ~ x1
eta1 ~ x2
