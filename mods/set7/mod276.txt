eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x9 ~ x6
x8 ~ x1 + x9
eta1 ~ x8
eta2 ~ x5 + x6
x1 ~ x2
x3 ~ x1 + x10 + x7
x4 ~ x6
