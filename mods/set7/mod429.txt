eta1 =~ y1 + y2
eta2 =~ y3 + y4
x4 ~ eta2 + x8
x9 ~ x4
x7 ~ x1 + x9
x3 ~ x4
x5 ~ x8
x1 ~ eta1 + x10
x2 ~ x9
x6 ~ x9
