eta1 =~ y1 + y2
eta2 =~ y3 + y4
x7 ~ x4 + x8
x6 ~ x4
x5 ~ x6
x2 ~ x5
eta2 ~ x2
x1 ~ eta2
eta1 ~ x4 + x9
x8 ~ x10
x3 ~ x6
