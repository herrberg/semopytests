eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x10 ~ x4
eta2 ~ x10 + x3 + x7
eta1 ~ eta2 + x5 + x6
x1 ~ eta1
x9 ~ eta2
x8 ~ x7
x2 ~ eta2
