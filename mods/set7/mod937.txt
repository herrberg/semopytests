eta1 =~ y1 + y2
eta2 =~ y3 + y4
x4 ~ x10 + x7 + x9
x1 ~ x4
x3 ~ eta2 + x1 + x2
x8 ~ x3
eta1 ~ x5
x7 ~ eta1
x6 ~ x9
