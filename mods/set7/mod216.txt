eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x10 ~ eta1 + x1 + x4
eta1 ~ x9
x5 ~ x1
x2 ~ x5
x3 ~ x2
eta2 ~ x3
x7 ~ x4
x6 ~ x7
x8 ~ x4
