eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta2 ~ x3 + x5
x1 ~ eta1
x5 ~ x1 + x2 + x4
