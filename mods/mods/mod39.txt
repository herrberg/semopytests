eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta1 ~ x1
x3 ~ eta1 + x4
eta2 ~ x3
x5 ~ eta2
x2 ~ eta2
