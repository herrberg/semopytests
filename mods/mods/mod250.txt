eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x4 ~ x5
eta2 ~ x4
x3 ~ eta1 + eta2
x1 ~ x3
eta1 ~ x2
