eta1 =~ y1 + y2 + y3
eta1 ~ x1 + x6
x5 ~ eta1
x2 ~ x1 + x4
x4 ~ x7
x3 ~ eta1
