eta1 =~ y1 + y2 + y3
x8 ~ eta1
x2 ~ x8 + x9
x1 ~ x9
x3 ~ x5 + x9
x6 ~ x8
x7 ~ eta1
x4 ~ x5
