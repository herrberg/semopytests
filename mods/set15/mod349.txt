eta1 =~ y1 + y2 + y3 + y4 + y5
x1 ~ x2 + x3
x6 ~ x1
eta1 ~ x2 + x4
x5 ~ x1
