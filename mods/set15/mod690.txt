eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y4 + y5 + y6 + y7 + y8
eta3 =~ y10 + y11 + y12 + y13 + y14
eta3 ~ eta1 + x2
x3 ~ eta1
eta2 ~ eta1 + x1
