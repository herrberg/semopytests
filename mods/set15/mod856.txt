eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y6 + y7 + y8 + y9
eta3 =~ y10 + y11 + y12 + y13 + y9
x1 ~ eta1 + eta2 + x2
x3 ~ x1
x2 ~ x3
eta3 ~ x3
