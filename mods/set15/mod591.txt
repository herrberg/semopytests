eta1 =~ y1 + y2 + y3 + y4
x5 ~ x10 + x3
x3 ~ eta1 + x8
x7 ~ x1 + x10 + x4 + x6
x2 ~ x3
x9 ~ x1
