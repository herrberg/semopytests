eta1 =~ y1 + y2 + y3 + y4 + y5
x4 ~ eta1 + x2
x9 ~ x4 + x7
x2 ~ x9
x7 ~ x5
x8 ~ x4
x3 ~ x8
x1 ~ x7
x6 ~ x9
