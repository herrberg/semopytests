eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
x3 ~ eta3 + x4
x1 ~ x3 + x5
x6 ~ x1
x2 ~ x6
eta1 ~ eta3
eta2 ~ eta1
