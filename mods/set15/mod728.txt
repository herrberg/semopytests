eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y11 + y8 + y9
eta5 =~ y13 + y14 + y15 + y4
x6 ~ x5
eta1 ~ eta4 + x6
eta5 ~ x4 + x5
eta2 ~ eta5
x3 ~ x1 + x5
x2 ~ eta3 + x6
