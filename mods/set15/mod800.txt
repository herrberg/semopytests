eta1 =~ y1 + y2 + y3
eta2 =~ y13 + y4 + y5 + y6 + y7
eta3 =~ y10 + y11 + y9
eta4 =~ y12 + y13
x4 ~ x1
x5 ~ x10 + x4
eta1 ~ x2 + x5
x9 ~ x1
x8 ~ eta2 + x10 + x3
eta3 ~ x1
x6 ~ eta3 + x7
eta4 ~ x2
