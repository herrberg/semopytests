eta1 =~ y1 + y2 + y3 + y4
x4 ~ x5 + x6
x8 ~ x4
x6 ~ x3 + x8
x1 ~ eta1 + x6
x2 ~ x3
x7 ~ x8
