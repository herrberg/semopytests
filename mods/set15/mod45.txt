eta1 =~ y1 + y2 + y3 + y4 + y5
eta2 =~ y6 + y7 + y8
eta3 =~ y11 + y12 + y6 + y9
eta4 =~ y13 + y14 + y16 + y21
eta5 =~ y17 + y18 + y19 + y20 + y21
x3 ~ x10 + x7
eta2 ~ x3
eta4 ~ eta2 + x5 + x8
x1 ~ eta4
eta3 ~ eta1 + x5
x6 ~ eta3 + eta5
x5 ~ x6
x11 ~ x5
x9 ~ x6
x4 ~ x3
x8 ~ x2
