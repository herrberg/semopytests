eta1 =~ y1 + y2 + y3 + y4 + y8
eta2 =~ y6 + y7 + y8
eta3 =~ y10 + y11 + y12 + y9
x5 ~ x1 + x4
eta1 ~ x5
eta3 ~ eta2 + x4
x3 ~ x1
x2 ~ x4
