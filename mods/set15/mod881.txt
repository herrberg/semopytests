eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6
eta3 =~ y10 + y13 + y7 + y9
eta4 =~ y11 + y12
eta5 =~ y13 + y14 + y15
eta4 ~ eta5 + x4 + x7
eta3 ~ eta4 + x3
eta2 ~ eta4 + x2 + x6
eta5 ~ eta2
x8 ~ x2
x1 ~ x6
x5 ~ eta1 + x4
