eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6 + y7
eta3 =~ y8 + y9
eta4 =~ y11 + y12 + y8
eta5 =~ y13 + y14 + y15 + y16 + y17
eta3 ~ eta1 + eta2 + x11
x6 ~ eta3
eta4 ~ eta5
eta1 ~ eta4 + x1
x4 ~ eta2
x8 ~ eta3 + x3 + x7
x2 ~ eta3
x10 ~ x2
x5 ~ eta4
x9 ~ eta3
