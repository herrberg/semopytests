eta1 =~ y1 + y16 + y2 + y3
eta2 =~ y5 + y6 + y7 + y8 + y9
eta3 =~ y10 + y11 + y12 + y13
eta4 =~ y14 + y15 + y16 + y17
x1 ~ x2 + x3
eta2 ~ x1
eta1 ~ x3
eta3 ~ x2
eta4 ~ x2
