eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5 + y6 + y7
x2 ~ x3
x9 ~ x2
x1 ~ x9
x4 ~ x1 + x8
x6 ~ x2
x7 ~ x9
x10 ~ x1
x5 ~ x1
eta2 ~ x5
eta1 ~ eta2
