eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
x11 ~ x6 + x7
x10 ~ eta3 + x11 + x8
x4 ~ x10
x9 ~ x4
x2 ~ x3 + x7
eta1 ~ x2
eta2 ~ x1
x3 ~ eta2
x5 ~ x6
