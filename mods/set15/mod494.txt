eta1 =~ y1 + y12 + y3
eta2 =~ y4 + y5 + y6 + y7
eta3 =~ y10 + y11 + y12 + y8 + y9
eta4 =~ y13 + y14 + y15 + y16
x3 ~ x6
x9 ~ x3
x8 ~ x9
eta1 ~ x10 + x3
x1 ~ x2 + x9
eta2 ~ x1
eta4 ~ x3 + x7
eta3 ~ eta4
x4 ~ x2
x5 ~ x4
