eta1 =~ y1 + y2 + y3 + y4 + y5
eta2 =~ y6 + y7 + y8 + y9
eta3 =~ y10 + y11 + y12
eta4 =~ y12 + y13 + y15
x4 ~ x3
x2 ~ x4
eta2 ~ x1 + x2
eta1 ~ x2
eta3 ~ eta1
eta4 ~ x2
