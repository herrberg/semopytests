eta1 =~ y1 + y2
x5 ~ x2
x7 ~ x5
x8 ~ x7
x3 ~ x4 + x8
x9 ~ x2 + x6
eta1 ~ x7
x1 ~ x6
