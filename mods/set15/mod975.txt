eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6
eta3 =~ y6 + y7 + y9
eta4 =~ y10 + y11 + y12 + y13 + y14
eta3 ~ eta1 + x2
eta4 ~ eta2 + eta3 + x1
