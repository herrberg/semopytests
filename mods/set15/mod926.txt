eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6
eta3 =~ y11 + y7
eta4 =~ y10 + y9
eta5 =~ y11 + y12
x5 ~ eta4 + x1
eta5 ~ x5
eta1 ~ x5 + x6
x1 ~ eta1
eta2 ~ x5
x7 ~ eta2 + x10
x4 ~ eta4
eta3 ~ eta4 + x3
x6 ~ x2
x10 ~ x9
x8 ~ x6
