eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta2 ~ eta3
x8 ~ eta2
x9 ~ x8
x7 ~ eta1 + x9
x4 ~ x10 + x7
eta4 ~ x1 + x4 + x6
x5 ~ x1
x2 ~ x7
x3 ~ x7
