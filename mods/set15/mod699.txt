eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5 + y6
eta3 =~ y10 + y11 + y3 + y7 + y8
eta4 =~ y12 + y13 + y14
x2 ~ eta3 + x6
eta1 ~ x2
x9 ~ eta1 + eta4
x5 ~ x4 + x9
x1 ~ x2 + x8
eta2 ~ x6
x3 ~ x8
x7 ~ x4
