eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6 + y7 + y8 + y9
eta3 =~ y11 + y12 + y13 + y9
x7 ~ eta1 + eta2
x2 ~ x5 + x7
eta2 ~ x2
eta3 ~ eta2
x1 ~ x5
x4 ~ x1
x8 ~ x5
x6 ~ x8
x3 ~ x5
