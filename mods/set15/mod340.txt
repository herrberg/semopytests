eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6 + y7
eta3 =~ y8 + y9
x5 ~ x2
x6 ~ x5
eta1 ~ eta2 + x6
x3 ~ x5
x1 ~ x3
x4 ~ x2
eta3 ~ x6
