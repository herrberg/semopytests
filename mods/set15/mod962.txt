eta1 =~ y1 + y2 + y3 + y4
x11 ~ eta1 + x3 + x6
x10 ~ x4
x3 ~ x10
x9 ~ eta1 + x1 + x5
x8 ~ x7
x1 ~ x8
x2 ~ x4
