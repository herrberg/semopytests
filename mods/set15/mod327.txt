eta1 =~ y1 + y2
eta2 =~ y1 + y3 + y4 + y6 + y7
eta3 =~ y10 + y11 + y12 + y8 + y9
x1 ~ eta2
eta3 ~ x1 + x3
x4 ~ eta2 + x5
x3 ~ eta1
x2 ~ eta2
