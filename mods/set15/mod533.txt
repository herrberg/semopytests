eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6
eta3 =~ y10 + y7 + y8 + y9
eta4 =~ y11 + y12 + y13
eta5 =~ y11 + y14 + y15 + y16 + y17
x5 ~ eta4 + x8
x1 ~ eta4
x6 ~ eta2
x8 ~ x10 + x6
x3 ~ x8
x7 ~ x3
x4 ~ eta1 + x3
x2 ~ x8
x9 ~ x10
eta5 ~ x6
eta3 ~ x6
