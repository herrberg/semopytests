eta1 =~ y1 + y2 + y3 + y4 + y5
eta2 =~ y6 + y7 + y8
eta3 =~ y11 + y14 + y9
eta4 =~ y12 + y13 + y14
x2 ~ eta3
eta1 ~ eta3
x1 ~ eta3
eta2 ~ eta3
eta4 ~ eta2
