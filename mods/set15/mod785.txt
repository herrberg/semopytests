eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6 + y7
eta3 =~ y8 + y9
eta2 ~ x1 + x9
eta3 ~ eta2 + x3
x2 ~ eta3
x3 ~ x6
x5 ~ eta2 + x10 + x4
x1 ~ x5
x10 ~ x7
eta1 ~ x10
x8 ~ eta1
x11 ~ eta2
