eta1 =~ y1 + y3 + y4 + y5 + y6
eta2 =~ y6 + y7 + y8
eta3 =~ y10 + y11 + y9
eta4 =~ y11 + y12 + y13 + y15 + y16
eta5 =~ y17 + y18 + y19 + y20
x7 ~ eta3 + x1
eta2 ~ eta1 + x4 + x7
eta4 ~ eta2 + x5
x2 ~ x7
eta3 ~ x2
x3 ~ eta3
eta5 ~ x5
x6 ~ x2
