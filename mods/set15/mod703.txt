eta1 =~ y1 + y2
x1 ~ x2 + x7
x4 ~ eta1 + x2
x5 ~ eta1
x6 ~ x5
x3 ~ x6
