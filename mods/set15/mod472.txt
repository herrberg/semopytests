eta1 =~ y1 + y2 + y3 + y4
x4 ~ x7
x8 ~ x4
x5 ~ x8
x2 ~ x5
x3 ~ x1 + x2
x6 ~ eta1 + x2
eta1 ~ x9
