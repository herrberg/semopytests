eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5 + y6 + y7
eta3 =~ y10 + y8 + y9
eta4 =~ y11 + y12 + y13 + y14 + y15
eta5 =~ y16 + y17 + y18 + y9
eta1 ~ x2
eta3 ~ eta1 + eta2 + eta5 + x1
eta4 ~ eta3 + x8
x1 ~ eta4 + x6
x3 ~ eta4
x5 ~ eta4
x4 ~ eta2
x7 ~ eta2
