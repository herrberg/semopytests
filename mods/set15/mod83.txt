eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6 + y7 + y8
eta3 =~ y10 + y9
eta4 =~ y11 + y12 + y13
eta5 =~ y14 + y16 + y17 + y18 + y6
x7 ~ eta5
x5 ~ eta1 + eta5 + x3 + x4
eta1 ~ x2
eta3 ~ x6
x4 ~ eta3
eta2 ~ eta4 + eta5
x1 ~ eta2
x8 ~ x2
