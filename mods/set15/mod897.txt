eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y2 + y9
eta5 =~ y11 + y12
eta4 ~ x2 + x3
eta1 ~ eta4
x3 ~ eta1
eta5 ~ eta1 + eta3
x1 ~ eta4
eta2 ~ eta4
