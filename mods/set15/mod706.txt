eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y10 + y13 + y7 + y9
eta4 =~ y11 + y12 + y13 + y14 + y15
x7 ~ x8
x1 ~ eta4 + x7
x5 ~ x1
eta1 ~ x5
eta3 ~ eta1
x2 ~ x1
x3 ~ x1
eta2 ~ x3
x4 ~ x8
x6 ~ x1
