eta1 =~ y1 + y2 + y3 + y4 + y5
eta2 =~ y6 + y7 + y8
x2 ~ eta1 + x1
x5 ~ x2
x4 ~ eta1
x3 ~ eta1 + eta2
eta2 ~ x6
