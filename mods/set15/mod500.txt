eta1 =~ y1 + y2
eta1 ~ x4 + x5 + x6 + x8
x9 ~ eta1
x2 ~ x7 + x9
x3 ~ x9
x1 ~ x9
