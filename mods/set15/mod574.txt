eta1 =~ y1 + y2
eta2 =~ y4 + y6
eta3 =~ y5 + y6 + y7 + y8 + y9
eta4 =~ y10 + y11 + y12 + y13 + y14
eta4 ~ eta2 + x2
eta1 ~ eta4
eta3 ~ eta1
x1 ~ x2 + x3
