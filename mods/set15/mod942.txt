eta1 =~ y1 + y4
eta2 =~ y3 + y4 + y5 + y6 + y7
eta3 =~ y10 + y8 + y9
eta4 =~ y11 + y12
x2 ~ x1
eta2 ~ x2
eta1 ~ eta2
x4 ~ eta1 + x3
eta4 ~ x4
eta3 ~ x1
