eta1 =~ y2 + y3 + y6
eta2 =~ y4 + y5 + y6 + y7 + y8
eta3 =~ y10 + y11 + y12 + y13 + y9
eta3 ~ x3
eta1 ~ eta3 + x2
x5 ~ x3 + x6
x2 ~ x1
x4 ~ x2
eta2 ~ x2
