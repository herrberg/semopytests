eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y13 + y6 + y7 + y8 + y9
eta3 =~ y10 + y11
eta4 =~ y12 + y13 + y14 + y15
eta3 ~ eta4
x1 ~ eta4
eta1 ~ eta2 + eta4
x2 ~ eta4
