eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7 + y8
eta4 =~ y10 + y11 + y12 + y13 + y9
eta5 =~ y15 + y2
eta4 ~ eta1 + eta2 + eta3 + eta5 + x1
x2 ~ eta5
