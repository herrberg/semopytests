eta1 =~ y1 + y2 + y3 + y4 + y5
eta2 =~ y6 + y7 + y8 + y9
x1 ~ eta2 + x3
x2 ~ x1
eta1 ~ x2
