eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6
eta1 ~ x1 + x2 + x3
eta2 ~ eta1
x4 ~ eta2
