eta1 =~ y1 + y2 + y3 + y4 + y5
eta1 ~ x4 + x5
x1 ~ eta1 + x2 + x7
x6 ~ eta1
x3 ~ eta1
