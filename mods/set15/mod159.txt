eta1 =~ y1 + y2 + y3 + y4
x6 ~ x7
x2 ~ x1 + x6
x4 ~ x2
x5 ~ x6
x1 ~ eta1
x3 ~ x6
