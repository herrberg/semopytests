eta1 =~ y2 + y3 + y8
eta2 =~ y4 + y5 + y6 + y7 + y8
eta3 =~ y10 + y9
eta4 =~ y11 + y12 + y13
eta2 ~ eta1 + x1 + x2
eta3 ~ x2
eta4 ~ eta1
