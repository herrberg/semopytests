eta1 =~ y1 + y2 + y3
eta2 =~ y11 + y4 + y6
eta3 =~ y10 + y11 + y7 + y8 + y9
eta4 =~ y12 + y13 + y14 + y15 + y16
x1 ~ eta1
eta4 ~ eta3 + x1
x2 ~ eta1 + eta2
