eta1 =~ y10 + y22 + y3 + y4
eta2 =~ y5 + y6 + y7 + y8
eta3 =~ y10 + y11 + y12 + y13 + y9
eta4 =~ y14 + y15 + y16 + y17 + y18
eta5 =~ y19 + y20 + y21 + y22
x4 ~ x5 + x6
x7 ~ x4
x2 ~ eta4 + x1 + x7
x8 ~ x2
x1 ~ eta2 + x3
eta3 ~ x2
eta1 ~ eta5 + x7
x5 ~ x2
