eta1 =~ y15 + y2 + y3
eta2 =~ y4 + y5 + y6 + y7 + y8
eta3 =~ y10 + y11 + y12 + y13 + y9
eta4 =~ y14 + y15 + y16 + y17 + y18
eta5 =~ y17 + y19 + y21 + y22
eta1 ~ eta4 + x3
eta3 ~ eta1
x1 ~ eta4 + eta5
eta5 ~ x2 + x4
eta2 ~ eta5
x4 ~ eta2
