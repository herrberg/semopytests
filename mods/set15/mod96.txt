eta1 =~ y1 + y2 + y3 + y4 + y5
x2 ~ x4
eta1 ~ x2
x3 ~ x4
x1 ~ x2
