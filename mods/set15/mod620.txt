eta1 =~ y1 + y2 + y3 + y4
x1 ~ x2 + x3
eta1 ~ x3
