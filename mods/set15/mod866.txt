eta1 =~ y1 + y2 + y3 + y4 + y5
eta2 =~ y6 + y7 + y8
eta2 ~ x6 + x7
x1 ~ eta2
x4 ~ x1
x6 ~ x4
x3 ~ x6
x5 ~ eta1 + x3
x2 ~ eta2
x8 ~ x2
