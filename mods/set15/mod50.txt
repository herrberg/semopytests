eta1 =~ y1 + y10 + y2 + y3 + y4
eta2 =~ y6 + y7
eta3 =~ y10 + y11 + y8 + y9
x2 ~ eta3 + x1
eta2 ~ eta1 + x2
eta3 ~ eta2
x3 ~ eta3
