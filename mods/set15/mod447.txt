eta1 =~ y1 + y2
x1 ~ x5
x3 ~ x1
x2 ~ x3 + x6
eta1 ~ x1
x4 ~ x3
