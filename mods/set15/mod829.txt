eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y6 + y7 + y9
eta3 =~ y8 + y9
eta4 =~ y10 + y11 + y12 + y13
x3 ~ eta1
x6 ~ x3
x1 ~ x6 + x7
x8 ~ x1
x7 ~ x2 + x4
x5 ~ eta3 + x7
x4 ~ eta2 + x5
eta4 ~ x4
