eta1 =~ y10 + y2 + y3 + y4 + y5
eta2 =~ y6 + y7 + y8 + y9
eta3 =~ y10 + y11 + y12 + y13 + y14
x4 ~ eta1 + x1
eta1 ~ x2 + x5
x3 ~ eta1
eta3 ~ x1
eta2 ~ x2
x6 ~ x1
