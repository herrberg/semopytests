eta1 =~ y1 + y11 + y3 + y4 + y5
eta2 =~ y6 + y7
eta3 =~ y10 + y11 + y12 + y8 + y9
x1 ~ eta3
x2 ~ eta1 + x1
eta2 ~ x1
