eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6
eta3 =~ y1 + y11 + y7 + y8 + y9
x5 ~ x2 + x4
eta1 ~ x5
eta3 ~ eta1
x4 ~ eta2
x1 ~ eta1
x3 ~ x2
