eta1 =~ y1 + y2 + y3
x3 ~ eta1
x1 ~ x2 + x3 + x5
x4 ~ x3
