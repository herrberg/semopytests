eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5 + y6 + y7
eta3 =~ y8 + y9
eta1 ~ x7 + x9
x10 ~ eta1 + eta3 + x4 + x5
x1 ~ x10
x3 ~ x1
x8 ~ eta1
x6 ~ x8
x2 ~ x9
eta2 ~ x7
