eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6 + y7 + y9
eta3 =~ y10 + y11 + y12 + y9
eta1 ~ x1 + x5
x3 ~ eta1 + x4
x2 ~ x3
x4 ~ x2
eta3 ~ eta1
x8 ~ x5
x6 ~ eta2 + x5
eta2 ~ x7
