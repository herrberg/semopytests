eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6 + y7 + y8
x5 ~ x4 + x6 + x9
x9 ~ x3 + x8
eta2 ~ x1 + x4
eta1 ~ eta2 + x7
x6 ~ x2
