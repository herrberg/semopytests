eta1 =~ y1 + y2
eta2 =~ y11 + y3 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13
eta2 ~ x2 + x5
eta4 ~ eta2
x2 ~ x6
x1 ~ x2
x4 ~ x1
eta5 ~ x2
eta3 ~ x1
x3 ~ eta3
eta1 ~ x6
