eta1 =~ y1 + y2 + y3 + y4
x7 ~ x1 + x5 + x8
x6 ~ x7
x8 ~ x6
x2 ~ x3 + x7
eta1 ~ x2
x4 ~ x1
