eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6 + y7
x3 ~ x1
eta2 ~ x2 + x3
eta1 ~ eta2
