eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5 + y6 + y7
x3 ~ x5
x8 ~ x3
x2 ~ x8
x1 ~ eta2 + x2
x6 ~ x1
eta1 ~ x6
x10 ~ eta2 + x9
x7 ~ x2
x4 ~ x3
