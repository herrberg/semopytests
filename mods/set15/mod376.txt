eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8 + y9
x2 ~ x1 + x3 + x4
eta1 ~ x2 + x5
x3 ~ eta1
x6 ~ eta3 + x1
eta2 ~ x6
