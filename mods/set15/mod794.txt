eta1 =~ y1 + y2
eta1 ~ x1
x2 ~ eta1
