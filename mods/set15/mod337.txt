eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6 + y7
x6 ~ eta1 + x3 + x7
x4 ~ x6
x8 ~ eta1
eta2 ~ eta1 + x2
x1 ~ eta2
x5 ~ eta1
