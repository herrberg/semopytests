eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7 + y8 + y9
eta4 =~ y11 + y12 + y4
x4 ~ eta2 + eta3 + eta4
eta1 ~ x4
x5 ~ eta3 + x3
x2 ~ x5
eta3 ~ x2
x1 ~ eta3
x6 ~ eta4
