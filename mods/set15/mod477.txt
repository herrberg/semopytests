eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5 + y6 + y7
x6 ~ x2 + x5
eta2 ~ x6
x2 ~ eta1 + eta2 + x3
eta1 ~ x1
x7 ~ x1 + x4
