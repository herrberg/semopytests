eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6 + y7 + y8 + y9
eta2 ~ x8
x5 ~ x3 + x7 + x8
x2 ~ x4 + x5
x1 ~ x4
x6 ~ x1
x9 ~ x4
eta1 ~ x5
