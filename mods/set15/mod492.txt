eta1 =~ y1 + y2 + y3 + y4 + y5
eta2 =~ y6 + y7 + y8
eta1 ~ x1 + x6
x7 ~ x1
x4 ~ eta2 + x5 + x7
x5 ~ x3
x2 ~ x7
