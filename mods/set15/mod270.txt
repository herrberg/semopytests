eta1 =~ y1 + y2 + y3 + y4 + y5
x2 ~ x1 + x8
x3 ~ x2
eta1 ~ x3
x7 ~ x2
x8 ~ x7
x5 ~ x1
x6 ~ x1
x4 ~ x2
