eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta5 =~ y11 + y6
eta4 ~ x4 + x6
eta1 ~ eta4
eta5 ~ eta1
x4 ~ x1
x3 ~ eta4
x5 ~ x4
eta3 ~ x1
eta2 ~ x4
x2 ~ x1
