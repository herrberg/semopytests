eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7 + y9
eta4 =~ y10 + y9
eta2 ~ x3
eta1 ~ eta2 + eta4 + x1
x2 ~ eta2
x1 ~ eta3
x4 ~ eta4
