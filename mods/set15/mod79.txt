eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y11 + y5 + y6
eta3 =~ y10 + y11 + y8 + y9
eta4 =~ y12 + y13
eta5 =~ y14 + y15
eta4 ~ eta2 + x2
x1 ~ eta4
x5 ~ x1 + x3
x4 ~ x5
x3 ~ eta3 + eta5
eta1 ~ x3
eta5 ~ eta1
