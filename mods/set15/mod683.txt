eta1 =~ y1 + y2 + y3 + y4 + y5
x4 ~ x3
x7 ~ x4
x1 ~ x4
eta1 ~ x3 + x5
x2 ~ x4
x6 ~ x3
