eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5 + y6 + y7
x1 ~ x2 + x3
eta1 ~ x1
x2 ~ eta1
x5 ~ x3 + x4
eta2 ~ eta1
