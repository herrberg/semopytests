eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6 + y7 + y8
x9 ~ eta2 + x6
x4 ~ x5 + x9
eta2 ~ x4 + x8
x1 ~ x7 + x9
x3 ~ x8
x10 ~ x3
x2 ~ x6
eta1 ~ x6
