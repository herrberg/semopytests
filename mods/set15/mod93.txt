eta1 =~ y1 + y8
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y12 + y9
x1 ~ eta1 + eta2 + eta3
eta4 ~ x1
x2 ~ eta2
