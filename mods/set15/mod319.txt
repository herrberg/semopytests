eta1 =~ y1 + y3 + y4 + y5 + y6
eta2 =~ y10 + y6 + y7 + y8 + y9
eta3 =~ y11 + y12 + y13
eta3 ~ x1 + x2
x3 ~ eta3 + x5 + x6
x2 ~ eta2 + x3 + x9
eta1 ~ eta3
x8 ~ eta2 + x7
x4 ~ eta3
