eta1 =~ y1 + y2 + y3 + y4 + y5
eta2 =~ y6 + y7 + y8 + y9
eta3 =~ y11 + y12 + y2
eta4 =~ y13 + y14 + y15 + y16 + y17
x4 ~ eta3 + x1
eta1 ~ x4
x11 ~ eta1
x6 ~ x11
x2 ~ x11
eta2 ~ x2 + x7
x5 ~ eta2
eta4 ~ x3 + x4
x1 ~ eta4
x10 ~ eta2
x7 ~ x9
x8 ~ eta4
