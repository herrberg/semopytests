eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6 + y7 + y8
eta3 =~ y10 + y11 + y9
eta4 =~ y12 + y13 + y6
x7 ~ x4
x6 ~ x2 + x5 + x7
eta4 ~ x6
eta3 ~ x3 + x7
eta2 ~ eta1 + x6
x1 ~ eta2
x8 ~ x6
