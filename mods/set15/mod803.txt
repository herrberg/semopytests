eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y10 + y12 + y6 + y7 + y8
eta4 =~ y11 + y12
x2 ~ eta3 + eta4
x1 ~ eta2 + x2
x4 ~ x1
eta1 ~ x4
eta4 ~ x4
x3 ~ x1
