eta1 =~ y15 + y2 + y3 + y4 + y5
eta2 =~ y6 + y7 + y8
eta3 =~ y10 + y9
eta4 =~ y11 + y12 + y13 + y14 + y15
x1 ~ x3 + x5 + x7
eta2 ~ eta3 + x1
x8 ~ eta2
x9 ~ eta1 + x8
x7 ~ eta2
eta4 ~ x7
x4 ~ x8
x6 ~ x8
x2 ~ x8
