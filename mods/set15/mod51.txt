eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6 + y7 + y8
eta3 =~ y10 + y11 + y9
eta4 =~ y12 + y13 + y15 + y7
x2 ~ eta2 + x3
x1 ~ eta3 + x2
x3 ~ x1
eta3 ~ eta1
eta4 ~ eta3
