eta1 =~ y1 + y2 + y3 + y4 + y5
eta1 ~ x4 + x5 + x6
x1 ~ eta1 + x8
x3 ~ x1
x2 ~ x6 + x7
x5 ~ x1
