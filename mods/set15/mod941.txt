eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5 + y6 + y7
eta1 ~ x2
x1 ~ eta1 + eta2
