eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta2 ~ eta1 + eta3 + x8 + x9
x4 ~ eta2 + x10 + x5
x8 ~ x4
x10 ~ x2
x3 ~ eta2
x5 ~ x7
x6 ~ eta2
x1 ~ x9
