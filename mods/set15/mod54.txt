eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y10 + y11 + y3 + y7 + y9
eta4 =~ y12 + y13 + y14 + y15 + y16
x2 ~ eta3 + x3
x1 ~ x2
x3 ~ eta1 + eta4
eta4 ~ x2 + x4
eta2 ~ x4
