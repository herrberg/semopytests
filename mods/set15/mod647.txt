eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y6 + y9
eta3 =~ y10 + y11 + y7 + y8 + y9
x2 ~ x1 + x10 + x7
eta2 ~ x2 + x8
x3 ~ eta1 + x1
x4 ~ x2
eta3 ~ x4
x9 ~ x2
x6 ~ x9
x5 ~ x10
x7 ~ x4
