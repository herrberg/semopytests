eta1 =~ y1 + y2
x5 ~ x6
eta1 ~ x5 + x8
x7 ~ eta1 + x3
x1 ~ x7
x2 ~ x5
x4 ~ x5
