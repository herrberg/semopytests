eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5 + y6 + y7
x2 ~ eta2
x1 ~ x2
x3 ~ x2
eta1 ~ x3 + x4
