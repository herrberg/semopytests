eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6 + y7
x2 ~ x1 + x6
x3 ~ x2
eta1 ~ x2
x7 ~ x2
x5 ~ x1 + x4
eta2 ~ x1
