eta1 =~ y1 + y2 + y3 + y4 + y5
x1 ~ eta1
x2 ~ eta1
