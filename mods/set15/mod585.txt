eta1 =~ y1 + y2 + y3 + y4 + y5
eta2 =~ y6 + y7
eta1 ~ eta2 + x5
x2 ~ eta1
x4 ~ eta2 + x1
x9 ~ eta2
x8 ~ x5
x6 ~ x1 + x7
x10 ~ eta2
x3 ~ x5
