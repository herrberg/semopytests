eta1 =~ y2 + y9
eta2 =~ y3 + y4 + y5 + y6 + y7
eta3 =~ y10 + y8 + y9
eta4 =~ y11 + y12 + y13 + y14
eta5 =~ y15 + y16 + y17
eta2 ~ eta4 + eta5 + x1
x2 ~ eta2
eta4 ~ eta1 + x2
eta5 ~ eta3
