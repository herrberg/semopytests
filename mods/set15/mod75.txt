eta1 =~ y1 + y2 + y3
eta1 ~ x2 + x5
x1 ~ eta1
x6 ~ eta1
x5 ~ x6
x3 ~ x2 + x7
x8 ~ x3
x4 ~ x7
