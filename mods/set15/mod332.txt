eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y12 + y7
eta1 ~ x1 + x2
eta4 ~ eta1
eta3 ~ eta4
eta2 ~ eta3
x2 ~ eta3
