eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5 + y6 + y7
eta3 =~ y11 + y8
eta4 =~ y10 + y11 + y12 + y13 + y14
eta5 =~ y15 + y16
eta5 ~ eta4 + x6 + x8
x3 ~ eta5 + x2 + x4
eta4 ~ eta2 + x7
eta2 ~ eta5
x2 ~ x1
x5 ~ x4
eta1 ~ x5
eta3 ~ eta5
