eta1 =~ y1 + y2 + y3 + y4
x2 ~ x3 + x4 + x5
x3 ~ x1
eta1 ~ x4
