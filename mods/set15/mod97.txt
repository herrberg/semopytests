eta1 =~ y1 + y2 + y3 + y4 + y5
eta2 =~ y6 + y7 + y8 + y9
eta3 =~ y10 + y11 + y12 + y15
eta4 =~ y14 + y15
eta2 ~ eta1 + eta3 + x7
x6 ~ eta2
eta1 ~ x6 + x9
eta4 ~ eta3 + x1 + x4
x5 ~ eta4
x8 ~ eta4 + x2 + x3
