eta1 =~ y1 + y2 + y3 + y4 + y5
eta2 =~ y6 + y7 + y8
x3 ~ eta2 + x2
x1 ~ x3
eta2 ~ x1
eta1 ~ x1
