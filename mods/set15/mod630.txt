eta1 =~ y1 + y12 + y3 + y4 + y5
eta2 =~ y6 + y7
eta3 =~ y8 + y9
eta4 =~ y10 + y11 + y12
eta5 =~ y13 + y14
eta4 ~ eta1 + x2
x1 ~ eta4
x3 ~ x1
eta5 ~ eta4
x5 ~ eta5
x4 ~ eta4
x6 ~ x4
eta2 ~ x6
x7 ~ eta3 + x4
eta1 ~ x1
