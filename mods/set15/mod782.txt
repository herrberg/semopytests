eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y17 + y6 + y7 + y8
eta3 =~ y10 + y11 + y12 + y13 + y9
eta4 =~ y14 + y15 + y16 + y17
x2 ~ eta1 + eta2
x5 ~ x2
eta1 ~ eta3 + x5
x3 ~ eta2
eta4 ~ x1 + x5
x4 ~ eta3
