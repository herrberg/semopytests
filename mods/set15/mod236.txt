eta1 =~ y2 + y7
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7 + y8
eta4 =~ y10 + y9
eta5 =~ y11 + y12 + y13 + y14
x10 ~ x8
x9 ~ x10
eta2 ~ eta3 + eta4 + x1 + x3 + x9
x4 ~ eta4
x11 ~ x4
x7 ~ eta1 + eta5 + x5 + x9
x5 ~ x6
x2 ~ x5
