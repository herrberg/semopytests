eta1 =~ y1 + y2 + y3 + y4 + y5
eta2 =~ y15 + y6 + y7 + y8
eta3 =~ y10 + y11 + y12 + y13 + y14
eta4 =~ y15 + y16 + y17 + y18
eta5 =~ y19 + y6
x2 ~ eta2 + eta5
eta4 ~ x2
eta3 ~ eta2
eta1 ~ eta2
x1 ~ x2
