eta1 =~ y1 + y2
x2 ~ x1
x3 ~ x2
eta1 ~ x1
