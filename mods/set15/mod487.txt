eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y10 + y6 + y7 + y8
eta3 =~ y10 + y9
eta3 ~ x2
x1 ~ eta2 + x2
eta1 ~ eta2
