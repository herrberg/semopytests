eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6 + y7
eta3 =~ y10 + y11 + y3 + y8
eta4 =~ y12 + y13
eta3 ~ x2
eta1 ~ eta2 + eta3
x7 ~ eta1
x4 ~ x1 + x7
eta2 ~ x3
x1 ~ x5
x6 ~ eta4 + x1
x8 ~ eta2
x9 ~ x8
