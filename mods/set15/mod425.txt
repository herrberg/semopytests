eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5 + y6
x7 ~ eta1 + eta2 + x5
eta1 ~ x1
x2 ~ eta1
x6 ~ x5
x3 ~ x6
x4 ~ eta1
