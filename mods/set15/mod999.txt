eta1 =~ y1 + y2 + y3 + y4 + y5
eta2 =~ y6 + y7
eta1 ~ x1 + x2 + x3
x4 ~ eta1
x3 ~ x4
eta2 ~ x2
