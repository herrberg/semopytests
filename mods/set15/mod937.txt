eta1 =~ y1 + y11 + y2 + y3
eta2 =~ y5 + y6 + y7 + y8
eta3 =~ y10 + y11 + y12 + y9
x3 ~ eta2
eta3 ~ x3
x1 ~ eta3 + x9
x2 ~ x1
x9 ~ x7 + x8
x5 ~ x8
eta1 ~ x5
x6 ~ eta3
x10 ~ eta3 + x4
