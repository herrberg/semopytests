eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8 + y9
x1 ~ x2 + x6 + x7
eta3 ~ eta2 + x3 + x4 + x6
x5 ~ x4
eta1 ~ x5
