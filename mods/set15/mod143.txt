eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6 + y7
x5 ~ eta1
x1 ~ x4 + x5 + x6
x2 ~ x5
eta2 ~ x5
x3 ~ eta1
