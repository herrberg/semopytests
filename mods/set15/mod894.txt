eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y4 + y5 + y6 + y8 + y9
eta3 =~ y10 + y11 + y12 + y13 + y14
eta4 =~ y15 + y16 + y17
eta2 ~ eta3 + x1
x2 ~ eta2
eta3 ~ x2
eta1 ~ x2
eta4 ~ x2
