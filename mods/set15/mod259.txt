eta1 =~ y1 + y2 + y3 + y4 + y5
eta2 =~ y3 + y6 + y7 + y8
eta3 =~ y10 + y11 + y12 + y13 + y14
eta3 ~ eta2
x3 ~ eta2 + x6
x7 ~ eta2 + x5
x4 ~ eta1 + x1
x5 ~ x4
x2 ~ eta2
