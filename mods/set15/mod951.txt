eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6 + y7 + y8 + y9
x1 ~ x2
eta1 ~ eta2 + x1
