eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6 + y7 + y8 + y9
eta3 =~ y10 + y11
eta4 =~ y11 + y12 + y13 + y14 + y16
x7 ~ eta3
eta2 ~ x3 + x7
x6 ~ eta2
eta4 ~ x6
x8 ~ eta2 + x2
x4 ~ x8
x9 ~ x3
x5 ~ eta1 + eta2
eta1 ~ x1
