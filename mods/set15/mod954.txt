eta1 =~ y1 + y2 + y3 + y4 + y5
eta2 =~ y6 + y7 + y8
x9 ~ eta1 + x3 + x5
x4 ~ x9
x5 ~ x4
x7 ~ x6 + x9
x1 ~ x9
eta2 ~ x8 + x9
x2 ~ eta1
