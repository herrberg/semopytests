eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
x1 ~ x4
x10 ~ eta1 + x1
eta3 ~ x10
x3 ~ eta3
x6 ~ eta2 + x1 + x2
x7 ~ x9
x2 ~ x7
x5 ~ eta3
x8 ~ x2
