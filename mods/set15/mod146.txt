eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6 + y7
x1 ~ eta2
x5 ~ x1
x9 ~ x5
x7 ~ x9
eta1 ~ x7
x2 ~ eta2 + x6
x10 ~ eta2
x3 ~ x10
x8 ~ x10
x4 ~ eta2
