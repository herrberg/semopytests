eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta1 ~ x8
x2 ~ eta1 + x3
x10 ~ eta3 + x2
x1 ~ x2
x5 ~ eta1
x6 ~ x4 + x5
x7 ~ x3
x4 ~ x9
eta2 ~ eta1
