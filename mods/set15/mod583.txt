eta1 =~ y1 + y2 + y3 + y4 + y5
eta2 =~ y6 + y7
eta3 =~ y10 + y11 + y7 + y9
eta1 ~ x4 + x9
x6 ~ eta1 + x10 + x8
x10 ~ x7
x2 ~ eta3 + x7
x11 ~ x8
x5 ~ x8
x3 ~ eta2 + eta3
x1 ~ x7
