eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y4 + y6
eta4 =~ y10 + y11 + y8 + y9
eta5 =~ y12 + y13 + y14 + y15 + y16
eta1 ~ eta2 + x2 + x6
x1 ~ eta1
eta3 ~ eta5 + x1
x3 ~ x2 + x4
eta4 ~ x3
x4 ~ eta4 + x8
x7 ~ x4
x5 ~ eta1
