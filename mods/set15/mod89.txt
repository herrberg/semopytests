eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5 + y6
eta3 =~ y7 + y8
eta1 ~ eta2 + x2 + x3
eta2 ~ x1
eta3 ~ x2
