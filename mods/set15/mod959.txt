eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y10 + y7 + y8 + y9
eta4 =~ y11 + y13 + y14 + y15 + y4
x2 ~ x11
x6 ~ x2 + x5
eta2 ~ eta3 + x11
x9 ~ eta2 + x3
x8 ~ x11 + x4
x1 ~ x8
eta1 ~ x11
x10 ~ x2
eta4 ~ eta3
x7 ~ eta4
