eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5 + y6
eta3 =~ y10 + y11 + y7 + y8 + y9
eta4 =~ y12 + y4
eta1 ~ x1 + x2
eta2 ~ eta1
x1 ~ eta2 + eta4
eta3 ~ x1
