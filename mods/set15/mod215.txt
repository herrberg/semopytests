eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5 + y6
eta3 =~ y10 + y11 + y7 + y8 + y9
eta4 =~ y12 + y13 + y4
eta5 =~ y15 + y16 + y17 + y18
x1 ~ x8
x10 ~ x1 + x2 + x4
eta5 ~ eta4 + x10
x6 ~ x10
eta1 ~ x2
x9 ~ eta1 + eta2 + x5
x3 ~ x1
x4 ~ eta3
x7 ~ eta4
