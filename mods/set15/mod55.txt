eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6 + y7
eta3 =~ y3 + y8
eta4 =~ y10 + y11 + y12 + y13
x10 ~ x2 + x6
x5 ~ eta1 + x10 + x8
x4 ~ x5
x3 ~ x4
eta4 ~ x10
x9 ~ x10
x7 ~ x9
x1 ~ x2
eta3 ~ x1
eta2 ~ eta3
eta1 ~ x4
