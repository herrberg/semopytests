eta1 =~ y1 + y2 + y3 + y4 + y5
eta2 =~ y15 + y6 + y7 + y9
eta3 =~ y10 + y11 + y12
eta4 =~ y13 + y14 + y15
x4 ~ eta4 + x6
x7 ~ eta3 + x4
eta4 ~ x7
x1 ~ eta4
eta3 ~ eta2
x3 ~ x2 + x4
x5 ~ eta1 + eta3
x8 ~ x5
