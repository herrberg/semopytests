eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y6 + y7 + y8 + y9
eta4 =~ y11 + y12
x9 ~ x2 + x8
x5 ~ x9
x3 ~ eta4 + x4 + x5
eta3 ~ x8
x7 ~ eta3
eta2 ~ x5
x1 ~ x2
eta1 ~ x8
x6 ~ x5
