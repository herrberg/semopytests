eta1 =~ y1 + y2 + y3 + y4
x6 ~ x2 + x7
x10 ~ x3 + x6
x1 ~ eta1 + x10 + x5
x3 ~ x4
x2 ~ x10 + x8
x9 ~ x6
