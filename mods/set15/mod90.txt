eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6 + y7 + y8 + y9
x3 ~ x2
eta1 ~ x3
x6 ~ eta1
x5 ~ x3
x7 ~ x1 + x5
eta2 ~ x7
x4 ~ eta1
