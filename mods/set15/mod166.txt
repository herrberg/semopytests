eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6 + y7
eta3 =~ y10 + y8 + y9
eta4 =~ y11 + y13 + y14 + y17
eta5 =~ y15 + y16 + y17 + y18
x5 ~ eta5
x3 ~ x1 + x2 + x5
eta1 ~ eta3 + x3
x2 ~ eta1
eta4 ~ eta1
eta2 ~ x2
x4 ~ x5
