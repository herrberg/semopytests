eta1 =~ y1 + y2 + y3 + y4 + y9
eta2 =~ y6 + y7 + y8 + y9
eta3 =~ y10 + y11 + y12
eta4 =~ y13 + y14 + y15 + y16 + y17
eta5 =~ y19 + y20 + y21 + y22 + y6
x2 ~ eta2
eta3 ~ eta1 + x2
eta4 ~ eta3
x1 ~ eta2 + eta5
