eta1 =~ y1 + y2 + y3 + y4
x5 ~ x3 + x6
x6 ~ x2
x4 ~ eta1 + x3
x1 ~ x4
eta1 ~ x1
