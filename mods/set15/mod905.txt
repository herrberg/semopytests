eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y5 + y6 + y7
eta3 =~ y10 + y11 + y12 + y8 + y9
eta4 =~ y13 + y4
eta5 =~ y15 + y16
eta1 ~ x2 + x3
eta5 ~ eta1 + eta4
x4 ~ eta2 + x3
x1 ~ x3
eta3 ~ eta2
