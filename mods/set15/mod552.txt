eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y2 + y6 + y7 + y8
eta4 =~ y10 + y11 + y12 + y13 + y9
eta2 ~ x1 + x7
x2 ~ eta2 + eta4 + x6
x4 ~ x2 + x8
eta4 ~ eta1 + x5
eta3 ~ x2
x9 ~ x5
x3 ~ eta2
x1 ~ x3
