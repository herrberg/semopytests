eta1 =~ y1 + y2 + y3
eta2 =~ y2 + y4 + y6 + y7
eta3 =~ y10 + y11 + y8 + y9
eta2 ~ eta1
x4 ~ eta2
x8 ~ x2 + x4
x6 ~ x10 + x11 + x8
x9 ~ eta2
x5 ~ x1 + x8
x2 ~ x7
x3 ~ x7
eta3 ~ x7
