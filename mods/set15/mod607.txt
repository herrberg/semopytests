eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y4 + y5 + y6 + y7 + y9
eta3 =~ y10 + y11 + y12 + y13
x5 ~ eta3 + x1 + x7
x6 ~ x4 + x5
x8 ~ x5
eta2 ~ x5
x3 ~ x7
eta1 ~ x1
x2 ~ x1
