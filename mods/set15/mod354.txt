eta1 =~ y10 + y2 + y3
eta2 =~ y4 + y5 + y6 + y7 + y8
eta3 =~ y10 + y9
eta4 =~ y11 + y12 + y13 + y14 + y15
eta5 =~ y15 + y16 + y17 + y18 + y20
eta1 ~ eta2 + x3 + x4
x7 ~ eta1
eta5 ~ x4
eta3 ~ eta5
x2 ~ eta2
x1 ~ x3
eta4 ~ eta2
x6 ~ x3
x5 ~ eta1
