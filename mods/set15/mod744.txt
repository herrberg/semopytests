eta1 =~ y1 + y2 + y3 + y4
eta2 =~ y15 + y6 + y7 + y8 + y9
eta3 =~ y10 + y11 + y12
eta4 =~ y13 + y14 + y15 + y16
x1 ~ eta3 + x2 + x4
eta2 ~ x1
x3 ~ eta1 + eta2
eta4 ~ x2
