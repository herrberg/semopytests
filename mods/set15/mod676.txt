eta1 =~ y1 + y2
eta2 =~ y2 + y3 + y4 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta5 =~ y12 + y13
x4 ~ eta1
eta2 ~ eta4 + eta5 + x4
x6 ~ eta1 + x2
eta3 ~ eta4 + x5
x3 ~ x4
eta5 ~ x1
