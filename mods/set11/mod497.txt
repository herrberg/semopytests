eta1 =~ y1 + y2
x2 ~ x5
x6 ~ x2
x9 ~ x6 + x7
x1 ~ x8 + x9
eta1 ~ x9
x7 ~ eta1
x3 ~ x5
x10 ~ x3
x8 ~ x4
