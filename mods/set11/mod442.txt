eta1 =~ y1 + y2 + y3
x9 ~ x10 + x2
x5 ~ x9
eta1 ~ x5
x3 ~ eta1
x8 ~ x3
x1 ~ x8
x4 ~ x1
x6 ~ x3
x2 ~ x6 + x7
