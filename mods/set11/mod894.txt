eta1 =~ y1 + y2 + y3
eta1 ~ x3 + x5
x1 ~ eta1 + x4
x3 ~ x1 + x9
x6 ~ x4
x7 ~ x6
x2 ~ x7
x10 ~ x5
x8 ~ x7
