eta1 =~ y1 + y2
x4 ~ x1 + x2
x9 ~ x4 + x5
eta1 ~ x4
x2 ~ eta1
x5 ~ x6
x3 ~ x1
x10 ~ x1
x7 ~ x1
x8 ~ x6
