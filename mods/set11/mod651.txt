eta1 =~ y1 + y2
x2 ~ eta1 + x6 + x8
x5 ~ x1 + x2 + x7 + x9
x8 ~ x5
x10 ~ x8
x3 ~ x1
x4 ~ x8
