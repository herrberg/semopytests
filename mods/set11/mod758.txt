eta1 =~ y1 + y2 + y3
x7 ~ x10 + x4 + x9
x3 ~ x7
x8 ~ x6 + x9
x5 ~ eta1 + x8
x1 ~ x10
x2 ~ x8
