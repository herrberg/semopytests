eta1 =~ y1 + y2
x10 ~ eta1 + x3
x5 ~ x10
x3 ~ x1 + x5 + x7
x6 ~ x3
x8 ~ eta1
x4 ~ eta1
x1 ~ x9
x2 ~ x3
