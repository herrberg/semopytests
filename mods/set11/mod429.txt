eta1 =~ y1 + y2 + y3
x9 ~ x4 + x6 + x7
x8 ~ x9
x6 ~ eta1 + x1 + x10 + x8
x3 ~ x9
x10 ~ x2
x5 ~ x6
