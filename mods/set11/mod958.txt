eta1 =~ y1 + y2 + y3
x5 ~ x8 + x9
eta1 ~ x4 + x5 + x7
x8 ~ eta1 + x3
x1 ~ x10 + x2 + x5
x6 ~ x1
