eta1 =~ y1 + y2
x1 ~ x4 + x5
x6 ~ x1 + x7 + x9
x3 ~ x1
x7 ~ eta1 + x10
x8 ~ x5
x2 ~ x10
