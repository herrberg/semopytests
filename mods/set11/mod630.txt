eta1 =~ y1 + y2 + y3
x5 ~ x10 + x6
x10 ~ eta1 + x2
x9 ~ x1 + x10
x3 ~ x10
x8 ~ eta1
x7 ~ x4 + x6
