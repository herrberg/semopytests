eta1 =~ y1 + y2 + y3
x8 ~ x4 + x6
eta1 ~ x8
x7 ~ x10 + x2 + x4
x9 ~ x5 + x7
x3 ~ x7
x1 ~ x10
