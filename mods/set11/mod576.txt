eta1 =~ y1 + y2 + y3
x3 ~ x10 + x5
x9 ~ eta1 + x3 + x4 + x8
x2 ~ x3 + x6
x10 ~ x2
eta1 ~ x1
x7 ~ x3
