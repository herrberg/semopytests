eta1 =~ y1 + y2
x9 ~ x2 + x4
x7 ~ x10 + x9
x1 ~ x7
x3 ~ x2
x10 ~ x8
x6 ~ eta1 + x10
x5 ~ eta1
x4 ~ x7
