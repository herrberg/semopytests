eta1 =~ y1 + y2
x3 ~ x10
x8 ~ x3 + x9
x6 ~ x8
x9 ~ eta1 + x2 + x4 + x6
x4 ~ x5
x1 ~ x2
x7 ~ x3
