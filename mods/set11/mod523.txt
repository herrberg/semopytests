eta1 =~ y1 + y2
x1 ~ eta1
x8 ~ x1 + x3 + x6
x4 ~ x8 + x9
x2 ~ x8
x6 ~ x2
x5 ~ x6
x7 ~ x1
x10 ~ x1
