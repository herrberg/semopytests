eta1 =~ y1 + y2 + y3
x4 ~ eta1 + x3
x7 ~ x4 + x8
x9 ~ x7
x3 ~ x5
x8 ~ x2 + x6
x10 ~ x8
x2 ~ x10
x1 ~ x7
