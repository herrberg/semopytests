eta1 =~ y1 + y2 + y3
x3 ~ x2 + x6 + x8
x9 ~ x10 + x3
x2 ~ x1 + x4 + x9
x7 ~ x10
eta1 ~ x6
x5 ~ eta1
