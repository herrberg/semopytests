eta1 =~ y1 + y2 + y3
eta1 ~ x7 + x8
x9 ~ eta1 + x4
x7 ~ x6 + x9
x10 ~ x2 + x5
x6 ~ x10
x1 ~ x4
x3 ~ x1
