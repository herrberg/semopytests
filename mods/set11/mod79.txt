eta1 =~ y1 + y2 + y3
eta1 ~ x10 + x4 + x9
x5 ~ eta1 + x2
x10 ~ x1 + x5
x8 ~ x5
x3 ~ eta1
x7 ~ x4
x6 ~ eta1
