eta1 =~ y1 + y2 + y3
x4 ~ x10 + x9
x7 ~ x4
x3 ~ x7
x1 ~ x10 + x8
x5 ~ x1
eta1 ~ x1
x9 ~ x6 + x7
x6 ~ x2
