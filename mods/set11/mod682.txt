eta1 =~ y1 + y2 + y3
x10 ~ x3 + x5
x9 ~ x10 + x2
eta1 ~ x2
x8 ~ x10 + x6
x5 ~ x8
x4 ~ x2
x1 ~ x10
x7 ~ x10
