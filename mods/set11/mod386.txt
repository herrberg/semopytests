eta1 =~ y1 + y2
x1 ~ x2 + x5
x6 ~ eta1 + x1
x2 ~ x3 + x6
x8 ~ x6
x9 ~ x5
x10 ~ x1
x7 ~ x5
x4 ~ x7
