eta1 =~ y1 + y2 + y3
x8 ~ x3 + x9
x2 ~ x1 + x10 + x7 + x8
x6 ~ x4 + x8
x5 ~ x9
eta1 ~ x3
