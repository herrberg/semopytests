eta1 =~ y1 + y2 + y3
x2 ~ x5 + x8
x4 ~ x2
x10 ~ x4 + x9
x7 ~ eta1 + x5
x3 ~ x1 + x7
x5 ~ x3
x6 ~ x7
