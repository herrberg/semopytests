eta1 =~ y1 + y2
x10 ~ x1 + x7
x2 ~ x10 + x5
x3 ~ x2
x9 ~ x3 + x6
x8 ~ x9
eta1 ~ x10
x4 ~ x9
x6 ~ x4
