eta1 =~ y1 + y2 + y3
x1 ~ eta1
x9 ~ x1 + x5 + x6 + x8
x7 ~ x2 + x5
x8 ~ x3 + x4
x10 ~ x8
