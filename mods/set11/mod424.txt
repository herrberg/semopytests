eta1 =~ y1 + y2 + y3
x2 ~ x5 + x8
x3 ~ x1 + x10 + x2
x8 ~ x3
x1 ~ eta1 + x7
x9 ~ x1 + x6
x4 ~ x3
