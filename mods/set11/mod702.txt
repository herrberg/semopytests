eta1 =~ y1 + y2
x7 ~ x10 + x8
x4 ~ x7
x8 ~ x4
x9 ~ x4 + x6
x2 ~ x7
x1 ~ eta1 + x7
x3 ~ eta1 + x5
