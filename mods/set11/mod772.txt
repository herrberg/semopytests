eta1 =~ y1 + y2
x3 ~ x9
eta1 ~ x3 + x6
x4 ~ eta1
x6 ~ x2 + x4
x8 ~ x4 + x5
x1 ~ x8
x2 ~ x10
x7 ~ x3
