eta1 =~ y1 + y2
x9 ~ x3 + x8
x7 ~ x9
eta1 ~ x7
x2 ~ x1 + x7
x8 ~ x2
x6 ~ x10 + x7
x5 ~ x3
x4 ~ x5
