eta1 =~ y1 + y2
x9 ~ x2 + x4
x8 ~ x9
x4 ~ x8
eta1 ~ x8
x3 ~ x1 + x7 + x8
x6 ~ x3
x10 ~ x2
x5 ~ x8
