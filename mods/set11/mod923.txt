eta1 =~ y1 + y2
eta1 ~ x4 + x8
x3 ~ x7 + x8
x9 ~ x3
x10 ~ x3
x2 ~ x1 + x4
x6 ~ x2
x1 ~ x6
x5 ~ x2
