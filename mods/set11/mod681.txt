eta1 =~ y1 + y2 + y3
x4 ~ x10 + x8
x1 ~ eta1 + x4
x3 ~ x1 + x9
x7 ~ x2 + x4
x10 ~ x7
x5 ~ x1
x6 ~ x1
