eta1 =~ y1 + y2
x5 ~ eta1 + x9
x8 ~ x3 + x5 + x6 + x7
x9 ~ x2 + x8
x4 ~ x7
x1 ~ x7
x10 ~ x6
