eta1 =~ y1 + y2 + y3
x8 ~ x2
eta1 ~ x8
x6 ~ eta1 + x1
x7 ~ x6
x9 ~ x3 + x7
x1 ~ x10 + x5
x10 ~ x7
x4 ~ x2
