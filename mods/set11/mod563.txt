eta1 =~ y1 + y2
x5 ~ x10
x8 ~ x5 + x6
x2 ~ x3 + x8
x6 ~ x2
x3 ~ eta1 + x9
x1 ~ x4 + x8
x7 ~ eta1
