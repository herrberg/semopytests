eta1 =~ y1 + y2 + y3
x6 ~ x2 + x4
x7 ~ x5 + x6 + x8
x3 ~ x7
x5 ~ x10
x1 ~ x7
x8 ~ x1
x4 ~ eta1
x9 ~ x5
