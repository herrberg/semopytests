eta1 =~ y1 + y2 + y3
x1 ~ x3
x4 ~ x1 + x6
eta1 ~ x4
x2 ~ x4
x6 ~ x2
x9 ~ x6
x8 ~ x1
x7 ~ x8
x10 ~ x2
x5 ~ x6
