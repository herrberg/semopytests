eta1 =~ y1 + y2 + y3
x7 ~ eta1 + x1 + x3
x8 ~ x4 + x7
eta1 ~ x8
x1 ~ x10 + x2 + x9
x6 ~ x7
x5 ~ x2
