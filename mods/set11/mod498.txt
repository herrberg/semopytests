eta1 =~ y1 + y2 + y3
eta1 ~ x10 + x3 + x6 + x8
x2 ~ eta1 + x5
x6 ~ x2
x7 ~ eta1
x5 ~ x1 + x4 + x9
