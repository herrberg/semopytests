eta1 =~ y1 + y2 + y3
x6 ~ eta1 + x7
x4 ~ x6 + x9
x3 ~ x6
x5 ~ x10 + x3
x1 ~ x5
x7 ~ x1 + x2
x8 ~ x3
