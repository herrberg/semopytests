eta1 =~ y1 + y2
x3 ~ x2 + x4
x7 ~ x3
x5 ~ x7
x8 ~ x5
x6 ~ eta1 + x4
x9 ~ x6
x10 ~ x3
x1 ~ x10
x2 ~ x10
