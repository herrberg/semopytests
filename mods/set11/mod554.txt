eta1 =~ y1 + y2 + y3
x6 ~ x1 + x7
eta1 ~ x6
x1 ~ eta1 + x2
x4 ~ x1 + x3
x9 ~ x4
x10 ~ x4
x5 ~ eta1 + x8
