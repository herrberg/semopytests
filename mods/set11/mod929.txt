eta1 =~ y1 + y2 + y3
x7 ~ eta1 + x3 + x4
x1 ~ x7
eta1 ~ x1
x3 ~ x6
x5 ~ x3
x2 ~ x3 + x9
x9 ~ x10
x8 ~ x9
