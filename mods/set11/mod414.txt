eta1 =~ y1 + y2
x1 ~ x10 + x7
x2 ~ x1 + x5
x10 ~ x2 + x6
x8 ~ x6
x3 ~ eta1 + x6
x9 ~ x7
x4 ~ x7
