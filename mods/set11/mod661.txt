eta1 =~ y1 + y2
x6 ~ eta1 + x1
x4 ~ x6
x8 ~ x4
x3 ~ x5 + x6 + x7
x2 ~ x3
x1 ~ x2
x10 ~ x1
x9 ~ x3
