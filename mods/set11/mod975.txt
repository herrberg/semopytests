eta1 =~ y1 + y2
x5 ~ x7
eta1 ~ x5
x4 ~ eta1 + x2
x8 ~ x4 + x6
x2 ~ x8
x10 ~ x3 + x4
x9 ~ x4
x1 ~ eta1
