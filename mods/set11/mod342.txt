eta1 =~ y1 + y2
x7 ~ x4 + x8
x9 ~ x2 + x7
x10 ~ x9
x1 ~ x7
x4 ~ x1
x6 ~ x4
x3 ~ x5 + x7
x5 ~ eta1
