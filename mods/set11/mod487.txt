eta1 =~ y1 + y2
x9 ~ x10 + x3 + x6
x5 ~ x9
x2 ~ eta1 + x5
x10 ~ x7
x3 ~ x5
x1 ~ x3
eta1 ~ x4 + x8
