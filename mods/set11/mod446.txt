eta1 =~ y1 + y2
x10 ~ eta1 + x7 + x8
x4 ~ x10 + x2
x3 ~ eta1 + x6
x5 ~ x3
x2 ~ x1
x9 ~ x6
