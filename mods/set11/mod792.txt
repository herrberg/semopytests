eta1 =~ y1 + y2
x4 ~ eta1 + x8
x1 ~ x3 + x4
eta1 ~ x1
x3 ~ x10 + x7 + x9
x2 ~ x8
x6 ~ x9
x5 ~ x8
