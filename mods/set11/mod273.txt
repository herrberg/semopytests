eta1 =~ y1 + y2 + y3
x9 ~ x1 + x10
x2 ~ eta1 + x6 + x9
x8 ~ x2
eta1 ~ x5 + x8
x7 ~ x10 + x4
x3 ~ x1
