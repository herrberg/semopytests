eta1 =~ y1 + y2
x9 ~ eta1 + x10 + x3
x1 ~ x9
x7 ~ x1 + x4
x10 ~ x7
x5 ~ x9
x6 ~ x4
x2 ~ x6
x8 ~ x9
