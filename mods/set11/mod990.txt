eta1 =~ y1 + y2
x4 ~ x1 + x2 + x6
x10 ~ x4
x2 ~ x10
x8 ~ x1 + x5
x5 ~ eta1
x7 ~ x6
x3 ~ x5
x9 ~ x6
