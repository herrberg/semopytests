eta1 =~ y1 + y2 + y3
eta1 ~ x10 + x3 + x6
x4 ~ eta1 + x7
x3 ~ x4
x1 ~ eta1
x8 ~ eta1
x9 ~ x5 + x8
x2 ~ x7
