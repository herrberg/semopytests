eta1 =~ y1 + y2
x3 ~ x6 + x9
x6 ~ x4 + x5
x10 ~ x9
eta1 ~ x4
x8 ~ x6
x7 ~ x6
x1 ~ x6
x2 ~ x4
