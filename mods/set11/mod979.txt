eta1 =~ y1 + y2 + y3
x8 ~ eta1 + x2 + x5 + x9
x7 ~ x6 + x8
x5 ~ x7
x10 ~ x8
x4 ~ x1 + x8
x2 ~ x3
