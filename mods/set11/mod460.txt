eta1 =~ y1 + y2
x7 ~ eta1 + x2 + x4
x9 ~ x3 + x6 + x7
eta1 ~ x9
x3 ~ x1
x6 ~ x8
x10 ~ x2
x5 ~ x6
