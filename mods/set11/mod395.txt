eta1 =~ y1 + y2 + y3
x1 ~ eta1 + x10 + x3
x7 ~ x1 + x5 + x8
x10 ~ x7
x4 ~ x1
x9 ~ x1 + x6
x2 ~ x6
