eta1 =~ y1 + y2 + y3
x5 ~ x6 + x8
eta1 ~ x5
x6 ~ eta1
x1 ~ x10 + x8
x7 ~ x10
x9 ~ x7
x3 ~ x2 + x4 + x8
