eta1 =~ y1 + y2
x8 ~ x1 + x4
eta1 ~ x6 + x8
x9 ~ eta1 + x10
x2 ~ x7 + x9
x1 ~ eta1
x3 ~ x8
x5 ~ x10
