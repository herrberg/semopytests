eta1 =~ y1 + y2 + y3
x5 ~ eta1 + x10 + x4 + x7 + x8
x1 ~ x2 + x3 + x5
x4 ~ x1
x6 ~ x8 + x9
