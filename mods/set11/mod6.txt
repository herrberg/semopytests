eta1 =~ y1 + y2 + y3
x10 ~ x6
x7 ~ x10
x9 ~ x10
x3 ~ eta1 + x10 + x4 + x5
x8 ~ x10
x1 ~ x5
x2 ~ eta1
