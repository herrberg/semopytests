eta1 =~ y1 + y2 + y3
x9 ~ x3 + x6 + x8
x5 ~ eta1 + x10 + x9
x10 ~ x4 + x7
x1 ~ x9
x2 ~ x8
