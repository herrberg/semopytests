eta1 =~ y1 + y2
x7 ~ x10 + x4 + x5
eta1 ~ x7
x5 ~ eta1 + x3 + x8
x6 ~ x7
x1 ~ x8
x2 ~ x1
x3 ~ x9
