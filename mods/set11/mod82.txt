eta1 =~ y1 + y2
x6 ~ x1 + x5
x2 ~ x6
x1 ~ x2 + x8
eta1 ~ x1 + x4
x10 ~ eta1
x7 ~ x10
x3 ~ eta1
x9 ~ x2
