eta1 =~ y1 + y2 + y3
eta1 ~ x7 + x8
x10 ~ eta1 + x1
x7 ~ x10 + x4
x5 ~ eta1
x2 ~ x5 + x9
x3 ~ eta1
x6 ~ x9
