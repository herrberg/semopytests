eta1 =~ y1 + y2
x5 ~ x4
x1 ~ x5
x6 ~ x1
eta1 ~ x4 + x8
x9 ~ eta1
x10 ~ eta1 + x7
x8 ~ x10
x3 ~ x10 + x2
