eta1 =~ y1 + y2 + y3
x10 ~ eta1 + x3 + x5 + x6
x8 ~ x10 + x7
x6 ~ x8
x3 ~ x2
x4 ~ eta1
x1 ~ x7
x9 ~ x7
