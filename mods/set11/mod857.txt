eta1 =~ y1 + y2 + y3
x2 ~ x5
x1 ~ x2 + x9
x7 ~ eta1 + x2 + x8
x9 ~ x10 + x3
x4 ~ x9
x6 ~ x8
