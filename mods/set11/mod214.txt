eta1 =~ y1 + y2
x9 ~ x10 + x2 + x6
eta1 ~ x3 + x9
x6 ~ eta1 + x8
x3 ~ x4
x1 ~ eta1
x8 ~ x5
x7 ~ x5
