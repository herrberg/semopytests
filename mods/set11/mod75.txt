eta1 =~ y1 + y2
x10 ~ x4
x8 ~ x10
x7 ~ eta1 + x8 + x9
x1 ~ x7
x6 ~ x3 + x5
x9 ~ x6
eta1 ~ x2
x3 ~ x9
