eta1 =~ y1 + y2
x2 ~ x3 + x5 + x6 + x9
x8 ~ x10 + x2
x1 ~ x7 + x8
eta1 ~ x1
x6 ~ eta1
x4 ~ x6
