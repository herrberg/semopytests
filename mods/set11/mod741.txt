eta1 =~ y1 + y2
x7 ~ x10 + x4
x6 ~ x1 + x7 + x8
x10 ~ x6
eta1 ~ x7
x2 ~ eta1
x1 ~ x5 + x9
x3 ~ x7
