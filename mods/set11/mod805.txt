eta1 =~ y1 + y2 + y3
x10 ~ eta1 + x2 + x8
x1 ~ x10 + x5
eta1 ~ x1
x4 ~ eta1
x6 ~ x4
x7 ~ x4
x8 ~ x9
x3 ~ x4
