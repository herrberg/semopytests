eta1 =~ y1 + y2 + y3
x9 ~ x3 + x8
x5 ~ eta1 + x4 + x9
x3 ~ x1 + x5
x7 ~ x3
x2 ~ x7
x6 ~ x8
x10 ~ x8
