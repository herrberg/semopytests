eta1 =~ y1 + y2 + y3
x2 ~ x10 + x3
x8 ~ x2 + x4
x3 ~ x5 + x8
eta1 ~ x3
x7 ~ x3 + x6
x9 ~ x4
x1 ~ x5
