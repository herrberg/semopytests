eta1 =~ y1 + y2
x2 ~ x3 + x4 + x5 + x6 + x9
x1 ~ x2
x7 ~ x1
eta1 ~ x2 + x8
x4 ~ eta1 + x10
