eta1 =~ y1 + y2 + y3
x7 ~ x1 + x5
x10 ~ x7
x1 ~ x10 + x2 + x9
eta1 ~ x1
x4 ~ x6 + x7
x3 ~ x9
x8 ~ x5
