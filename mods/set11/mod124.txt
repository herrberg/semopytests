eta1 =~ y1 + y2 + y3
eta1 ~ x3 + x4 + x6
x10 ~ eta1 + x8
x1 ~ x10
x7 ~ x5
x6 ~ x2 + x7
x2 ~ eta1
x9 ~ x3
