eta1 =~ y1 + y2
x9 ~ x6
x1 ~ x8 + x9
x3 ~ x1
x8 ~ x3 + x7
eta1 ~ x9
x2 ~ x1 + x5
x5 ~ x10
x4 ~ x6
