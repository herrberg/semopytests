eta1 =~ y1 + y2 + y3
x8 ~ x3 + x4
x5 ~ x8
x4 ~ eta1 + x9
x2 ~ x3
x7 ~ x2
x6 ~ eta1 + x1
x10 ~ x2
