eta1 =~ y1 + y2
x6 ~ x2 + x3
eta1 ~ x6 + x7
x2 ~ eta1
x8 ~ x2
x1 ~ eta1
x9 ~ x10 + x6
x5 ~ x6
x4 ~ x3
