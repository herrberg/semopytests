eta1 =~ y1 + y2 + y3
x6 ~ x4
x5 ~ x10 + x6
x7 ~ x5 + x8
x3 ~ x1 + x7
x8 ~ x3
x10 ~ x9
eta1 ~ x6
x2 ~ x4
