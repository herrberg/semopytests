eta1 =~ y1 + y2 + y3
x4 ~ x3 + x5 + x9
x6 ~ eta1 + x4
x5 ~ x6
x2 ~ x5 + x7
x1 ~ x10 + x5
eta1 ~ x8
