eta1 =~ y1 + y2
x7 ~ x9
eta1 ~ x4 + x7
x10 ~ eta1 + x3
x8 ~ x5 + x9
x6 ~ x8
x5 ~ x2
x4 ~ x10
x1 ~ x2
