eta1 =~ y1 + y2
x10 ~ x4 + x5 + x7
eta1 ~ x10 + x8
x6 ~ x10 + x9
x4 ~ x6
x1 ~ x7
x3 ~ x1
x8 ~ x2
