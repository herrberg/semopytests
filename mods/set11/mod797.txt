eta1 =~ y1 + y2 + y3
x6 ~ eta1
x3 ~ x5 + x6
x4 ~ x3 + x9
x8 ~ x4
x1 ~ x8
x2 ~ x1
x5 ~ x1 + x10
x9 ~ x7
