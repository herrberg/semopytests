eta1 =~ y1 + y2 + y3
x2 ~ x1 + x4 + x9
x7 ~ x2
x1 ~ x5 + x7
eta1 ~ x2
x5 ~ x3 + x8
x6 ~ x2
x4 ~ x10
