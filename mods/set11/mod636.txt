eta1 =~ y1 + y2 + y3
x1 ~ x10 + x2 + x3 + x8
x7 ~ x1
x6 ~ x1 + x4
x10 ~ x6
eta1 ~ x3
x5 ~ x3 + x9
