eta1 =~ y1 + y2 + y3
x1 ~ x5 + x7
x10 ~ x1
x7 ~ x10 + x6
x6 ~ x3
x8 ~ x10
x2 ~ x8
x9 ~ eta1 + x3
x4 ~ x5
