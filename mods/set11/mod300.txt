eta1 =~ y1 + y2
x3 ~ x5 + x9
x5 ~ x4 + x7
x1 ~ x5
eta1 ~ x2 + x9
x8 ~ x2
x6 ~ x4
x10 ~ x2
