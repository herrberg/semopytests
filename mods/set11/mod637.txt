eta1 =~ y1 + y2 + y3
x10 ~ x2 + x8
x6 ~ x10 + x7
x5 ~ x1 + x10 + x9
x3 ~ x7
eta1 ~ x2
x4 ~ eta1
