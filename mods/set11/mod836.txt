eta1 =~ y1 + y2
x10 ~ x7
x6 ~ x10 + x4
eta1 ~ x7
x3 ~ eta1 + x5
x8 ~ x7
x9 ~ x8
x4 ~ x2
x1 ~ x2
