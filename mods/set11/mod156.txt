eta1 =~ y1 + y2
x8 ~ x3 + x5
x1 ~ x8 + x9
x2 ~ x1
x4 ~ x10 + x2
x7 ~ x8
eta1 ~ x6 + x8
x3 ~ eta1
