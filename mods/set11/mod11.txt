eta1 =~ y1 + y2 + y3
eta1 ~ x2 + x4
x2 ~ x8
x9 ~ x2 + x3
x6 ~ x8
x10 ~ x6
x7 ~ x2
x1 ~ x3
x5 ~ x6
