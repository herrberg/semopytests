eta1 =~ y1 + y2
x8 ~ x2 + x3 + x9
x5 ~ x4 + x8
x2 ~ x5
x7 ~ x9
x6 ~ x7
x1 ~ x9
eta1 ~ x5
x10 ~ x5
