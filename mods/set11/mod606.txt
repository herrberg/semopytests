eta1 =~ y1 + y2 + y3
x3 ~ x9
x6 ~ x1 + x3 + x8
eta1 ~ x6 + x7
x5 ~ eta1 + x10 + x2
x7 ~ x5
x4 ~ x5
