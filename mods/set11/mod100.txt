eta1 =~ y1 + y2 + y3
x10 ~ x4 + x6
x8 ~ x10 + x2
x5 ~ x10 + x9
x1 ~ x10
x7 ~ x9
x3 ~ eta1 + x9
