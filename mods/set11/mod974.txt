eta1 =~ y1 + y2
x8 ~ x2 + x4 + x6
x3 ~ x5 + x8
x10 ~ x8 + x9
x7 ~ x10
x2 ~ x7
x1 ~ x2
eta1 ~ x8
