eta1 =~ y1 + y2
x5 ~ x10 + x2 + x7
x6 ~ x5
x7 ~ x3 + x6
x9 ~ x4 + x7
x8 ~ x5
x10 ~ x1
eta1 ~ x10
