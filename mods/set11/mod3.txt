eta1 =~ y1 + y2 + y3
x5 ~ eta1 + x8 + x9
eta1 ~ x6
x4 ~ x9
x2 ~ eta1 + x7
x7 ~ x3
x1 ~ x9
x10 ~ eta1
