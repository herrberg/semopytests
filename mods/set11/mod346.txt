eta1 =~ y1 + y2
x10 ~ x1 + x8 + x9
x7 ~ x10 + x6
x4 ~ x7
x9 ~ x4
eta1 ~ x8
x5 ~ x4
x3 ~ x1 + x2
