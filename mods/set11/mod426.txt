eta1 =~ y1 + y2
x3 ~ x4 + x8
x8 ~ x9
x1 ~ x4 + x6
x7 ~ x10 + x4
x5 ~ x4
x2 ~ x5
eta1 ~ x9
