eta1 =~ y1 + y2
x4 ~ x10 + x2 + x7
x6 ~ x4
x2 ~ x6 + x8
x8 ~ x9
x1 ~ x4 + x5
x5 ~ eta1
x3 ~ x9
