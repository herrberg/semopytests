eta1 =~ y1 + y2
x6 ~ x5 + x7
x4 ~ x3 + x6 + x8
x1 ~ x4 + x9
x7 ~ x4
x10 ~ x8
x2 ~ x9
eta1 ~ x5
