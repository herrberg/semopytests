eta1 =~ y1 + y2
eta1 ~ x1 + x5
x8 ~ eta1
x10 ~ x8
x3 ~ x10 + x6 + x7
x4 ~ x3
x5 ~ x4
x2 ~ x3
x9 ~ x8
