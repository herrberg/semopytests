eta1 =~ y1 + y2 + y3
x8 ~ x10
x1 ~ x5 + x6 + x8
x2 ~ x1
x6 ~ x2 + x3
x7 ~ x4 + x8
x9 ~ x3
eta1 ~ x4
