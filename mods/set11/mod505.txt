eta1 =~ y1 + y2
x4 ~ x3 + x7
x1 ~ x4 + x5
x7 ~ x1 + x6
x10 ~ x6
x8 ~ x10
eta1 ~ x3
x2 ~ x6
x9 ~ x6
