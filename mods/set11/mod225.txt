eta1 =~ y1 + y2 + y3
x3 ~ x5 + x8
x2 ~ x3
x8 ~ x2
x7 ~ x1 + x5 + x6
x9 ~ x7
x10 ~ x9
eta1 ~ x3
x4 ~ eta1
