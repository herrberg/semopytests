eta1 =~ y1 + y2 + y3
x8 ~ x2 + x5
x9 ~ x1 + x8
x6 ~ x3 + x9
x10 ~ x9
eta1 ~ x2
x7 ~ x2
x1 ~ x4
x5 ~ x9
