eta1 =~ y1 + y2
x5 ~ x4
x3 ~ x5 + x7
x8 ~ x3
x7 ~ x8
x6 ~ x4
eta1 ~ x5 + x9
x2 ~ x3
x10 ~ x3
x1 ~ x10
