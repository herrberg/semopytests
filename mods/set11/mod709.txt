eta1 =~ y1 + y2
x10 ~ x7 + x8
x7 ~ x5
x2 ~ x1 + x5 + x9
x4 ~ eta1 + x8
x6 ~ x4
eta1 ~ x6
x3 ~ x9
