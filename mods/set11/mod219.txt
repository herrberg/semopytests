eta1 =~ y1 + y2
x5 ~ x4 + x7 + x9
x7 ~ x10 + x3 + x8
x2 ~ x9
x6 ~ x7
x3 ~ x6
eta1 ~ x9
x1 ~ eta1
