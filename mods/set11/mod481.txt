eta1 =~ y1 + y2
x10 ~ x4 + x9
x2 ~ x10 + x3
x1 ~ x2 + x7
eta1 ~ x1
x8 ~ eta1
x9 ~ x2
x7 ~ x6
x5 ~ eta1
