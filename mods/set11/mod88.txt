eta1 =~ y1 + y2
x7 ~ x10 + x4
x8 ~ x5 + x7
x6 ~ x8
x5 ~ x1 + x9
eta1 ~ x5
x1 ~ eta1
x3 ~ eta1
x2 ~ x8
