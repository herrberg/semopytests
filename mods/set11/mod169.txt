eta1 =~ y1 + y2
x5 ~ x9
x3 ~ x2 + x5
x8 ~ x1 + x3
x2 ~ x8
x1 ~ eta1
x10 ~ x1 + x4
x6 ~ x10
x7 ~ eta1
