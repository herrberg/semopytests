eta1 =~ y1 + y2
x10 ~ eta1 + x1 + x8
x3 ~ x10
x1 ~ x2 + x3
x5 ~ x10 + x9
x4 ~ x5
x6 ~ x10
x7 ~ x2
