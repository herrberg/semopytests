eta1 =~ y1 + y2 + y3
x10 ~ x1 + x8
x5 ~ x10
x1 ~ x5 + x7
eta1 ~ x8
x3 ~ eta1 + x4
x9 ~ x1
x6 ~ x8
x2 ~ eta1
