eta1 =~ y1 + y2 + y3
x7 ~ x1
x2 ~ x1 + x8
x10 ~ x2
x4 ~ x10 + x3
x8 ~ x4 + x5
x9 ~ eta1 + x8
x6 ~ x3
