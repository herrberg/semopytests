eta1 =~ y1 + y2
x1 ~ x5 + x7
eta1 ~ x1
x9 ~ eta1 + x10
x4 ~ x9
x2 ~ x3 + x4
x5 ~ eta1
x8 ~ x5
x6 ~ x1
