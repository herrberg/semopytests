eta1 =~ y1 + y2
eta1 ~ x6 + x9
x7 ~ eta1 + x8
x4 ~ x2 + x5 + x7
x10 ~ x4
x1 ~ x7
x6 ~ x3 + x7
