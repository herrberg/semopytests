eta1 =~ y1 + y2 + y3
x4 ~ x2 + x3
x6 ~ x4
x2 ~ x6
eta1 ~ x2
x10 ~ x4
x1 ~ x10
x9 ~ x1 + x5
x7 ~ x9
x8 ~ x3
