eta1 =~ y1 + y2 + y3
x1 ~ x5 + x9
x8 ~ x1
x9 ~ x8
x2 ~ x8
x6 ~ x2
x3 ~ x1 + x10
x4 ~ x8
eta1 ~ x2
x7 ~ x10
