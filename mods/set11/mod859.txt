eta1 =~ y1 + y2
x5 ~ x2 + x3 + x7
x9 ~ x4 + x5
x6 ~ x5
x2 ~ x6
x8 ~ x5
x10 ~ x8
x1 ~ x3
eta1 ~ x7
