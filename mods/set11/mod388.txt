eta1 =~ y1 + y2
x1 ~ x10 + x3 + x8
x2 ~ x1 + x9
x7 ~ x2
x10 ~ x7
eta1 ~ x10
x5 ~ x6 + x9
x4 ~ x8
