eta1 =~ y1 + y2 + y3
x9 ~ eta1 + x7
x2 ~ x10 + x5 + x9
x6 ~ x9
x4 ~ x6
x7 ~ x6
x5 ~ x1
x3 ~ x5
x8 ~ x1
