eta1 =~ y1 + y2
x7 ~ x2
x5 ~ x7
x10 ~ x4 + x5 + x6
x8 ~ x10
x6 ~ eta1 + x8
x9 ~ x6
eta1 ~ x1
x3 ~ x4
