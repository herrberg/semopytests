eta1 =~ y1 + y2 + y3
x7 ~ x2 + x3
x8 ~ x7
x3 ~ x8
eta1 ~ x10 + x3 + x4
x6 ~ eta1 + x1
x9 ~ x6
x5 ~ x6
