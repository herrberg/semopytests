eta1 =~ y1 + y2
x7 ~ eta1 + x1
x5 ~ x7
eta1 ~ x10 + x5 + x8
x10 ~ x6
x4 ~ x10
x9 ~ x2 + x7
x3 ~ x6
