eta1 =~ y1 + y2
x4 ~ x6 + x8
x3 ~ x4
x7 ~ eta1 + x3
x10 ~ eta1
x2 ~ eta1
x5 ~ x4
x9 ~ x3
x6 ~ x3
x1 ~ x3
