eta1 =~ y1 + y2 + y3
x1 ~ x4 + x7
x5 ~ x1 + x10 + x3
x4 ~ x5
x6 ~ x1 + x2
x8 ~ eta1 + x1
x9 ~ x1
