eta1 =~ y1 + y2
x5 ~ x1 + x6 + x8
x7 ~ x5
x9 ~ x10 + x6
x3 ~ eta1 + x8
x4 ~ x10
x2 ~ x1
