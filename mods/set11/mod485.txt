eta1 =~ y1 + y2
x9 ~ x6 + x8
x5 ~ x2 + x9
x8 ~ x1 + x5
x7 ~ x8
x1 ~ x4
x10 ~ x4
x3 ~ x2
eta1 ~ x9
