eta1 =~ y1 + y2 + y3
x7 ~ eta1
x9 ~ x10 + x7
x3 ~ x9
x4 ~ x2 + x3
x6 ~ x4
x1 ~ x6
x8 ~ x9
x10 ~ x8
x5 ~ x6
