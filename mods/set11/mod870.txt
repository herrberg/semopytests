eta1 =~ y1 + y2 + y3
x1 ~ x2 + x7
x5 ~ x1 + x6
x10 ~ x3 + x5
x2 ~ x10
eta1 ~ x2
x4 ~ x10 + x8
x9 ~ x5
