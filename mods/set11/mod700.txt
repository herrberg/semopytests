eta1 =~ y1 + y2 + y3
x9 ~ x10
x8 ~ eta1 + x9
x1 ~ x4 + x8
x3 ~ x1 + x6
x2 ~ x4
x7 ~ x2
x5 ~ x8
eta1 ~ x5
