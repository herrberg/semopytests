eta1 =~ y1 + y2
x7 ~ x9
x8 ~ x6 + x7
x4 ~ x9
x10 ~ x2 + x4
x5 ~ x3 + x9
eta1 ~ x5
x3 ~ eta1 + x1
