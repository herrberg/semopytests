eta1 =~ y1 + y2 + y3
x9 ~ x1 + x5 + x7
eta1 ~ x9
x7 ~ eta1 + x10 + x4 + x8
x3 ~ x6 + x7
x2 ~ x1
