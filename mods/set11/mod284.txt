eta1 =~ y1 + y2
x2 ~ x1
x10 ~ x1 + x7 + x9
x8 ~ x10
x7 ~ x6 + x8
x4 ~ x1
x5 ~ x4
eta1 ~ x1
x3 ~ x4
