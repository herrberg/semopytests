eta1 =~ y1 + y2 + y3
x9 ~ x2 + x5 + x6 + x7
x4 ~ eta1 + x1 + x9
x2 ~ x4
x10 ~ x2
x3 ~ x5
x8 ~ x6
