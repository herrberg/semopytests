eta1 =~ y1 + y2 + y3
x9 ~ x2 + x4 + x6
x2 ~ eta1
x5 ~ x4
x1 ~ x5
x7 ~ x4 + x8
x6 ~ x3
x10 ~ x2
