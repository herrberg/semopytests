eta1 =~ y1 + y2
x8 ~ x1 + x10 + x3 + x9
eta1 ~ x10 + x2
x5 ~ eta1
x7 ~ x9
x1 ~ x6
x4 ~ x10
