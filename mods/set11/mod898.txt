eta1 =~ y1 + y2 + y3
x1 ~ x4 + x5
x7 ~ x1 + x10
x6 ~ x1
x5 ~ x6
x10 ~ eta1 + x3
x9 ~ x4
x8 ~ x4
x2 ~ x1
