eta1 =~ y1 + y2 + y3
x3 ~ x2 + x6
eta1 ~ x3 + x4 + x8 + x9
x7 ~ eta1
x10 ~ x3
x2 ~ eta1
x1 ~ x9
x5 ~ x3
