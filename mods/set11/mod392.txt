eta1 =~ y1 + y2
x7 ~ x6
x10 ~ x1 + x4 + x7 + x8 + x9
x5 ~ eta1 + x10
x9 ~ x3
x4 ~ x5
x2 ~ x1
