eta1 =~ y1 + y2 + y3
x2 ~ eta1 + x6 + x8
x7 ~ x2
x9 ~ x2
x8 ~ x4 + x9
eta1 ~ x10
x1 ~ eta1 + x3 + x5
