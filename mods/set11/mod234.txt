eta1 =~ y1 + y2 + y3
eta1 ~ x5
x8 ~ eta1 + x4
x7 ~ x8
x10 ~ x7
x2 ~ x10
x6 ~ x8 + x9
x4 ~ x1 + x7
x3 ~ eta1
