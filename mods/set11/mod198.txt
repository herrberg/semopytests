eta1 =~ y1 + y2
x5 ~ x7
x3 ~ x5
x9 ~ x10 + x5
x6 ~ eta1 + x5
x4 ~ x7
x8 ~ eta1 + x1
x2 ~ x5
