eta1 =~ y1 + y2
eta1 ~ x2
x10 ~ eta1 + x7
x6 ~ x10
x3 ~ x1 + x6 + x9
x7 ~ x6
x1 ~ x5
x8 ~ x1
x4 ~ eta1
