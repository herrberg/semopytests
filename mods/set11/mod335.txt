eta1 =~ y1 + y2
x8 ~ x10 + x2
x4 ~ x8
x6 ~ x1 + x3 + x4
x5 ~ x6
eta1 ~ x5
x10 ~ x6 + x7
x9 ~ x4
