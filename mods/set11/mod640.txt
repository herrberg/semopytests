eta1 =~ y1 + y2 + y3
x6 ~ x2 + x7
eta1 ~ x6
x9 ~ x6 + x8
x2 ~ x10 + x9
x4 ~ x5 + x7
x3 ~ x4
x1 ~ x2
