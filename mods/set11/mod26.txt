eta1 =~ y1 + y2 + y3
x4 ~ x3 + x5
x8 ~ x1 + x10 + x4 + x9
x7 ~ x6 + x8
x3 ~ x8
x2 ~ x5
eta1 ~ x5
