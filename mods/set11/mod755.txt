eta1 =~ y1 + y2 + y3
x9 ~ x10 + x8
x6 ~ x10 + x4 + x7
x1 ~ x6
x2 ~ x1
x7 ~ x2
eta1 ~ x10
x3 ~ x6
x5 ~ x4
