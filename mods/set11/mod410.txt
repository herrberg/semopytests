eta1 =~ y1 + y2 + y3
x7 ~ x3
x5 ~ x10 + x7 + x9
x8 ~ x10
x6 ~ x4 + x8
eta1 ~ x7
x2 ~ x9
x1 ~ x9
