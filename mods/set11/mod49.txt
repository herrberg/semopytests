eta1 =~ y1 + y2
x7 ~ x3 + x6
x9 ~ x7
x5 ~ x2 + x7
x1 ~ x7
x6 ~ x10 + x8
eta1 ~ x4 + x8
