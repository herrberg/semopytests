eta1 =~ y1 + y2
x5 ~ x2 + x4
x9 ~ x10 + x5
x4 ~ x9
x3 ~ x4
x7 ~ x3
x6 ~ x2
eta1 ~ x6 + x8
x1 ~ x6
