eta1 =~ y1 + y2 + y3
x9 ~ x7 + x8
x2 ~ eta1 + x9
x4 ~ x2
eta1 ~ x10 + x4
x1 ~ eta1 + x6
x5 ~ x9
x3 ~ x8
