eta1 =~ y1 + y2 + y3
x8 ~ x1 + x6
x10 ~ x8
x6 ~ eta1 + x10 + x3 + x4 + x5
x7 ~ x5
x2 ~ x3
x9 ~ x8
