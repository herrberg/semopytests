eta1 =~ y1 + y2 + y3
x5 ~ eta1 + x7
x2 ~ x5 + x6
x9 ~ x10 + x2 + x8
x1 ~ x9
x7 ~ x3 + x4 + x9
