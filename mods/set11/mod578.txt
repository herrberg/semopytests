eta1 =~ y1 + y2 + y3
x2 ~ x10 + x4 + x9
x1 ~ x2
x10 ~ x1
x5 ~ x1 + x3
x6 ~ x2
x7 ~ x6
eta1 ~ x6
x8 ~ x3
