eta1 =~ y1 + y2
x6 ~ eta1 + x8
x10 ~ eta1
x5 ~ x10
x4 ~ eta1 + x1 + x3
x2 ~ eta1
x7 ~ x2 + x9
