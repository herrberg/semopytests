eta1 =~ y1 + y2
x8 ~ x7 + x9
x5 ~ x8
x2 ~ x5
x9 ~ x4 + x5
x3 ~ x7
eta1 ~ x7
x10 ~ eta1
x1 ~ x10
x6 ~ x7
