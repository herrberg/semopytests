eta1 =~ y1 + y2 + y3
x1 ~ x2 + x5
x4 ~ x1
x2 ~ x4
x3 ~ x2 + x6
x10 ~ x4
x8 ~ x5
x7 ~ x1 + x9
eta1 ~ x7
