eta1 =~ y1 + y2
x9 ~ eta1 + x7
x6 ~ x9
x4 ~ x3 + x6
x8 ~ x7
x2 ~ x1 + x10 + x7
x5 ~ x2
x1 ~ x5
