eta1 =~ y1 + y2
x5 ~ x10
x3 ~ x5
eta1 ~ x2 + x5 + x8 + x9
x8 ~ x1 + x6
x4 ~ x1 + x7
