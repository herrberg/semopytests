eta1 =~ y1 + y2
x2 ~ x4 + x7
x8 ~ x10 + x2 + x6
x1 ~ x8
x6 ~ eta1 + x9
x3 ~ x8
x9 ~ x3
x5 ~ x2
