eta1 =~ y1 + y2 + y3
eta1 ~ x3 + x5 + x8
x6 ~ eta1 + x1
x8 ~ x6
x9 ~ x10 + x3
x2 ~ x9
x7 ~ x9
x4 ~ x3
