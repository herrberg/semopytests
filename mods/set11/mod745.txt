eta1 =~ y1 + y2
x9 ~ eta1 + x10 + x7
x1 ~ x9
x2 ~ x1
x8 ~ x7
x4 ~ x3 + x9
eta1 ~ x1 + x5
x6 ~ x9
