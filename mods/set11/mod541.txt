eta1 =~ y1 + y2
x4 ~ x1 + x9
x3 ~ x4
x1 ~ x10 + x2 + x3
x8 ~ x3
x5 ~ x8
x2 ~ x6
eta1 ~ x4
x7 ~ x3
