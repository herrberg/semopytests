eta1 =~ y1 + y2
x10 ~ x2 + x3 + x8
x3 ~ eta1 + x1 + x6 + x7
x2 ~ x4
x5 ~ x6
x9 ~ x7
