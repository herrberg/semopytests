eta1 =~ y1 + y2 + y3
x4 ~ x3 + x9
x8 ~ eta1 + x4 + x5
x7 ~ x1 + x8
x5 ~ x7
x6 ~ x9
x2 ~ x7
x10 ~ x9
