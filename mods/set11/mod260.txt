eta1 =~ y1 + y2 + y3
x1 ~ x3
x2 ~ x1 + x7
eta1 ~ x2 + x5
x8 ~ eta1
x9 ~ eta1
x5 ~ x9
x4 ~ eta1
x6 ~ x3
x10 ~ x9
