eta1 =~ y1 + y2
x9 ~ eta1 + x8
x2 ~ x1 + x9
x6 ~ x2
eta1 ~ x3 + x4 + x6 + x7
x10 ~ x2
x5 ~ x10
