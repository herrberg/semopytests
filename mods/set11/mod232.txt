eta1 =~ y1 + y2
eta1 ~ x10 + x7 + x8
x1 ~ eta1
x5 ~ x1
x10 ~ x5
x6 ~ x4 + x7
x4 ~ x3
x2 ~ x8
x9 ~ x8
