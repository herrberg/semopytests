eta1 =~ y1 + y2
eta1 ~ x1 + x2
x5 ~ eta1 + x3 + x6 + x7
x10 ~ eta1
x1 ~ x10 + x8
x4 ~ x1
x7 ~ x9
