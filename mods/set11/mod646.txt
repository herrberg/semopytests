eta1 =~ y1 + y2
x9 ~ eta1 + x5
x10 ~ x3 + x7 + x9
x5 ~ x10 + x8
x3 ~ x1
x2 ~ eta1 + x4
x6 ~ x7
