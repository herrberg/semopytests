eta1 =~ y1 + y2
x4 ~ x8 + x9
x6 ~ x1 + x4
x1 ~ x10
x7 ~ x4 + x5
x9 ~ x7
x2 ~ x7
x3 ~ x5
eta1 ~ x4
