eta1 =~ y1 + y2 + y3
x8 ~ eta1 + x2 + x6 + x9
x7 ~ x8
x10 ~ x3 + x7
x1 ~ x7
x4 ~ x7
x6 ~ x4
x5 ~ x7
