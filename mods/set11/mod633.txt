eta1 =~ y1 + y2 + y3
x8 ~ x10 + x7
x5 ~ x8
x2 ~ x3 + x5
x1 ~ x8
x7 ~ x1
x3 ~ x9
x6 ~ eta1 + x4 + x8
