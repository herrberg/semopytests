eta1 =~ y1 + y2
x9 ~ x10
x4 ~ x7 + x9
x8 ~ x4
x7 ~ x3 + x5
x1 ~ x7
x5 ~ x1
x6 ~ x9
x2 ~ x3
eta1 ~ x7
