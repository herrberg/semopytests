eta1 =~ y1 + y2
x3 ~ x1 + x2
x5 ~ x3
x2 ~ x10 + x5
eta1 ~ x2
x4 ~ eta1
x8 ~ x3
x7 ~ x8
x10 ~ x9
x6 ~ x2
