eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x3 ~ eta2 + x6 + x9
x8 ~ x3
eta1 ~ x8
x10 ~ x4
x9 ~ x10
x2 ~ x5 + x6
x7 ~ x6
x1 ~ x5
eta2 ~ x8
