eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta1 ~ x1
x7 ~ eta1 + x10 + x5
x3 ~ x7
x10 ~ x3 + x8
x6 ~ eta1 + x9
x4 ~ eta2 + x10 + x2
