eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x5 ~ x2 + x3
x9 ~ x5 + x8
eta2 ~ x10 + x4 + x9
x3 ~ eta2
x8 ~ eta1
x1 ~ x9
x6 ~ x2
x7 ~ eta1
