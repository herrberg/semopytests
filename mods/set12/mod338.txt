eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x5 ~ x8
x3 ~ x7 + x8
eta2 ~ x3
x7 ~ eta1 + eta2
x4 ~ x8
x10 ~ x4
x9 ~ x8
x6 ~ x9
x2 ~ x8
x1 ~ x9
