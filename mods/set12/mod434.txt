eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x5 ~ x8 + x9
x2 ~ eta2 + x5
x10 ~ x2 + x6
x3 ~ x2
x4 ~ x3
eta1 ~ x4
x7 ~ x5
x8 ~ x7
x1 ~ x5
