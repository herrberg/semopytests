eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta2 ~ x10 + x2 + x3 + x4
x2 ~ eta1 + x9
x5 ~ x3
x6 ~ x7 + x9
x10 ~ x1
x8 ~ x4
