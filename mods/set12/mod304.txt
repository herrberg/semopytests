eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta1 ~ x1 + x3 + x9
x8 ~ eta1
x6 ~ x8
eta2 ~ x4 + x6
x2 ~ eta2
x7 ~ eta1 + x5
x3 ~ x7
x10 ~ eta1
