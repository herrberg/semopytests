eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x10 ~ x5
x6 ~ x10
x7 ~ x6 + x9
x8 ~ x7
x9 ~ eta1 + x1 + x8
eta2 ~ x4 + x9
x3 ~ eta1
x2 ~ x4
