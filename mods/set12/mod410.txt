eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta2 ~ x10 + x5 + x7 + x9
x4 ~ eta2 + x2
x6 ~ x4 + x8
eta1 ~ eta2 + x1
x7 ~ x3 + x4
