eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x6 ~ x2 + x7
x4 ~ x6
x2 ~ x4 + x8
eta1 ~ x5 + x6
x3 ~ eta1
x1 ~ x7
x5 ~ eta2
x10 ~ eta1
x9 ~ eta2
