eta1 =~ y1 + y2
eta2 =~ y3 + y4
x6 ~ eta2 + x4
x5 ~ x3 + x6
eta2 ~ x5 + x9
x10 ~ eta2
eta1 ~ x5
x1 ~ x4
x8 ~ x6 + x7
x2 ~ x6
