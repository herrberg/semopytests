eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x3 ~ x10 + x2
x7 ~ eta2 + x10 + x6 + x8
x4 ~ eta1 + x7
x8 ~ x4
x2 ~ x1
x9 ~ x10
x5 ~ x2
