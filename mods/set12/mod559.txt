eta1 =~ y1 + y2
eta2 =~ y3 + y4
x6 ~ x10 + x3
x4 ~ eta2 + x2 + x6
x5 ~ x4
x8 ~ eta1 + x1 + x6
x10 ~ x8
x7 ~ x1
x9 ~ x7
