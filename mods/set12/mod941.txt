eta1 =~ y1 + y2
eta2 =~ y3 + y4
x8 ~ eta1 + eta2 + x2 + x5
x9 ~ x8
x10 ~ x9
x3 ~ x10
x6 ~ x8
eta2 ~ x9
x1 ~ x8
x4 ~ x5
x7 ~ x2
