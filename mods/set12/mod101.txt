eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x9 ~ x10 + x5
x6 ~ eta1 + x9
x5 ~ eta2 + x2 + x6
x1 ~ x10
x4 ~ x1
x2 ~ x7 + x8
x3 ~ x2
