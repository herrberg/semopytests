eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x7 ~ eta1 + x6
x9 ~ x10 + x3 + x7
x2 ~ x7
x5 ~ x6
eta2 ~ x1 + x7
eta1 ~ eta2
x8 ~ x10
x4 ~ x8
