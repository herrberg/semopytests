eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x6 ~ eta2 + x7
x5 ~ x1 + x6 + x9
x7 ~ x4 + x5
x2 ~ x7
x10 ~ x4
eta1 ~ x7 + x8
x3 ~ x9
