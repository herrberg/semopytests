eta1 =~ y1 + y2
eta2 =~ y3 + y4
x8 ~ x4
eta1 ~ x3 + x4 + x6
x2 ~ eta2 + x1 + x4
x5 ~ x2
eta2 ~ x5
x3 ~ x10 + x9
x1 ~ x7
