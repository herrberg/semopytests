eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x8 ~ eta1 + x1 + x9
x7 ~ eta2 + x2 + x3 + x4 + x8
x9 ~ x7
x6 ~ x1 + x10
x5 ~ x6
