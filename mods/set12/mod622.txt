eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x9 ~ eta1
eta2 ~ x9
x3 ~ eta2
x7 ~ eta2 + x6
x6 ~ x1 + x2 + x4
x5 ~ x6
x4 ~ x5
x8 ~ x2
x10 ~ x8
