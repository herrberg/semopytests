eta1 =~ y1 + y2
eta2 =~ y3 + y4
x7 ~ x1 + x10
x6 ~ x2 + x7
x3 ~ x6
eta1 ~ eta2 + x7
x8 ~ x7
x1 ~ x8
x9 ~ x7
x5 ~ x2
x4 ~ x10
