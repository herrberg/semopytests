eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x5 ~ eta1
eta2 ~ x5
x8 ~ x10 + x4 + x5
x1 ~ x5 + x9
x3 ~ x9
x6 ~ eta1 + x2
x7 ~ x6
