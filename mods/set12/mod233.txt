eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x5 ~ eta2 + x6
x7 ~ x3 + x4 + x5
x9 ~ eta2
x2 ~ x9
x4 ~ x1 + x10
eta1 ~ x10
x8 ~ eta2
