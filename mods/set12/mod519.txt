eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x4 ~ eta1 + x1 + x10 + x7
x6 ~ x4
eta1 ~ x2 + x9
x7 ~ eta2
x9 ~ x3 + x5 + x8
x8 ~ eta1
