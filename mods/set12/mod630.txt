eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x7 ~ x3 + x5
x9 ~ x10
x5 ~ x9
x1 ~ x3 + x6 + x8
x4 ~ x1
x6 ~ x2 + x4
eta2 ~ x6
eta1 ~ x6
