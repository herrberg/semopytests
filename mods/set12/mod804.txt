eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x5 ~ x10 + x8
eta2 ~ x5
eta1 ~ eta2
x2 ~ eta1
x4 ~ x10
x8 ~ eta2 + x3 + x6
x3 ~ x7 + x9
x1 ~ x6
