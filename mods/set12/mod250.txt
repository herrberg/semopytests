eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x4 ~ eta1
x3 ~ x4
x2 ~ x3 + x6
x1 ~ x3 + x7
x5 ~ eta1 + x8
x9 ~ x5
x10 ~ x9
x8 ~ eta2 + x9
