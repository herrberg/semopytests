eta1 =~ y1 + y2
eta2 =~ y3 + y4
x4 ~ eta2 + x1
x10 ~ eta1 + x4
x6 ~ x10
eta1 ~ x6 + x8
x2 ~ x4 + x7
x3 ~ eta1 + x9
x5 ~ x4
