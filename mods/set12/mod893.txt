eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x5 ~ x1 + x3 + x8
eta2 ~ x5
x8 ~ eta1 + eta2 + x4
x3 ~ x10
x4 ~ x7
x2 ~ x1
eta1 ~ x6
x9 ~ eta1
