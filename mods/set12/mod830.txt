eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x8 ~ x9
x1 ~ eta2 + x2 + x8
x3 ~ x9
x4 ~ eta1 + x3
eta1 ~ x10 + x7
x6 ~ eta1
x5 ~ x8
