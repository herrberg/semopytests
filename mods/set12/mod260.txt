eta1 =~ y1 + y2
eta2 =~ y3 + y4
x6 ~ x1 + x3 + x4
x9 ~ x6
x2 ~ x1 + x10 + x7
eta2 ~ eta1 + x2
x1 ~ eta2 + x5
x8 ~ x1
