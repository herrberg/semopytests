eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x9 ~ eta2 + x10 + x5
x4 ~ x2 + x9
x10 ~ x3 + x4
x8 ~ x9
x1 ~ x8
x6 ~ x8
x7 ~ x8
eta1 ~ x5
