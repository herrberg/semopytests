eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x3 ~ eta2 + x8 + x9
x2 ~ x10 + x8
x6 ~ x10 + x7
eta1 ~ x6
x10 ~ eta1 + x4
x5 ~ x8
x1 ~ x9
