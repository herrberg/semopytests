eta1 =~ y1 + y2
eta2 =~ y3 + y4
x6 ~ x2
eta1 ~ x6
x3 ~ eta1 + x1 + x10 + x5 + x9
x8 ~ x3
x5 ~ x8
x4 ~ x3
x10 ~ eta2
x7 ~ x8
