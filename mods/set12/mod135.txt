eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x9 ~ x2 + x7
x6 ~ x9
eta2 ~ x1 + x6
x8 ~ eta2 + x5
x3 ~ x8
x7 ~ eta2
eta1 ~ x4 + x6
x10 ~ x8
