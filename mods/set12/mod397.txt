eta1 =~ y1 + y2
eta2 =~ y3 + y4
x8 ~ x3 + x4 + x6 + x9
eta2 ~ x8
eta1 ~ eta2 + x1 + x7
x3 ~ x5
x4 ~ eta2
x10 ~ x1 + x2
