eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x4 ~ eta2 + x10 + x2 + x8
x3 ~ x4
x1 ~ x3
x8 ~ x6
x7 ~ x4 + x5
eta1 ~ x7
eta2 ~ eta1
x9 ~ eta1
