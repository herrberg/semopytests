eta1 =~ y1 + y2
eta2 =~ y3 + y4
x6 ~ x1 + x5 + x9
x7 ~ x6
x1 ~ eta1 + x7
x4 ~ eta2 + x5
x8 ~ x4
x3 ~ x9
x2 ~ x7
x10 ~ eta2
