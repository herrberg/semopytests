eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x4 ~ x6 + x9
x3 ~ x1 + x4
x9 ~ eta2 + x3
eta1 ~ x4
x1 ~ x5
x10 ~ x6 + x8
x2 ~ eta2 + x7
