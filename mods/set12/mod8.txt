eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x4 ~ x2 + x6
x5 ~ eta1 + x4
x10 ~ x1 + x5
x8 ~ x5 + x7
eta2 ~ x5
x3 ~ eta1
x9 ~ x4
x6 ~ x5
