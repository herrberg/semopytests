eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x4 ~ x1 + x9
x2 ~ eta2 + x4 + x5
eta2 ~ eta1 + x10
x8 ~ x10
x3 ~ x1
x6 ~ x10
x7 ~ x4
x9 ~ x7
