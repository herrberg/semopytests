eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta1 ~ eta2 + x8
x9 ~ eta1 + x3 + x5
x2 ~ eta2
x5 ~ x1
x4 ~ eta2 + x6
x3 ~ x10
x7 ~ eta2
