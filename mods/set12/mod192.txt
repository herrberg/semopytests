eta1 =~ y1 + y2
eta2 =~ y3 + y4
x4 ~ eta2 + x3
x5 ~ x4
eta1 ~ x5
x3 ~ x6
x7 ~ x4 + x9
x1 ~ x7
x9 ~ x1
x2 ~ x4
x10 ~ x3
x8 ~ x3
