eta1 =~ y1 + y2
eta2 =~ y3 + y4
x5 ~ x2 + x3 + x9
x1 ~ x5
x9 ~ x1 + x6
eta1 ~ x2
x4 ~ x2
x10 ~ x4 + x8
eta2 ~ x4
x7 ~ x1
