eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x2 ~ eta1 + x10 + x4
eta1 ~ x7 + x9
x3 ~ eta1
x9 ~ x1 + x3
x8 ~ x9
x6 ~ x4
x5 ~ eta1
eta2 ~ x10
