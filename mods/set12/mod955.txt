eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x7 ~ x8
eta2 ~ x3 + x7
x10 ~ eta2 + x1
x3 ~ x10 + x5
x9 ~ eta1 + x3
x4 ~ x5
x6 ~ x3
x2 ~ eta1
