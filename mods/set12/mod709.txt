eta1 =~ y1 + y2
eta2 =~ y3 + y4
x1 ~ x6
x4 ~ x1 + x10 + x9
x2 ~ x4
x10 ~ x2 + x7 + x8
x5 ~ x3 + x6
eta2 ~ x3
eta1 ~ x6
