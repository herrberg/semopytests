eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x5 ~ x2 + x4
x8 ~ eta1 + eta2 + x5
x7 ~ x8
x6 ~ x8
eta2 ~ x6
x9 ~ eta2
eta1 ~ x10 + x3
x1 ~ eta1
