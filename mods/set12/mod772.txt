eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x2 ~ x6 + x9
x4 ~ x2
x1 ~ x4 + x5
eta1 ~ x1
x9 ~ x10 + x4
x7 ~ x2
x8 ~ x7
x3 ~ x6
eta2 ~ x10
