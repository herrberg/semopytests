eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x2 ~ x3 + x4
x7 ~ x1 + x2 + x5 + x8
eta2 ~ x2
x10 ~ x1
x6 ~ x2
x4 ~ eta1 + x6
x5 ~ x9
