eta1 =~ y1 + y2
eta2 =~ y3 + y4
x4 ~ eta1 + eta2 + x8
x3 ~ x4 + x7
eta2 ~ x10 + x3 + x9
x2 ~ x8
x9 ~ x6
x5 ~ x6
x1 ~ x6
