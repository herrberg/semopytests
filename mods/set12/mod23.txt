eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x3 ~ eta2 + x1 + x10 + x2
x4 ~ x1
x8 ~ x1
x9 ~ eta1 + x2 + x5
x6 ~ x2
x7 ~ x6
