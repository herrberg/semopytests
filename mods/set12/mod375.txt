eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta2 ~ x2
x6 ~ eta2 + x10
x5 ~ x6
x1 ~ eta1 + x2
x4 ~ x1
x10 ~ x3 + x8
x8 ~ x6
x9 ~ x8
x7 ~ x1
