eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x3 ~ eta2 + x8 + x9
x10 ~ x2 + x3
x1 ~ x10 + x7
eta1 ~ x1
x6 ~ x8
x2 ~ x1
x7 ~ x5
x4 ~ x1
