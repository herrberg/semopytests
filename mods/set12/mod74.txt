eta1 =~ y1 + y2
eta2 =~ y3 + y4
x7 ~ x2 + x3 + x5
x9 ~ x7
x5 ~ eta1 + x8 + x9
x4 ~ x5
eta2 ~ x4
x6 ~ x10 + x9
x1 ~ eta1
