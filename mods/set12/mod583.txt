eta1 =~ y1 + y2
eta2 =~ y3 + y4
x8 ~ x1 + x4 + x7
x5 ~ x8
x9 ~ x5
x7 ~ x10 + x3 + x6
x1 ~ x2 + x5
eta2 ~ x7
eta1 ~ x10
