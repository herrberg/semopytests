eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x5 ~ x2 + x7
x6 ~ x3 + x5 + x8
x1 ~ x6
x2 ~ x1
x9 ~ eta2 + x1 + x4
x4 ~ x10
eta1 ~ x7
