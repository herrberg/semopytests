eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta2 ~ x7
x8 ~ eta2
x1 ~ x3 + x8
x9 ~ eta2
x6 ~ x4 + x9
x2 ~ x6
x4 ~ x2 + x5
eta1 ~ eta2
x10 ~ x4
