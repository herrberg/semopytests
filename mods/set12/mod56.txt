eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x1 ~ x2 + x3 + x4 + x5 + x7 + x8
eta1 ~ x1
x7 ~ eta1
x10 ~ x1
x9 ~ x3
x6 ~ x9
eta2 ~ x8
