eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x4 ~ eta2 + x1 + x3 + x6
x8 ~ x2 + x4
x1 ~ x8
x10 ~ x1
x5 ~ x6
eta2 ~ eta1
x9 ~ x3
x7 ~ x3
