eta1 =~ y1 + y2
eta2 =~ y3 + y4
x2 ~ x6
x7 ~ x2
eta1 ~ x1 + x6
x10 ~ eta1 + x9
x8 ~ x10 + x3 + x4
x1 ~ x8
eta2 ~ x9
x5 ~ x4
