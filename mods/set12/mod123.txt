eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta2 ~ eta1 + x1
x3 ~ eta2 + x5
x6 ~ x3
x5 ~ x6
x2 ~ eta2
x9 ~ eta1 + x4
x7 ~ x9
x10 ~ eta1
x8 ~ x10
