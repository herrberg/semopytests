eta1 =~ y1 + y2
eta2 =~ y3 + y4
x4 ~ x1 + x10 + x7
x1 ~ x5 + x9
x10 ~ x3
eta1 ~ x1
eta2 ~ x3
x8 ~ x5
x2 ~ x5 + x6
