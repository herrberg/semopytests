eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x7 ~ eta2 + x1
x3 ~ x2 + x4 + x7
eta1 ~ x7
x6 ~ x2
x9 ~ x5 + x7
x8 ~ x1
x10 ~ x1
