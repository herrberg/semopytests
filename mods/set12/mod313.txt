eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x4 ~ eta2 + x8
x1 ~ x4 + x5
x5 ~ x10 + x2
x9 ~ x5 + x6
x8 ~ eta1
x7 ~ eta2 + x3
