eta1 =~ y1 + y2
eta2 =~ y3 + y4
x2 ~ eta2
x3 ~ x2
x7 ~ x2 + x6
x10 ~ x4 + x7
x6 ~ x5 + x9
eta1 ~ x7
x5 ~ x7
x8 ~ x6
x1 ~ x2
