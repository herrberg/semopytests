eta1 =~ y1 + y2
eta2 =~ y3 + y4
x7 ~ x1 + x4
x8 ~ x5 + x7
eta1 ~ x6 + x8
x2 ~ eta1
eta2 ~ x2
x3 ~ eta2
x9 ~ x2
x4 ~ x9
x10 ~ x7
