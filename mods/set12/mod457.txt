eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x9 ~ x1 + x2
x3 ~ x9
x10 ~ eta2 + x3 + x7
x2 ~ x10 + x4 + x8
eta1 ~ x10
x5 ~ x3
x6 ~ x4
