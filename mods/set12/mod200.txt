eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x1 ~ x2 + x6 + x8 + x9
x10 ~ eta2 + x1
x8 ~ x10 + x4
x7 ~ x3 + x6
eta1 ~ x7
x5 ~ x3
