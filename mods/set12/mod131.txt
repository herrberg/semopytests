eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x8 ~ x3 + x9
x10 ~ eta1 + x2 + x8
x3 ~ x10
x6 ~ x3
x7 ~ x5 + x8
x4 ~ x2
eta2 ~ x2
x1 ~ x9
