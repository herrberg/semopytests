eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta1 ~ x6 + x7
x4 ~ eta1
x1 ~ x4
x6 ~ x1 + x9
x5 ~ x2 + x7
x3 ~ x4
eta2 ~ x1
x10 ~ x9
x8 ~ x10
