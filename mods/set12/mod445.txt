eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x10 ~ x2 + x5
x1 ~ x10
x5 ~ x1 + x9
eta1 ~ eta2 + x3 + x5
x9 ~ x6 + x7
x4 ~ x5
eta2 ~ x8
