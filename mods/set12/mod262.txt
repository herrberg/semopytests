eta1 =~ y1 + y2
eta2 =~ y3 + y4
x1 ~ x5 + x9
x4 ~ x1
x2 ~ eta2 + x9
x8 ~ x9
x10 ~ x3 + x7 + x9
x6 ~ x10
x7 ~ x6
eta1 ~ x9
