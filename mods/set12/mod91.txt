eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x8 ~ x2 + x3
x6 ~ x10 + x5 + x8 + x9
x1 ~ x6
x10 ~ x1
x9 ~ eta1
x4 ~ eta2 + x5
x7 ~ x9
