eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x2 ~ x6
x10 ~ x1 + x2
x3 ~ eta1 + x10
x1 ~ x3 + x7
x9 ~ x6
x4 ~ x7
x5 ~ x7
eta2 ~ x6
x8 ~ x6
