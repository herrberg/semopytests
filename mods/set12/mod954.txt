eta1 =~ y1 + y2
eta2 =~ y3 + y4
x4 ~ eta1 + x9
x10 ~ x1 + x4
eta1 ~ eta2 + x2
x6 ~ x4
x3 ~ eta1
x2 ~ x3
x7 ~ x9
x8 ~ x9
x5 ~ eta1
