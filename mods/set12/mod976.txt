eta1 =~ y1 + y2
eta2 =~ y3 + y4
x3 ~ eta1 + x7
x5 ~ x10 + x3
x6 ~ x4 + x5
x1 ~ x6
eta2 ~ x2 + x5 + x8
x9 ~ eta1
x7 ~ x5
