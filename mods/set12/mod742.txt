eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x8 ~ x3 + x9
x1 ~ x8
x3 ~ x1
eta1 ~ x3
x7 ~ eta1
x10 ~ x1 + x2 + x4
x6 ~ x1
eta2 ~ x3
x5 ~ x1
