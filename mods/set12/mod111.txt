eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta1 ~ x9
x7 ~ eta1 + x2
x4 ~ eta1 + x8
x8 ~ x1
x10 ~ x3 + x8
x3 ~ eta2
x5 ~ x6 + x9
