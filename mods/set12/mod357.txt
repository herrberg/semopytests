eta1 =~ y1 + y2
eta2 =~ y3 + y4
x5 ~ x2 + x7
x1 ~ x5
x10 ~ x1
x8 ~ x10
eta1 ~ x5
x4 ~ eta1
x2 ~ x1 + x6
x3 ~ x5
x9 ~ eta2 + x3
