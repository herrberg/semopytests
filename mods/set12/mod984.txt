eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x2 ~ x6 + x9
x4 ~ x2
eta2 ~ x4
eta1 ~ eta2
x1 ~ x2
x7 ~ x1
x3 ~ x4
x6 ~ x10 + x4
x5 ~ x4
x8 ~ x4
