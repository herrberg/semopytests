eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x5 ~ x1 + x4 + x7
x10 ~ x5 + x8
x9 ~ x10
x3 ~ x9
eta2 ~ x10
x2 ~ x10 + x6
eta1 ~ x5
x7 ~ eta1
