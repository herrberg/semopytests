eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x1 ~ eta2 + x4 + x7
eta1 ~ x1
x7 ~ eta1 + x2
x3 ~ x1 + x8
x2 ~ x9
x4 ~ x10
x5 ~ x2
x6 ~ eta1
