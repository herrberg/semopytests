eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta2 ~ x10 + x2 + x4 + x8
x7 ~ eta2 + x3
eta1 ~ x7
x2 ~ eta1
x1 ~ x10
x5 ~ x1 + x6
x9 ~ x3
