eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x8 ~ eta1 + x2
x6 ~ x8
x9 ~ x5 + x6
x2 ~ x9
x1 ~ x2
x4 ~ x10 + x9
eta2 ~ x7 + x9
x3 ~ x9
