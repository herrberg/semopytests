eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x7 ~ x10 + x6
eta1 ~ eta2 + x7
x6 ~ eta1
eta2 ~ x4
x2 ~ x7
x1 ~ eta2
x5 ~ x1 + x8
x3 ~ eta2
x9 ~ eta1
