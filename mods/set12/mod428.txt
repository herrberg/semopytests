eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x9 ~ x3 + x4
x5 ~ x1 + x9
x10 ~ x5 + x7
eta2 ~ x10
x6 ~ eta2
eta1 ~ x6
x2 ~ x10
x3 ~ x5
x8 ~ x9
