eta1 =~ y1 + y2
eta2 =~ y3 + y4
x10 ~ x8
x5 ~ x10 + x4 + x6
x3 ~ x8
eta1 ~ x8
x1 ~ eta1
x2 ~ x10
x7 ~ x4
x9 ~ x7
eta2 ~ x10
