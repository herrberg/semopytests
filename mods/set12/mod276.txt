eta1 =~ y1 + y2
eta2 =~ y3 + y4
x5 ~ x3 + x8
x4 ~ eta1 + x8 + x9
x3 ~ x2
x1 ~ x3
x10 ~ x3
x7 ~ x9
eta2 ~ x7
x6 ~ x9
