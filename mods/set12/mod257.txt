eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta2 ~ x4 + x9
x6 ~ eta2 + x3
x8 ~ x6
x1 ~ x10 + x5 + x9
x2 ~ x1
x10 ~ x2
eta1 ~ eta2
x3 ~ x7
