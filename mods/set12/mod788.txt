eta1 =~ y1 + y2
eta2 =~ y3 + y4
x3 ~ x5 + x9
x10 ~ x3
eta2 ~ x5 + x6 + x8
eta1 ~ eta2 + x4
x7 ~ x8
x2 ~ x8
x1 ~ x3
