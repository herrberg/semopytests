eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x10 ~ x9
x3 ~ x10
x6 ~ x4 + x9
eta2 ~ x8 + x9
x1 ~ eta2
x5 ~ x10
eta1 ~ x4
x2 ~ eta1 + x7
