eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x1 ~ x10 + x6
x2 ~ x1
x10 ~ x2
x4 ~ x1 + x8 + x9
x7 ~ x5
x9 ~ x7
x3 ~ x5
eta1 ~ eta2 + x7
