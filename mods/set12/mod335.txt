eta1 =~ y1 + y2
eta2 =~ y3 + y4
x3 ~ x10 + x9
x7 ~ x3
x9 ~ x6 + x7
x4 ~ x9
x5 ~ eta1 + x2 + x3
x1 ~ x5
x8 ~ x3
eta2 ~ x9
