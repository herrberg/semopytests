eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta2 ~ x2 + x6 + x9
x8 ~ eta2 + x10 + x3
x9 ~ x8
x10 ~ x5
x1 ~ x10
x4 ~ x3
x7 ~ x10
eta1 ~ x6
