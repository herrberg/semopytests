eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x3 ~ x10 + x8
x2 ~ eta1 + x3
x8 ~ eta2 + x2
x6 ~ x8
x4 ~ x6
x5 ~ x4
x1 ~ x8
x9 ~ x8
x7 ~ eta2
