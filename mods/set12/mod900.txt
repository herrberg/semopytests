eta1 =~ y1 + y2
eta2 =~ y3 + y4
x4 ~ eta1 + x6 + x9
eta2 ~ x2 + x3 + x8 + x9
x1 ~ eta2
x5 ~ eta2
x2 ~ x5
x7 ~ x5
x10 ~ x6
