eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x6 ~ eta1 + x4 + x8
x7 ~ x6
x9 ~ x3 + x7
x1 ~ x9
x10 ~ x8
eta1 ~ x9
x5 ~ eta1
eta2 ~ x6
x2 ~ x6
