eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x4 ~ x2 + x8
eta1 ~ x4
x3 ~ eta1 + eta2
x5 ~ x4 + x6 + x9
x9 ~ x10
x7 ~ x4
eta2 ~ x1
x2 ~ eta1
