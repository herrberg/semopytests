eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x9 ~ x7
x2 ~ eta2 + x9
x6 ~ eta1 + x2 + x4
x1 ~ x6
x4 ~ x1 + x5
x3 ~ eta1
x8 ~ x9
x10 ~ x9
