eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta1 ~ x8 + x9
x1 ~ eta1 + x4 + x7
x6 ~ x1 + x10
x5 ~ x6
x2 ~ eta2 + x1
x7 ~ x2 + x3
