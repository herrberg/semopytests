eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x1 ~ eta2 + x5
x3 ~ eta1 + x1 + x6
x7 ~ x10 + x8
eta1 ~ x7
x2 ~ eta1
x8 ~ x2
x9 ~ eta2
x4 ~ x7
