eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x1 ~ x10 + x7 + x9
x4 ~ x1 + x6
x7 ~ x4 + x5
eta2 ~ x1
eta1 ~ x1
x5 ~ x2
x3 ~ x5
x8 ~ x3
