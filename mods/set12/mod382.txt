eta1 =~ y1 + y2
eta2 =~ y3 + y4
x3 ~ x1 + x2 + x6
eta2 ~ x3
x5 ~ eta2 + x7 + x8
x1 ~ eta1 + eta2
x9 ~ x1
x10 ~ eta1
x4 ~ eta1
