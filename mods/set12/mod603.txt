eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x3 ~ eta1 + x2 + x7
x1 ~ x3 + x5
eta1 ~ x1 + x9
x6 ~ x4 + x7
x10 ~ x6
eta2 ~ x2 + x8
