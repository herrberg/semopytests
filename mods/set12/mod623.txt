eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x7 ~ x5
eta2 ~ x7
x4 ~ eta2 + x6
x1 ~ x4
x6 ~ x1
x9 ~ eta2
x8 ~ eta2
eta1 ~ eta2
x3 ~ x2 + x4
x10 ~ x1
