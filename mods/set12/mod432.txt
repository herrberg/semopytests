eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x10 ~ eta1 + x1 + x5
eta2 ~ x10 + x2 + x4 + x6 + x7 + x9
eta1 ~ eta2
x8 ~ x10 + x3
