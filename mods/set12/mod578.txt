eta1 =~ y1 + y2
eta2 =~ y3 + y4
x6 ~ x7
eta2 ~ x3 + x4 + x6
x1 ~ x2 + x6
eta1 ~ x6 + x8
x2 ~ x10
x9 ~ x8
x5 ~ x6
