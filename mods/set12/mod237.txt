eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta2 ~ x5 + x8
x4 ~ eta2 + x10
x5 ~ x4 + x9
x1 ~ x4
x6 ~ x4
x7 ~ x4
eta1 ~ x10 + x3
x2 ~ x10
