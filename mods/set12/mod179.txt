eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x1 ~ eta1
x8 ~ x1 + x10 + x5 + x9
x6 ~ x4 + x8
x9 ~ x3
x10 ~ x2 + x6
eta2 ~ x6
x7 ~ x4
