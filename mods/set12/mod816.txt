eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x10 ~ x2 + x5 + x8
x3 ~ x10
eta2 ~ x3
x8 ~ x6
x1 ~ x10
x4 ~ x1
x7 ~ eta1 + x10 + x9
x2 ~ x7
