eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x6 ~ eta2 + x3
x8 ~ x6
x10 ~ x8
x7 ~ x5 + x6
eta2 ~ x7 + x9
x1 ~ eta1 + x3
x4 ~ x5
x2 ~ x4
