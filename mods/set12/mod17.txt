eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x8 ~ x10 + x4
eta1 ~ x3 + x8
x5 ~ eta1 + x9
x6 ~ eta2 + x2 + x8
x1 ~ x7 + x8
x10 ~ x1
