eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x3 ~ x8
x4 ~ x3 + x7
x1 ~ x3
x5 ~ eta1 + x1 + x10
eta2 ~ x5
x10 ~ eta2
x6 ~ x10
x2 ~ x3 + x9
