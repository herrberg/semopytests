eta1 =~ y1 + y2
eta2 =~ y3 + y4
x3 ~ eta1 + eta2 + x6 + x9
x2 ~ x3
x8 ~ x2
x7 ~ x3
x4 ~ x7
x5 ~ x10 + x3
x1 ~ x3
x6 ~ x7
