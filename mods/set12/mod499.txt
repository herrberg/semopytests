eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x2 ~ x4 + x5 + x6 + x9
eta1 ~ x10 + x4
x1 ~ eta1 + eta2
x10 ~ x1
x7 ~ x5
x3 ~ x4
x8 ~ x6
