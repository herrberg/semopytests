eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x3 ~ eta1 + x1 + x2 + x8
x6 ~ x3 + x5
x1 ~ x6
x9 ~ eta2 + x1
x10 ~ x2
x4 ~ x1 + x7
