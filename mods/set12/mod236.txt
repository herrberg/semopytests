eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x10 ~ x3 + x5 + x8 + x9
x7 ~ x4
x9 ~ x1 + x7
x6 ~ eta2 + x9
x1 ~ x6
eta1 ~ x8
x2 ~ x3
