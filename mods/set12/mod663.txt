eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta2 ~ x3 + x4 + x9
x7 ~ eta1 + eta2 + x2
x4 ~ x5 + x8
x8 ~ eta2
x1 ~ x4
x6 ~ x3
x10 ~ x6
