eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x10 ~ x7 + x8 + x9
x6 ~ x10
x2 ~ x6
eta2 ~ x2
x3 ~ x8
x5 ~ x3
x1 ~ x8
x4 ~ x1
x7 ~ eta1 + x6
