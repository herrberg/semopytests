eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta2 ~ eta1 + x1
x10 ~ eta2 + x7
x6 ~ x10 + x9
x2 ~ x6
x8 ~ eta2
x1 ~ x10 + x4
x5 ~ eta1
x3 ~ eta2
