eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x8 ~ eta2 + x2 + x7
eta1 ~ x8
x1 ~ eta1 + x6
x3 ~ x8
x5 ~ x10 + x3
x4 ~ x10
x9 ~ x10
x7 ~ eta1
