eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x8 ~ x2 + x5 + x6
x9 ~ x8
x6 ~ x4 + x9
x7 ~ x6
x3 ~ x1 + x9
eta1 ~ x3
eta2 ~ x4
x10 ~ x5
