eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x3 ~ x2 + x6 + x8
x9 ~ eta2 + x3
x7 ~ x1 + x3
x6 ~ x7
x5 ~ x2
x4 ~ x3
eta1 ~ x1
x10 ~ x3
