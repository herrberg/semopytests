eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta1 ~ x5 + x6
x2 ~ eta1 + x1
x5 ~ x2 + x9
x1 ~ x10
x3 ~ eta1 + x4
x4 ~ eta2
x7 ~ x4
x8 ~ x7
