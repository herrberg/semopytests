eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x1 ~ x2 + x6
x5 ~ eta1 + eta2 + x3
x6 ~ x5
x7 ~ x4 + x5
eta2 ~ x10 + x7
x9 ~ x2
x8 ~ x7
