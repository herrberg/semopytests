eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta2 ~ eta1 + x5
x6 ~ eta2
x7 ~ x6
x4 ~ x7
eta1 ~ x3 + x6 + x8
x10 ~ eta2 + x9
x3 ~ x1
x2 ~ x8
