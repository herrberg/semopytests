eta1 =~ y1 + y2
eta2 =~ y3 + y4
x10 ~ eta1 + x3 + x5
x4 ~ x10
x5 ~ x4
x3 ~ x9
eta2 ~ eta1
x6 ~ eta2
x1 ~ x3 + x8
x2 ~ x10
x7 ~ x3
