eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x5 ~ x2 + x3
x8 ~ x4 + x5
x3 ~ x1 + x10 + x6 + x8
x4 ~ eta2
x9 ~ x4
x7 ~ x6
eta1 ~ eta2
