eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x2 ~ x10 + x6
x1 ~ eta2 + x2 + x4
x6 ~ x1
x8 ~ x10
x7 ~ x8 + x9
x9 ~ eta1
x5 ~ x10
x3 ~ x10
