eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x9 ~ x1 + x6
eta2 ~ eta1 + x3 + x9
x5 ~ eta2
x4 ~ x5
x2 ~ x4
x10 ~ x6 + x7 + x8
x3 ~ x5
