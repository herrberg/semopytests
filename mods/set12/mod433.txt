eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x2 ~ x5
x6 ~ x2 + x8
x1 ~ x6
x8 ~ x1 + x9
x9 ~ x10
eta1 ~ x8
x4 ~ eta1
x7 ~ x5
eta2 ~ x2
x3 ~ eta1
