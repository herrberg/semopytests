eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x8 ~ eta1 + x1 + x2
x4 ~ eta2 + x8
x9 ~ x8
x3 ~ x9
x2 ~ x3
x10 ~ x1
x7 ~ x8
x6 ~ eta2
x5 ~ x8
