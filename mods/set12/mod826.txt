eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x1 ~ x2 + x4
x5 ~ x1
x3 ~ x5
eta1 ~ x1
x4 ~ eta1
x8 ~ x2
x10 ~ x6 + x8 + x9
x6 ~ eta2
x7 ~ x8
