eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x6 ~ eta2 + x2 + x8
eta1 ~ x6
eta2 ~ eta1
x3 ~ x10 + x4 + x6
x7 ~ eta1
x2 ~ x9
x1 ~ x6
x5 ~ x8
