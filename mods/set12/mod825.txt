eta1 =~ y1 + y2
eta2 =~ y3 + y4
x10 ~ x9
x2 ~ x10
x7 ~ x2 + x6
eta2 ~ eta1 + x1 + x8 + x9
x4 ~ eta2
x1 ~ x4
x5 ~ x2
x3 ~ x4
