eta1 =~ y1 + y2
eta2 =~ y3 + y4
x10 ~ eta2 + x5 + x7
eta1 ~ x10
x1 ~ eta1
x5 ~ x1
x2 ~ x10
x4 ~ x2
x8 ~ x7 + x9
x6 ~ eta1
x3 ~ x6
