eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x7 ~ x3 + x5
eta2 ~ x7
x8 ~ eta2
x3 ~ eta1 + eta2 + x4
x10 ~ x5
x9 ~ x10
x6 ~ x9
x2 ~ x10
x1 ~ x7
