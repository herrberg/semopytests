eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta2 ~ eta1 + x1 + x6 + x9
x8 ~ eta2
x7 ~ eta1 + x2 + x3
x5 ~ x7
x2 ~ x5
x10 ~ eta1
x4 ~ x3
