eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x9 ~ x4
x7 ~ eta1 + eta2 + x9
x5 ~ x10 + x2 + x7
eta2 ~ x5
x6 ~ x3 + x9
x8 ~ x5
x3 ~ x1
