eta1 =~ y1 + y2
eta2 =~ y3 + y4
x10 ~ x2 + x6 + x7
x9 ~ eta1 + x10 + x5
x2 ~ x9
x5 ~ eta2 + x3
x1 ~ x3
x8 ~ x5
eta2 ~ x4
