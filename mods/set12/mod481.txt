eta1 =~ y1 + y2
eta2 =~ y3 + y4
x8 ~ x2 + x3 + x5 + x6
eta1 ~ eta2 + x10 + x8 + x9
x1 ~ x8
x3 ~ x1
x4 ~ x3
x7 ~ x8
