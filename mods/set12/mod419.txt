eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta1 ~ x2 + x3
x7 ~ eta1 + x5
x6 ~ x7 + x8
x2 ~ x6
x8 ~ x1
x5 ~ x10
x9 ~ x1
x4 ~ x1
eta2 ~ x8
