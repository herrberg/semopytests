eta1 =~ y1 + y2
eta2 =~ y3 + y4
x7 ~ eta1 + x8 + x9
x10 ~ x7
x4 ~ x10 + x2
x5 ~ x4
eta2 ~ x1 + x3 + x5
x6 ~ eta2
x9 ~ x4
