eta1 =~ y1 + y2
eta2 =~ y3 + y4
x4 ~ eta1 + x9
x6 ~ x1 + x4 + x7
eta1 ~ x10 + x3 + x6
x8 ~ x9
eta2 ~ x3
x5 ~ eta1
x2 ~ x9
