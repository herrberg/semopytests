eta1 =~ y1 + y2
eta2 =~ y3 + y4
x3 ~ x2 + x8
x7 ~ eta2 + x3
x8 ~ x7
x6 ~ x8
x5 ~ x8
x1 ~ x3
x9 ~ eta1 + x8
x10 ~ eta2
x4 ~ x10
