eta1 =~ y1 + y2
eta2 =~ y3 + y4
x7 ~ x10 + x8 + x9
eta2 ~ x7
x9 ~ eta1 + x6
x8 ~ x1
x4 ~ x9
eta1 ~ x4
x3 ~ x2 + x6
x5 ~ x2
