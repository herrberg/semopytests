eta1 =~ y1 + y2
eta2 =~ y3 + y4
x7 ~ x1 + x10 + x2 + x6
eta1 ~ x1 + x9
x8 ~ eta1
x9 ~ eta2 + x8
x2 ~ x5
x3 ~ x9
x4 ~ x10
