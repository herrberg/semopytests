eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x2 ~ eta2 + x4
x6 ~ x2 + x5 + x8
eta1 ~ x6
x8 ~ x10
eta2 ~ x6
x1 ~ x2
x7 ~ x8
x3 ~ x8 + x9
