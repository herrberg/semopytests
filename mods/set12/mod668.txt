eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x5 ~ x7 + x8
x6 ~ eta2 + x5
eta1 ~ x2 + x6
x9 ~ eta1
x3 ~ eta2 + x4
x10 ~ x1 + x3
x4 ~ x10
