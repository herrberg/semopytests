eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x6 ~ eta1 + x4 + x8
x10 ~ x6
eta2 ~ x10
x5 ~ x1 + x8
x2 ~ x5
x1 ~ x2
x9 ~ x1
x7 ~ eta1 + x3
