eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x4 ~ x10 + x6 + x7
eta1 ~ x4
x3 ~ eta1
x1 ~ x3
x2 ~ x4 + x5 + x8
x6 ~ x2
x9 ~ x4
eta2 ~ x7
