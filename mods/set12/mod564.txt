eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x5 ~ x10 + x2
x3 ~ x5
x1 ~ x3
x4 ~ x5
eta2 ~ x5
x8 ~ eta2
x9 ~ eta1 + x5
x10 ~ eta2 + x7
x6 ~ x5
