eta1 =~ y1 + y2
eta2 =~ y3 + y4
x3 ~ eta1 + x6 + x9
x10 ~ x3 + x8
x6 ~ x10
eta2 ~ x10
x2 ~ x3
x5 ~ x2
x7 ~ x1 + x10
x4 ~ x2
