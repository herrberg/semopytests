eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x2 ~ x6 + x8
x9 ~ x2
x1 ~ x9
eta2 ~ x1
x6 ~ x9
x10 ~ x9
x7 ~ x10
x4 ~ x3 + x9
eta1 ~ x1
x5 ~ x1
