eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta1 ~ eta2 + x7
x8 ~ eta1
x10 ~ x8
x2 ~ x1 + x10 + x6
x7 ~ x8
x3 ~ x7
x1 ~ x5
x9 ~ x8
x4 ~ x6
