eta1 =~ y1 + y2
eta2 =~ y3 + y4
x6 ~ x8
x5 ~ x10 + x3 + x6
x1 ~ x4 + x5
x7 ~ x5 + x9
x10 ~ x7
eta2 ~ x10
eta1 ~ x4
x2 ~ x9
