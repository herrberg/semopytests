eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x5 ~ x2
x1 ~ eta1 + x4 + x5 + x6
x8 ~ eta2 + x1
x10 ~ x1
x9 ~ x10 + x3
x4 ~ x9
x7 ~ x5
