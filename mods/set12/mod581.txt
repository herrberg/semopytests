eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x1 ~ eta1 + x5 + x7
x4 ~ x1
x6 ~ x2 + x4
x9 ~ x4
x7 ~ x8
x3 ~ eta2 + x10
x2 ~ x3
x5 ~ x4
