eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x7 ~ eta1
x5 ~ x7
x8 ~ eta2 + x3 + x5
x4 ~ x8
x1 ~ x4
eta2 ~ x9
x2 ~ x7
x6 ~ x10 + x4
x3 ~ x6
