eta1 =~ y1 + y2
eta2 =~ y3 + y4
x1 ~ eta2 + x10 + x4
x7 ~ x1 + x3 + x9
x2 ~ x10
x5 ~ x2
x6 ~ x10
x8 ~ x10
eta1 ~ eta2
