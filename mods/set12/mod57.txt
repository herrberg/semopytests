eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x4 ~ x2
x10 ~ x4 + x6
x6 ~ eta2
x3 ~ x4
x1 ~ x2
x5 ~ eta1 + x1
x9 ~ x4
x8 ~ x2
x7 ~ eta1
