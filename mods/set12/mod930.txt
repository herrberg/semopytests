eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x1 ~ eta1
x7 ~ eta1 + x5
x10 ~ x7 + x8
x9 ~ x10
x2 ~ x9
x3 ~ eta1
x6 ~ x7
x5 ~ x6
eta2 ~ x6
x4 ~ x9
