eta1 =~ y1 + y2
eta2 =~ y3 + y4
x4 ~ x2 + x8
x9 ~ x3 + x4 + x5
x2 ~ x9
x7 ~ eta1 + x2
x1 ~ x9
eta2 ~ x5
x10 ~ x4
x6 ~ x5
