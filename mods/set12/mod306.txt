eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x9 ~ eta1 + x2
x6 ~ eta2 + x9
eta2 ~ x1 + x10
x3 ~ x9
x8 ~ x3
x7 ~ x9
x4 ~ eta2
x1 ~ x4
x5 ~ x2
