eta1 =~ y1 + y2
eta2 =~ y3 + y4
x10 ~ x1 + x3 + x4
x2 ~ x10
eta2 ~ x2 + x5
x4 ~ eta2
x5 ~ eta1
x6 ~ x3 + x9
x7 ~ x1
x8 ~ eta1
