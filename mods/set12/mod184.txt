eta1 =~ y1 + y2
eta2 =~ y3 + y4
x1 ~ eta1 + x7
eta1 ~ x2 + x5
x4 ~ eta1
x5 ~ x3 + x4 + x9
x6 ~ eta1 + x10
x8 ~ x2
eta2 ~ x9
