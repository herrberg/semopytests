eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x7 ~ x10 + x5
x1 ~ x2 + x3 + x7
x6 ~ eta1 + x1
x2 ~ x6
x3 ~ eta2
x9 ~ x5
x8 ~ x7
x4 ~ x7
