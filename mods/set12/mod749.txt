eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta1 ~ x3 + x4
x2 ~ eta1
x3 ~ x10 + x2
x10 ~ x1
x7 ~ eta1
x9 ~ x1
x5 ~ eta1 + x8
eta2 ~ x5
x6 ~ x10
