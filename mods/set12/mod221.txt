eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x6 ~ x1 + x10
x2 ~ x6 + x9
x9 ~ x5
eta1 ~ x6
x1 ~ eta1
x3 ~ x1
x4 ~ eta1
x7 ~ eta1 + eta2
x8 ~ x6
