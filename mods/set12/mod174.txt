eta1 =~ y1 + y2
eta2 =~ y3 + y4
x4 ~ eta1 + x1 + x6
x9 ~ x4 + x5
x8 ~ x9
eta2 ~ x8
x5 ~ x10 + x7
eta1 ~ x8
x3 ~ x4
x10 ~ x2
