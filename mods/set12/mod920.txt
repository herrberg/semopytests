eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x7 ~ x8
x1 ~ x7
x5 ~ x8
x2 ~ eta1 + x5
x4 ~ x2
eta1 ~ x3 + x4
x10 ~ eta1
x9 ~ x8
x6 ~ eta1
eta2 ~ x8
