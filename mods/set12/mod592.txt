eta1 =~ y1 + y2
eta2 =~ y3 + y4
x2 ~ x10 + x8
x5 ~ eta2 + x2
x10 ~ x3 + x5 + x7
eta1 ~ x4 + x8
x4 ~ x6
x1 ~ x8
x9 ~ x4
