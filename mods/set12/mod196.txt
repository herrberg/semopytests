eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x1 ~ x2 + x3
x5 ~ x1
x4 ~ x1 + x10 + x7
x6 ~ x4
x7 ~ eta2 + x6
x9 ~ x7 + x8
eta1 ~ x3
