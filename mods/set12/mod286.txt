eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x1 ~ x9
x5 ~ x1 + x2 + x8
eta2 ~ x5
x10 ~ eta2 + x4
eta1 ~ x10
x3 ~ eta1
x7 ~ x8
x2 ~ x10
x6 ~ x1
