eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x5 ~ eta1 + eta2
x2 ~ x10 + x5
x4 ~ x2
x9 ~ x3 + x5
eta1 ~ x9
x7 ~ x5
x3 ~ x1 + x6
x8 ~ x2
