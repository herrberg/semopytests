eta1 =~ y1 + y2
eta2 =~ y3 + y4
x9 ~ x3 + x8
x7 ~ x9
x3 ~ eta2 + x7
x1 ~ x3 + x4
x4 ~ x6
x2 ~ x8
eta1 ~ x4
x10 ~ x3
x5 ~ x7
