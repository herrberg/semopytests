eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x4 ~ eta2 + x2 + x3
x3 ~ x5
x1 ~ eta1 + x5
