eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x2 ~ x4
eta2 ~ x1 + x2
x5 ~ x4
eta1 ~ x1
x3 ~ x1
