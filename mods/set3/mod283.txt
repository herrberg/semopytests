eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta2 ~ x1
eta1 ~ eta2 + x2 + x5
x3 ~ eta1 + x4
