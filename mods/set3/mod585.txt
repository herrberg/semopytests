eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x5 ~ eta2
x1 ~ x5
x2 ~ eta1 + x1
x3 ~ x1 + x4
