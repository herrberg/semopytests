eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta2 ~ x2 + x4
x1 ~ eta1 + x4
x3 ~ x4 + x5
