eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x1 ~ x2 + x3 + x4
x5 ~ x1
x2 ~ eta2
eta1 ~ x4
