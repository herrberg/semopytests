eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta2 ~ x2 + x4
eta1 ~ x3
x1 ~ eta1
x2 ~ x1
x5 ~ x4
