eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta2 ~ eta1
x3 ~ eta2
x2 ~ eta1
x1 ~ x2
x5 ~ eta2
x4 ~ eta2
