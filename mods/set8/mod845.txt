eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x4 ~ eta3
x8 ~ x3 + x4
x5 ~ x4
eta4 ~ x5 + x6
x2 ~ eta4 + x9
eta1 ~ x5
eta2 ~ x3
x10 ~ x4 + x7
x1 ~ x6
