eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y2 + y8 + y9
eta4 =~ y10 + y11
eta3 ~ x6 + x7
x4 ~ eta3 + x1 + x3 + x8
eta1 ~ eta3
x9 ~ x10 + x3 + x5
eta2 ~ eta4 + x7
x6 ~ x2
