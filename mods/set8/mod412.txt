eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y10 + y8
eta4 =~ y10 + y9
x9 ~ x7
x6 ~ x9
x2 ~ x1 + x6
eta2 ~ x3 + x9
x8 ~ x6
x5 ~ x8
x4 ~ x10
x3 ~ eta1 + x4
eta4 ~ x7
eta3 ~ x3
