eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y5 + y7
eta4 =~ y10 + y8 + y9
eta4 ~ x3 + x9
x10 ~ eta2 + eta4
x4 ~ eta4 + x8
x9 ~ x1
x6 ~ x9
x2 ~ x6
x7 ~ eta1 + x5 + x9
eta3 ~ eta1
