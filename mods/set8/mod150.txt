eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y1 + y10
x2 ~ x8 + x9
x3 ~ x2
x7 ~ x10 + x3
eta3 ~ x7
eta2 ~ eta3
x4 ~ x3 + x5
x1 ~ x4
x9 ~ eta4
eta1 ~ x2
x6 ~ eta1
