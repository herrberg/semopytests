eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y7
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta3 ~ x1 + x10 + x8
x7 ~ eta3 + x2 + x3
x9 ~ x7
x4 ~ x9
x6 ~ x4
x5 ~ x6
eta2 ~ eta3
eta4 ~ eta3
eta1 ~ x9
