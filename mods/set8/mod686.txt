eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
x10 ~ x7
eta2 ~ x10
x8 ~ eta2
x4 ~ eta4 + x5 + x8
x2 ~ x8
x6 ~ eta2
x1 ~ eta2
eta1 ~ x1
x9 ~ x10
x3 ~ x7
eta3 ~ x8
