eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
x5 ~ x4
eta2 ~ x2 + x5
eta1 ~ eta2
x7 ~ eta1 + x1 + x3 + x9
x6 ~ x5
x9 ~ eta4
x2 ~ x8
x10 ~ x4
eta3 ~ x1
