eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x5 ~ eta2 + eta4
x10 ~ x3 + x8
eta2 ~ x10 + x2
x9 ~ eta1 + eta4 + x1
x6 ~ eta4 + x4
eta3 ~ eta2 + x7
