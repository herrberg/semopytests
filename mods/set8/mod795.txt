eta1 =~ y1 + y2 + y3
eta2 =~ y5 + y6 + y7
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
x9 ~ x1 + x3 + x5 + x7
x10 ~ eta2 + x9
x2 ~ x10
x4 ~ x5
eta1 ~ x1
x8 ~ x5
x3 ~ eta3
eta2 ~ x6
eta4 ~ x10
