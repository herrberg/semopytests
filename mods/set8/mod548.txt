eta1 =~ y1 + y2
eta2 =~ y10 + y3 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
x5 ~ x1 + x2 + x3
x6 ~ x5
x9 ~ x6
x2 ~ x8
eta1 ~ x10 + x3
x1 ~ eta4
x4 ~ eta3 + x10 + x7
eta2 ~ x6
