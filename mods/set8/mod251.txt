eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y3
eta4 ~ x6
eta3 ~ eta4 + x10 + x8
eta1 ~ eta3
x4 ~ eta1
x3 ~ x6 + x7
x5 ~ x7
x9 ~ eta2 + x5
eta2 ~ x1
x8 ~ x2
