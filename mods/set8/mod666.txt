eta1 =~ y1 + y2 + y3
eta2 =~ y10 + y4
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
x6 ~ x5
x9 ~ x6
eta1 ~ x4 + x8 + x9
eta2 ~ eta1 + eta4
x3 ~ eta2
x10 ~ x2 + x3
x1 ~ x4
eta3 ~ x5
x7 ~ x5
