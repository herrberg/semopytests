eta1 =~ y1 + y2 + y3
eta2 =~ y5 + y7
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
x7 ~ x1
x2 ~ x4 + x7
eta4 ~ eta2 + x2
x3 ~ eta4 + x10 + x8
x5 ~ x3
eta2 ~ x6
x9 ~ eta2
eta3 ~ x10
eta1 ~ eta3
