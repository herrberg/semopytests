eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y3 + y8
eta4 =~ y10 + y9
x10 ~ eta2
x3 ~ x10 + x2 + x8
eta4 ~ x10 + x7
eta3 ~ eta4
x7 ~ eta1
x2 ~ x9
x6 ~ x5 + x7
x4 ~ x5
x1 ~ eta1
