eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
x4 ~ x7
eta3 ~ eta1 + x1 + x4 + x9
x2 ~ eta3
x1 ~ x10
x6 ~ x4
eta2 ~ x4
x8 ~ x4
x5 ~ x1
x3 ~ x7
eta4 ~ x1
