eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x7 ~ x10
eta3 ~ x7
x4 ~ eta3 + x1
x6 ~ eta2 + x4
x2 ~ x5 + x6
eta4 ~ x2
eta2 ~ x8
x9 ~ eta1
x5 ~ x9
x3 ~ x2
