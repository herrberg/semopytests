eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta1 ~ eta2 + x4 + x5 + x7
x7 ~ x3
x2 ~ x6
x5 ~ x2
eta3 ~ x3
x1 ~ x7
eta4 ~ x4
x9 ~ eta2
x8 ~ x9
x10 ~ x9
