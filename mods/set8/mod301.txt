eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
x5 ~ eta2
x3 ~ x1 + x5 + x7
eta1 ~ x10 + x3
x1 ~ eta4
eta3 ~ x1 + x4
x6 ~ x7 + x8
x2 ~ x4
x9 ~ x7
