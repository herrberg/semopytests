eta1 =~ y10 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
x7 ~ x3
x10 ~ x7
x5 ~ x10
x9 ~ x5
x8 ~ x1 + x5
x1 ~ eta4 + x2
x4 ~ x10
eta1 ~ x10 + x6
eta2 ~ x7
eta3 ~ x2
