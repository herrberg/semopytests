eta1 =~ y1 + y2 + y3
eta2 =~ y1 + y4 + y5
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
x6 ~ x8
eta1 ~ x6
x10 ~ eta1
x2 ~ x10
eta2 ~ eta1
x9 ~ eta1 + x5
x4 ~ x9
x1 ~ x4
x7 ~ eta3 + x4
eta4 ~ x9
x3 ~ eta1
