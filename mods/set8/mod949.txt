eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y2
x3 ~ x8
eta4 ~ x3
x10 ~ eta1 + eta4
x5 ~ x1 + x3 + x4
x6 ~ x5 + x9
eta2 ~ x3
x7 ~ eta2
x1 ~ x2
eta3 ~ x1
