eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x7 ~ x2
x10 ~ eta3 + x5 + x7
x9 ~ x10
eta3 ~ x4
x3 ~ x8
x5 ~ x3
x1 ~ eta1 + eta3
x6 ~ x1
eta1 ~ eta4
eta2 ~ x8
