eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y7 + y8
x9 ~ x10 + x2
x4 ~ x9
eta3 ~ eta2 + x4 + x5
x7 ~ eta4 + x9
x8 ~ x7
eta1 ~ x8
eta2 ~ x3
x6 ~ x1 + x7
