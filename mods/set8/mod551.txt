eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x1 ~ x8
eta1 ~ x1
x2 ~ eta1 + x3
x6 ~ x2
eta2 ~ eta3 + x6
eta4 ~ eta2
x7 ~ x8 + x9
x10 ~ x6
x5 ~ eta1
x4 ~ x8
