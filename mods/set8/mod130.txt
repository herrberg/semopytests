eta1 =~ y1 + y2 + y3
eta2 =~ y11 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
x8 ~ x10 + x4 + x5 + x9
x1 ~ eta3 + x8
x3 ~ x10
eta2 ~ eta4 + x5
x9 ~ x6
x2 ~ x8
eta1 ~ x5
x7 ~ x6
