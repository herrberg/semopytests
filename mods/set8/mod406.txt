eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y10 + y7
eta4 =~ y10 + y8 + y9
x4 ~ eta4 + x1
eta1 ~ x4 + x9
eta3 ~ eta1 + x3
x6 ~ x4
x5 ~ x6
x8 ~ x5 + x7
x1 ~ eta2
x2 ~ x1
x10 ~ x5
