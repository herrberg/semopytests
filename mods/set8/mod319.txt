eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y2 + y6 + y7
eta4 =~ y10 + y11 + y9
eta3 ~ x10
eta4 ~ eta1 + eta3 + x4
eta2 ~ eta4
eta1 ~ x8
x7 ~ eta4 + x3 + x5
x2 ~ x7
x6 ~ x8
x9 ~ x6
x1 ~ x10
