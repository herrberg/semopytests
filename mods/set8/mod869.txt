eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y1 + y10 + y8
x4 ~ x10
x7 ~ eta3 + x4
eta4 ~ x7
x9 ~ eta4 + x2 + x6
x5 ~ eta2 + x8
x2 ~ x5
eta1 ~ x7
x1 ~ x5
x3 ~ x4
