eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y10 + y7 + y8
eta4 =~ y10 + y11
eta4 ~ eta2
x4 ~ eta4 + x5
x2 ~ x4
x6 ~ eta1 + x2 + x7
eta3 ~ x6
x8 ~ x6
x9 ~ x2
x3 ~ x9
x10 ~ eta4
x1 ~ eta4
