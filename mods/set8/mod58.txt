eta1 =~ y1 + y2 + y3
eta2 =~ y3 + y4
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
x8 ~ eta1 + x7
eta2 ~ x2 + x8
x1 ~ x2
x3 ~ x1
x10 ~ x8
x6 ~ x2 + x5
x9 ~ eta3 + x7
x4 ~ eta4 + x9
