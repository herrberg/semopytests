eta1 =~ y1 + y2
eta2 =~ y10 + y3 + y4
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
x6 ~ x8
eta3 ~ x6 + x7
x5 ~ eta3 + x4
eta1 ~ x5
x4 ~ x2
x3 ~ eta2 + eta3
x1 ~ x3
x9 ~ x4
eta4 ~ x7
x10 ~ x4
