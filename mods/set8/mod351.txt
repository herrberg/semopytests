eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y5 + y7
eta4 =~ y10 + y8 + y9
x7 ~ eta3
x4 ~ eta1 + x7
x2 ~ x4
x10 ~ x2
x5 ~ x10
x1 ~ x4 + x6 + x9
eta4 ~ eta2 + x1 + x8
x3 ~ x9
