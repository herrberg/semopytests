eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x3 ~ eta1
x7 ~ x2 + x3
x6 ~ x1 + x7
eta3 ~ x6
x4 ~ eta3
x10 ~ x4 + x8
eta2 ~ x7
x2 ~ x5
x9 ~ x4
eta4 ~ x4
