eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y5 + y8
x8 ~ eta1 + x5 + x6
x5 ~ x4
eta2 ~ x10 + x5
x1 ~ x5
eta4 ~ x4
x3 ~ eta3 + x2 + x6
eta1 ~ x7
eta3 ~ x9
