eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x3 ~ eta3 + x1 + x8
x7 ~ x3 + x4 + x6
eta4 ~ x2 + x7
eta3 ~ eta2 + x9
x10 ~ x7
x5 ~ x8
eta1 ~ x1
