eta1 =~ y1 + y2 + y3
eta2 =~ y3 + y4 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
x3 ~ x1 + x6
x7 ~ x2 + x3
eta2 ~ x10 + x7
x8 ~ eta2 + x9
x2 ~ eta1 + x4
eta3 ~ x3
x9 ~ eta4
x5 ~ x10
