eta1 =~ y1 + y2 + y3
eta2 =~ y2 + y4 + y5
eta3 =~ y7 + y8
eta4 =~ y10 + y9
x2 ~ eta4 + x1 + x4
eta3 ~ x2 + x7
x6 ~ x1 + x10
x3 ~ x6
eta1 ~ x1
x9 ~ x7
x5 ~ eta2
x10 ~ x5
x8 ~ eta2
