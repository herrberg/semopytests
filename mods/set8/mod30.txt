eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
x1 ~ x2 + x5
x6 ~ x3 + x5
x3 ~ eta2
eta4 ~ eta3 + x3
x10 ~ eta1 + x5
x4 ~ x3
x8 ~ eta2 + x9
x7 ~ x8
