eta1 =~ y1 + y2 + y3
eta2 =~ y1 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta4 ~ x2
x8 ~ eta4 + x3
x6 ~ x8
eta2 ~ x6
x1 ~ eta2
x7 ~ x1
eta1 ~ x10 + x3
eta3 ~ eta1
x5 ~ eta2
x9 ~ x5
x4 ~ x1
