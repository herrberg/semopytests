eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y3 + y6
eta4 =~ y10 + y8 + y9
x9 ~ eta2 + eta3 + x6 + x8
x10 ~ x9
x2 ~ x8
x4 ~ x2
x3 ~ eta4
eta2 ~ x3 + x7
eta1 ~ x9
x5 ~ x3
x1 ~ x5
