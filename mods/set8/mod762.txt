eta1 =~ y1 + y2 + y3
eta2 =~ y1 + y4 + y5
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
x8 ~ x4
eta2 ~ x6 + x8
x9 ~ eta2 + x7
eta1 ~ x9
x3 ~ eta2
x6 ~ x2
x7 ~ x1
eta3 ~ x6
x10 ~ x6
eta4 ~ x2
x5 ~ x7
