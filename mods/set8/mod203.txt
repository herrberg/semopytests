eta1 =~ y1 + y2 + y3
eta2 =~ y5 + y6 + y8
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
x3 ~ x4 + x8
eta4 ~ x1 + x3
x9 ~ eta4
eta2 ~ x4
eta1 ~ eta2
x10 ~ x4
x5 ~ x10
x7 ~ x3
x8 ~ x2
eta3 ~ x3
x6 ~ x4
