eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y5 + y8
x7 ~ eta4
x8 ~ x10 + x7
eta1 ~ eta4
eta2 ~ eta1 + eta3
x1 ~ eta2
x2 ~ eta4 + x5 + x6
x3 ~ x10
x9 ~ eta1
x4 ~ eta2
