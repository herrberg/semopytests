eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y4 + y7 + y8
eta4 =~ y10 + y11 + y9
eta2 ~ x7
x4 ~ eta2 + x1
eta3 ~ x4
x6 ~ eta3
eta1 ~ x6
x5 ~ x3 + x4 + x9
x2 ~ x5
x1 ~ x8
x10 ~ x1
eta4 ~ x6
