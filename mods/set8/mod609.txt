eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y3 + y8
x7 ~ x6
eta1 ~ eta3 + x7 + x9
x5 ~ eta4 + x7
x2 ~ x5
x8 ~ x2
eta4 ~ x3
eta2 ~ x9
x1 ~ x9
x4 ~ x1
eta3 ~ x10
