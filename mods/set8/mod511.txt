eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y3 + y8 + y9
x1 ~ eta4
x2 ~ x1
x9 ~ eta1 + x2
x3 ~ x9
x5 ~ x1
eta2 ~ eta1 + x4
x4 ~ x8
eta3 ~ x2
x6 ~ eta4
x10 ~ x1
x7 ~ x4
