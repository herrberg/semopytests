eta1 =~ y2 + y8
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
x3 ~ eta4 + x4
eta3 ~ x3
x6 ~ eta3 + x7
x1 ~ x6
x8 ~ eta2 + x4 + x5
eta1 ~ x10 + x8
x2 ~ eta1
x9 ~ x10
