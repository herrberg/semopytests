eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y7
eta2 ~ x4
eta1 ~ eta2 + x6
x7 ~ eta1
eta4 ~ x5
x6 ~ eta4
x9 ~ eta2
x2 ~ x3 + x9
x1 ~ eta1
eta3 ~ x1
x8 ~ eta2
x10 ~ x1
