eta1 =~ y2 + y8
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta4 ~ x3
x8 ~ eta2 + eta4 + x4
x5 ~ eta4
x9 ~ x1 + x4
x6 ~ x9
eta1 ~ x6
x7 ~ eta1
x2 ~ x4
eta3 ~ x2
x10 ~ x9
