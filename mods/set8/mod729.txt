eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta4 ~ x10 + x6
x1 ~ eta3 + x10
eta2 ~ x1
x7 ~ x10 + x9
x4 ~ x7
x5 ~ x6 + x8
eta1 ~ x3 + x5
x3 ~ x2
