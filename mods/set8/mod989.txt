eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y1 + y10
x3 ~ eta1 + x5
x6 ~ x3
x9 ~ x6
x8 ~ x1 + x9
x2 ~ x3
eta3 ~ x2
eta4 ~ eta3
x4 ~ x6
eta2 ~ x4
x7 ~ x1 + x10
