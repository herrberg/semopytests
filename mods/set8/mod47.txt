eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta4 ~ eta2 + x8 + x9
eta1 ~ eta4 + x2
x7 ~ eta1
x1 ~ x9
x4 ~ x9
x3 ~ x6 + x8
x10 ~ x8
x5 ~ x6
eta3 ~ x6
