eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta3 ~ x1 + x3 + x6 + x9
x2 ~ x9
x10 ~ x2
x4 ~ eta1
x6 ~ x4
x7 ~ x9
x8 ~ x5 + x6
eta2 ~ x8
eta4 ~ x4
