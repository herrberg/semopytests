eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta1 ~ eta3
x3 ~ eta1
x10 ~ x3
eta4 ~ x10
x9 ~ eta1 + x1
x8 ~ eta1
x4 ~ eta1
eta2 ~ x4
x7 ~ eta2
x6 ~ eta3
x2 ~ x6
x5 ~ x2
