eta1 =~ y1 + y10 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta4 ~ eta1
x8 ~ eta4 + x4
x10 ~ eta4
eta3 ~ x10
eta2 ~ eta3 + x1
x3 ~ x10
x4 ~ x5 + x6
x9 ~ x1 + x7
x2 ~ x1
