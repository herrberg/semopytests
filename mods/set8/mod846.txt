eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
x6 ~ eta2 + x4
eta4 ~ eta2 + eta3
eta3 ~ x1
x2 ~ eta3 + x3
x5 ~ x1
x10 ~ eta2
x4 ~ eta1 + x7
x8 ~ x1
x9 ~ eta3
