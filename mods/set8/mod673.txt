eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x1 ~ eta3 + x4
x3 ~ x1 + x8
x7 ~ x3
x5 ~ x1
eta4 ~ eta3 + x10
eta2 ~ x8
eta1 ~ x1 + x2
x6 ~ x8
x9 ~ x1
