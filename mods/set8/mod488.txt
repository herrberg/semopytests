eta1 =~ y1 + y2 + y3
eta2 =~ y5 + y7
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
x1 ~ x10 + x6 + x7
eta2 ~ x3 + x6 + x9
eta3 ~ eta2
x8 ~ eta4
x7 ~ x2 + x8
x4 ~ x6
x5 ~ x6
eta1 ~ x6
