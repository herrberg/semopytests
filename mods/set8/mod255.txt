eta1 =~ y1 + y2 + y3
eta2 =~ y11 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
x6 ~ eta2
x4 ~ x6
x9 ~ x4
x10 ~ x6
x2 ~ x10 + x8
x5 ~ x10
x7 ~ x5
eta3 ~ x1 + x5
x3 ~ x1
eta4 ~ x10
eta1 ~ x6
