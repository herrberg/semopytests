eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y3
x7 ~ x2
x4 ~ x7
x3 ~ x4 + x9
x8 ~ eta1 + eta3 + x3
eta4 ~ x3
x10 ~ x9
x6 ~ x9
eta2 ~ x6
x5 ~ x7
x1 ~ x7
