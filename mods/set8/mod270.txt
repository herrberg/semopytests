eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta4 ~ x10
x8 ~ eta1 + eta4 + x6
x2 ~ eta4
x6 ~ x1
x7 ~ x6
x5 ~ x7 + x9
x4 ~ eta1 + eta2
eta3 ~ x7
x3 ~ eta2
