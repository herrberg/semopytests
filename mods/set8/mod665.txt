eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x9 ~ x10
x5 ~ eta2 + x9
x7 ~ x5
eta2 ~ x6
eta4 ~ x6
x3 ~ eta4
x1 ~ x3
x2 ~ eta3 + x9
x4 ~ eta2 + x8
eta1 ~ x10
