eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x4 ~ eta2
x3 ~ x4
x8 ~ eta3 + x10 + x2 + x3 + x5
eta4 ~ x8
x2 ~ x1
eta3 ~ x9
x7 ~ x5
x6 ~ x7
eta1 ~ eta3
