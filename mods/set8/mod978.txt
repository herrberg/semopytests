eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x6 ~ x4 + x8
eta2 ~ x6
eta4 ~ eta2
x9 ~ x2 + x5 + x8
x4 ~ x7
x10 ~ x5
x1 ~ x5
x3 ~ x2
eta3 ~ x8
eta1 ~ x7
