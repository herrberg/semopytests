eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y4 + y7 + y8
eta4 =~ y10 + y11 + y9
x4 ~ x1 + x3
x2 ~ x10 + x4
x7 ~ x2 + x5
x6 ~ x4 + x8
eta3 ~ x6
x8 ~ eta2
eta1 ~ x4
eta4 ~ x5 + x9
