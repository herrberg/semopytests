eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y4 + y7 + y8
eta4 =~ y10 + y11 + y9
x3 ~ x9
x7 ~ x3 + x5
x10 ~ eta1 + x7
x8 ~ x10
eta2 ~ eta1
x6 ~ eta2
x2 ~ eta3 + eta4 + x3
x1 ~ eta1
x4 ~ x1
