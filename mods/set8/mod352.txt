eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y2
x8 ~ eta2
x9 ~ x3 + x8
x5 ~ x9
eta4 ~ x6 + x9
x3 ~ x7
x10 ~ x9
eta1 ~ x1 + x4 + x9
x6 ~ x2
eta3 ~ x6
