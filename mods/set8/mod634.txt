eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
x3 ~ x2 + x6
x4 ~ x1 + x3
eta4 ~ x10 + x3
eta3 ~ eta1
x10 ~ eta3
x1 ~ x5
x9 ~ x1
eta2 ~ x9
x8 ~ eta3
x7 ~ x9
