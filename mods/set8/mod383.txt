eta1 =~ y1 + y2 + y3
eta2 =~ y2 + y4 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
x2 ~ eta3
x8 ~ x2
eta1 ~ x4 + x8 + x9
eta2 ~ eta1
eta4 ~ eta2 + x3
x7 ~ eta4
x5 ~ eta3
x3 ~ x10
x1 ~ x3
x6 ~ x1
