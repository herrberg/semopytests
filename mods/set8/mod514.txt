eta1 =~ y2 + y3
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
x7 ~ x4
x6 ~ eta2 + x7
eta1 ~ x5 + x6
x10 ~ x1 + x3 + x4
x8 ~ eta4 + x7 + x9
x2 ~ x1
eta3 ~ x2
