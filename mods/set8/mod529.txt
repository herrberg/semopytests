eta1 =~ y1 + y2 + y3
eta2 =~ y5 + y8
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
x3 ~ x8
x4 ~ x3
x9 ~ eta2 + x4
x5 ~ x9
eta3 ~ x9
x7 ~ eta3
x2 ~ x7
x1 ~ x9
eta1 ~ x8
eta4 ~ x9
x10 ~ x9
x6 ~ x8
