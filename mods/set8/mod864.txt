eta1 =~ y1 + y3 + y8
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
x10 ~ x6
x5 ~ eta2 + x10
x1 ~ x5 + x7
eta2 ~ eta1 + eta4
x4 ~ eta3 + x5
x9 ~ x4
x2 ~ x5
x8 ~ x2
eta1 ~ x3
