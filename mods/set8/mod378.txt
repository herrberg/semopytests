eta1 =~ y2 + y3 + y5
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta3 ~ x8
x2 ~ eta3
x3 ~ eta3 + x1 + x10 + x7
eta1 ~ eta3 + x9
x5 ~ eta1 + x6
eta2 ~ x5
x4 ~ eta1
eta4 ~ eta1
