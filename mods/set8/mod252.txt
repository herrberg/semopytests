eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta2 ~ x4
x2 ~ eta2
x7 ~ x2 + x3
x6 ~ eta1
x3 ~ x6
x10 ~ x3
x8 ~ eta3 + x10
x9 ~ x2 + x5
eta4 ~ eta3
x1 ~ eta4
