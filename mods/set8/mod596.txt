eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
x2 ~ eta2
eta4 ~ x1 + x2
eta3 ~ eta4 + x7
x8 ~ eta3 + x5
x9 ~ x2
x4 ~ eta3 + x10
x3 ~ x1
eta1 ~ x3
x6 ~ x2
