eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
x5 ~ eta3 + x2
x7 ~ x5
x1 ~ eta1 + x7
x10 ~ x1
x4 ~ x10
eta2 ~ x4
eta4 ~ x1 + x3
x2 ~ x8 + x9
x6 ~ x5
