eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y3 + y9
x7 ~ x5
eta3 ~ eta2 + x1 + x7
x6 ~ eta3
x10 ~ x6
x3 ~ eta3
x9 ~ x1
eta1 ~ eta2 + eta4
x8 ~ eta1
x2 ~ eta3
eta4 ~ x4
