eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y9
eta3 ~ eta4
eta2 ~ eta3
x10 ~ eta2
x8 ~ x10 + x4
x4 ~ x1 + x2 + x5 + x7
x9 ~ x4
x2 ~ x6
eta1 ~ x1
x3 ~ x6
