eta1 =~ y1 + y3
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
eta2 ~ eta1 + x10 + x7
x9 ~ eta2 + eta4 + x1
x6 ~ x9
x4 ~ x3
x7 ~ x4 + x5
x2 ~ eta2 + eta3
x8 ~ x7
