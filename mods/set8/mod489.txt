eta1 =~ y1 + y2 + y3
eta2 =~ y11 + y4 + y5
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta1 ~ x4
x6 ~ eta1 + x7 + x8
x10 ~ x2 + x6
x9 ~ x10
x8 ~ x1
eta3 ~ x6
x5 ~ eta1
x2 ~ eta4
eta2 ~ x8
x3 ~ x10
