eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y7 + y8
eta4 =~ y10 + y8 + y9
x5 ~ x8
x3 ~ x5
x2 ~ eta1 + x3
x7 ~ x8
eta1 ~ eta4 + x1
x10 ~ x8
eta2 ~ x10 + x6
x4 ~ x3
x1 ~ eta3
x9 ~ x8
