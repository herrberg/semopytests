eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x8 ~ eta3 + x10 + x3
x1 ~ eta2 + x3
x2 ~ x1
eta4 ~ x2 + x6 + x9
eta1 ~ x10 + x5
x4 ~ eta1
x7 ~ eta2
