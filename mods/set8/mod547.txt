eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y2 + y6 + y7
eta4 =~ y10 + y9
eta2 ~ x1
x5 ~ eta2 + eta3 + x8
x7 ~ x5
x9 ~ eta2
x8 ~ x6
eta3 ~ eta4
x4 ~ x10 + x6
x2 ~ x4
x3 ~ eta4
eta1 ~ x6
