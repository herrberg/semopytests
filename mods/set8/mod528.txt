eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x4 ~ x2
eta1 ~ eta4 + x4
x1 ~ eta1
x7 ~ x1 + x6 + x9
x5 ~ x4
eta4 ~ x3
x10 ~ x2
eta3 ~ x1
eta2 ~ x4
x8 ~ eta4
