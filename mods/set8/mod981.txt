eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y8
eta4 =~ y10 + y8 + y9
x10 ~ eta3
eta1 ~ x10
x3 ~ eta1 + x9
x7 ~ eta2
x9 ~ x7
eta4 ~ x10
x4 ~ eta1
x1 ~ x6 + x9
x8 ~ x5 + x9
x2 ~ x10
