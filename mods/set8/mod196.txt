eta1 =~ y1 + y3
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
x4 ~ x6
x8 ~ eta3 + x4
x5 ~ x2 + x8
x9 ~ x5
x3 ~ x9
eta1 ~ x7 + x8
x10 ~ eta4
x7 ~ x10
eta2 ~ x8
x1 ~ eta3
