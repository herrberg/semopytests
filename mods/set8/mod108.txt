eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y3 + y9
eta4 ~ x2
x10 ~ eta4
x5 ~ x10 + x7
x8 ~ x5
x1 ~ eta4
eta3 ~ eta1
x7 ~ eta3 + x3
x6 ~ eta4
x4 ~ eta4
eta2 ~ x2
x9 ~ x10
