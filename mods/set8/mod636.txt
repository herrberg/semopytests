eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
x5 ~ eta3
eta4 ~ x5
x8 ~ eta1 + eta4
x7 ~ x5 + x9
x10 ~ x7
x9 ~ x6
x1 ~ x3 + x9
x4 ~ x1
eta2 ~ x7
x2 ~ eta4
