eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y8
eta1 ~ eta4
eta2 ~ eta1
x6 ~ eta2
eta3 ~ eta2 + x4 + x8 + x9
x2 ~ eta3
x4 ~ x7
x1 ~ eta1
x5 ~ eta2
x3 ~ eta4
x10 ~ x3
