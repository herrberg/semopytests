eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta2 ~ eta4
x9 ~ eta2 + x10 + x8
x8 ~ x4
x5 ~ eta4
x7 ~ x5
eta3 ~ x7
eta1 ~ x8
x3 ~ eta1
x6 ~ x8
x2 ~ x6
x1 ~ x8
