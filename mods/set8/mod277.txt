eta1 =~ y1 + y2 + y3
eta2 =~ y10 + y4
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
x9 ~ eta1
x7 ~ x10 + x4 + x9
x8 ~ x7
x3 ~ eta4 + x9
x6 ~ x1
x5 ~ x6
x4 ~ x5
eta2 ~ x7
eta3 ~ x6
x2 ~ eta3
