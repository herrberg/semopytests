eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x5 ~ x2 + x4
x6 ~ x1 + x5
x7 ~ x6 + x8
eta2 ~ x10 + x7
eta1 ~ eta2
eta3 ~ x7
x3 ~ eta3
x9 ~ x3
eta4 ~ eta2
