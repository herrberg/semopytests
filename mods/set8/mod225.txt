eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
x3 ~ x5
eta3 ~ x3 + x6
x9 ~ eta3 + x10 + x7 + x8
x4 ~ x9
x8 ~ x2
eta1 ~ x6
x1 ~ eta1
eta2 ~ x10
eta4 ~ x2
