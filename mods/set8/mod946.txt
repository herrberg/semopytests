eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta4 ~ eta3
x1 ~ eta4 + x4
x8 ~ x1
x7 ~ x8
x2 ~ eta2 + x7
eta1 ~ x2
x10 ~ eta1 + x3
x5 ~ x8
x4 ~ x6
x9 ~ eta4
