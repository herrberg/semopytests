eta1 =~ y1 + y2 + y3
eta2 =~ y3 + y4 + y5
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
x10 ~ eta1 + x1 + x4
x5 ~ x10
eta3 ~ x5
x2 ~ eta3 + x3 + x7 + x8
x9 ~ x5
eta4 ~ x9
eta2 ~ x5
x6 ~ x5
