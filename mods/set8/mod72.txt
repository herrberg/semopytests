eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y1 + y10 + y11
x9 ~ x7
x8 ~ x4 + x6 + x9
x4 ~ eta4
x6 ~ eta1
x3 ~ x4
x1 ~ eta4
eta3 ~ x1
x10 ~ x6
eta2 ~ x10
x2 ~ eta4
x5 ~ x6
