eta1 =~ y1 + y10
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
x5 ~ x9
x3 ~ x5
x10 ~ x3
x2 ~ x10
x4 ~ eta2 + x2
eta1 ~ x3
eta3 ~ eta1
eta2 ~ x1 + x8
x7 ~ eta4 + x1
x6 ~ x3
