eta1 =~ y1 + y2
eta2 =~ y2 + y3 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta4 ~ x10
x5 ~ eta3 + eta4 + x4 + x6
x2 ~ x4
x9 ~ x6
x8 ~ x9
eta3 ~ x3
x7 ~ x9
eta1 ~ x6
x1 ~ x10
eta2 ~ x6
