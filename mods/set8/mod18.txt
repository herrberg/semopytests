eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y2 + y7 + y8
eta4 =~ y10 + y11 + y9
eta4 ~ x3
x6 ~ eta4
eta1 ~ x6 + x9
x5 ~ eta1
eta2 ~ x4 + x6
x8 ~ x3
x7 ~ x10
x9 ~ x7
x1 ~ eta1
x2 ~ x9
eta3 ~ x2
