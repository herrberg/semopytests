eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y7 + y8
eta3 ~ eta1 + x9
x4 ~ eta3
eta4 ~ x4
x2 ~ eta4 + x1
x6 ~ x2
x8 ~ x1
x5 ~ x8
x10 ~ eta3
x7 ~ eta3
eta2 ~ eta3
x3 ~ eta2
