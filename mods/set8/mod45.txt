eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta1 ~ eta3 + x1
x8 ~ eta1 + x9
eta4 ~ x8
x6 ~ eta4 + x3 + x5
x9 ~ x10
x2 ~ x8
x4 ~ eta2 + x2
x7 ~ eta3
