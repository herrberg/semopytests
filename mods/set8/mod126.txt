eta1 =~ y2 + y8
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
x6 ~ eta1 + eta4 + x5 + x7
x10 ~ x1 + x6
eta2 ~ x10
x9 ~ x7
x8 ~ x9
x3 ~ eta1
x4 ~ eta1
x5 ~ eta3
x2 ~ eta4
