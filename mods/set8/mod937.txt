eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y7
eta3 =~ y7 + y8
eta4 =~ y10 + y9
x8 ~ eta2 + eta4 + x1 + x3
x6 ~ x10 + x8
eta3 ~ x1 + x5 + x7
x3 ~ x2
x4 ~ eta2 + x9
eta1 ~ x4
