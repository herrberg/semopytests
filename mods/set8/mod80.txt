eta1 =~ y2 + y3
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
x3 ~ eta2 + x6
x7 ~ eta4 + x1 + x3
x10 ~ eta3 + x5 + x7
x4 ~ x7
eta1 ~ x2 + x4
x9 ~ eta4
x8 ~ x3
