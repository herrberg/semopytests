eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y5 + y8
x10 ~ x8
x9 ~ x10
eta2 ~ x1 + x9
x2 ~ x9
x3 ~ x10 + x5
eta3 ~ x3
eta4 ~ x8
x4 ~ x6 + x8
x6 ~ eta1
x7 ~ x3
