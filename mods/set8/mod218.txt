eta1 =~ y1 + y2 + y3
eta2 =~ y5 + y6 + y7
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
x10 ~ x5 + x7
eta3 ~ x10 + x9
x2 ~ eta3
eta2 ~ x2
x6 ~ eta2
x8 ~ x7
eta1 ~ x1 + x10
x5 ~ x4
eta4 ~ x2
x3 ~ x4
