eta1 =~ y1 + y2 + y3
eta2 =~ y10 + y4 + y5
eta3 =~ y7 + y8
eta4 =~ y10 + y9
x9 ~ eta2
eta3 ~ eta4 + x2 + x9
eta1 ~ eta3
eta4 ~ x1 + x10 + x5
x4 ~ x1
x8 ~ x1
x7 ~ eta2 + x3
x3 ~ x6
