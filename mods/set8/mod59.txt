eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y4 + y7 + y8
eta4 =~ y10 + y11 + y9
eta1 ~ eta4
x5 ~ eta1
x3 ~ x5 + x6
x1 ~ x2 + x3
eta3 ~ x1 + x8
x4 ~ eta1
eta2 ~ x10
x8 ~ eta2
x9 ~ x5
x7 ~ x9
