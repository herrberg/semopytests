eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
x6 ~ x7 + x9
x10 ~ x6
x2 ~ x10
eta2 ~ eta1 + x2
x5 ~ eta2
eta4 ~ eta3 + x5
x3 ~ eta2 + x8
x1 ~ x2
x4 ~ eta1
