eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y4 + y7 + y8
eta4 =~ y10 + y9
x9 ~ eta1 + eta4 + x2 + x7
x7 ~ x3 + x5
eta3 ~ x10 + x7
x4 ~ eta1 + x1
x8 ~ eta1 + eta2
x6 ~ eta1
