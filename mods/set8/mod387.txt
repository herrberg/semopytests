eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta2 ~ x2 + x3 + x7
eta4 ~ eta2 + x5
x3 ~ x1 + x4
x10 ~ x3
x9 ~ eta2
eta3 ~ x9
x5 ~ x8
eta1 ~ x2
x6 ~ eta2
