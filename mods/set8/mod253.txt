eta1 =~ y1 + y3 + y8
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
x10 ~ x3
x4 ~ x10
x5 ~ x4
eta4 ~ x5
x1 ~ eta3 + x3 + x6
x8 ~ x1
x7 ~ x9
x6 ~ x7
x2 ~ x3
eta1 ~ x1
eta2 ~ x7
