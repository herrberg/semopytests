eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta3 ~ eta4 + x6 + x9
x1 ~ eta3
x4 ~ eta3
x7 ~ x4
x6 ~ x10
x8 ~ eta3
eta1 ~ x4
eta2 ~ x3 + x4
x2 ~ eta4
x5 ~ eta4
