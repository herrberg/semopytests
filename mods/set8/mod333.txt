eta1 =~ y1 + y2 + y3
eta2 =~ y10 + y4 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta3 ~ x6
x10 ~ eta3 + x2 + x4 + x7
x2 ~ x5
x3 ~ x2
x8 ~ x3
x9 ~ eta2 + eta4 + x3
x1 ~ x3
eta1 ~ x6
