eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
x10 ~ x1
x6 ~ eta2 + x10
x5 ~ eta1 + x6
x8 ~ x5
x2 ~ x9
eta2 ~ eta3 + x2 + x7
x3 ~ eta2 + x4
eta4 ~ x9
