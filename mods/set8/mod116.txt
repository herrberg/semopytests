eta1 =~ y2 + y3 + y7
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
x5 ~ eta1 + eta3 + x9
x7 ~ eta4 + x5
x8 ~ x7
x4 ~ eta3
x10 ~ eta3
eta1 ~ x1
x2 ~ x9
eta2 ~ eta4 + x3
x6 ~ x9
