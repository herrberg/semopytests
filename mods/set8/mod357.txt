eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
x7 ~ x6
x3 ~ x7
eta3 ~ x3
x10 ~ eta3
x1 ~ x3 + x5
x2 ~ x3
eta2 ~ x6
eta1 ~ x3
x4 ~ x6 + x9
eta4 ~ x6
x8 ~ x5
