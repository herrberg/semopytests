eta1 =~ y1 + y2 + y3
eta2 =~ y3 + y4 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
x4 ~ eta4 + x1
eta3 ~ x10 + x3 + x4
x8 ~ eta3
x5 ~ eta2 + x2 + x4
eta1 ~ x10
x6 ~ eta3
x9 ~ x7
x3 ~ x9
