eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y5
x2 ~ x7 + x9
eta1 ~ eta4 + x1 + x2 + x8
x5 ~ eta4
eta2 ~ eta3 + x10 + x5
x1 ~ x6
x4 ~ x9
x3 ~ x6
