eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y1 + y10
eta4 ~ x4 + x6
x5 ~ eta4 + x10
x1 ~ x4
x2 ~ x1
eta2 ~ eta4
x9 ~ eta3 + x4
eta1 ~ x6
eta3 ~ x3
x8 ~ eta3
x7 ~ eta3
