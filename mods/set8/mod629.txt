eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y11 + y6 + y8
eta4 =~ y10 + y11 + y9
x10 ~ x6
eta3 ~ x10
x8 ~ eta3
x7 ~ eta4 + x8
eta2 ~ x7
eta4 ~ x3
x2 ~ eta4
x1 ~ eta3
x5 ~ eta3
x4 ~ x10
x9 ~ eta3
eta1 ~ x8
