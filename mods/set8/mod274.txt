eta1 =~ y2 + y3 + y5
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
x7 ~ x4
x8 ~ eta2 + x7
eta1 ~ x8
x9 ~ eta1
eta4 ~ x7
x1 ~ eta4
x5 ~ x1
x3 ~ eta1 + x10
eta2 ~ x6
eta3 ~ eta4
x2 ~ eta3
