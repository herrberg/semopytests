eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x4 ~ x6 + x7
x8 ~ x4
x5 ~ x10 + x8
eta2 ~ x10
eta3 ~ eta2 + x2
eta1 ~ eta3
eta4 ~ x10
x1 ~ x2
x3 ~ x4
x9 ~ x8
