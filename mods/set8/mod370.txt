eta1 =~ y1 + y2 + y3
eta2 =~ y11 + y4 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta3 ~ x9
x2 ~ eta3 + x10
x5 ~ eta1 + eta4 + x2
x4 ~ eta3
x8 ~ eta2 + eta3
x1 ~ eta4
x6 ~ x10 + x7
x3 ~ x2
