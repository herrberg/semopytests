eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y5
x10 ~ eta4 + x1
x2 ~ x10
eta1 ~ x10
x5 ~ x10 + x8
eta2 ~ eta3 + x5
x4 ~ eta2 + x7
eta3 ~ x6
x9 ~ eta2
x3 ~ eta2
