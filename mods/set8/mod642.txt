eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
x5 ~ eta1
x6 ~ x3 + x5
x2 ~ x7 + x9
x10 ~ eta4 + x1 + x2
x3 ~ x10
x4 ~ eta3 + x5 + x8
eta3 ~ eta2
