eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y7
eta4 ~ eta1
x2 ~ eta4
eta2 ~ x2
x9 ~ eta2
x10 ~ eta1 + x8
x4 ~ x10 + x3
eta3 ~ x4
x6 ~ x3
x7 ~ x3
x1 ~ x8
x5 ~ x1
