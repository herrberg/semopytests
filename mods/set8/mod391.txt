eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y5 + y7 + y8
eta4 =~ y10 + y9
x4 ~ x1 + x2 + x3 + x7
eta3 ~ x4
x2 ~ x8
x5 ~ eta4 + x8
x9 ~ x7
eta1 ~ eta2 + x2
x6 ~ x8
x10 ~ x7
