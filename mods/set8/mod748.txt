eta1 =~ y1 + y2 + y3
eta2 =~ y11 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
x3 ~ x5
x1 ~ eta2 + x3 + x6
eta4 ~ x1 + x4
x7 ~ eta4
x2 ~ x7
x10 ~ x1
eta1 ~ x6
x9 ~ eta1
x8 ~ eta1
eta3 ~ eta2
