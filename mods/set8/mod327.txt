eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y5 + y7
eta4 =~ y10 + y8 + y9
x3 ~ eta1
x9 ~ eta2 + x3
x8 ~ x10 + x9
eta2 ~ x5 + x7
eta4 ~ eta2
x4 ~ x5 + x6
x1 ~ x10
x2 ~ x5
eta3 ~ x6
