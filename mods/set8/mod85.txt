eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x6 ~ eta4 + x10 + x4 + x5
x2 ~ x6 + x9
eta2 ~ x5
eta3 ~ x1 + x5
eta1 ~ eta3
x7 ~ eta1
x3 ~ x9
x8 ~ x6
