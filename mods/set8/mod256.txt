eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y1 + y5 + y7
eta4 =~ y10 + y8 + y9
eta3 ~ x4
eta1 ~ eta3
eta4 ~ eta1
x5 ~ eta4
x9 ~ x5
x3 ~ x9
x10 ~ eta1
eta2 ~ eta3
x2 ~ eta2
x7 ~ x2
x1 ~ eta1
x8 ~ eta1
x6 ~ eta4
