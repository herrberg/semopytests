eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y2
x4 ~ eta1
eta3 ~ x4
x10 ~ eta3 + x2 + x9
eta4 ~ x10 + x8
x5 ~ eta4
x9 ~ x7
x6 ~ x4
eta2 ~ x2
x3 ~ eta2 + x1
