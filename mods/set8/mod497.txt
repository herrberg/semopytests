eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
x1 ~ eta2 + x4 + x7
x9 ~ eta2 + x5
eta1 ~ eta2
eta4 ~ x4 + x8
x3 ~ eta2 + x10 + x6
x2 ~ x6
eta3 ~ eta2
