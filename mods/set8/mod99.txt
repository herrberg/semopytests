eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y5 + y8
x8 ~ x4
x5 ~ x8
x10 ~ eta1 + eta2 + x4
eta4 ~ x8
x6 ~ eta4
eta1 ~ x2
x9 ~ x4
x3 ~ eta4
eta3 ~ x3
x7 ~ x4
x1 ~ x2
