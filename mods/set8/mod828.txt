eta1 =~ y1 + y2 + y3
eta2 =~ y3 + y4
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
x9 ~ x3
x7 ~ x1 + x5 + x9
x1 ~ eta1
eta4 ~ eta1
x8 ~ x2
x5 ~ x8
x6 ~ x9
x10 ~ x9
eta2 ~ x10
eta3 ~ x1
x4 ~ x1
