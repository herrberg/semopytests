eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta1 ~ x10 + x3
x4 ~ eta1
x9 ~ eta3 + x4 + x8
eta4 ~ eta1
x7 ~ eta4
x6 ~ eta1 + eta2
x5 ~ x10
x1 ~ x5
x2 ~ eta1
