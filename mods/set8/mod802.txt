eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x1 ~ x4 + x7
eta3 ~ eta1 + x1
x6 ~ eta3
x3 ~ x6
x10 ~ x3
x9 ~ x4
eta2 ~ x4
eta4 ~ eta2
x2 ~ eta4
x5 ~ x2
eta1 ~ x8
