eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y6 + y7
eta3 =~ y7 + y8
eta4 =~ y10 + y9
x2 ~ x4 + x7 + x9
x6 ~ x2
eta4 ~ eta1 + x7
eta3 ~ eta1 + eta2
x10 ~ x7
x5 ~ x7
x3 ~ x5 + x8
x1 ~ x7
