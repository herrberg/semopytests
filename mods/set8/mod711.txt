eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y8
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta2 ~ x3
eta3 ~ eta2
x8 ~ eta3
x6 ~ x3 + x9
x4 ~ x6
eta1 ~ x4
x5 ~ eta1
x1 ~ eta2 + x2
eta4 ~ x3
x10 ~ eta2
x7 ~ x10
