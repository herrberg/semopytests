eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
x7 ~ x5
x6 ~ x10 + x7
eta3 ~ x6
x1 ~ eta3 + x2
eta1 ~ x6
x4 ~ eta1
eta4 ~ x4
x8 ~ eta1 + x9
x3 ~ eta2 + x5
