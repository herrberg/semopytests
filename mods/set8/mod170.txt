eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y1 + y10
x2 ~ x4
x10 ~ x1 + x2
eta2 ~ eta1 + x10
x7 ~ eta2
x9 ~ x10
x5 ~ x9
eta1 ~ eta3 + x3 + x8
x6 ~ eta2
eta4 ~ x4
