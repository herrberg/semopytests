eta1 =~ y1 + y2 + y3
eta2 =~ y3 + y4 + y5
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
x1 ~ x2 + x5
x6 ~ x1
x10 ~ x6
x8 ~ x6
x4 ~ x8
eta1 ~ eta2 + x6
x7 ~ eta3 + x3 + x8
x9 ~ x7
eta4 ~ x2
