eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y1 + y7 + y8
eta4 =~ y10 + y11
x9 ~ x7 + x8
x5 ~ eta4 + x9
x1 ~ x5
x2 ~ x1
x8 ~ x10
eta3 ~ x9
eta2 ~ eta3
eta4 ~ x6
x4 ~ x5
eta1 ~ x5
x3 ~ eta4
