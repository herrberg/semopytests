eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x7 ~ x1 + x10 + x3
x1 ~ eta1 + x4
x8 ~ x1
eta2 ~ x1 + x6
eta3 ~ eta2
x5 ~ x1
x9 ~ x5
x2 ~ x1
x4 ~ eta4
