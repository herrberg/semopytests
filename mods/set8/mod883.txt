eta1 =~ y2 + y3 + y5
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
x6 ~ x3
x9 ~ eta1 + x1 + x2 + x6
eta4 ~ x9
x8 ~ x6 + x7
x10 ~ x8
eta3 ~ x9
eta2 ~ x6
eta1 ~ x5
x4 ~ x3
