eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y8
x6 ~ x1
x2 ~ x4 + x6
x9 ~ eta3 + x10 + x2
eta2 ~ x9
x3 ~ x6
eta4 ~ x3 + x8
x7 ~ eta4
x5 ~ eta1
x10 ~ x5
