eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y8
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta2 ~ eta3 + x3
x10 ~ eta2
eta3 ~ eta1 + x8
x4 ~ x3
x5 ~ x4
x2 ~ x3
x1 ~ x2
x6 ~ eta3 + eta4
x7 ~ x3
x9 ~ eta1
