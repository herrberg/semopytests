eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y2
x5 ~ eta2 + x2 + x7
x1 ~ x2
x3 ~ x2 + x9
x8 ~ x10 + x3
eta3 ~ x8
x7 ~ x4
x6 ~ eta4 + x4
eta1 ~ x8
