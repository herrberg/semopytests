eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y9
x8 ~ eta3 + x7
x5 ~ eta2 + x8
eta4 ~ x9
eta3 ~ eta4 + x10
x10 ~ x4
eta1 ~ x10 + x3
x6 ~ x8
x2 ~ x10
x3 ~ x1
