eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
x3 ~ x2
eta3 ~ x3
x9 ~ eta3 + x7
eta4 ~ x9
x7 ~ x4
x8 ~ eta3
x10 ~ eta2 + x9
eta2 ~ x1
x6 ~ x7
eta1 ~ x6
x5 ~ x4
