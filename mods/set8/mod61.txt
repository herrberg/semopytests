eta1 =~ y1 + y2 + y3
eta2 =~ y1 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
x10 ~ eta1 + x6 + x9
x8 ~ eta2 + x10
eta3 ~ x8
x7 ~ eta4
eta2 ~ x7
x2 ~ x10
eta1 ~ x5
x4 ~ x10
x3 ~ x5
x1 ~ x8
