eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y6 + y8
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
x10 ~ eta1
x5 ~ x10 + x4
x3 ~ x5
eta4 ~ eta3 + x5 + x8
eta2 ~ eta4
x9 ~ x5
x1 ~ x6 + x9
x2 ~ x7
x8 ~ x2
