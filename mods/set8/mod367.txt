eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y5 + y8
x7 ~ eta1 + x3
x1 ~ eta2 + x7
x10 ~ x1
x8 ~ eta1
x4 ~ x8
x3 ~ x5 + x9
x2 ~ x3
x6 ~ x2
eta4 ~ eta2
eta3 ~ x9
