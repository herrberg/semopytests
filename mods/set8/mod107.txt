eta1 =~ y1 + y3
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
x3 ~ x1 + x10
eta1 ~ x10 + x7
x9 ~ eta1 + x2 + x6
x5 ~ eta2 + x10
eta4 ~ eta1
x4 ~ x7
eta3 ~ eta2
x8 ~ eta2
