eta1 =~ y1 + y2
eta2 =~ y3 + y5 + y7
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
x2 ~ x3 + x5 + x9
x7 ~ x2
x4 ~ x7
eta2 ~ x10 + x4
eta3 ~ x9
eta4 ~ x2
x1 ~ x9
x8 ~ x1
eta1 ~ x2
x6 ~ x9
