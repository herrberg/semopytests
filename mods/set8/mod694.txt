eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y4 + y7 + y8
eta4 =~ y10 + y9
eta2 ~ x4
x5 ~ eta2
x10 ~ x5
x3 ~ x10
x1 ~ eta1 + x5
x9 ~ x1
eta3 ~ eta4 + x1
x6 ~ x1
x8 ~ eta1 + x2
x7 ~ x4
