eta1 =~ y1 + y2 + y3
eta2 =~ y5 + y6 + y7
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
x9 ~ eta1
x5 ~ x10 + x9
x8 ~ x1 + x5
eta2 ~ x8
x4 ~ eta2
x2 ~ x4 + x6
eta3 ~ x2
x3 ~ x8
x7 ~ x8
eta4 ~ x4
