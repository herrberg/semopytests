eta1 =~ y2 + y3 + y7
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta2 ~ x10 + x9
x8 ~ eta2 + x5
eta3 ~ x8
x4 ~ eta2
eta1 ~ x10 + x2
eta4 ~ x10 + x6
x3 ~ x5 + x7
x1 ~ eta2
