eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta3 ~ x10
x2 ~ eta3
x8 ~ x2
x7 ~ x8
eta2 ~ x10
eta1 ~ eta2
x1 ~ eta3 + x5
x6 ~ eta4 + x1
x4 ~ x2
x9 ~ x2 + x3
