eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y11 + y12 + y3
x1 ~ x10 + x5
x7 ~ eta1 + x1 + x9
eta3 ~ x7
eta2 ~ x1
eta1 ~ eta4
x6 ~ eta1 + x3
x4 ~ x6
x8 ~ x7
x2 ~ x1
