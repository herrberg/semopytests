eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y3 + y8 + y9
x1 ~ eta2 + eta3 + x9
x3 ~ x7
x8 ~ x3
x9 ~ eta4 + x8
eta1 ~ x8
x4 ~ x9
x6 ~ x7
x5 ~ x3
x2 ~ eta3
x10 ~ x9
