eta1 =~ y10 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta3 ~ x7
x6 ~ eta2 + eta3 + x5
x10 ~ x6
x2 ~ x10
x9 ~ x5
eta1 ~ x3 + x5
eta4 ~ eta1
x1 ~ x3
x8 ~ x1
x4 ~ x3
