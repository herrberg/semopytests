eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y2
x5 ~ eta2 + x2 + x8
x3 ~ eta3 + x5
eta3 ~ x7
x8 ~ eta1
x10 ~ eta3
eta4 ~ x10
x1 ~ eta1
x6 ~ x7
x4 ~ x7
x9 ~ eta3
