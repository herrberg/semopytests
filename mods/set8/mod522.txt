eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y8
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta2 ~ eta1 + eta3 + x1
x3 ~ eta2 + x10 + x2
eta4 ~ x3
x5 ~ eta2 + x7 + x9
x4 ~ x5
x6 ~ x10
x8 ~ x7
