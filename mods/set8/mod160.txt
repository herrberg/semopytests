eta1 =~ y2 + y3 + y8
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta2 ~ x1
x5 ~ eta2
x10 ~ eta4 + x5
x3 ~ x1
x9 ~ x2 + x3
x4 ~ x8 + x9
x6 ~ eta1 + x1
eta3 ~ x6
x7 ~ x1
