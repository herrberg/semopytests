eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y4 + y5 + y7
eta4 =~ y10 + y8 + y9
x8 ~ eta1
x5 ~ x8
x6 ~ x2 + x5
x10 ~ eta2 + x6
x7 ~ eta2
x1 ~ x5
x9 ~ x1
eta4 ~ x2 + x4
x3 ~ eta2
eta3 ~ x2
