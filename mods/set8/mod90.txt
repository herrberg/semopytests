eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y5 + y7 + y8
eta4 =~ y10 + y11 + y9
x8 ~ x10
eta3 ~ x8
x2 ~ x8 + x9
x5 ~ eta4 + x2
x4 ~ x1 + x8
x7 ~ eta1 + x10
x3 ~ x9
eta2 ~ x3
x6 ~ x10
