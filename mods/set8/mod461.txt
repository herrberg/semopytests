eta1 =~ y10 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
x4 ~ eta1 + eta2 + x5 + x8
x5 ~ x9
x2 ~ eta1
x6 ~ x5
x7 ~ x6
x10 ~ eta1 + eta4
x3 ~ x5
x1 ~ x6
x8 ~ eta3
