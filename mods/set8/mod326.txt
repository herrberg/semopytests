eta1 =~ y1 + y2 + y3
eta2 =~ y10 + y4 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
x9 ~ eta1 + x7
eta4 ~ x9
eta2 ~ eta3 + eta4 + x3 + x5
x4 ~ eta2
x6 ~ x7
x10 ~ x2 + x6
x2 ~ x8
x1 ~ x9
