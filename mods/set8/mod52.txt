eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
x1 ~ eta4
x2 ~ x1 + x10 + x4 + x5
x8 ~ eta2 + x2 + x7
eta3 ~ x8
eta1 ~ x1 + x6
x3 ~ x2
x9 ~ x1
