eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta4 ~ eta1 + x8
x9 ~ eta4 + x1 + x3 + x5
x10 ~ eta1 + x4
x2 ~ eta1 + eta2
x7 ~ x1
eta2 ~ eta3
x6 ~ x4
