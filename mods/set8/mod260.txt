eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x10 ~ eta1
eta3 ~ eta2 + eta4 + x10
x2 ~ eta3
x5 ~ eta2
x4 ~ x5
x7 ~ x4 + x6
x9 ~ x1 + x7 + x8
x6 ~ x3
