eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
x10 ~ eta4
eta3 ~ x10 + x6
x8 ~ eta3
x3 ~ x1 + x8
x7 ~ x10
eta2 ~ eta1 + x5 + x7
x1 ~ x9
x2 ~ eta3
x4 ~ eta4
