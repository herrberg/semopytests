eta1 =~ y2 + y5
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
x1 ~ eta3
x10 ~ x1
x7 ~ eta4 + x10
eta1 ~ x7
eta2 ~ x1
x6 ~ eta2
x8 ~ x10 + x3 + x4
x9 ~ x1
x2 ~ x1
x5 ~ x7
