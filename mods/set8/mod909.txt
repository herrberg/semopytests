eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y3
x1 ~ x5 + x9
eta2 ~ x4 + x6 + x9
x7 ~ eta1 + eta2 + x10
x3 ~ eta3
x4 ~ x3
x5 ~ eta4
x8 ~ eta2
x2 ~ x9
