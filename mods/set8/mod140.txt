eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y2 + y5 + y6
eta4 =~ y10 + y8 + y9
x4 ~ eta2 + x2
x10 ~ x3 + x4
x3 ~ eta4
x9 ~ x1 + x4
eta1 ~ eta4
x5 ~ x3
eta3 ~ x5 + x8
x7 ~ eta4 + x6
