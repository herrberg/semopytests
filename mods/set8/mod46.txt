eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta2 ~ x4
x2 ~ eta2 + eta3
x1 ~ x2 + x5 + x7
eta4 ~ x1
x10 ~ eta1
eta3 ~ x10
x8 ~ eta3
x3 ~ x9
x6 ~ x3
x5 ~ x6
