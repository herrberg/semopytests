eta1 =~ y1 + y2
eta2 =~ y4 + y9
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
x10 ~ eta2 + eta3
x6 ~ x10 + x3
x2 ~ x6
eta2 ~ x6 + x7
x1 ~ eta2 + eta4 + x5
eta1 ~ x10
x8 ~ x10
x9 ~ eta4
x4 ~ x7
