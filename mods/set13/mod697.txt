eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y1 + y11 + y12
x8 ~ x3 + x4
eta2 ~ x8
eta1 ~ eta2 + eta4
x6 ~ x9
eta4 ~ x6
x2 ~ x4
x5 ~ x2
eta3 ~ x5
x1 ~ x6 + x7
x7 ~ x10
x3 ~ eta2
