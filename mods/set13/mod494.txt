eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
x8 ~ eta1 + eta3
x5 ~ eta2 + x8
eta1 ~ x5
x4 ~ eta1
x3 ~ x1
eta2 ~ x3
x10 ~ x8
x6 ~ eta2
x2 ~ x6
x9 ~ x2
x7 ~ x1
eta4 ~ x3
