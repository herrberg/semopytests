eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
x3 ~ eta1 + x8
x4 ~ x3 + x7
x8 ~ x4
x10 ~ eta1
x5 ~ eta4 + x10 + x9
eta3 ~ x10
eta2 ~ eta3
eta4 ~ x2
x1 ~ x4
x6 ~ x9
