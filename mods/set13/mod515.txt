eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta2 ~ eta3 + eta4
x3 ~ eta2 + x4
x1 ~ x3 + x6
eta3 ~ x1 + x2 + x9
x6 ~ x10
x7 ~ eta2
eta1 ~ x7
x2 ~ x5
x8 ~ x10
