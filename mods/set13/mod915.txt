eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y1 + y11 + y9
eta4 ~ x1 + x3
x4 ~ eta4 + x10
x10 ~ eta2 + x6
x7 ~ eta4 + x2
x3 ~ x7 + x8
x9 ~ x10 + x5
eta3 ~ eta4
eta1 ~ x10
