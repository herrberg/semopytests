eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x3 ~ eta1 + eta2
eta4 ~ x3
x6 ~ eta3 + eta4
x4 ~ x3
eta1 ~ x4
x10 ~ x2 + x3
x8 ~ eta4
eta3 ~ x7
x5 ~ x2
x9 ~ x3
x1 ~ x2
