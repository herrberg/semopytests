eta1 =~ y1 + y2 + y3
eta2 =~ y11 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta3 ~ x1 + x5
eta1 ~ eta3
x1 ~ eta1 + eta4 + x10 + x3
eta2 ~ x1
x7 ~ eta2 + x9
x2 ~ x1
x4 ~ eta2
x8 ~ eta4
x6 ~ eta1
