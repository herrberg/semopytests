eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y8 + y9
x2 ~ eta4
x9 ~ x2 + x6
x10 ~ eta3 + x9
x6 ~ x10 + x7
x8 ~ x3 + x6
x5 ~ x8
eta3 ~ eta2
eta1 ~ x3
x4 ~ x9
x1 ~ x8
