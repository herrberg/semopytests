eta1 =~ y1 + y10 + y2
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
x3 ~ x6
x9 ~ x3 + x5 + x8
eta4 ~ x2 + x9
x4 ~ eta4
x7 ~ eta3 + x9
x8 ~ x7
eta1 ~ x2
eta2 ~ x2
x1 ~ eta2
eta3 ~ x10
