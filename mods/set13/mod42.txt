eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y2 + y8
eta4 =~ y10 + y9
x3 ~ eta3 + x10
x7 ~ x3 + x6
eta3 ~ x7
eta1 ~ eta2 + eta3 + eta4
x5 ~ x1 + x8
x6 ~ x5
x2 ~ x5
x9 ~ eta4
x4 ~ x9
