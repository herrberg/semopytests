eta1 =~ y1 + y2
eta2 =~ y10 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
x3 ~ eta1 + eta4 + x5
x6 ~ x3
x5 ~ x6
x2 ~ eta3 + x1
eta1 ~ x2 + x8
eta2 ~ eta1
x4 ~ x6
x7 ~ x6
x9 ~ x8
x10 ~ x6
