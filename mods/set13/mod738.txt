eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y4 + y6 + y8
eta4 =~ y10 + y11 + y9
x7 ~ eta1 + x1 + x2
x8 ~ x7 + x9
x5 ~ x8
x2 ~ x5
x6 ~ eta4 + x2
x4 ~ x8
eta3 ~ eta1
x9 ~ eta2
x3 ~ x1
x10 ~ eta2
