eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x8 ~ eta1
eta4 ~ x8
x4 ~ eta4 + x6
eta2 ~ x5 + x8
x3 ~ eta1 + x7 + x9
eta3 ~ x3
x9 ~ eta3 + x2
x10 ~ x1 + x7
