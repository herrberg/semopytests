eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x5 ~ eta3 + x2 + x4 + x6 + x8
x1 ~ x5 + x7
x8 ~ x1
x10 ~ x3 + x5
eta1 ~ x5
x7 ~ eta4
eta3 ~ eta2 + x9
