eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y6 + y8 + y9
eta1 ~ x5
eta4 ~ eta1 + eta3 + x7
x2 ~ eta4
x9 ~ eta1 + x8
x1 ~ eta3 + x4
x10 ~ x1
eta3 ~ x10 + x6
x3 ~ eta4
eta2 ~ x7
