eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y12 + y8
eta4 ~ eta2 + x1 + x5
x9 ~ eta4 + x7 + x8
x8 ~ x3 + x4
x10 ~ eta4
eta3 ~ eta1 + x4
x2 ~ eta3
eta1 ~ x2
x6 ~ x1
