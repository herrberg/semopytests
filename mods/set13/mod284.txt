eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
x9 ~ eta4 + x2
x4 ~ x1 + x10 + x3 + x7 + x9
eta1 ~ x4 + x6
x3 ~ eta1 + eta2 + x5
x8 ~ x3
eta3 ~ eta1
