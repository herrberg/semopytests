eta1 =~ y1 + y3 + y8
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
x7 ~ x1 + x3 + x4 + x6
eta2 ~ x6
x2 ~ eta2 + x5 + x8 + x9
x10 ~ x2
x9 ~ eta3 + x10
eta4 ~ x4
eta1 ~ x5
