eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta2 ~ eta4 + x1
x9 ~ eta2
x1 ~ x9
x4 ~ x1
x7 ~ eta2
x5 ~ x3 + x6 + x7
eta3 ~ x9
x2 ~ x1
eta1 ~ x1
x10 ~ x1
x8 ~ x1
