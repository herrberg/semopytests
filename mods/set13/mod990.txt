eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
x2 ~ x4
x3 ~ eta1 + eta2 + x10 + x2
x7 ~ x4
x1 ~ x7
x9 ~ x2
x6 ~ x4
x5 ~ eta3 + x6
eta4 ~ x7
x8 ~ eta2
