eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta2 ~ x6
x5 ~ x1 + x6 + x7 + x9
x4 ~ x6
x8 ~ x4
eta4 ~ x6
x2 ~ x7
x9 ~ x3
eta1 ~ x7
eta3 ~ x7
x10 ~ x3
