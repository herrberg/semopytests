eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y6
x10 ~ x2 + x6
x1 ~ eta2 + eta4 + x10
x9 ~ eta3 + x1
eta2 ~ x9
eta4 ~ x5
eta1 ~ x1
x3 ~ x10
x7 ~ x10
x8 ~ x10
x4 ~ eta4
