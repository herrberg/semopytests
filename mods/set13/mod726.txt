eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y6
x6 ~ eta1
x4 ~ x6 + x8
x1 ~ x4
eta2 ~ x1 + x2 + x9
x8 ~ eta2
x3 ~ x9
x5 ~ x2
eta4 ~ x5
eta3 ~ x9
x10 ~ eta3
x7 ~ x4
