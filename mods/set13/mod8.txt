eta1 =~ y1 + y5
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
x1 ~ eta1 + eta2
x5 ~ x1 + x2
x8 ~ x5
x9 ~ x8
x4 ~ x5
x10 ~ x4
x2 ~ eta3 + x10
x6 ~ x10
x3 ~ x4
x7 ~ x10
eta4 ~ eta2
