eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta1 ~ eta3 + x3
x6 ~ eta1
eta3 ~ x6
x8 ~ eta3
x4 ~ x8
eta4 ~ eta1 + x10
x7 ~ eta4 + x5
x9 ~ eta1
eta2 ~ x9
x1 ~ eta2
x2 ~ x6
