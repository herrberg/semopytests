eta1 =~ y1 + y2
eta2 =~ y2 + y3
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
x2 ~ eta1 + eta2 + eta4 + x4 + x8
x3 ~ x2
x7 ~ eta1 + x9
x6 ~ x7
x5 ~ eta2
x1 ~ x5
x10 ~ x2
eta3 ~ eta2
