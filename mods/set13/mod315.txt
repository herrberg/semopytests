eta1 =~ y1 + y2 + y3
eta2 =~ y1 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
x8 ~ eta3 + x4 + x6
x5 ~ x8
eta3 ~ eta1 + x5
x10 ~ x6
x2 ~ x1 + x6 + x9
x7 ~ eta1
eta2 ~ x7
x3 ~ x4
eta4 ~ x4
