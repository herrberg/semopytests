eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta3 ~ x8 + x9
eta1 ~ eta3
eta4 ~ eta1 + x7
x6 ~ x4 + x7
x2 ~ x7
eta2 ~ x2
x5 ~ x4
x3 ~ eta1
x8 ~ x3
x10 ~ eta3
x1 ~ x4
