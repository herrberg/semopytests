eta1 =~ y2 + y3 + y8
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
x9 ~ x10 + x2
eta1 ~ eta3 + x3 + x8 + x9
x5 ~ eta4 + x10 + x4
eta3 ~ eta2
x6 ~ eta2
x1 ~ x10
x7 ~ x3
