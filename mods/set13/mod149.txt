eta1 =~ y1 + y2 + y3
eta2 =~ y5 + y6 + y8
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
x2 ~ x6
eta4 ~ x2 + x3 + x7
x4 ~ eta4 + x5
x8 ~ eta4 + x1
x3 ~ x8
x9 ~ x1 + x10
x10 ~ eta3
eta1 ~ x5
eta2 ~ x10
