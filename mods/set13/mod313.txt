eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y1 + y10 + y8
eta2 ~ x1 + x3 + x6
x4 ~ eta2
x5 ~ x4 + x9
x3 ~ x5
x7 ~ x4
eta3 ~ x7
x9 ~ eta4
x2 ~ x9
x10 ~ x7 + x8
eta1 ~ x9
