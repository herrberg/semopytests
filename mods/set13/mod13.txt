eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x5 ~ x2 + x8
eta1 ~ eta4 + x5
x10 ~ x2
x6 ~ x10 + x4
x8 ~ x3
eta3 ~ x1 + x8 + x9
eta2 ~ x2
x7 ~ x4
