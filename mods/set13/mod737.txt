eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y1 + y10 + y9
x1 ~ x8 + x9
eta1 ~ eta3 + x1 + x2
eta4 ~ eta1
x3 ~ eta4 + x6
x6 ~ x5
x9 ~ eta1
x10 ~ eta4
x4 ~ x10
eta2 ~ x4
x7 ~ x5
