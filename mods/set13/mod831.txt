eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x4 ~ eta1 + x6
x1 ~ x4
eta4 ~ x1
eta3 ~ eta4 + x2
eta2 ~ x4 + x9
x6 ~ eta2
x7 ~ x6
x3 ~ x4
x8 ~ x3
x5 ~ x1
x10 ~ eta2
