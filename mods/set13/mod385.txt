eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
x1 ~ eta1 + eta3
eta2 ~ x1
eta1 ~ eta2
x10 ~ eta1 + x2 + x8
eta4 ~ eta2 + x4
x5 ~ x2
x7 ~ x2
x8 ~ x3 + x6
x9 ~ eta2
