eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
x7 ~ eta2
x4 ~ x10 + x7
x10 ~ x6
eta1 ~ x5 + x7 + x9
x8 ~ eta1
x1 ~ x8
x3 ~ eta2
eta4 ~ eta1
x5 ~ eta4
x2 ~ x9
eta3 ~ x8
