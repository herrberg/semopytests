eta1 =~ y1 + y2
eta2 =~ y4 + y5 + y9
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
x9 ~ eta2 + eta4 + x7 + x8
x6 ~ x1 + x9
eta3 ~ eta1 + x9
x4 ~ eta3
x10 ~ x3
x1 ~ x10 + x2
x5 ~ x8
x7 ~ eta3
