eta1 =~ y1 + y2 + y3
eta2 =~ y12 + y4 + y5
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
eta2 ~ eta3 + x5
x6 ~ eta1 + eta2 + x9
eta3 ~ x6
x1 ~ eta3
eta1 ~ x10
x8 ~ x6
x3 ~ x10
x7 ~ eta2 + eta4
x2 ~ x7
x4 ~ x9
