eta1 =~ y1 + y2
eta2 =~ y10 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta4 ~ eta2
eta1 ~ eta4 + x6 + x9
eta3 ~ x6 + x8
x5 ~ eta3
x6 ~ x4 + x5
x10 ~ x5 + x7
x3 ~ eta4
x9 ~ x1
x2 ~ eta4
