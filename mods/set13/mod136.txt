eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y2 + y8 + y9
x10 ~ eta2 + x8
x8 ~ x2
x9 ~ eta2
eta3 ~ x9
x4 ~ eta4 + x6 + x7 + x8
x1 ~ eta2
x3 ~ x8
eta1 ~ x8
x5 ~ eta4
