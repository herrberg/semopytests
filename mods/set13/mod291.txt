eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y1 + y8
eta4 =~ y10 + y9
eta1 ~ x3 + x4
x8 ~ eta1
eta4 ~ x8
x10 ~ eta4
x7 ~ x10
x9 ~ eta4
x2 ~ x3
eta2 ~ x2
x5 ~ x1 + x3 + x6
eta3 ~ x5
x4 ~ x8
