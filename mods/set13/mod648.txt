eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
x4 ~ x1 + x3 + x9
eta1 ~ x4
x3 ~ eta1
eta4 ~ x9
eta2 ~ x10 + x9
x5 ~ eta2
x7 ~ x5
x2 ~ x1
x6 ~ eta3 + x4
x8 ~ eta2
