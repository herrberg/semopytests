eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
x10 ~ eta3
x5 ~ eta1 + eta4 + x1 + x10 + x7
x2 ~ x5
x9 ~ x5
x1 ~ x9
eta2 ~ eta1
x3 ~ eta2
x6 ~ x10
x8 ~ x10
x4 ~ x8
