eta1 =~ y2 + y3 + y5
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
eta2 ~ x3
x1 ~ eta1 + eta2 + x10 + x8
x6 ~ eta4 + x1 + x5
x7 ~ eta3 + x6
x2 ~ x1
x5 ~ x7
x9 ~ x5
x4 ~ x10
