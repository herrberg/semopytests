eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y5 + y6 + y8
eta4 =~ y10 + y9
x10 ~ eta3 + x2 + x7
eta4 ~ x10 + x8 + x9
x8 ~ eta2
x6 ~ eta1 + x5
eta3 ~ x4 + x6
eta1 ~ x10
x1 ~ x5
x3 ~ eta2
