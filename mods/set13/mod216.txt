eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y2
x1 ~ x2
x10 ~ eta4 + x2
x4 ~ eta2 + x10 + x8
x5 ~ x3 + x4
eta4 ~ x5
eta1 ~ eta3 + x2
x7 ~ x3 + x9
x6 ~ x8
