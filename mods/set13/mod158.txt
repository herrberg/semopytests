eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y1 + y11 + y9
eta4 ~ x3 + x4 + x7
x4 ~ eta3 + x6
x3 ~ x1 + x9
eta1 ~ x2 + x4
x5 ~ x4
eta2 ~ x9
x8 ~ x9
x10 ~ x6
