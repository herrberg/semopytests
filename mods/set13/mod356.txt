eta1 =~ y1 + y2
eta2 =~ y10 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
x10 ~ x3 + x8
x5 ~ x10
x6 ~ x5
eta1 ~ x6
eta4 ~ eta1
eta3 ~ x6
x7 ~ eta3
x8 ~ eta2 + x6
x4 ~ x8
x9 ~ eta1 + x1
eta2 ~ x2
