eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y3 + y5 + y6
eta4 =~ y10 + y8 + y9
eta1 ~ x5
x9 ~ eta1 + x1 + x2 + x4
x8 ~ x10 + x9
x4 ~ eta3 + x6 + x8
x7 ~ eta1
eta4 ~ x1
eta2 ~ eta1
x3 ~ eta3
