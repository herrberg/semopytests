eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y8
eta4 =~ y10 + y8 + y9
eta2 ~ eta1 + x7
x10 ~ eta2 + x3 + x8
x3 ~ x5
x2 ~ x1 + x7
x9 ~ eta2 + x6
eta1 ~ x4 + x9
eta3 ~ x3
x6 ~ eta4
