eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
x9 ~ eta2
x10 ~ eta3 + x9
x5 ~ x3 + x9
x7 ~ x5
x4 ~ x7
x3 ~ x4
x6 ~ eta3 + eta4
eta1 ~ x5
x8 ~ eta1
x2 ~ x9
x1 ~ x5
