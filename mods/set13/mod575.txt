eta1 =~ y1 + y2 + y3
eta2 =~ y2 + y4 + y5
eta3 =~ y7 + y8
eta4 =~ y10 + y9
x5 ~ x3 + x8
eta2 ~ x2 + x5 + x7
eta4 ~ eta2 + x6
eta1 ~ eta2
x9 ~ x5
x3 ~ x9
x10 ~ x3
x2 ~ x4
x7 ~ eta3
x1 ~ eta3
