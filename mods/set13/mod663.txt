eta1 =~ y1 + y5
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
x5 ~ x10 + x4
x8 ~ x5 + x6
x1 ~ x8 + x9
x7 ~ x1
eta3 ~ x7
x2 ~ eta3
x3 ~ x2
x4 ~ eta4 + x8
eta4 ~ eta1
eta2 ~ x9
