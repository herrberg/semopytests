eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
x8 ~ x10 + x2 + x5
eta4 ~ x8
x4 ~ eta4
x2 ~ x4 + x9
eta3 ~ eta4
eta1 ~ x1 + x4
eta2 ~ x5 + x7
x7 ~ x3
x6 ~ x9
