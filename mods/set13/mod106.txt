eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x8 ~ eta3 + eta4 + x4
x5 ~ x1 + x10 + x6 + x8
eta4 ~ x5
x4 ~ x2
x6 ~ eta2
x3 ~ eta3
x9 ~ eta1 + x3
x7 ~ x4
