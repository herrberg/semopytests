eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y2 + y8
eta4 =~ y10 + y11 + y9
x4 ~ eta3 + x6
eta2 ~ x4 + x8
x7 ~ eta2
eta4 ~ x2 + x7
eta1 ~ eta4
x10 ~ x4
x9 ~ x10
x5 ~ x4
x3 ~ x5
x1 ~ eta4
eta3 ~ x5
