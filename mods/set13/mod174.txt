eta1 =~ y1 + y3 + y8
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
x3 ~ x7
x9 ~ x3
x2 ~ x9
x4 ~ eta2 + x2
x5 ~ x4
eta2 ~ x5
x10 ~ eta2
x6 ~ x2
eta1 ~ x9
x8 ~ eta1
x1 ~ x4
eta3 ~ eta1
eta4 ~ x2
