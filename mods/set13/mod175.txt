eta1 =~ y1 + y2 + y3
eta2 =~ y10 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta4 ~ eta3 + x3 + x8
x3 ~ eta1
x2 ~ eta2 + x1
x8 ~ x2
x7 ~ x3
eta2 ~ x8
x6 ~ eta2
x9 ~ x3 + x5
x4 ~ x8
x5 ~ x10
