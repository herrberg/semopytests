eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta1 ~ x7 + x9
x8 ~ eta1
x2 ~ x10 + x8
eta2 ~ x2 + x5
x4 ~ x2
x7 ~ x2
x5 ~ eta3
x1 ~ x10
x3 ~ x1
eta4 ~ x5
x6 ~ x5
