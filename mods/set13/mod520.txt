eta1 =~ y1 + y3 + y5
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
x1 ~ eta1 + x2 + x6
x3 ~ x1 + x8 + x9
eta2 ~ eta3 + x3
x9 ~ eta2
x4 ~ x1
eta4 ~ x3 + x5
x8 ~ x10
x7 ~ x2
