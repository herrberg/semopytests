eta1 =~ y1 + y2
eta2 =~ y2 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
x6 ~ eta1
x5 ~ x10 + x6
x1 ~ x5
x10 ~ x1 + x8
x2 ~ x10
x9 ~ x2 + x3 + x7
eta4 ~ x9
x4 ~ eta3
x7 ~ eta2 + x4
