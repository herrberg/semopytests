eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x3 ~ eta1 + x1 + x6
x10 ~ x3 + x9
x2 ~ x10
eta4 ~ x2 + x5
x4 ~ x10
x7 ~ x4
eta1 ~ x7
eta3 ~ x10
x8 ~ x10
eta2 ~ x4
