eta1 =~ y1 + y3 + y8
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta3 ~ x2 + x8 + x9
eta1 ~ eta3
x2 ~ eta1
x4 ~ x2
x7 ~ eta1 + x1
eta2 ~ x7
x6 ~ eta2
x10 ~ x8
x3 ~ eta3 + eta4
x5 ~ x8
