eta1 =~ y1 + y2 + y8
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
eta1 ~ x2 + x5
x9 ~ eta1
x10 ~ eta4 + x7 + x9
x4 ~ x10 + x6
eta4 ~ x4
x8 ~ x2
eta2 ~ eta1 + x3
eta3 ~ x5
x1 ~ x6
