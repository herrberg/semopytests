eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y7
x1 ~ x10 + x6
eta3 ~ eta2 + x1
x2 ~ eta3 + x7 + x8
eta1 ~ x1
x10 ~ eta1
eta4 ~ x8
x4 ~ x1
x9 ~ x4
x3 ~ x9
x7 ~ x5
