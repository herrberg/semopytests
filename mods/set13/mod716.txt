eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y1 + y5 + y7
eta4 =~ y10 + y8 + y9
x5 ~ eta2
eta4 ~ x1 + x5
eta1 ~ eta4 + x4 + x6
x1 ~ eta1
eta3 ~ x3 + x5
x9 ~ eta3 + x10 + x7
x8 ~ x2
x10 ~ x8
