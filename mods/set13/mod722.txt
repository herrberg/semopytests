eta1 =~ y1 + y2
eta2 =~ y4 + y8
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
x7 ~ eta4 + x9
x2 ~ x7
x3 ~ x2
eta1 ~ x1 + x3
x6 ~ x3 + x4
eta3 ~ x7
eta4 ~ x2
x8 ~ eta4
eta2 ~ x9
x10 ~ x4
x5 ~ x1
