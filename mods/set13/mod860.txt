eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y2 + y7 + y8
eta4 =~ y10 + y9
x1 ~ eta1 + eta4 + x2 + x4
x3 ~ x1
eta1 ~ x10 + x3
x4 ~ x7 + x9
x6 ~ eta4 + x8
eta3 ~ x10
x5 ~ eta1
eta2 ~ eta4
