eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
x3 ~ eta3 + x6
x10 ~ x3
x6 ~ x10 + x7
x8 ~ x6
x2 ~ x10
x1 ~ x7
eta1 ~ eta2 + x1
x5 ~ eta1
x4 ~ x3 + x9
eta4 ~ x4
