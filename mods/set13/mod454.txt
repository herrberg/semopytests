eta1 =~ y1 + y2 + y3
eta2 =~ y5 + y8
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
x1 ~ x3
eta4 ~ x1 + x6 + x8
x5 ~ x1
x4 ~ x1 + x10
x7 ~ x4 + x9
x10 ~ x7
eta1 ~ eta2 + eta3 + x3
x2 ~ eta1
