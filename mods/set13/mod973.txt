eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y10 + y6 + y8
eta4 =~ y10 + y9
x4 ~ x1 + x2
eta3 ~ eta1 + eta4 + x4
x7 ~ eta3 + x3
eta4 ~ x5 + x7
x8 ~ x4
eta2 ~ x8
x10 ~ x2 + x6
x5 ~ x9
