eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta1 ~ eta2 + x4
x2 ~ eta1
x10 ~ eta3 + x2
eta4 ~ x10
x9 ~ eta3 + x1 + x6
x8 ~ x9
eta3 ~ x3 + x5 + x8
x1 ~ x7
