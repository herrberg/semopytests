eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x4 ~ x9
eta2 ~ x4
eta3 ~ eta2 + x8
x2 ~ eta3 + x7
x8 ~ eta4 + x3
x6 ~ eta4
x5 ~ x6
x10 ~ x8
x3 ~ x1 + x10
eta1 ~ x9
