eta1 =~ y1 + y2
eta2 =~ y10 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
x10 ~ x1
eta2 ~ x10 + x8
x4 ~ eta2
x2 ~ eta3 + eta4 + x4 + x6
x8 ~ x2
x7 ~ x9
x6 ~ x7
eta1 ~ x5 + x7
x3 ~ x7
