eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y8 + y9
x7 ~ eta1
x9 ~ x4 + x6 + x7
eta3 ~ x9
x6 ~ eta3 + x3
x8 ~ x1 + x7
x5 ~ eta2 + x2 + x4
x1 ~ x10
eta4 ~ eta1
