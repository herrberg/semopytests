eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
x6 ~ eta4 + x5
x3 ~ x6
x5 ~ x10 + x3 + x8
eta2 ~ x10 + x4
x2 ~ eta4
x1 ~ eta1 + eta3 + x2
x8 ~ x7
eta1 ~ x9
