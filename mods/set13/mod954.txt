eta1 =~ y1 + y2 + y3
eta2 =~ y5 + y8
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
x4 ~ x1 + x8
x2 ~ eta2 + x4
x1 ~ eta4 + x10 + x2 + x7
x6 ~ x4
eta2 ~ eta3
x5 ~ x2
x3 ~ eta2 + x9
eta1 ~ x10
