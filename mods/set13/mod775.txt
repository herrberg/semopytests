eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta3 ~ eta1 + x10 + x9
x2 ~ eta3 + x1
x5 ~ x2 + x4 + x8
x7 ~ eta3
x9 ~ x2
eta2 ~ x9
x4 ~ eta4 + x6
eta4 ~ x3
