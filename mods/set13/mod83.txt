eta1 =~ y1 + y2 + y3
eta2 =~ y10 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta3 ~ eta1 + eta4 + x9
x8 ~ eta3
x9 ~ x1 + x2
x2 ~ eta3
x3 ~ x9
x4 ~ x3
eta4 ~ x6
x10 ~ eta3
x7 ~ x9
x5 ~ x9
eta2 ~ x9
