eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
x6 ~ eta4
eta1 ~ x6
eta2 ~ eta1 + x7
x2 ~ eta1 + x9
x5 ~ x1 + x7
x8 ~ x5
eta3 ~ x10 + x3 + x5
x1 ~ eta3
x4 ~ eta1
