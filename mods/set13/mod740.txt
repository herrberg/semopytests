eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y8
x2 ~ x5 + x9
eta1 ~ x2
x9 ~ eta1 + x10 + x3 + x6 + x7 + x8
x7 ~ eta2 + eta3
x4 ~ x7
x1 ~ x6
eta4 ~ eta3
