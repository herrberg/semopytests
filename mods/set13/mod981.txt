eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y11 + y5 + y9
x1 ~ x8
x2 ~ x1 + x6
x4 ~ eta1 + x2
eta2 ~ x4 + x5
eta3 ~ eta2
x3 ~ eta3
x5 ~ eta4 + x9
x9 ~ eta2
x10 ~ eta3
x7 ~ x10
