eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y2
x4 ~ x1 + x2 + x3 + x6
x8 ~ eta3 + x10 + x4
eta2 ~ x8
x2 ~ x8 + x9
x5 ~ eta4 + x2
eta1 ~ x5 + x7
