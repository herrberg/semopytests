eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y12
x2 ~ eta1 + eta3
eta4 ~ eta2 + x2
eta1 ~ eta4 + x10 + x5 + x7 + x9
x3 ~ eta1
x9 ~ x4
x8 ~ eta2 + x6
x1 ~ x6
