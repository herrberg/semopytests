eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y10 + y6 + y8
eta4 =~ y10 + y9
x3 ~ eta2
eta3 ~ x3
x7 ~ eta3 + x8
x8 ~ eta1 + eta4 + x2
x6 ~ x8
x1 ~ eta3 + x10
x5 ~ x1
x10 ~ x5 + x9
x4 ~ eta3
