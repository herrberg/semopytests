eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x4 ~ x8
eta3 ~ x1 + x4 + x6
x1 ~ eta1 + x9
x3 ~ x1
x9 ~ x3
x2 ~ x9
x10 ~ x2
x7 ~ x1
x5 ~ x4
eta2 ~ x6
eta4 ~ eta2
