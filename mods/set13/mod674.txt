eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta3 ~ eta1 + x1 + x10 + x7 + x8
eta4 ~ eta3
x7 ~ eta4
x1 ~ x9
eta2 ~ x6
eta1 ~ eta2
x2 ~ x9
x4 ~ eta1
x3 ~ x6
x5 ~ x3
