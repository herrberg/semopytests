eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y8 + y9
x2 ~ x1 + x7
x8 ~ x2
x1 ~ x3 + x6 + x9
x5 ~ eta2 + eta4 + x1
x4 ~ x5
x3 ~ x5
x10 ~ x1
eta3 ~ eta2
eta1 ~ x5
