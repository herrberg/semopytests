eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
x1 ~ eta2 + eta4
x10 ~ x1
x7 ~ x10 + x9
x2 ~ x7
x4 ~ x5 + x7
x6 ~ x4
eta3 ~ x6
x8 ~ x4
x3 ~ x10
eta2 ~ x3
eta1 ~ x4
