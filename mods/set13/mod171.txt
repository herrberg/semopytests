eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y3 + y8
eta4 =~ y10 + y9
x1 ~ x2
eta2 ~ x1 + x10
x4 ~ eta2 + x7
x9 ~ eta1 + x4
x7 ~ eta4 + x8 + x9
x5 ~ x8
x6 ~ x5
x3 ~ x7
eta3 ~ x1
