eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y11 + y6
x5 ~ eta2 + eta3
x6 ~ x1 + x2 + x5
eta3 ~ x6
x7 ~ eta1 + x5
x9 ~ x7
x10 ~ x7
x8 ~ eta1
eta4 ~ eta2 + x4
x3 ~ eta2
