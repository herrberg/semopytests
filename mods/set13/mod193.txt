eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y7 + y8
eta4 =~ y10 + y8 + y9
x7 ~ eta2 + eta3 + x2
x3 ~ x6 + x7
eta2 ~ x3
x6 ~ x10
x1 ~ x3 + x9
x8 ~ x10
x4 ~ eta1 + x2 + x5
eta4 ~ x10
