eta1 =~ y1 + y2 + y3
eta2 =~ y10 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta2 ~ x8
x10 ~ eta2 + eta4 + x3
x2 ~ x10
x3 ~ eta1 + x2
eta3 ~ x3 + x5
x4 ~ eta3
eta1 ~ x7
x9 ~ eta2
x6 ~ x9
x1 ~ x7
