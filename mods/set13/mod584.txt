eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
x10 ~ x3 + x8
x9 ~ x10 + x4
x3 ~ eta1 + x9
eta3 ~ eta4 + x2 + x3
x6 ~ x9
x5 ~ x3
eta4 ~ x1 + x7
eta2 ~ x3
