eta1 =~ y1 + y2 + y3
eta2 =~ y5 + y8
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta4 ~ eta2 + x1 + x10 + x7 + x8
eta1 ~ eta4 + x9
eta2 ~ eta1
x6 ~ x2
x7 ~ x6
eta3 ~ x7
x3 ~ x10 + x5
x4 ~ x8
