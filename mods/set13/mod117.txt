eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y7 + y8
x9 ~ eta1 + x6
eta4 ~ x9
eta1 ~ eta3 + eta4 + x4 + x7 + x8
x7 ~ eta2 + x10
eta3 ~ x5
x2 ~ eta4
x1 ~ eta2
x3 ~ eta4
