eta1 =~ y1 + y2 + y3
eta2 =~ y2 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
x5 ~ x6
x8 ~ eta4 + x2 + x3 + x4 + x5
x9 ~ x8
eta1 ~ x4
eta4 ~ eta2 + x1
x7 ~ eta4
eta2 ~ x7
x10 ~ eta4
eta3 ~ x8
