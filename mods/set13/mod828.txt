eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y11 + y12 + y5
x2 ~ eta2 + eta3
x5 ~ x2 + x7
x4 ~ eta1 + x5
eta4 ~ x4
x7 ~ x4
x8 ~ eta1 + x10
x1 ~ eta3
x3 ~ x5
x6 ~ x3
x9 ~ x10
