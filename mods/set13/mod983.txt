eta1 =~ y1 + y2
eta2 =~ y1 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta2 ~ x3 + x9
x1 ~ eta2 + x7
x3 ~ x2 + x8
eta3 ~ x3
x10 ~ x3
x6 ~ x7
x2 ~ eta2
eta4 ~ x7
x4 ~ x7
x5 ~ x3
eta1 ~ x7
