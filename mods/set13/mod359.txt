eta1 =~ y1 + y2
eta2 =~ y3 + y5 + y7
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
x3 ~ x4 + x8
x2 ~ x3
x4 ~ x2
eta4 ~ x4
x10 ~ x1 + x2
x7 ~ x10
eta2 ~ eta1 + x3
x5 ~ eta1 + x9
x6 ~ x3
eta3 ~ x9
