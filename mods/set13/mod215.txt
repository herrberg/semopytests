eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y2 + y7 + y8
eta4 =~ y10 + y11 + y9
eta1 ~ x2
x6 ~ eta1 + x1
x3 ~ eta3 + x5 + x6
x8 ~ x3
eta4 ~ x2 + x4
eta2 ~ eta4
x9 ~ x6
x7 ~ x9
x1 ~ x7
x10 ~ x2
