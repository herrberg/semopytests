eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y12 + y6
x5 ~ eta2 + eta3 + x7 + x8
x10 ~ x2 + x5
x7 ~ x10
x1 ~ eta2
x6 ~ eta1 + x5
x9 ~ x6
eta4 ~ eta2
x4 ~ x2
x3 ~ x10
