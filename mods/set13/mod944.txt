eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
x4 ~ x1
x2 ~ eta3 + x4 + x8 + x9
eta1 ~ x2
eta4 ~ eta1 + x3
x9 ~ eta4
eta3 ~ x6
eta2 ~ eta1
x7 ~ eta1
x10 ~ eta1
x5 ~ eta3
