eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x5 ~ eta2 + eta4 + x4
x1 ~ x5
x8 ~ x1
x10 ~ eta1 + x5
x9 ~ x10
eta3 ~ x1 + x3
x6 ~ eta3
x2 ~ x5 + x7
eta2 ~ x2
