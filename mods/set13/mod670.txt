eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y6
x3 ~ x6
x8 ~ eta3 + x3 + x4
x2 ~ eta1 + x8
eta4 ~ eta2
x4 ~ eta4
x9 ~ x1 + x3
x10 ~ x9
eta3 ~ x2
x7 ~ x4
x5 ~ x1
