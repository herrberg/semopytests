eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y5
x6 ~ x5 + x7
x2 ~ eta1 + x6
x8 ~ eta2 + x2
eta1 ~ x4 + x8
x3 ~ eta1
eta3 ~ x10 + x4
x1 ~ eta3
eta4 ~ x6
x9 ~ eta4
