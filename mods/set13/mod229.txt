eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y6 + y8
x8 ~ eta2 + x5 + x7
x9 ~ x1 + x8
x2 ~ x3 + x9
x3 ~ x10
eta2 ~ x9
eta4 ~ x6 + x9
eta3 ~ x3
x4 ~ x3
eta1 ~ x8
