eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y1 + y9
eta2 ~ eta3 + x4
eta4 ~ eta2 + x10 + x2
x3 ~ eta2
x4 ~ x3 + x5 + x9
x6 ~ x4
x9 ~ x8
x1 ~ x3
eta1 ~ x10
x7 ~ eta2
