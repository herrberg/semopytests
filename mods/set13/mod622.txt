eta1 =~ y1 + y3 + y5
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
x5 ~ x1 + x9
x6 ~ x5
x1 ~ eta2 + eta4 + x4 + x6 + x8
x3 ~ eta1 + x2 + x9
eta3 ~ x3
x10 ~ x3
x7 ~ x3
