eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y3
x9 ~ x1
x8 ~ eta2 + x9
x2 ~ x8
eta2 ~ eta3 + x10 + x6
x3 ~ x9
eta4 ~ eta2 + x5
eta1 ~ eta4
eta3 ~ eta1 + x7
x4 ~ x1
