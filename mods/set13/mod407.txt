eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y1 + y10 + y8
x10 ~ eta1 + eta2 + eta4 + x7
eta3 ~ x10
x6 ~ eta3
x4 ~ x6
x5 ~ x10 + x9
x2 ~ x5
x8 ~ eta2
eta4 ~ eta3
x3 ~ eta1
x1 ~ x5
