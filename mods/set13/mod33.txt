eta1 =~ y1 + y2
eta2 =~ y1 + y3 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta4 ~ x6
eta1 ~ eta4 + x5
eta2 ~ eta1
eta3 ~ eta4
x4 ~ eta3
x9 ~ x3 + x4
x7 ~ x9
x1 ~ eta3
x8 ~ eta1
x2 ~ x8
x5 ~ x2
x10 ~ x5
