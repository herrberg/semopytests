eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y5 + y7 + y8
eta4 =~ y10 + y11
eta3 ~ eta2 + x9
x2 ~ eta3 + x5
eta4 ~ x2 + x6
eta2 ~ eta4 + x8
x3 ~ eta3
x4 ~ eta3
x1 ~ x4
x7 ~ x2
eta1 ~ eta4
x10 ~ x2
