eta1 =~ y1 + y2 + y3
eta2 =~ y5 + y6
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta3 ~ x1
eta1 ~ eta3 + x5 + x7 + x9
x4 ~ eta1
x9 ~ x10 + x4 + x8
x10 ~ x6
eta4 ~ x8
eta2 ~ eta4
x3 ~ eta4
x2 ~ x5
