eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta4 ~ eta1 + x10
eta2 ~ eta4
x8 ~ eta2 + x1
x9 ~ eta4 + x5
x1 ~ x2 + x3 + x7
eta3 ~ x1
x7 ~ eta3
x4 ~ x7
x6 ~ x1
