eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y10 + y8
eta4 =~ y10 + y9
eta1 ~ x1 + x3
x4 ~ eta1 + eta2 + eta4 + x7 + x8
x3 ~ x4
eta3 ~ x7
x5 ~ x8
x6 ~ x5
eta2 ~ x2
x10 ~ eta2
x9 ~ x2
