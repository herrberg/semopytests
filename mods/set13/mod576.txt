eta1 =~ y1 + y2 + y3
eta2 =~ y2 + y4 + y5
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
x10 ~ eta4 + x2
x5 ~ x10
eta4 ~ x1 + x4 + x5
eta2 ~ eta1 + x5
x1 ~ eta3
x9 ~ x10
x6 ~ x10
x7 ~ x10 + x8
x3 ~ x8
