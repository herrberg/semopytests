eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
x7 ~ eta3 + x1 + x2
eta2 ~ eta4 + x7 + x9
x5 ~ eta2
x8 ~ x7
x6 ~ eta1 + x8
x3 ~ x10 + x2
eta4 ~ x4
x1 ~ x8
