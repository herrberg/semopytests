eta1 =~ y2 + y8
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
x4 ~ eta1 + eta3 + x6 + x7
eta4 ~ x4
x3 ~ eta2 + eta4
x5 ~ x7
x8 ~ x5
x10 ~ x4
x6 ~ x10
x2 ~ eta1
x1 ~ eta3
x9 ~ x7
