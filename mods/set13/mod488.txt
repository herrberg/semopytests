eta1 =~ y1 + y11 + y2
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y11 + y9
x1 ~ x9
x2 ~ eta1 + eta2 + x1 + x3
x10 ~ eta3 + x2
x3 ~ x10
x5 ~ x1
x7 ~ x1
x6 ~ x7
x8 ~ eta2
eta4 ~ x8
x4 ~ eta2
