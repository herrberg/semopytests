eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta4 ~ eta2 + x1 + x4 + x8
x2 ~ eta4 + x10
x8 ~ x2
eta3 ~ x8
eta1 ~ x4
x7 ~ eta1
x10 ~ x6
x9 ~ x1
eta2 ~ x3
x5 ~ x1
