eta1 =~ y1 + y2 + y3
eta2 =~ y11 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
x3 ~ eta1 + x8
x10 ~ x3 + x4
x8 ~ x10
eta4 ~ x8
x7 ~ eta4 + x2 + x5
x4 ~ x1
x9 ~ x10
eta3 ~ x2
eta2 ~ eta1
x6 ~ x1
