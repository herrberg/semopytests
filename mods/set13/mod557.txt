eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
x4 ~ x1 + x8
x5 ~ x10 + x3 + x4
eta3 ~ x6 + x8
x9 ~ eta2 + eta4 + x2 + x8
eta1 ~ eta2
x7 ~ eta2
