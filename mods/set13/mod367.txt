eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y2 + y7 + y8
eta4 =~ y10 + y11 + y9
x5 ~ x2 + x9
x8 ~ x5
x3 ~ x8
eta3 ~ x9
eta2 ~ eta3
x2 ~ x8
x1 ~ eta1 + x2
x7 ~ x4 + x8
x4 ~ x6
eta4 ~ x10 + x4
