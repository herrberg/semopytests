eta1 =~ y1 + y2
eta2 =~ y3 + y5 + y8
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta3 ~ x7
x3 ~ eta2 + eta3 + x1
x2 ~ eta3 + x5
x6 ~ eta1 + x10 + x4 + x7 + x9
x8 ~ x7
x1 ~ eta4
