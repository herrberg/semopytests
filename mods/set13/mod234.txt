eta1 =~ y1 + y2 + y3
eta2 =~ y2 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
x4 ~ eta3 + x10 + x9
x5 ~ x1 + x4
x8 ~ x5
x3 ~ x8
x7 ~ x2 + x4
eta3 ~ eta1 + eta2
eta4 ~ x4
x10 ~ eta4 + x6
