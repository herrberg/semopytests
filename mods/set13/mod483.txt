eta1 =~ y1 + y2
eta2 =~ y1 + y3
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
x9 ~ eta1 + eta2 + eta4 + x2 + x4
x7 ~ x9
x2 ~ x7
x5 ~ x4
eta3 ~ x5 + x8
eta2 ~ x3
x10 ~ x6 + x7
x1 ~ x10
