eta1 =~ y1 + y2 + y3
eta2 =~ y2 + y4 + y5
eta3 =~ y7 + y8
eta4 =~ y10 + y9
eta4 ~ x8 + x9
x10 ~ eta4 + x2
x8 ~ eta2 + x10
eta2 ~ x7
x2 ~ x1 + x5
eta3 ~ x2 + x3
x3 ~ x4
x6 ~ x3
eta1 ~ x7
