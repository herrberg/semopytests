eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x8 ~ x9
x1 ~ eta1 + x10 + x2 + x8
x7 ~ eta4 + x1 + x5
eta2 ~ x7
x3 ~ eta2 + x4
x6 ~ x7
eta1 ~ x7
eta3 ~ x7
