eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y1 + y10
x3 ~ eta1
x7 ~ x3 + x6
x1 ~ x3 + x8
eta2 ~ x6
x10 ~ x4 + x6
x2 ~ eta4 + x3 + x9
x5 ~ x6
eta3 ~ x8
