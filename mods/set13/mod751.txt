eta1 =~ y1 + y2
eta2 =~ y10 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
eta2 ~ x10 + x7 + x8
eta3 ~ x1 + x10
x2 ~ x10
x6 ~ x2 + x3
x5 ~ eta4 + x10 + x9
x9 ~ eta1
x4 ~ x10
