eta1 =~ y1 + y2 + y3
eta2 =~ y5 + y6 + y9
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11 + y12
eta2 ~ eta1 + x6
x8 ~ eta2
eta1 ~ x8
x2 ~ eta1
x7 ~ eta3 + x2
x9 ~ x8
x1 ~ x9
x5 ~ x1
x4 ~ eta4 + x1
x3 ~ x1
x10 ~ eta1
