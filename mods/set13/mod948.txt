eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta1 ~ x5 + x6
x7 ~ eta1 + x1
x2 ~ x7
eta4 ~ x2
x3 ~ eta4
x6 ~ x7
x9 ~ x6
x4 ~ x2
x10 ~ eta2 + x4
eta3 ~ x10
x8 ~ x5
