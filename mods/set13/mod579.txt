eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y5
eta4 ~ eta1 + eta3 + x3
x8 ~ eta4 + x9
x7 ~ x4 + x8
x10 ~ x3
x5 ~ eta3
x1 ~ x3
eta2 ~ x1
x2 ~ x3
eta1 ~ x6 + x7
