eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
x6 ~ x10 + x8
x1 ~ x4 + x6
eta1 ~ x1 + x7
x10 ~ eta1 + x5
x2 ~ x1
x3 ~ eta3 + x5
x9 ~ x3
x7 ~ eta2
eta4 ~ x4
