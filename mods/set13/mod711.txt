eta1 =~ y1 + y2 + y3
eta2 =~ y1 + y4 + y5
eta3 =~ y7 + y8
eta4 =~ y10 + y9
x5 ~ eta1 + eta3
eta3 ~ x2
x6 ~ eta3 + x1 + x8
eta4 ~ x10 + x6
eta2 ~ eta1 + x4 + x7
x1 ~ eta4 + x9
x3 ~ eta3
