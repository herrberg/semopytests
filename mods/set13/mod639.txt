eta1 =~ y1 + y2 + y3
eta2 =~ y1 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
x5 ~ x8
x3 ~ x2 + x5
eta3 ~ x5 + x7
eta4 ~ eta2 + x8
x6 ~ eta1 + eta4
eta2 ~ x6
x4 ~ x8
x9 ~ x4
x1 ~ eta4
x10 ~ x8
