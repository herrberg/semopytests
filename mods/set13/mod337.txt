eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
eta4 ~ eta3 + x2
x7 ~ eta4
x10 ~ x7
x9 ~ eta2 + x1 + x7
x3 ~ x9
x4 ~ x3
eta1 ~ eta4 + x8
x2 ~ eta1
x5 ~ eta3
x6 ~ eta2
