eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y1 + y11
x2 ~ x1 + x10 + x3
x5 ~ x2
eta2 ~ x5 + x6
eta3 ~ eta2 + x7
x6 ~ eta3
eta1 ~ x10
x8 ~ eta2 + x4
x1 ~ eta4
x9 ~ x4
