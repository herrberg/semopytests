eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y2
x9 ~ x3 + x8
x7 ~ eta2 + x9
x3 ~ x4 + x7
x10 ~ x1 + x3 + x5
x2 ~ x10
eta3 ~ eta1 + x3
eta4 ~ x7
x6 ~ x1
