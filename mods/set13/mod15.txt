eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y5 + y6 + y8
eta4 =~ y10 + y9
x7 ~ eta4 + x3
x8 ~ x7
x9 ~ x2 + x8
x6 ~ x4 + x9
eta2 ~ eta1 + x6
x1 ~ eta2
x10 ~ eta3 + x6
x5 ~ eta3
x3 ~ x8
