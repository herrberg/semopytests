eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y5 + y6 + y8
eta4 =~ y10 + y9
x5 ~ eta3 + eta4 + x3 + x9
x7 ~ x4 + x5
x10 ~ x1 + x3
eta1 ~ x1 + x8
eta3 ~ x7
eta4 ~ x2
eta2 ~ x3
x8 ~ x6
