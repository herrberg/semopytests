eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y6 + y8
eta4 =~ y10 + y11 + y9
x4 ~ eta2 + x10 + x2
eta3 ~ eta4 + x4
x7 ~ x3 + x4 + x6
x8 ~ x4
x2 ~ x8 + x9
eta1 ~ x2
x5 ~ x4
x1 ~ x5
