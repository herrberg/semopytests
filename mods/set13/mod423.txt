eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x8 ~ x4 + x7
x1 ~ eta1 + x8
x6 ~ x8
x5 ~ eta3 + x10 + x6
eta4 ~ x8
x7 ~ eta4 + x3
x2 ~ x7
x9 ~ x10
eta2 ~ eta3
