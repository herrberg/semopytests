eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y1 + y7 + y8
eta4 =~ y10 + y9
eta2 ~ x3 + x6
eta4 ~ eta2 + x4
x10 ~ eta4
eta1 ~ eta2
x1 ~ eta1
x6 ~ x2 + x7 + x8 + x9
x7 ~ eta2
x5 ~ x3
eta3 ~ eta2
