eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
x10 ~ eta2 + x1 + x5 + x7
x8 ~ x10
x9 ~ eta4 + x8
x6 ~ x10
eta2 ~ x6
x3 ~ x10
eta1 ~ eta4
x4 ~ x10
x2 ~ x1
eta3 ~ x7
