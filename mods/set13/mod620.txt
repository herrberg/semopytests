eta1 =~ y2 + y5
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
x3 ~ x7
x9 ~ x3
x4 ~ x10 + x9
eta4 ~ x4
x2 ~ eta4
x5 ~ eta1 + x1
x10 ~ x5
x1 ~ x10
eta2 ~ x10
x6 ~ x10
x8 ~ x3
eta3 ~ x7
