eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y4
x9 ~ eta2
eta3 ~ x5 + x8 + x9
x5 ~ x3 + x6
x2 ~ x6 + x7
eta1 ~ x7
x10 ~ x9
x4 ~ x1 + x6
eta4 ~ x4
