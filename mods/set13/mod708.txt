eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y5 + y6 + y8
eta4 =~ y10 + y9
x5 ~ eta1 + eta3 + x10 + x8 + x9
x7 ~ x5
eta2 ~ eta4 + x3
x9 ~ eta2
x4 ~ x8
x2 ~ x4
x6 ~ eta2
x3 ~ x6
x1 ~ x3
