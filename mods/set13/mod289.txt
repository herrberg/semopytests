eta1 =~ y1 + y3 + y5
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
x5 ~ eta2 + x1 + x4 + x9
x8 ~ x5
x3 ~ x9
x1 ~ eta1 + eta4 + x10
eta3 ~ x5
eta2 ~ x6
eta1 ~ x5
x2 ~ eta1
x7 ~ x6
