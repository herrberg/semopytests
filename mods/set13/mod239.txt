eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
x8 ~ x1 + x6
x9 ~ x4 + x8
x1 ~ eta2 + x9
eta1 ~ x1
eta4 ~ x8
eta2 ~ x2
eta3 ~ x6
x3 ~ eta3 + x5
x7 ~ eta2
x10 ~ x1
