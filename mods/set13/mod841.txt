eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta1 ~ eta3 + x6
x8 ~ eta1
x5 ~ x3 + x8
x4 ~ x10 + x5 + x7
eta2 ~ x4
x10 ~ eta2
eta4 ~ eta1
x2 ~ eta4
x1 ~ x6
x9 ~ x3
