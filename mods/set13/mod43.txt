eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y5 + y7
eta4 =~ y10 + y9
x5 ~ eta1 + x6
eta1 ~ eta2 + x8
x2 ~ x1 + x6
x10 ~ eta2
eta3 ~ eta1
x7 ~ eta1 + eta4
x8 ~ x7
x4 ~ x6
x9 ~ eta1
eta4 ~ x3
