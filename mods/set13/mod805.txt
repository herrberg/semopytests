eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta1 ~ x2 + x7
eta2 ~ eta1
x5 ~ eta2 + x1 + x6
x4 ~ x5
eta4 ~ eta1
x2 ~ eta4 + x3
eta3 ~ x6
x10 ~ eta4
x9 ~ x10
x8 ~ x1
