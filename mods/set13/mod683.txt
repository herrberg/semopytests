eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y4 + y6 + y8
eta4 =~ y10 + y11 + y9
x9 ~ eta4
x1 ~ x4 + x9
eta1 ~ eta2 + eta3 + eta4 + x10
x4 ~ x8
x10 ~ x6 + x7
x2 ~ eta4 + x3 + x5
