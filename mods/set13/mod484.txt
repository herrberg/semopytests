eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
x2 ~ x3 + x6 + x8
x1 ~ x2
x10 ~ x1
x8 ~ x1 + x7
x5 ~ eta1 + x8 + x9
x4 ~ x3
eta3 ~ x4
eta4 ~ eta1
eta2 ~ x3
