eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x1 ~ x3 + x4 + x5
eta1 ~ x1
x3 ~ eta1
x7 ~ x3
eta2 ~ eta1 + x10
x6 ~ x5
x9 ~ x1
eta3 ~ x9
x8 ~ eta4 + x2 + x5
