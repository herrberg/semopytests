eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
x1 ~ eta1 + x6
x8 ~ eta3 + x1 + x2
x3 ~ x8
x4 ~ x1
x6 ~ x4
eta2 ~ x1 + x5
x5 ~ eta4
x2 ~ x10
x7 ~ eta1
x9 ~ x10
