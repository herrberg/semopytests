eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta3 ~ eta1 + x3 + x5 + x9
x10 ~ eta3 + x8
x4 ~ x1 + x10
eta1 ~ x4
eta4 ~ x2 + x8
x7 ~ eta4
eta2 ~ x4
x6 ~ x9
