eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7
eta4 =~ y10 + y2 + y8
x9 ~ eta4 + x1
x4 ~ eta2 + x5 + x9
eta1 ~ x2 + x4
x7 ~ eta1
x8 ~ x9
eta2 ~ eta3
eta4 ~ x10 + x3 + x4
x6 ~ x9
