eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y5 + y9
x3 ~ x10 + x2
x1 ~ eta1 + x3 + x8
x5 ~ eta2 + x1
x4 ~ x10
x7 ~ x4
x6 ~ x10
eta3 ~ x10
x2 ~ x1
x9 ~ x3
eta4 ~ x10
