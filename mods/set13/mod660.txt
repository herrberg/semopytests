eta1 =~ y1 + y2
eta2 =~ y11 + y3 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
x8 ~ x3
x5 ~ eta4 + x8
eta2 ~ x5
eta3 ~ eta2
x4 ~ x10 + x5
eta4 ~ x1 + x4
x2 ~ x8 + x9
eta1 ~ x3
x6 ~ x3
x7 ~ x3
