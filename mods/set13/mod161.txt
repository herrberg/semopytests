eta1 =~ y1 + y2
eta2 =~ y1 + y3 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
x6 ~ eta1 + x5
eta3 ~ x10 + x4 + x6
x3 ~ eta3 + x2
x10 ~ x3
eta1 ~ x1
eta2 ~ x5 + x9
eta4 ~ x2
x7 ~ x9
x8 ~ x5
