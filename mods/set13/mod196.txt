eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
eta4 ~ x7
x10 ~ eta1 + eta4 + x5
x9 ~ x10 + x2 + x8
x5 ~ x9
eta2 ~ eta4 + x6
eta3 ~ eta1 + x1
x3 ~ eta3
x4 ~ x8
