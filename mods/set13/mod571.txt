eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y2 + y7
eta4 =~ y10 + y8 + y9
x6 ~ x1 + x8
x10 ~ x6
x5 ~ x10
eta3 ~ eta2 + x5
x9 ~ eta1 + x6
x7 ~ x6
x3 ~ x7
x1 ~ eta4 + x10
x4 ~ x1
x2 ~ x5
