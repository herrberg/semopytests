eta1 =~ y1 + y2
eta2 =~ y3 + y5
eta3 =~ y5 + y6 + y7
eta4 =~ y10 + y8 + y9
x10 ~ eta1 + x9
x3 ~ x10 + x7
x2 ~ x10
eta1 ~ x2 + x4
x1 ~ eta1
x7 ~ eta3 + eta4 + x5
eta2 ~ x10
x8 ~ x9
x6 ~ x8
