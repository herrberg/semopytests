eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
eta2 ~ eta3 + x2 + x6 + x8 + x9
x5 ~ eta2
x8 ~ x5
x10 ~ eta4 + x9
eta1 ~ eta3
x3 ~ eta1
eta4 ~ x4
x7 ~ eta2
x1 ~ x9
