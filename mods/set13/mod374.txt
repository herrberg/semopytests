eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y5
x3 ~ x9
eta4 ~ x2 + x3
x8 ~ eta4 + x10
x1 ~ x8
x2 ~ eta1 + eta2 + eta3
x4 ~ x2
x7 ~ x4
eta2 ~ x4
x5 ~ x9
x6 ~ x10
