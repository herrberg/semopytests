eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta3 =~ y8 + y9
eta4 =~ y10 + y11 + y9
x2 ~ eta2 + x3
x7 ~ x1 + x2 + x4
eta1 ~ x2 + x6
eta2 ~ eta1
eta3 ~ x8
x1 ~ eta3 + x10
x9 ~ x4
eta4 ~ eta1
x5 ~ x8
