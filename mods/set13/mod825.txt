eta1 =~ y1 + y10 + y2
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
eta2 ~ eta1 + x1 + x3 + x4 + x9
eta3 ~ eta2
eta4 ~ x3
x2 ~ x10 + x3 + x5
x7 ~ x2
x10 ~ x6 + x7
x8 ~ x9
