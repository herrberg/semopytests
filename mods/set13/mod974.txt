eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y10 + y5 + y6
eta4 =~ y10 + y8 + y9
x6 ~ eta2
x3 ~ x6
x1 ~ eta2
eta1 ~ eta2 + x7 + x8
eta4 ~ eta3 + x2
x8 ~ eta4 + x5
x2 ~ x8 + x9
x9 ~ x10
x5 ~ x4
