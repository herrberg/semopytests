eta1 =~ y1 + y2
eta2 =~ y1 + y3 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
eta3 ~ x6
x4 ~ eta3 + x3
eta4 ~ x4 + x7
x9 ~ eta2 + eta4
x3 ~ x9
x1 ~ x9
x2 ~ eta2
x8 ~ eta2
eta1 ~ x9
x10 ~ eta1
x5 ~ eta3
