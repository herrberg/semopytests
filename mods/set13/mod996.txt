eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y1 + y5 + y7
eta4 =~ y10 + y8 + y9
x8 ~ x1 + x7 + x9
eta1 ~ eta3 + x4 + x6 + x7
x3 ~ eta1
x6 ~ x3
eta2 ~ x7
eta4 ~ x1
x10 ~ x1 + x5
x2 ~ x5
