eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y7 + y8
eta4 =~ y10 + y8 + y9
x1 ~ x5 + x6 + x9
x3 ~ x1
x2 ~ eta3 + x8
eta1 ~ x2
x5 ~ eta1 + x10
x8 ~ x4 + x5
eta2 ~ x4
x7 ~ eta2 + eta4
