eta1 =~ y1 + y3 + y8
eta2 =~ y4 + y5 + y6
eta3 =~ y7 + y8 + y9
eta4 =~ y10 + y11
x4 ~ eta2 + x1 + x3 + x9
x2 ~ eta4 + x4 + x5
x8 ~ x2
x6 ~ x9
eta1 ~ x6
x10 ~ eta3 + x2
x3 ~ x10
x7 ~ x10
