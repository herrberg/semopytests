eta1 =~ y2 + y5
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y9
x2 ~ eta4 + x8
x6 ~ x2
x1 ~ x6 + x7
eta1 ~ eta2 + x6 + x9
x5 ~ eta1 + x4
x10 ~ x3
x7 ~ x10
eta3 ~ x7
eta4 ~ x6
