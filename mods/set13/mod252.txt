eta1 =~ y1 + y2 + y6
eta2 =~ y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y10 + y11 + y9
x3 ~ eta4 + x4 + x7
x1 ~ x3
eta4 ~ eta1 + x1 + x2
eta3 ~ x3 + x6
x8 ~ eta3
x5 ~ x7
x10 ~ x7
x9 ~ x7
eta2 ~ x7
