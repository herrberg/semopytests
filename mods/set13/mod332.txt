eta1 =~ y1 + y2 + y3
eta2 =~ y2 + y5 + y6
eta3 =~ y7 + y8
eta4 =~ y10 + y9
x2 ~ x3 + x6 + x7 + x9
x10 ~ x2
x3 ~ x10
x5 ~ eta4 + x10
eta4 ~ x1
eta2 ~ x6
x8 ~ x7
x4 ~ x8
eta1 ~ x10
eta3 ~ eta1
