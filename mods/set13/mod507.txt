eta1 =~ y1 + y2
eta2 =~ y4 + y5 + y9
eta3 =~ y6 + y7
eta4 =~ y10 + y8 + y9
eta4 ~ x2 + x8
x5 ~ eta4
x2 ~ x1 + x5
eta2 ~ eta4 + x4 + x6
x6 ~ x3
x10 ~ x4 + x7
x9 ~ eta1 + eta4
eta3 ~ x7
