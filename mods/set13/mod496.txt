eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8 + y9
x1 ~ eta1 + eta4
x4 ~ x1
x2 ~ eta3 + x10 + x4
eta4 ~ eta2 + x2
eta2 ~ x7
x3 ~ eta2
x8 ~ x2
x9 ~ eta2
x6 ~ x2
x5 ~ x2
