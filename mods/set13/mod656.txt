eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta3 =~ y6 + y7
eta4 =~ y8 + y9
x5 ~ x2 + x7
x9 ~ eta2 + x5
eta4 ~ x1 + x6 + x9
eta3 ~ eta1 + eta4
x3 ~ x9
x7 ~ x9
x4 ~ x1
x10 ~ eta1
x8 ~ x1
