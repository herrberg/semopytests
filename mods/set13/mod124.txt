eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6
eta4 =~ y7 + y8
eta1 ~ x4
x10 ~ eta1 + x3
x1 ~ eta2 + x6
x3 ~ x1 + x7
x6 ~ x3 + x5
x8 ~ eta3 + x6
eta3 ~ eta4
x2 ~ x3
x9 ~ x4
