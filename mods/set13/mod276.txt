eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta3 =~ y5 + y6 + y7
eta4 =~ y8 + y9
x9 ~ eta2 + eta3 + x10 + x3
x4 ~ x9
x1 ~ x4
x8 ~ x1
x10 ~ x8
x6 ~ x7 + x9
eta1 ~ x1 + x5
x2 ~ eta1
eta4 ~ x8
