eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
eta3 =~ y6 + y7 + y8
eta4 =~ y1 + y10
x7 ~ eta1 + eta2
x8 ~ x1 + x6 + x7
eta3 ~ x7
eta1 ~ eta3
x6 ~ x9
x5 ~ eta4 + x3 + x9
x3 ~ x4
x2 ~ x10 + x3
