eta1 =~ y1 + y2
x1 ~ x2 + x5
eta1 ~ x1 + x3
x3 ~ x4 + x6 + x9
x6 ~ x7
x10 ~ x3
x8 ~ x6
