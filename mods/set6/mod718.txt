eta1 =~ y1 + y2 + y3
x5 ~ x10
x2 ~ x5 + x7 + x8
x6 ~ eta1 + x2
x7 ~ x9
eta1 ~ x4
x3 ~ x5
x1 ~ x7
