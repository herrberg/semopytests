eta1 =~ y1 + y2
x4 ~ x5 + x9
x2 ~ x3 + x4 + x8
eta1 ~ x4
x6 ~ eta1
x5 ~ x1 + x7
x10 ~ x1
