eta1 =~ y1 + y2 + y3
x4 ~ x5 + x9
eta1 ~ x10 + x4
x2 ~ x1 + x6 + x9
x3 ~ x4
x10 ~ x7
x8 ~ x1
