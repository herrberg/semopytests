eta1 =~ y1 + y2
x4 ~ x10
x3 ~ eta1 + x4
x8 ~ x3 + x9
eta1 ~ x1
x5 ~ x4
x7 ~ x5
x2 ~ x4
x6 ~ x2
