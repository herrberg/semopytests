eta1 =~ y1 + y2
x6 ~ x10
x2 ~ x3 + x6
x5 ~ x2
x4 ~ x5
x9 ~ eta1
x1 ~ x9
x3 ~ x1
x8 ~ x2
x7 ~ x3
