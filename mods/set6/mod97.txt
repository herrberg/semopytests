eta1 =~ y1 + y2
x3 ~ x10 + x5 + x8
x6 ~ x5 + x9
x10 ~ x4
x8 ~ x1
eta1 ~ x7 + x9
x2 ~ x5
