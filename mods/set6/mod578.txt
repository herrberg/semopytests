eta1 =~ y1 + y2 + y3
x2 ~ x10 + x6 + x8
x9 ~ x2 + x3
x8 ~ x1
x7 ~ x2
x5 ~ x2
eta1 ~ x2
x4 ~ x2
