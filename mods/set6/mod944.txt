eta1 =~ y1 + y2 + y3
eta1 ~ x10 + x6
x8 ~ eta1 + x1
x7 ~ x4
x10 ~ x7
x2 ~ x4
x9 ~ x7
x3 ~ x9
x1 ~ x5
