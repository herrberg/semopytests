eta1 =~ y1 + y2 + y3
x7 ~ x3
x5 ~ x10 + x7
x1 ~ x2 + x5
x9 ~ x1
x8 ~ x3
x6 ~ x1
eta1 ~ x5
x4 ~ x5
