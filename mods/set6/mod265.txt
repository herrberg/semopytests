eta1 =~ y1 + y2 + y3
x9 ~ x6 + x8
eta1 ~ x7 + x9
x10 ~ eta1 + x3
x4 ~ x1 + x9
x3 ~ x2
x5 ~ x3
