eta1 =~ y1 + y2
x10 ~ x2 + x8
x1 ~ x10 + x5
x3 ~ x10
x6 ~ x2 + x9
x7 ~ x6
x4 ~ x9
eta1 ~ x5
