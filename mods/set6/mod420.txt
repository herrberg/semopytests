eta1 =~ y1 + y2
x3 ~ eta1 + x7
x9 ~ x3
x8 ~ x9
x10 ~ x2 + x3
x5 ~ x6 + x7
x1 ~ x9
x4 ~ x6
