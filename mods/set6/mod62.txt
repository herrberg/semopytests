eta1 =~ y1 + y2 + y3
x5 ~ x3
x1 ~ x5 + x6 + x8
x4 ~ x1
x2 ~ x5
eta1 ~ x2
x9 ~ x8
x10 ~ x3
x7 ~ x5
