eta1 =~ y1 + y2 + y3
x6 ~ x3
x2 ~ eta1 + x6
x1 ~ eta1 + x5 + x8
x4 ~ x1
x7 ~ x4
x9 ~ x1 + x10
