eta1 =~ y1 + y2
x7 ~ x8
x6 ~ x2 + x3 + x7
x10 ~ x6
x1 ~ x10
x5 ~ eta1 + x2 + x9
x4 ~ x3
