eta1 =~ y1 + y2 + y3
x1 ~ x2
eta1 ~ x1 + x6
x4 ~ x2
x3 ~ x4 + x7
x10 ~ x4 + x9
x7 ~ x5
x8 ~ x6
