eta1 =~ y1 + y2 + y3
x6 ~ eta1
x1 ~ x6 + x7
x9 ~ x1
x2 ~ x1
x5 ~ x1 + x10 + x8
x3 ~ x1
x4 ~ eta1
