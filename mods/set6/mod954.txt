eta1 =~ y1 + y2 + y3
x6 ~ x1 + x3
x8 ~ x3
eta1 ~ x7
x1 ~ eta1
x4 ~ x3
x2 ~ x1 + x9
x5 ~ x7
x10 ~ x5
