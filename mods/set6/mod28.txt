eta1 =~ y1 + y2
x7 ~ x10
x9 ~ x7
x1 ~ x9
x4 ~ x5 + x7
x8 ~ x4
eta1 ~ x8
x3 ~ x2 + x7
x6 ~ x2
