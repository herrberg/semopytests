eta1 =~ y1 + y2
x6 ~ x4
x10 ~ x6
eta1 ~ x10 + x3
x2 ~ eta1
x8 ~ x10
x7 ~ x4
x1 ~ x7 + x9
x5 ~ x7
