eta1 =~ y1 + y2
x6 ~ x2
x1 ~ x6
x9 ~ x1
x4 ~ x10 + x9
x7 ~ x4
eta1 ~ x7 + x8
x5 ~ x1
x3 ~ x5
