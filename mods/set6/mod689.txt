eta1 =~ y1 + y2
x8 ~ x4
x9 ~ x8
x1 ~ x3 + x6 + x8
x3 ~ eta1 + x7
x10 ~ x3
x5 ~ x3
x2 ~ eta1
