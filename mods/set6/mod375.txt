eta1 =~ y1 + y2
x5 ~ x10 + x3 + x9
x6 ~ x5
x10 ~ x1
x7 ~ x10
eta1 ~ x9
x4 ~ x3
x8 ~ x9
x2 ~ x3
