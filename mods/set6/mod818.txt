eta1 =~ y1 + y2
x10 ~ x1 + x2 + x7 + x8
x3 ~ x10
x8 ~ x4
eta1 ~ x1
x9 ~ x4
x5 ~ x8
x6 ~ x5
