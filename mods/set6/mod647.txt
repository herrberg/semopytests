eta1 =~ y1 + y2
eta1 ~ x1
x9 ~ eta1 + x10
x3 ~ x9
x5 ~ eta1
x4 ~ x10
x7 ~ eta1 + x6
x2 ~ x9
x8 ~ x6
