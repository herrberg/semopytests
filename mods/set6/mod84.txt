eta1 =~ y1 + y2 + y3
x8 ~ x1 + x6
x5 ~ x8
x10 ~ x5
x4 ~ x10
x2 ~ x5
x3 ~ x2
x9 ~ x3
x7 ~ x8
eta1 ~ x2
