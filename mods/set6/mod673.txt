eta1 =~ y1 + y2
x3 ~ x4
x1 ~ x3
x9 ~ x1 + x2
x6 ~ x7 + x9
x7 ~ eta1
x2 ~ x5
x8 ~ x4
x10 ~ x1
