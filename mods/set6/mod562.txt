eta1 =~ y1 + y2
x6 ~ x2 + x7
x4 ~ x6
eta1 ~ x10 + x4
x1 ~ eta1
x8 ~ x1 + x5
x9 ~ x4
x3 ~ x7
