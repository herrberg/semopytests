eta1 =~ y1 + y2 + y3
x10 ~ x6
eta1 ~ x10
x8 ~ eta1
x1 ~ eta1
x4 ~ x1
x9 ~ x2 + x4
x7 ~ eta1
x3 ~ x1
x2 ~ x5
