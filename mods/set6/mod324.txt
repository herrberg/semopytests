eta1 =~ y1 + y2
x7 ~ x6
x2 ~ x7 + x8
x10 ~ x2
x9 ~ x1 + x10
x3 ~ x9
x1 ~ x4
eta1 ~ x1
x5 ~ eta1
