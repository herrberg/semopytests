eta1 =~ y1 + y2 + y3
x7 ~ x2 + x3
x5 ~ x7
x8 ~ x2 + x9
eta1 ~ x8
x9 ~ x10
x4 ~ x1 + x8
x6 ~ x9
