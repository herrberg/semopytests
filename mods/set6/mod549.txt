eta1 =~ y1 + y2 + y3
x1 ~ x10
x6 ~ x1 + x9
x3 ~ x2 + x6
x4 ~ x3
x2 ~ x7
x5 ~ x10
eta1 ~ x1
x8 ~ x2
