eta1 =~ y1 + y2
x3 ~ x10 + x5 + x7
x1 ~ x3
x4 ~ x1 + x6 + x8
x8 ~ eta1
x6 ~ x2 + x9
