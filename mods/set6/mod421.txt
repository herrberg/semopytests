eta1 =~ y1 + y2
x1 ~ x2
x8 ~ x1 + x3 + x4
eta1 ~ x6 + x8 + x9
x5 ~ x4
x6 ~ x10
x7 ~ x4
