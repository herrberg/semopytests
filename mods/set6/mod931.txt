eta1 =~ y1 + y2 + y3
x8 ~ eta1 + x2
x7 ~ x8
eta1 ~ x3
x6 ~ x5 + x8
x4 ~ x6
x10 ~ x5
x1 ~ x10
x9 ~ x1
