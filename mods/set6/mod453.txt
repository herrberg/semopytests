eta1 =~ y1 + y2 + y3
x4 ~ x7 + x8
x3 ~ x4
x10 ~ x3
x1 ~ x4
eta1 ~ x2 + x3 + x5 + x9
x6 ~ x8
