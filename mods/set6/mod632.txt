eta1 =~ y1 + y2 + y3
x3 ~ x10 + x4
x5 ~ x3 + x8
eta1 ~ x5
x8 ~ x7 + x9
x1 ~ x3
x6 ~ x8
x2 ~ x7
