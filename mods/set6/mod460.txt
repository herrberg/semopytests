eta1 =~ y1 + y2 + y3
x7 ~ x4 + x6 + x8
x2 ~ x1 + x7 + x9
x3 ~ x2
eta1 ~ x7
x5 ~ x10 + x4
