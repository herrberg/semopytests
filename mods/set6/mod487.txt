eta1 =~ y1 + y2 + y3
eta1 ~ x2 + x4 + x6
x7 ~ x2 + x9
x5 ~ x7
x3 ~ x5
x10 ~ x4 + x8
x1 ~ x8
