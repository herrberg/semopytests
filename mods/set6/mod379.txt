eta1 =~ y1 + y2 + y3
x5 ~ eta1 + x3 + x7
x6 ~ x5
x9 ~ x4 + x6
x1 ~ x3
x10 ~ x1
x2 ~ x1
eta1 ~ x8
