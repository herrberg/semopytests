eta1 =~ y1 + y2
x4 ~ x2 + x9
x6 ~ eta1 + x10 + x4 + x5
x1 ~ x6
x7 ~ x1
x5 ~ x3
x8 ~ x2
