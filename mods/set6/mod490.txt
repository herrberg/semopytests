eta1 =~ y1 + y2
x7 ~ eta1 + x4
x10 ~ x2 + x7
x5 ~ x10
x6 ~ eta1
x8 ~ x6
x9 ~ x2
x1 ~ x4
x3 ~ x1
