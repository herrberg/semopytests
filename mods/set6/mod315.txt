eta1 =~ y1 + y2 + y3
x4 ~ x5
x10 ~ x4 + x8
eta1 ~ x4
x9 ~ x7 + x8
x1 ~ x9
x2 ~ x6 + x9
x3 ~ x2
