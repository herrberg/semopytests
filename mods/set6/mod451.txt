eta1 =~ y1 + y2
x8 ~ x9
x4 ~ x3 + x8
x2 ~ eta1 + x4
x1 ~ x2
x6 ~ x1 + x5
x7 ~ x9
x10 ~ x1
