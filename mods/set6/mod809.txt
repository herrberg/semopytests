eta1 =~ y1 + y2 + y3
x1 ~ x3 + x8
x4 ~ eta1 + x1 + x2
x9 ~ x4
x2 ~ x7
x5 ~ x1
x6 ~ x8
x10 ~ x6
