eta1 =~ y1 + y2 + y3
x6 ~ x10
x5 ~ eta1 + x6
x4 ~ x5
x3 ~ x6
x7 ~ x3
x8 ~ x5
x1 ~ x3 + x9
x9 ~ x2
