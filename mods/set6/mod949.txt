eta1 =~ y1 + y2
x1 ~ x2
eta1 ~ x1 + x3 + x6 + x9
x4 ~ x1
x8 ~ x2
x10 ~ x1 + x7
x5 ~ x1
