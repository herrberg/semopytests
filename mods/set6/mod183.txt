eta1 =~ y1 + y2 + y3
x2 ~ eta1 + x6
x7 ~ x10 + x2
x9 ~ x3 + x5 + x7
x4 ~ x9
x8 ~ x7
x1 ~ eta1
