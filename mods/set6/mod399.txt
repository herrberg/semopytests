eta1 =~ y1 + y2
eta1 ~ x9
x7 ~ eta1 + x10 + x6
x5 ~ x7
x3 ~ x5
x6 ~ x1
x4 ~ x5 + x8
x2 ~ x4
