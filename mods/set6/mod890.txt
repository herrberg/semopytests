eta1 =~ y1 + y2 + y3
x7 ~ x10
x1 ~ x7
x6 ~ eta1 + x1
x5 ~ x10 + x2
x8 ~ x9
x2 ~ x8
x3 ~ x7
x4 ~ x2
