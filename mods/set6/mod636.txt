eta1 =~ y1 + y2
x8 ~ x10 + x2 + x7
x4 ~ x5 + x8
x5 ~ x9
x6 ~ x5
eta1 ~ x1 + x2
x3 ~ x7
