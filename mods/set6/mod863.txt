eta1 =~ y1 + y2
x4 ~ eta1 + x1
x8 ~ eta1 + x6
x10 ~ x8
x1 ~ x3 + x5
x2 ~ eta1
x9 ~ x8
x7 ~ x5
