eta1 =~ y1 + y2
eta1 ~ x9
x1 ~ eta1
x5 ~ eta1
x3 ~ x5
x2 ~ x10 + x6 + x9
x6 ~ x4
x7 ~ x5
x8 ~ x5
