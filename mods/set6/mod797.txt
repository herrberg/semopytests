eta1 =~ y1 + y2
x3 ~ x4
x1 ~ x3
x6 ~ x1
x5 ~ x3
x7 ~ x1 + x10 + x2
x8 ~ x7
x9 ~ x1
eta1 ~ x10
