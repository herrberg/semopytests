eta1 =~ y1 + y2
x4 ~ x1 + x9
eta1 ~ x4
x6 ~ x1 + x7
x10 ~ x9
x8 ~ x10 + x5
x7 ~ x3
x2 ~ x9
