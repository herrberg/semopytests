eta1 =~ y1 + y2
eta1 ~ x3
x7 ~ eta1
x5 ~ x7
x9 ~ x5 + x6
x8 ~ x7
x10 ~ x3
x2 ~ eta1
x4 ~ x2
x6 ~ x1
