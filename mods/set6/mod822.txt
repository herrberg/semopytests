eta1 =~ y1 + y2
x7 ~ eta1 + x5 + x6
x2 ~ x7
x8 ~ x2
x9 ~ x10 + x5
x1 ~ x2
x4 ~ x1
x3 ~ x1
