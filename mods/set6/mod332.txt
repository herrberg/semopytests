eta1 =~ y1 + y2 + y3
x2 ~ eta1 + x9
x5 ~ x2
x6 ~ x4 + x5
x8 ~ x6
x1 ~ x3 + x5
x7 ~ x2
x10 ~ x2
