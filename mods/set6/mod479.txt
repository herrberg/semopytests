eta1 =~ y1 + y2 + y3
x1 ~ x8
x5 ~ x1
x6 ~ x5
x2 ~ x6
x9 ~ eta1 + x1 + x7
x4 ~ x9
eta1 ~ x3
x10 ~ x1
