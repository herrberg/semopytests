eta1 =~ y1 + y2 + y3
x7 ~ x6 + x9
x4 ~ x6
x3 ~ eta1 + x2 + x6
x10 ~ x9
x8 ~ x10
x1 ~ x6
eta1 ~ x5
