eta1 =~ y1 + y2
x5 ~ x3 + x4 + x7
x8 ~ x5
x10 ~ x1
x2 ~ eta1 + x10 + x9
x4 ~ x2
x9 ~ x6
