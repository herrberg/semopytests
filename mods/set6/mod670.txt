eta1 =~ y1 + y2 + y3
x6 ~ x4
x9 ~ eta1 + x5 + x6
x7 ~ x9
x2 ~ x1 + x3 + x9
x1 ~ x8
x10 ~ x6
