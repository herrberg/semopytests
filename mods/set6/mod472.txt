eta1 =~ y1 + y2
x9 ~ x3
x6 ~ x8 + x9
x7 ~ x1 + x10 + x6
eta1 ~ x4 + x7
x2 ~ x1
x5 ~ x2
