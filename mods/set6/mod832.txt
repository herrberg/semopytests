eta1 =~ y1 + y2 + y3
x9 ~ x1 + x8
x6 ~ eta1 + x9
x2 ~ x6 + x7
x10 ~ x1
x5 ~ x10
x4 ~ x6
x7 ~ x3
