eta1 =~ y1 + y2
x6 ~ x7
x4 ~ x6 + x9
x3 ~ x4
x10 ~ x3 + x8
x5 ~ x4
x1 ~ x4
x2 ~ x6
eta1 ~ x9
