eta1 =~ y1 + y2 + y3
x6 ~ x8
x4 ~ x6 + x9
x10 ~ x4 + x5 + x7
eta1 ~ x10
x1 ~ x3 + x6
x2 ~ x8
