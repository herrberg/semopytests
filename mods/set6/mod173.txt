eta1 =~ y1 + y2 + y3
x10 ~ x5
x3 ~ x10
x9 ~ x2 + x3 + x6
eta1 ~ x9
x8 ~ eta1
x7 ~ x8
x1 ~ x9
x4 ~ x2
