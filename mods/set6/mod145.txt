eta1 =~ y1 + y2
eta1 ~ x2 + x6 + x8
x9 ~ eta1
x5 ~ x10 + x6
x1 ~ x5
x4 ~ x5
x3 ~ x6
x7 ~ x10
