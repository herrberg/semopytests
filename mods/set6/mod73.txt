eta1 =~ y1 + y2
x2 ~ x5
x10 ~ x2 + x8
x7 ~ x1 + x2 + x4 + x6
x6 ~ x3
eta1 ~ x3
x9 ~ x2
