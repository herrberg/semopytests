eta1 =~ y1 + y2
x10 ~ x7
x2 ~ eta1 + x10
x9 ~ eta1 + x4
x3 ~ x10
x8 ~ x3
x4 ~ x6
x1 ~ x10
x5 ~ x10
