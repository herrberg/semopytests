eta1 =~ y1 + y2 + y3
x7 ~ x1 + x10
x6 ~ x4 + x7
x4 ~ x8
x9 ~ x7
x5 ~ x2 + x9
eta1 ~ x4
x3 ~ x1
