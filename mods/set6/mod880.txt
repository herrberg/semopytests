eta1 =~ y1 + y2
x1 ~ x3 + x5 + x9
x8 ~ x1 + x6
x4 ~ x2 + x5
x7 ~ x5
x10 ~ x7
x2 ~ eta1
