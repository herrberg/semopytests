eta1 =~ y1 + y2 + y3
x7 ~ x1 + x2
x5 ~ x7
x4 ~ x3 + x5 + x9
x8 ~ eta1 + x7
x3 ~ x6
x10 ~ x5
