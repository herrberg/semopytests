eta1 =~ y1 + y2
x4 ~ x10
x2 ~ eta1 + x4 + x9
x5 ~ x2
x3 ~ x1 + x2
x7 ~ x10
x8 ~ x7
x1 ~ x6
