eta1 =~ y1 + y2
x2 ~ x1 + x10 + x7 + x8
eta1 ~ x2 + x3
x4 ~ eta1
x5 ~ x2
x3 ~ x6
x9 ~ x3
