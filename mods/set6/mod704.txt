eta1 =~ y1 + y2
x6 ~ x2
x8 ~ x5 + x6
x10 ~ x8
eta1 ~ x10
x9 ~ x7
x5 ~ x9
x1 ~ x5
x4 ~ x6
x3 ~ x2
