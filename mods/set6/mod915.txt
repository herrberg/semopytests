eta1 =~ y1 + y2 + y3
x7 ~ x5
x1 ~ x7
eta1 ~ x1
x8 ~ x5 + x6
x9 ~ x8
x4 ~ x7
x10 ~ x7
x2 ~ x10
x3 ~ x2
