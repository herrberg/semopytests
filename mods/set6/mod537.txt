eta1 =~ y1 + y2
x4 ~ x3 + x6
x10 ~ x4
x8 ~ x2 + x4
x7 ~ x4
x9 ~ x5 + x7
x2 ~ x1
eta1 ~ x7
