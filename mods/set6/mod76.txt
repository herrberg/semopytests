eta1 =~ y1 + y2 + y3
eta1 ~ x7
x5 ~ eta1 + x10
x4 ~ x7
x8 ~ x10 + x2
x3 ~ x10
x1 ~ x3
x9 ~ x2
x6 ~ eta1
