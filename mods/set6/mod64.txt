eta1 =~ y1 + y2
x6 ~ eta1 + x2 + x4
eta1 ~ x7
x9 ~ eta1 + x3
x5 ~ x8 + x9
x3 ~ x10
x1 ~ x9
