eta1 =~ y1 + y2 + y3
x1 ~ x4 + x5 + x7 + x8
x8 ~ x6
x5 ~ x2
eta1 ~ x7
x10 ~ x6
x9 ~ x5
x3 ~ x4
