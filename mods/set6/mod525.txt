eta1 =~ y1 + y2 + y3
x10 ~ x5
eta1 ~ x10
x3 ~ eta1 + x1 + x4
x7 ~ eta1 + x8
x2 ~ x5
x1 ~ x6
x9 ~ x4
