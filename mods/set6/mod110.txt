eta1 =~ y1 + y2 + y3
eta1 ~ x3 + x7
x2 ~ eta1 + x5
x9 ~ x2 + x8
x8 ~ x10
x4 ~ x8
x6 ~ x4
x1 ~ eta1
