eta1 =~ y1 + y2
x1 ~ x2 + x7
x6 ~ x1 + x5 + x9
x9 ~ eta1
x3 ~ x9
x4 ~ x8 + x9
x10 ~ x2
