eta1 =~ y1 + y2 + y3
x2 ~ x1
x9 ~ x2 + x5 + x7
x5 ~ eta1 + x3
x8 ~ x2
x6 ~ x8
x4 ~ x6
x10 ~ x3
