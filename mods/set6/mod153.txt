eta1 =~ y1 + y2
x7 ~ x4 + x5 + x6
x9 ~ x2 + x7
x8 ~ x9
eta1 ~ x7
x1 ~ x7
x3 ~ x6
x10 ~ x3
