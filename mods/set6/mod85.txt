eta1 =~ y1 + y2 + y3
x6 ~ eta1 + x10 + x4 + x8
x2 ~ x6
x5 ~ x6
x7 ~ x5
x9 ~ x1 + x4
x3 ~ x9
