eta1 =~ y1 + y2 + y3
x7 ~ x10 + x4
x6 ~ x7 + x9
x8 ~ x7
x5 ~ x7
x9 ~ x3
x1 ~ eta1 + x4
x2 ~ x3
