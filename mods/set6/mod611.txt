eta1 =~ y1 + y2
x5 ~ x4
x1 ~ x10 + x5 + x6
x10 ~ x9
x6 ~ x2 + x3
x7 ~ x10
eta1 ~ x5
x8 ~ x4
