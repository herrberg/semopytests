eta1 =~ y1 + y2
x6 ~ x10
x5 ~ x6
x2 ~ x10 + x3 + x7
x8 ~ x2
x3 ~ x1
x4 ~ x3
x9 ~ x7
eta1 ~ x9
