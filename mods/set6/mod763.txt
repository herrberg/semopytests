eta1 =~ y1 + y2
x5 ~ eta1 + x4
x10 ~ x1 + x5
x6 ~ x5 + x8 + x9
x3 ~ x6
x7 ~ eta1
x2 ~ x8
