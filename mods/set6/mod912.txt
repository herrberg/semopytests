eta1 =~ y1 + y2 + y3
x10 ~ x7
x9 ~ x10
x5 ~ x9
x3 ~ eta1 + x1 + x10
x2 ~ x6 + x8
x1 ~ x2
x4 ~ x9
