eta1 =~ y1 + y2 + y3
x9 ~ x2
x1 ~ x9
x7 ~ x1 + x8
x10 ~ x9
x4 ~ x10 + x5 + x6
eta1 ~ x6
x3 ~ x5
