eta1 =~ y1 + y2 + y3
x2 ~ x3
x5 ~ x2
eta1 ~ x1 + x5
x4 ~ x2 + x9
x7 ~ x4
x8 ~ x7
x6 ~ x3
x10 ~ x1
