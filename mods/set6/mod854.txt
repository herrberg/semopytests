eta1 =~ y1 + y2 + y3
x7 ~ x3
x5 ~ eta1 + x7
x6 ~ x5
x1 ~ x9
eta1 ~ x1
x8 ~ x5
x4 ~ x7
x2 ~ eta1
x10 ~ eta1
