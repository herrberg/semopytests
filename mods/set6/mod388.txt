eta1 =~ y1 + y2
x5 ~ x3 + x7
x3 ~ x8
x10 ~ x8
x6 ~ x7
x2 ~ x7
eta1 ~ x2
x1 ~ x8
x9 ~ x3
x4 ~ x2
