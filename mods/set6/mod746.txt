eta1 =~ y1 + y2
x2 ~ x4
x3 ~ x2
x10 ~ x3
x7 ~ x4
x9 ~ x7
x6 ~ x9
x5 ~ x4
x1 ~ x5 + x8
eta1 ~ x9
