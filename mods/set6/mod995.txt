eta1 =~ y1 + y2 + y3
x9 ~ x7
x3 ~ x9
x8 ~ x3
x4 ~ x8
x2 ~ x4
x5 ~ x2
eta1 ~ x4
x1 ~ x10 + x9
x6 ~ x8
