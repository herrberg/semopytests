eta1 =~ y1 + y2 + y3
eta1 ~ x9
x3 ~ eta1 + x1 + x8
x2 ~ x10 + x3
x6 ~ x2
x4 ~ eta1
x5 ~ x3
x7 ~ eta1
