eta1 =~ y1 + y2
x4 ~ x2 + x7 + x9
x1 ~ x3 + x4
eta1 ~ x1
x6 ~ eta1
x2 ~ x10 + x5
x8 ~ x3
