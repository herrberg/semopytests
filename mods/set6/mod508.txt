eta1 =~ y1 + y2 + y3
x7 ~ x2 + x5
x3 ~ x7
x9 ~ x3
x6 ~ x2
x8 ~ x1 + x10 + x5
eta1 ~ x4 + x8
