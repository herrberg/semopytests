eta1 =~ y1 + y2 + y3
x9 ~ x3 + x6
eta1 ~ x9
x4 ~ eta1
x10 ~ x5 + x9
x8 ~ x10
x7 ~ x9
x2 ~ x10
x1 ~ x10
