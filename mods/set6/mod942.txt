eta1 =~ y1 + y2
x1 ~ x3 + x5 + x6 + x9
x7 ~ x5
x10 ~ x7
x4 ~ x2
x3 ~ x4
eta1 ~ x5
x6 ~ x8
