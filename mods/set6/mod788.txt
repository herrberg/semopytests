eta1 =~ y1 + y2
x6 ~ x10
x3 ~ x6
x4 ~ x2 + x3
x7 ~ x4
x5 ~ x3
eta1 ~ x10 + x8
x1 ~ eta1
x9 ~ x3
