eta1 =~ y1 + y2
x3 ~ x5 + x6
x8 ~ x10 + x3
x7 ~ x6
x2 ~ eta1
x5 ~ x2
x9 ~ x4 + x6
x4 ~ x1
