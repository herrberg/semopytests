eta1 =~ y1 + y2
x9 ~ x10 + x5
x4 ~ x2 + x9
x6 ~ eta1 + x4
x3 ~ x6
x1 ~ x6
eta1 ~ x7
x8 ~ x7
