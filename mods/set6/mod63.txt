eta1 =~ y1 + y2 + y3
x5 ~ x9
eta1 ~ x5 + x7
x3 ~ eta1
x2 ~ x3
x8 ~ x4 + x5
x10 ~ x1 + x4
x6 ~ x4
