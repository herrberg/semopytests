eta1 =~ y1 + y2 + y3
x8 ~ x5 + x7
x4 ~ x10 + x6 + x8
x3 ~ x7 + x9
x1 ~ eta1 + x6
x2 ~ x9
