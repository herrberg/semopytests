eta1 =~ y1 + y2
x4 ~ x5 + x7 + x9
x10 ~ x4
x7 ~ eta1
x2 ~ x7
x1 ~ x5
x6 ~ x1
x3 ~ x6
x8 ~ x7
