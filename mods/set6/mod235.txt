eta1 =~ y1 + y2 + y3
eta1 ~ x7
x9 ~ eta1
x6 ~ x9
x8 ~ x2 + x6
x4 ~ x8
x5 ~ x4
x10 ~ x3 + x6
x1 ~ x6
