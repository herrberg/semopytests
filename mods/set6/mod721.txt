eta1 =~ y1 + y2
x5 ~ x9
x2 ~ x5 + x7
x6 ~ x2
x1 ~ x6
x10 ~ x2
x3 ~ eta1 + x9
x8 ~ x2 + x4
