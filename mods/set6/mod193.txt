eta1 =~ y1 + y2
x1 ~ x4 + x5 + x7 + x8
x8 ~ x2
x6 ~ x8
x9 ~ eta1 + x4
x3 ~ x4
x10 ~ x3
