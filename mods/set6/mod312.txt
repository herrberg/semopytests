eta1 =~ y1 + y2 + y3
x4 ~ x7
x1 ~ x4
x10 ~ x1 + x3
x2 ~ x4 + x6
x5 ~ x2
x9 ~ x5
x3 ~ x8
eta1 ~ x5
