eta1 =~ y1 + y2 + y3
x2 ~ x9
eta1 ~ x1 + x2 + x7
x6 ~ x2
x5 ~ x10 + x6
x4 ~ x7
x3 ~ x7
x8 ~ x2
