eta1 =~ y1 + y2 + y3
x9 ~ x1
x7 ~ x4 + x9
x3 ~ x7
x2 ~ x3 + x8
eta1 ~ x9
x6 ~ eta1
x5 ~ x1
x10 ~ eta1
