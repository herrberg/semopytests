eta1 =~ y1 + y2
x8 ~ x2
x4 ~ eta1 + x8 + x9
x5 ~ x10 + x3
x9 ~ x5
x1 ~ x8
x6 ~ x9
x10 ~ x7
