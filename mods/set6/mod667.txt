eta1 =~ y1 + y2
x2 ~ eta1 + x1
x9 ~ x2
x3 ~ x9
x8 ~ eta1
x7 ~ x10 + x9
x5 ~ eta1 + x6
x4 ~ x1
