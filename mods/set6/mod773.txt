eta1 =~ y1 + y2
x6 ~ x1 + x10 + x4
x7 ~ x6
eta1 ~ x1
x9 ~ eta1
x2 ~ x5 + x9
x5 ~ x3
x8 ~ x9
