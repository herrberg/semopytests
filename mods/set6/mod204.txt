eta1 =~ y1 + y2
x2 ~ x3 + x5
x9 ~ x2
x10 ~ x8 + x9
eta1 ~ x5
x1 ~ eta1
x4 ~ x3
x6 ~ x5
x7 ~ x3
