eta1 =~ y1 + y2 + y3
x4 ~ x7
x10 ~ x4
x9 ~ x10
eta1 ~ x9
x2 ~ eta1 + x3
x5 ~ eta1
x6 ~ x5
x1 ~ eta1
x8 ~ x5
