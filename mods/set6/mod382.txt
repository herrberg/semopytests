eta1 =~ y1 + y2
x8 ~ x1
x2 ~ x10 + x8
x3 ~ x6 + x9
x10 ~ eta1 + x3 + x5 + x7
x4 ~ eta1
