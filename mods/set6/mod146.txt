eta1 =~ y1 + y2 + y3
x3 ~ x5
x7 ~ x3
x8 ~ x7
x1 ~ eta1 + x4 + x8
x10 ~ x6 + x7
x2 ~ x10
x9 ~ x2
