eta1 =~ y1 + y2
eta1 ~ x5
x8 ~ eta1 + x7
x3 ~ x4 + x8
x6 ~ x3 + x9
x2 ~ x6
x1 ~ x10 + x4
