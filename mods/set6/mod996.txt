eta1 =~ y1 + y2 + y3
x1 ~ x2
x6 ~ eta1 + x1 + x5
x10 ~ x4 + x6 + x9
x3 ~ x1
x8 ~ x1
x7 ~ x2
