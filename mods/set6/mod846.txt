eta1 =~ y1 + y2
x8 ~ eta1 + x1 + x7
x5 ~ x8
x4 ~ x10
eta1 ~ x2 + x4
x6 ~ x3 + x7 + x9
