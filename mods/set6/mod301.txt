eta1 =~ y1 + y2 + y3
x9 ~ x5
x4 ~ x1 + x8 + x9
x2 ~ x4
x10 ~ x4
x7 ~ x10
x3 ~ x10
eta1 ~ x3
x6 ~ x9
