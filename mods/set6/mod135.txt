eta1 =~ y1 + y2
x6 ~ eta1
x10 ~ x6
x7 ~ x10 + x3
x4 ~ x7
x8 ~ x10
x1 ~ x10
x5 ~ x10 + x9
x9 ~ x2
