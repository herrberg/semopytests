eta1 =~ y1 + y2
x3 ~ x1 + x5
x2 ~ x3
x9 ~ x2
x4 ~ x7 + x9
x5 ~ x6
eta1 ~ x2
x8 ~ x7
x10 ~ x2
