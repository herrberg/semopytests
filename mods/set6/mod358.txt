eta1 =~ y1 + y2
x1 ~ x10
x7 ~ x1
eta1 ~ x7
x6 ~ eta1
x4 ~ x6
x8 ~ x7
x5 ~ x8
x9 ~ x5
x3 ~ x7
x2 ~ eta1
