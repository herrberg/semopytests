eta1 =~ y1 + y2
x6 ~ x10 + x7
x2 ~ x6
x3 ~ x2
eta1 ~ x1 + x3 + x9
x4 ~ x3
x8 ~ x2
x5 ~ x9
