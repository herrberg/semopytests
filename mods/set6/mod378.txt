eta1 =~ y1 + y2 + y3
x5 ~ x6 + x7 + x8 + x9
x10 ~ x2 + x7
x4 ~ x3 + x7
x1 ~ x4
eta1 ~ x7
