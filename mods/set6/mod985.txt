eta1 =~ y1 + y2 + y3
x2 ~ eta1 + x10 + x3
x9 ~ x1 + x3 + x6
x5 ~ x3
x4 ~ x6
x8 ~ x4
x7 ~ x6
