eta1 =~ y1 + y2 + y3
x10 ~ x3
x4 ~ x10
x2 ~ x4
x9 ~ x2
x1 ~ x10
eta1 ~ x1 + x6 + x7
x6 ~ x5
x8 ~ x6
