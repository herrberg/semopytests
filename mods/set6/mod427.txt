eta1 =~ y1 + y2
x2 ~ x5
x3 ~ x2 + x6
x1 ~ x3
x10 ~ eta1 + x1
x4 ~ eta1
x8 ~ x4
x9 ~ x6
x7 ~ x9
