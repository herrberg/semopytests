eta1 =~ y1 + y2 + y3
x1 ~ x9
x10 ~ eta1 + x1 + x4
x4 ~ x2 + x6 + x7 + x8
x5 ~ x6
x3 ~ x6
