eta1 =~ y1 + y2
x4 ~ x1
x3 ~ x4
x9 ~ x3 + x5 + x6
x8 ~ x9
x2 ~ x1
x10 ~ x2
x6 ~ eta1
x7 ~ x1
