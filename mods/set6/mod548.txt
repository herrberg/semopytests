eta1 =~ y1 + y2
x6 ~ eta1 + x4 + x7
x10 ~ x6
x8 ~ x6 + x9
x1 ~ eta1
x2 ~ x1
x3 ~ x4
x7 ~ x5
