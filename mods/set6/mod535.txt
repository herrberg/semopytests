eta1 =~ y1 + y2 + y3
x6 ~ x3 + x9
x2 ~ x6
x4 ~ x2
x8 ~ x6
x7 ~ x6
x9 ~ x10
x1 ~ x10
eta1 ~ x9
x5 ~ x3
