eta1 =~ y1 + y2
x5 ~ eta1
x3 ~ x5 + x8
x7 ~ x3
x2 ~ x7
x10 ~ x2
x8 ~ x1
x6 ~ eta1 + x9
x4 ~ x2
