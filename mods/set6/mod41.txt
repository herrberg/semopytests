eta1 =~ y1 + y2
x10 ~ x3 + x5
x9 ~ x10 + x8
x3 ~ x1 + x7
x6 ~ x1
x2 ~ x3
x4 ~ x10
eta1 ~ x1
