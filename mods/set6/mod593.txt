eta1 =~ y1 + y2
x1 ~ x5
x10 ~ x1 + x6
x2 ~ x10
x4 ~ x1
x9 ~ x4 + x8
x7 ~ x5
eta1 ~ x4
x3 ~ x6
