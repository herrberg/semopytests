eta1 =~ y1 + y2 + y3
x5 ~ eta1 + x10 + x7
x1 ~ x2 + x5
x3 ~ x1
x6 ~ x5
x9 ~ x7
x4 ~ x1
x8 ~ x7
