eta1 =~ y1 + y2 + y3
x10 ~ x1 + x8
eta1 ~ x10 + x5 + x7
x8 ~ x2
x3 ~ x10
x6 ~ x10 + x4
x9 ~ x5
