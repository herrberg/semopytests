eta1 =~ y1 + y2
x8 ~ x4
x9 ~ eta1 + x7 + x8
x2 ~ x4
x6 ~ x2 + x5
x3 ~ x2
x10 ~ x2
x1 ~ x5
