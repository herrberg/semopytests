eta1 =~ y1 + y2
x6 ~ x7
x2 ~ x4 + x6
x1 ~ x2 + x3
x5 ~ x6
x8 ~ x5
eta1 ~ x8
x3 ~ x10
x9 ~ x4
