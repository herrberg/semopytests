eta1 =~ y1 + y2
x7 ~ eta1 + x3
x9 ~ x3 + x8
x2 ~ x3 + x6
x6 ~ x4
eta1 ~ x1 + x10
x5 ~ eta1
