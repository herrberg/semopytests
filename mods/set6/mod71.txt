eta1 =~ y1 + y2
x8 ~ x5
x2 ~ x8
x3 ~ x2
x1 ~ x3
x10 ~ x2 + x4 + x7 + x9
x6 ~ x10
eta1 ~ x10
