eta1 =~ y1 + y2
x4 ~ x2
x10 ~ x4
x9 ~ x10
x6 ~ x9
x3 ~ x6
x1 ~ x2 + x8
x5 ~ x1
eta1 ~ x5
x7 ~ x2
