eta1 =~ y1 + y2
x4 ~ x2 + x3
x9 ~ x4
x2 ~ x10
eta1 ~ x1 + x3
x5 ~ x10 + x6 + x7
x8 ~ x5
