eta1 =~ y1 + y2 + y3
x3 ~ x10 + x4
x1 ~ x2 + x3 + x7 + x8
x9 ~ x4
x6 ~ x9
x5 ~ eta1
x8 ~ x5
