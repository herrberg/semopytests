eta1 =~ y1 + y2 + y3
x8 ~ x2
x1 ~ eta1 + x6 + x8
x3 ~ x1
x9 ~ x8
x5 ~ x8
x4 ~ x5
x10 ~ x2
x7 ~ x8
