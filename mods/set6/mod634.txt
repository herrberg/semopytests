eta1 =~ y1 + y2 + y3
x4 ~ x6 + x7
x5 ~ x4
x10 ~ x6
x3 ~ x6
x9 ~ x1 + x3
x8 ~ x4
x2 ~ x8
eta1 ~ x7
