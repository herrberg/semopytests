eta1 =~ y1 + y2
x6 ~ x1 + x7
x5 ~ eta1 + x2 + x6
x4 ~ x6
x9 ~ x4
x8 ~ x6
x3 ~ x6
x10 ~ x3
