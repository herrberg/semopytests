eta1 =~ y1 + y2
x2 ~ x8
x7 ~ x2 + x4
x3 ~ x7
x1 ~ x10 + x3
x9 ~ x8
x5 ~ x2
eta1 ~ x7
x6 ~ x10
