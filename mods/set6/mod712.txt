eta1 =~ y1 + y2 + y3
x10 ~ eta1 + x2
x1 ~ x10 + x6
x8 ~ x1 + x7
x3 ~ x1
x9 ~ x7
x4 ~ x1
x5 ~ x4
