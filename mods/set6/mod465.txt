eta1 =~ y1 + y2 + y3
x1 ~ x6
x10 ~ x1 + x2
x9 ~ x10
x3 ~ x9
x2 ~ x4
x8 ~ x10
x5 ~ x9
eta1 ~ x5
x7 ~ x2
