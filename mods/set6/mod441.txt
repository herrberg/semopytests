eta1 =~ y1 + y2
x8 ~ eta1 + x2 + x5
x6 ~ x8
x9 ~ x6
x1 ~ x9
x7 ~ x1
x4 ~ x3 + x9
x10 ~ x8
