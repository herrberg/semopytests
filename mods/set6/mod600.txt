eta1 =~ y1 + y2
x1 ~ x4 + x5
x3 ~ x1 + x6
x2 ~ x3
eta1 ~ x1 + x8
x9 ~ x8
x10 ~ x8
x7 ~ x4
