eta1 =~ y1 + y2
x6 ~ x10
x1 ~ x3 + x6
eta1 ~ x1 + x8
x9 ~ eta1
x2 ~ x3
x7 ~ x8
x5 ~ x7
x4 ~ x8
