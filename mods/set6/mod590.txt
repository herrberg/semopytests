eta1 =~ y1 + y2
eta1 ~ x10
x2 ~ eta1
x7 ~ eta1 + x8
x4 ~ x7
x1 ~ x4 + x6
x3 ~ eta1 + x5
x9 ~ eta1
