eta1 =~ y1 + y2
x10 ~ x9
x3 ~ x10 + x5 + x7
x7 ~ eta1
x5 ~ x2
x6 ~ x7
x4 ~ x10
x8 ~ x7
x1 ~ x7
