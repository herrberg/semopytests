eta1 =~ y1 + y2 + y3
x9 ~ x7
x3 ~ x1 + x9
x4 ~ x3
eta1 ~ x3 + x8
x10 ~ eta1
x6 ~ x10 + x2
x5 ~ x9
