eta1 =~ y1 + y2
x2 ~ eta1 + x3 + x9
x6 ~ x9
x7 ~ x6
x1 ~ x7
x3 ~ x4
x8 ~ eta1 + x10 + x5
