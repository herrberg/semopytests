eta1 =~ y1 + y2 + y3
x4 ~ x2
x5 ~ x4
x7 ~ x5
x3 ~ x4
eta1 ~ x5
x1 ~ x2
x6 ~ x1 + x10
x9 ~ x4
x8 ~ x1
