eta1 =~ y1 + y2
x7 ~ x6
x5 ~ x7 + x9
x3 ~ x7
x8 ~ x3
eta1 ~ x6
x4 ~ eta1
x1 ~ x4
x2 ~ eta1
x9 ~ x10
