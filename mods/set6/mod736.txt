eta1 =~ y1 + y2 + y3
x8 ~ x6
x3 ~ x10 + x8
x9 ~ x3 + x4
x5 ~ x9
x2 ~ x3
eta1 ~ x2 + x7
x1 ~ x4
