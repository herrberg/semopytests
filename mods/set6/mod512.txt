eta1 =~ y1 + y2 + y3
x1 ~ x2 + x3 + x9
eta1 ~ x1 + x10
x2 ~ x8
x6 ~ x4 + x9
x7 ~ x8
x5 ~ x3
