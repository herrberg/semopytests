eta1 =~ y1 + y2
x10 ~ x4 + x6 + x9
x5 ~ x2
x7 ~ x5
x4 ~ x7
eta1 ~ x1 + x7
x8 ~ x6
x3 ~ x5
