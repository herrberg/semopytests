eta1 =~ y1 + y2
x7 ~ eta1 + x4
x3 ~ x7
x8 ~ x3
x5 ~ x8
x1 ~ x7
x10 ~ x3
x9 ~ x3
x2 ~ x4
x6 ~ x4
