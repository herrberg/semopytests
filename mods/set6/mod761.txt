eta1 =~ y1 + y2
x6 ~ x1 + x3 + x9
eta1 ~ x1 + x5 + x8
x4 ~ x7 + x8
x2 ~ x4
x10 ~ x1
