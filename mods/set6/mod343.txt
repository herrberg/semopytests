eta1 =~ y1 + y2
x6 ~ x1 + x2
x8 ~ x6 + x9
x4 ~ x5 + x8
x10 ~ x8
x3 ~ x2
x9 ~ x7
eta1 ~ x9
