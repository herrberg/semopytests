eta1 =~ y1 + y2 + y3
x9 ~ eta1
x5 ~ x9
x10 ~ x4 + x9
x8 ~ x4
x3 ~ x8
x7 ~ x3
x1 ~ x9
x6 ~ x1
x2 ~ x9
