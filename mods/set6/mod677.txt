eta1 =~ y1 + y2 + y3
x3 ~ x5
x9 ~ x2 + x3
x1 ~ x7 + x9
x4 ~ x1
eta1 ~ x3
x8 ~ x6 + x9
x10 ~ x7
