eta1 =~ y1 + y2 + y3
x2 ~ x8 + x9
x1 ~ x2
x4 ~ eta1 + x1
x9 ~ x5 + x6
x3 ~ x9
eta1 ~ x7
x10 ~ x9
