eta1 =~ y1 + y2
x9 ~ x1
x6 ~ eta1 + x5 + x9
x8 ~ x6
x10 ~ x8
x3 ~ x8
x2 ~ x6
x4 ~ x9
x7 ~ x5
