eta1 =~ y1 + y2 + y3
x10 ~ x5
eta1 ~ x1 + x10
x2 ~ eta1
x7 ~ x10 + x3
x8 ~ x1 + x9
x4 ~ x8
x6 ~ x1
