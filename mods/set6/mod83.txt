eta1 =~ y1 + y2
x4 ~ eta1 + x9
x7 ~ x4 + x8
x1 ~ x3
x8 ~ x1
x5 ~ eta1 + x10
x2 ~ eta1
x6 ~ x1
