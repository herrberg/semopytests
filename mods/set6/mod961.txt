eta1 =~ y1 + y2
x7 ~ x6
x9 ~ x7
x2 ~ x3 + x9
x10 ~ x2
x5 ~ eta1 + x7
x4 ~ x9
x8 ~ eta1
x1 ~ x9
