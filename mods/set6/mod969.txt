eta1 =~ y1 + y2 + y3
x4 ~ x7 + x8
x5 ~ x1 + x4 + x6
eta1 ~ x5
x10 ~ x8
x9 ~ x10
x6 ~ x2
x3 ~ x4
