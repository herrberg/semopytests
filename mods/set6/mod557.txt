eta1 =~ y1 + y2
x4 ~ x10 + x2 + x3 + x7
x7 ~ eta1
x1 ~ x3 + x5
x2 ~ x9
x6 ~ eta1
x8 ~ x6
