eta1 =~ y1 + y2 + y3
x8 ~ x2
x1 ~ x8
x9 ~ x1 + x7
x4 ~ eta1 + x1
x3 ~ x10 + x7
x5 ~ x10
x6 ~ x2
