eta1 =~ y1 + y2
x6 ~ x2 + x3
x7 ~ x2
x10 ~ x2
eta1 ~ x10
x5 ~ x2 + x8
x1 ~ x4 + x5
x9 ~ x5
