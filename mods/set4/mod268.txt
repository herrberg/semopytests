eta1 =~ y1 + y2
eta2 =~ y3 + y4
x3 ~ eta2 + x2
x4 ~ x2
x5 ~ x4
eta1 ~ x5
x1 ~ eta2
