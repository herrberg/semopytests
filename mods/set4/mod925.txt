eta1 =~ y1 + y2
eta2 =~ y3 + y4
x5 ~ eta1 + x4
x1 ~ x5
x3 ~ x2 + x4
eta2 ~ x4
