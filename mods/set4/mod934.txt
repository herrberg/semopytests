eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x1 ~ eta1
eta2 ~ x1 + x3 + x5
x2 ~ eta2
x5 ~ x4
