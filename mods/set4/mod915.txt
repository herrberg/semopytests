eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x4 ~ eta1
x5 ~ x4
x1 ~ x4
x3 ~ eta1
eta2 ~ x2 + x4
