eta1 =~ y1 + y2
eta2 =~ y3 + y4
x2 ~ eta2 + x3
eta1 ~ x2
x1 ~ eta1 + x4
x5 ~ x3
