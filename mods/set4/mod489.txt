eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta2 ~ x3
x5 ~ eta2
x4 ~ x5
eta1 ~ x5
x1 ~ eta1 + x2
