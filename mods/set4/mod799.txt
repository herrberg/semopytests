eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
x4 ~ eta2
x2 ~ x4 + x5
x5 ~ x1 + x3
eta1 ~ x4
