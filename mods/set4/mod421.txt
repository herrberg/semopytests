eta1 =~ y1 + y2
eta2 =~ y3 + y4
x3 ~ eta1 + x2
x5 ~ eta2 + x3
x1 ~ x5
x4 ~ x3
