eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5 + y6
eta2 ~ x1
x5 ~ eta2
eta1 ~ eta2
x3 ~ x1
x2 ~ eta2 + x4
