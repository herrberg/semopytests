eta1 =~ y1 + y2
eta2 =~ y3 + y4 + y5
x3 ~ eta2 + x5
eta1 ~ x3
x1 ~ x5
eta2 ~ x2
x4 ~ x5
