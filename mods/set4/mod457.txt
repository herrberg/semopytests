eta1 =~ y1 + y2
eta2 =~ y3 + y4
x4 ~ x5
x2 ~ eta2 + x4
x1 ~ x2
eta2 ~ x3
eta1 ~ x4
