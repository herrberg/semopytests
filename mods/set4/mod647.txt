eta1 =~ y1 + y2
eta2 =~ y3 + y4
x4 ~ eta1 + x2
x5 ~ x3 + x4
x1 ~ x3
eta2 ~ x2
