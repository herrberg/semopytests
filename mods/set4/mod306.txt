eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
x4 ~ x1
x2 ~ eta2 + x4
eta1 ~ x5
eta2 ~ eta1
x3 ~ x4
