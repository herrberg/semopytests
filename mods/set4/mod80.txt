eta1 =~ y1 + y2 + y3
eta2 =~ y4 + y5
eta1 ~ x2
x3 ~ eta1
eta2 ~ x2 + x4
x4 ~ x5
x1 ~ x2
