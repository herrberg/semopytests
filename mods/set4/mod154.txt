eta1 =~ y1 + y2
eta2 =~ y3 + y4
eta1 ~ x4
x3 ~ eta1
eta2 ~ x3
x2 ~ eta2 + x1
x5 ~ eta1
