lhs,op,rhs,est
eta1,~,eta2,0.17203141170864433
eta2,~,x2,-0.25396925810370746
eta2,~,x6,-0.7677031427402768
eta3,~,eta1,-0.4547127478047828
x1,~,x4,0.9465554398409332
x10,~,x4,-0.771167679959578
x2,~,eta4,0.44517059169447265
x2,~,x9,0.38861893102757666
x2,~,x5,-0.5847465284171927
x3,~,x7,-0.28543851598375747
x4,~,x9,1.283256018411822
x8,~,x4,-1.0805312745973867
x9,~,x3,0.2074966543480936
eta1,=~,y2,0.5037336101931871
eta2,=~,y4,0.8101612034953474
eta3,=~,y6,-0.6167628436782051
eta4,=~,y8,1.003694512761957
eta4,=~,y9,0.2759136457574314
eta1,~~,eta1,1.867348854761452
eta2,~~,eta2,0.8217873603199533
eta3,~~,eta3,0.4196484779806064
eta3,~~,x1,-0.0396290795552684
eta3,~~,x10,-0.020682791754200448
eta3,~~,x8,-0.052372831893293816
eta4,~~,eta4,1.6640128750395822
x1,~~,x1,1.1164312272517964
x1,~~,x10,0.012717464628997584
x1,~~,x8,0.011116791506081213
x10,~~,x10,0.6630730428346585
x10,~~,x8,0.08725607243241762
x2,~~,x2,0.595407999557318
x3,~~,x3,0.07395183166646872
x4,~~,x4,1.0555252092161544
x8,~~,x8,1.1088563588715619
x9,~~,x9,0.04314813812038287
y1,~~,y1,2.1057149822142374
y2,~~,y2,0.35859169877524744
y3,~~,y3,2.889513442236965
y4,~~,y4,2.890006047250396
y5,~~,y5,2.087167124103951
y6,~~,y6,0.5372539312995874
y7,~~,y7,1.186920505754392
y8,~~,y8,0.7314183462852195
y9,~~,y9,0.07597474230387824
