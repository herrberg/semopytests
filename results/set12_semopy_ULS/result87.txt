lhs,op,rhs,est
eta2,~,x3,-0.054021120469223076
eta2,~,x4,-0.5705761357197037
x10,~,eta1,0.3005101177642399
x10,~,x2,-0.4991491251483012
x2,~,x5,0.773836067943104
x2,~,x9,0.5928385410290964
x5,~,x10,0.2891702287553814
x6,~,x7,-0.37200764243608864
x6,~,x8,0.46225921125288105
x7,~,x4,-0.3417417257295401
x8,~,x1,-0.6717540510419698
x8,~,x9,-1.1497793885114467
eta1,=~,y2,-1.1004341632632468
eta1,=~,y3,0.18869010986161433
eta2,=~,y5,-0.40990877693752403
eta1,~~,eta1,1.559355327666644
eta2,~~,eta2,0.32940474136810555
eta2,~~,x6,0.1608074819619023
x10,~~,x10,0.6856910500456195
x2,~~,x2,1.549064421911351
x5,~~,x5,0.03666252161741368
x6,~~,x6,1.3732674410335333
x7,~~,x7,0.11633491011176778
x8,~~,x8,1.9193428859572297
y1,~~,y1,1.0413371262412339
y2,~~,y2,1.0037370424541316
y3,~~,y3,0.04386270280962806
y4,~~,y4,1.8475912222459996
y5,~~,y5,0.3117004483697257
