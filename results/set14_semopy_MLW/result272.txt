lhs,op,rhs,est
eta3,~,x9,-0.40730113227471676
eta5,~,eta4,0.4511235111270925
eta6,~,eta2,-0.1478585395354616
eta7,~,x10,0.36568180595984917
x1,~,eta4,0.3464078397532163
x10,~,x8,0.21849341923834953
x2,~,eta1,0.16421622814671796
x2,~,eta3,0.8464624607214288
x2,~,eta5,-0.8358858352560113
x3,~,x6,0.871971017146143
x4,~,eta6,0.3892748429396795
x4,~,eta8,-0.10837657107538257
x4,~,x5,0.5659651354994196
x4,~,x9,0.2069408075540529
x5,~,x8,-1.005647019707208
x9,~,x2,-0.24718499208324604
x9,~,x6,0.675958806804504
x9,~,x7,-0.40657900263530555
eta5,=~,y12,-0.9749950423958182
eta6,=~,y15,-0.6832954479012449
eta7,=~,y16,0.8962527165651949
eta7,=~,y17,0.3235755295313224
eta8,=~,y19,0.14835888456717256
eta8,=~,y20,0.8545719305062618
eta1,=~,y3,-0.7094597653821595
eta1,=~,y4,0.13417436337392025
eta2,=~,y5,0.7211409528149834
eta2,=~,y6,-0.5204794456666306
eta3,=~,y8,-0.25868166061141823
eta4,=~,y9,0.637603354622422
eta1,~~,eta1,2.4326019234021072
eta1,~~,eta2,0.0437192786895177
eta1,~~,eta4,-0.11389192472790968
eta1,~~,eta8,-0.09520621391051744
eta2,~~,eta2,1.6436780784050398
eta2,~~,eta4,0.08390848641250025
eta2,~~,eta8,0.07114903565846177
eta3,~~,eta3,0.48226953634840175
eta4,~~,eta4,1.44719482485073
eta4,~~,eta8,0.03655033081427626
eta5,~~,eta5,0.203494241255244
eta6,~~,eta6,0.1658074167059829
eta7,~~,eta7,0.17177546450293266
eta7,~~,x1,0.01928345880124278
eta7,~~,x3,-0.04046987768856915
eta7,~~,x4,-0.0014664757518877056
eta8,~~,eta8,1.9727050828731534
x1,~~,x1,0.11417298585721862
x1,~~,x3,0.0329494706852323
x1,~~,x4,-0.02179072929800883
x10,~~,x10,0.04399466611381376
x2,~~,x2,1.1786191126835053
x3,~~,x3,0.6820356194486688
x3,~~,x4,0.11302113025623985
x4,~~,x4,1.660318042761616
x5,~~,x5,0.8805527088047896
x9,~~,x9,0.8072362725734648
y1,~~,y1,0.22412816819460182
y10,~~,y10,1.056904609930899
y11,~~,y11,0.8523961174488828
y12,~~,y12,1.0088213410425384
y14,~~,y14,0.8562829443608166
y15,~~,y15,1.1209198875374278
y16,~~,y16,0.6848379641886706
y17,~~,y17,0.30858411810903547
y18,~~,y18,0.9021486021627549
y19,~~,y19,0.024157982971779568
y20,~~,y20,1.0233648386392125
y3,~~,y3,1.402793695556047
y4,~~,y4,1.1116620158631796
y5,~~,y5,0.39069023172841494
y6,~~,y6,0.22302050207176655
y7,~~,y7,0.950266063092167
y8,~~,y8,0.09770670615712414
y9,~~,y9,0.27169001596507675
