lhs,op,rhs,est
eta4,~,x5,-0.6499738430541733
eta5,~,x10,-0.36816169177043373
eta5,~,x2,0.33816548970106136
eta5,~,x5,0.3654847827110726
eta7,~,eta1,-0.1489460328169847
eta8,~,eta3,0.19073545945412507
eta8,~,eta5,0.04193780553484168
eta8,~,eta6,0.616414043751745
x1,~,x8,0.8211129782701326
x10,~,eta1,0.15875268983014704
x10,~,eta2,0.5567220507923584
x10,~,eta8,-0.09165051377507691
x10,~,x6,-0.44257541754277024
x3,~,eta6,0.908592011102356
x4,~,eta1,0.7804324806980476
x6,~,x7,0.14071827720297628
x8,~,eta5,0.0453212277428139
x9,~,eta1,0.7525609800754689
eta4,=~,y11,-0.5633546733057424
eta5,=~,y13,-1.0137911644673996
eta5,=~,y14,1.1217323836909543
eta6,=~,y16,-0.6022826974942943
eta7,=~,y18,-0.20342197659740338
eta7,=~,y19,0.21804118790768015
eta1,=~,y3,0.28364645572976643
eta2,=~,y5,-0.6611900234445667
eta1,=~,y6,0.9621163018750293
eta2,=~,y6,-1.0250918972977314
eta3,=~,y8,0.8349877229059383
eta4,=~,y9,0.9251054017421912
eta8,=~,y9,-0.2674048166127696
eta1,~~,eta1,1.7173709388313843
eta1,~~,eta2,-0.15379022384657665
eta1,~~,eta3,0.1778648041249237
eta1,~~,eta6,-0.04257081588993729
eta2,~~,eta2,1.5494229722699508
eta2,~~,eta3,-0.01611757002529656
eta2,~~,eta6,-0.010334724763744122
eta3,~~,eta3,2.116941026451221
eta3,~~,eta6,0.0009278369801325771
eta4,~~,eta4,0.4348704496844315
eta4,~~,eta7,0.005597024714678434
eta4,~~,x1,-0.003070169543756076
eta4,~~,x3,-0.037099632929828714
eta4,~~,x4,0.0708188132351445
eta4,~~,x9,0.06514940206519736
eta5,~~,eta5,2.1071291842022117
eta6,~~,eta6,1.59759880811029
eta7,~~,eta7,0.02844449015530214
eta7,~~,x1,-0.019752135815659475
eta7,~~,x3,0.012416489325315338
eta7,~~,x4,0.014047190811837413
eta7,~~,x9,0.05337533657955302
eta8,~~,eta8,1.3856586697641926
x1,~~,x1,0.7143676699447825
x1,~~,x3,0.02537807959455851
x1,~~,x4,0.026963552907522108
x1,~~,x9,-0.049509401326903624
x10,~~,x10,1.2263742585282729
x3,~~,x3,0.6780298357318374
x3,~~,x4,-0.02480272739180557
x3,~~,x9,0.009217000802875217
x4,~~,x4,0.5122965303346104
x4,~~,x9,-0.040622869303740296
x6,~~,x6,0.018971525602325432
x8,~~,x8,0.20242971349542274
x9,~~,x9,0.4758250313164225
y1,~~,y1,1.0609351790266748
y10,~~,y10,1.1620934550696274
y11,~~,y11,0.3949356941879518
y12,~~,y12,5.052555572308742
y13,~~,y13,4.676185132727279
y14,~~,y14,6.091932916263657
y15,~~,y15,1.138413516855783
y16,~~,y16,0.2950031644554045
y17,~~,y17,0.9711785767571003
y18,~~,y18,0.05157777611350567
y19,~~,y19,0.1073119546120274
y20,~~,y20,2.1301045333963793
y3,~~,y3,0.09198788115204157
y4,~~,y4,0.8618618848661097
y5,~~,y5,0.3340802675045851
y6,~~,y6,1.4430413800013533
y7,~~,y7,0.590190335468046
y8,~~,y8,1.4071922564406658
y9,~~,y9,1.4514560723728667
