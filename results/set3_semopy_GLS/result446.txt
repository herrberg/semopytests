lhs,op,rhs,est
eta1,~,x1,0.9382932056852241
eta2,~,x1,-0.6189186686817422
x2,~,x3,-0.42885844970822207
x5,~,eta2,-0.4885300740877154
x5,~,x2,1.3399517749763405
x5,~,x4,-0.5637869551303241
eta1,=~,y2,-0.3011286311790706
eta1,=~,y3,0.9914694861385198
eta2,=~,y5,-1.1607148999620458
eta1,~~,eta1,0.8813675880593953
eta1,~~,x5,-0.001353560566346767
eta2,~~,eta2,0.43614336142595045
x2,~~,x2,0.18054400712779195
x5,~~,x5,2.799824077805086
y1,~~,y1,1.1211683478390924
y2,~~,y2,0.09613076651943937
y3,~~,y3,0.7619100039158923
y4,~~,y4,0.9858169240277921
y5,~~,y5,1.5027566625234605
