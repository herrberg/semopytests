lhs,op,rhs,est
eta1,~,x3,0.14593150705105218
eta2,~,x4,0.2957312109058333
x10,~,x1,-0.2803983131575229
x3,~,x8,1.0121940185604923
x4,~,x10,0.5012030711316527
x4,~,x5,0.3811442479354154
x5,~,x9,0.43914387158107765
x6,~,x3,-0.8532263941070873
x6,~,x7,-0.057169251637807735
x7,~,x4,0.36153303346373994
x9,~,x2,-0.4213167850430437
eta1,=~,y2,0.8634341709599328
eta1,=~,y3,0.47404803499008136
eta2,=~,y5,0.8298741872066508
eta1,~~,eta1,0.061264298397563526
eta1,~~,eta2,-0.004879868039848897
eta1,~~,x6,-0.0036026510549956895
eta2,~~,eta2,0.2116597712269202
eta2,~~,x6,0.02429258059118268
x10,~~,x10,0.0945498464911867
x3,~~,x3,0.9587490159983855
x4,~~,x4,0.46176075276552025
x5,~~,x5,0.18030902425210477
x6,~~,x6,0.8071131920285766
x7,~~,x7,0.20126996947337208
x9,~~,x9,0.1845235149521319
y1,~~,y1,0.9907647782812924
y2,~~,y2,0.3944747221137436
y3,~~,y3,0.2643257773743377
y4,~~,y4,1.980041021573761
y5,~~,y5,2.3103545065633946
