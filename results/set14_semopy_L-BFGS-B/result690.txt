lhs,op,rhs,est
eta1,~,eta6,-0.21285690031865964
eta1,~,x2,0.7510224941938005
eta2,~,eta1,0.01203387539053431
eta2,~,eta5,0.007948954477143636
eta4,~,x4,0.3036139896377379
eta4,~,x10,0.34012400697151923
eta8,~,x7,0.3350010663048269
x1,~,eta3,-0.15801484571368662
x1,~,eta7,-0.12532854150043282
x1,~,x3,-0.09906525168799328
x2,~,x3,-0.683258586960848
x3,~,eta1,-0.4849060869729691
x3,~,x9,-0.22639668171456823
x4,~,x3,-0.4893609261514891
x5,~,x9,0.5986043512330144
x6,~,x8,-0.5395535014168723
x7,~,x2,-0.6445958381024862
x8,~,x9,0.8396016064841858
eta1,=~,y10,-0.8919968103044202
eta5,=~,y11,0.06380524156311278
eta6,=~,y13,-0.24253577870091875
eta7,=~,y15,0.38344245499542223
eta8,=~,y17,-0.31533383617309896
eta8,=~,y18,0.4979331251157798
eta1,=~,y3,1.034265137316699
eta2,=~,y5,-0.053391663053486
eta3,=~,y7,-0.44562266161590536
eta4,=~,y9,-0.3243860727849676
eta1,~~,eta1,0.9826578849015645
eta2,~~,eta2,0.31269961565010806
eta2,~~,eta4,0.01281972912857712
eta2,~~,eta8,0.025788955693894437
eta2,~~,x1,-0.008194826594101996
eta2,~~,x5,-0.003078949785395917
eta2,~~,x6,-0.03399779499250921
eta3,~~,eta3,0.5894681016383164
eta3,~~,eta5,0.023720830529252272
eta3,~~,eta6,0.029894396839982195
eta3,~~,eta7,0.010655697399733325
eta4,~~,eta4,0.3902919395799402
eta4,~~,eta8,-0.05877844482296736
eta4,~~,x1,0.03184094108668449
eta4,~~,x5,-0.009994574877218849
eta4,~~,x6,-0.001372811116461474
eta5,~~,eta5,0.14629644032956624
eta5,~~,eta6,0.2899709737564754
eta5,~~,eta7,0.017354149927165073
eta6,~~,eta6,0.4334431396384387
eta6,~~,eta7,0.013844476049770736
eta7,~~,eta7,0.546629034785587
eta8,~~,eta8,0.5780769980909716
eta8,~~,x1,0.008721923156723784
eta8,~~,x5,0.019595338634149025
eta8,~~,x6,0.005397881536857083
x1,~~,x1,2.07623417717349
x1,~~,x5,-0.014406463243779328
x1,~~,x6,-0.019137973221294922
x2,~~,x2,0.4207678879413043
x3,~~,x3,1.1550290028533605
x4,~~,x4,0.30187820787457653
x5,~~,x5,0.2682799176696047
x5,~~,x6,-0.019689025618288024
x6,~~,x6,0.20894687751342364
x7,~~,x7,0.2895734608212644
x8,~~,x8,0.5220290850300167
y1,~~,y1,2.0842032916569306
y10,~~,y10,3.130537668153104
y11,~~,y11,0.0837667022034648
y12,~~,y12,1.602441934595085
y13,~~,y13,0.03671002014550495
y14,~~,y14,1.5372513732493427
y15,~~,y15,0.14565882881092204
y16,~~,y16,0.9478952235484102
y17,~~,y17,0.053734196407828914
y18,~~,y18,1.026249230009928
y3,~~,y3,1.9138713635178046
y4,~~,y4,1.7235383185422677
y5,~~,y5,0.977777889328454
y6,~~,y6,1.5638989398721694
y7,~~,y7,0.46377900309374503
y8,~~,y8,2.029118242387625
y9,~~,y9,0.15041907765892049
