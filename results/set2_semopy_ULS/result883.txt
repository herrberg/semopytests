lhs,op,rhs,est
eta2,~,x4,-0.3389435625483476
x1,~,x4,-1.1381522261935488
x2,~,eta1,-1.215931928359531
x3,~,x4,0.28074173460655505
x4,~,eta1,0.25939601414736885
x4,~,x5,-0.6018948047119467
eta1,=~,y2,1.0564946521864371
eta1,=~,y3,0.6199150756884182
eta2,=~,y5,0.6882980560978733
eta2,=~,y6,-0.5813146606635681
eta1,~~,eta1,1.4738811272616588
eta2,~~,eta2,0.15156032511894152
eta2,~~,x1,0.08896743734699572
eta2,~~,x2,-0.0399946770284556
eta2,~~,x3,-0.0035509027469129042
x1,~~,x1,2.312651637938478
x1,~~,x2,0.16725176873033495
x1,~~,x3,-0.03654084092885278
x2,~~,x2,1.2799093483546518
x2,~~,x3,-0.019527545077992495
x3,~~,x3,0.19809059848668348
x4,~~,x4,0.376831000186275
y1,~~,y1,1.0086527767552573
y2,~~,y2,1.05469561186255
y3,~~,y3,0.31226515822672546
y4,~~,y4,2.234025159339236
y5,~~,y5,0.9481735643750088
y6,~~,y6,1.1830998789246634
