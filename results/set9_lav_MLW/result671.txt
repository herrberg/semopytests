"lhs","op","rhs","est"
"eta1","=~","y2",0.517977806425405
"eta2","=~","y4",0.512454973628978
"eta2","=~","y5",-0.727469648400184
"eta3","=~","y7",0.25160795714976
"eta4","=~","y8",0.97600883455103
"eta4","=~","y9",-0.383975726461819
"eta5","=~","y12",-1.02624854323767
"eta5","=~","y13",-0.199175516245517
"eta6","=~","y15",0.106420959836484
"eta6","=~","y16",-0.437145020279541
"eta7","=~","y18",0.549360136888979
"eta8","=~","y20",-0.812944815035503
"eta8","=~","y21",-0.961543757786941
"x2","~","eta3",-1.18254209580185
"x2","~","eta4",0.354253953180885
"x2","~","eta7",0.320110261395067
"x2","~","x1",0.94779282649576
"x2","~","x7",0.254127461736714
"x2","~","x9",-0.480919657425727
"x5","~","eta6",0.508411349317238
"x5","~","x2",-0.0370676759032425
"eta5","~","x6",-0.44598370680968
"x7","~","eta5",0.173300942176088
"eta3","~","eta8",-0.738447356417741
"x1","~","eta1",0.554475527735513
"x8","~","eta6",0.932713782436337
"x3","~","eta8",0.372828906473733
"x10","~","eta5",0.132494806261498
"x4","~","x9",0.986417654659806
"eta2","~","eta3",0.295415783565993
"y10","~~","y10",2.51845351688062
"y2","~~","y2",0.414242834590084
"y3","~~","y3",1.09977457955748
"y4","~~","y4",0.140035569939201
"y5","~~","y5",0.271325512853619
"y7","~~","y7",0.0433854454759208
"y8","~~","y8",0.773524696438829
"y9","~~","y9",0.114675007179634
"y11","~~","y11",1.06950083255831
"y12","~~","y12",0.7923016790155
"y13","~~","y13",0.0389342710586115
"y14","~~","y14",0.861749470093408
"y15","~~","y15",0.0113646254341795
"y16","~~","y16",0.179792094701872
"y17","~~","y17",0.997799316313373
"y18","~~","y18",0.315787011511004
"y19","~~","y19",1.08488237850141
"y20","~~","y20",0.621245884105176
"y21","~~","y21",0.810362608748316
"x2","~~","x2",2.33469921071177
"x5","~~","x5",0.342949983277376
"x7","~~","x7",0.0320334562872044
"x1","~~","x1",0.351997503659119
"x8","~~","x8",0.937341237830789
"x3","~~","x3",0.119815719868586
"x10","~~","x10",0.0166778075602576
"x4","~~","x4",0.93404152017554
"eta1","~~","eta1",2.13991871217617
"eta2","~~","eta2",0.0502930871671769
"eta3","~~","eta3",0.3834816898548
"eta4","~~","eta4",1.45797303744347
"eta5","~~","eta5",0.212093904450673
"eta6","~~","eta6",1.65031339736978
"eta7","~~","eta7",1.52856822781147
"eta8","~~","eta8",1.43273489045605
"eta1","~~","eta4",-0.0669138887119444
"eta1","~~","eta6",-0.0334265140314681
"eta1","~~","eta7",0.0737086166583519
"eta1","~~","eta8",0.283917193917462
"eta4","~~","eta6",0.107810831707224
"eta4","~~","eta7",-0.0631914798165985
"eta4","~~","eta8",0.102359434055849
"eta6","~~","eta7",0.0445423763477816
"eta6","~~","eta8",0.120521831062749
"eta7","~~","eta8",-0.0834708371729749
"eta2","~~","x5",0.00395816551307976
"eta2","~~","x8",-0.0121367292454679
"eta2","~~","x3",-0.00593978207919635
"eta2","~~","x10",0.00374712563842219
"eta2","~~","x4",-0.00241573293028522
"x5","~~","x8",0.030241173612806
"x5","~~","x3",-0.0181549368406849
"x5","~~","x10",-0.00120959517078877
"x5","~~","x4",0.0121664212856302
"x8","~~","x3",0.0121227253545464
"x8","~~","x10",0.00554658370474278
"x8","~~","x4",-0.0643338931379328
"x3","~~","x10",-0.00325906142134457
"x3","~~","x4",0.0232368675639073
"x10","~~","x4",-0.0108423439397277
