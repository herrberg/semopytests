#(Method)|  N_nonconv|    Mean OF|  Median OF| Mean error| Median error
\textbf{semopy}/ULS&        78&12.2871&5.7176&0.1365&0.1286&38 \\ \hline
\textbf{lavaan}/ULS&        77&12.2871&5.7176&0.1352&0.1281&255 \\ \hline


Diff of nonconv-sets:
lavULS              ( 77)|semopyULS           ( 78) :   6/  7
           semopyULS  lavULS
semopyULS       78.0     7.0
lavULS           6.0    77.0



lavULS:
[  5  35  48  57  65  74  77  83  84  98 103 112 120 124 141 152 156 172
 188 198 206 212 214 219 237 244 252 282 284 298 300 314 318 359 401 431
 481 484 529 537 563 566 577 582 585 588 591 610 630 642 647 650 695 696
 707 727 765 779 793 834 837 857 874 877 895 900 910 942 945 950 955 964
 969 978 988 989 990]
semopyULS:
[  5  48  57  65  74  77  83  84  98 103 124 141 152 156 167 172 188 198
 206 212 214 219 237 244 252 282 284 298 300 314 318 336 358 359 398 401
 430 431 447 481 484 529 537 563 566 577 582 585 588 591 607 610 630 642
 650 695 696 707 727 765 779 793 834 837 857 874 877 895 900 910 945 955
 964 969 978 988 989 990]

Did converge for semopyULS      , but didn't for lavULS         :
[ 35 112 120 647 942 950]
Did converge for lavULS         , but didn't for semopyULS      :
[167 336 358 398 430 447 607]