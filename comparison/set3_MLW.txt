#(Method)|  N_nonconv|    Mean OF|  Median OF| Mean error| Median error
\textbf{semopy}/MLW&        13&3.4445&3.1029&0.0864&0.0690&41 \\ \hline
\textbf{lavaan}/MLW&        13&3.4445&3.1029&0.0865&0.0690&208 \\ \hline


Diff of nonconv-sets:
lavMLW              ( 13)|semopyMLW           ( 13) :   0/  0
           semopyMLW  lavMLW
semopyMLW       13.0     0.0
lavMLW           0.0    13.0



lavMLW:
[ 12  29  75 281 309 409 558 591 637 648 682 691 730]
semopyMLW:
[ 12  29  75 281 309 409 558 591 637 648 682 691 730]

Did converge for semopyMLW      , but didn't for lavMLW         :
[]
Did converge for lavMLW         , but didn't for semopyMLW      :
[]