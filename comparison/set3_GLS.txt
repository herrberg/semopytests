#(Method)|  N_nonconv|    Mean OF|  Median OF| Mean error| Median error
\textbf{semopy}/GLS&        42&11.1607&4.3359&0.0944&0.0703&39 \\ \hline
\textbf{lavaan}/GLS&        56&11.1607&4.3359&0.0923&0.0694&347 \\ \hline


Diff of nonconv-sets:
lavGLS              ( 56)|semopyGLS           ( 42) :  26/ 12
           semopyGLS  lavGLS
semopyGLS       42.0    12.0
lavGLS          26.0    56.0



lavGLS:
[ 12  24  29  36  48  62  66  75  93 114 148 175 202 221 246 256 278 281
 309 318 330 356 367 389 406 409 441 468 496 518 534 558 591 597 619 624
 637 644 646 648 671 676 682 688 691 694 717 724 730 781 851 877 927 943
 972 981]
semopyGLS:
[ 12  29  36  62  66  75  77  79  93  96 114 161 202 246 278 281 309 330
 384 409 441 467 470 518 551 558 575 591 637 642 646 648 671 682 691 717
 724 730 833 943 962 981]

Did converge for semopyGLS      , but didn't for lavGLS         :
[ 24  48 148 175 221 256 318 356 367 389 406 468 496 534 597 619 624 644
 676 688 694 781 851 877 927 972]
Did converge for lavGLS         , but didn't for semopyGLS      :
[ 77  79  96 161 384 467 470 551 575 642 833 962]