#(Method)|  N_nonconv|    Mean OF|  Median OF| Mean error| Median error
\textbf{semopy}/GLS&        61&3.8458&2.3088&0.1250&0.1123&46 \\ \hline
\textbf{lavaan}/GLS&        74&3.8458&2.3088&0.1232&0.1109&675 \\ \hline


Diff of nonconv-sets:
lavGLS              ( 74)|semopyGLS           ( 61) :  19/  6
           semopyGLS  lavGLS
semopyGLS       61.0     6.0
lavGLS          19.0    74.0



lavGLS:
[  0   9  28  48  69  77  82 102 103 152 164 165 171 195 204 206 212 214
 219 237 244 252 254 255 282 284 291 298 300 319 331 371 430 431 447 477
 481 484 529 537 556 566 577 610 617 630 639 641 642 695 707 727 733 765
 766 774 777 785 793 802 837 843 856 857 874 895 900 918 923 926 950 988
 989 991]
semopyGLS:
[  0  48  57  70  77  83 102 103 123 152 164 165 171 204 206 212 214 219
 237 244 252 254 282 284 291 298 300 319 430 431 447 475 477 484 529 537
 566 577 585 610 630 642 695 707 727 765 766 785 793 802 837 857 874 895
 900 918 923 950 988 989 991]

Did converge for semopyGLS      , but didn't for lavGLS         :
[  9  28  69  82 195 255 331 371 481 556 617 639 641 733 774 777 843 856
 926]
Did converge for lavGLS         , but didn't for semopyGLS      :
[ 57  70  83 123 475 585]