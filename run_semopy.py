import pandas as pd
import argparse
import os
from utils import RESULTS_FOLDER, MODS_FOLDER
from semopy import Model
from semopy import Optimizer as Opt
from time import perf_counter
from tqdm import tqdm


def parse_argv():
    parser = argparse.ArgumentParser(description='Optimize generated models.')
    parser.add_argument('--models_range', nargs=2, type=int, default=(1, 1001))
    parser.add_argument('--objective', type=str, default='MLW')
    parser.add_argument('--method', type=str, default='SLSQP')
    parser.add_argument('--dir_name', type=str, default='mods')
    parser.add_argument('--dir_out_postfix', type=str, default='_semopy')
    return parser.parse_args()


def params_to_table(opt):
    params = opt.params
    mod = opt.model
    t = list()
    # Beta
    op = mod.operations.REGRESSION.value
    for i in range(*mod.beta_range):
        j = i - mod.beta_range[0]
        m, n = mod.parameters['Beta'][j]
        lval, rval = mod.beta_names[0][m], mod.beta_names[1][n]
        t.append({'lhs': lval, 'op': op, 'rhs': rval,
                  'est': params[i]})
    # Lambda
    op = mod.operations.MEASUREMENT.value
    for i in range(*mod.lambda_range):
        j = i - mod.lambda_range[0]
        m, n = mod.parameters['Lambda'][j]
        lval, rval = mod.lambda_names[1][n], mod.lambda_names[0][m]
        t.append({'lhs': lval, 'op': op, 'rhs': rval,
                  'est': params[i]})
    # Psi
    op = mod.operations.COVARIANCE.value
    for i in range(*mod.psi_range):
        j = i - mod.psi_range[0]
        m, n = mod.parameters['Psi'][j]
        lval, rval = mod.psi_names[0][m], mod.psi_names[1][n]
        t.append({'lhs': lval, 'op': op, 'rhs': rval,
                  'est': params[i]})
    # Theta
    op = mod.operations.COVARIANCE.value
    for i in range(*mod.theta_range):
        j = i - mod.theta_range[0]
        m, n = mod.parameters['Theta'][j]
        lval, rval = mod.theta_names[0][m], mod.theta_names[1][n]
        t.append({'lhs': lval, 'op': op, 'rhs': rval,
                  'est': params[i]})
    return pd.DataFrame(t, columns=['lhs', 'op', 'rhs', 'est'])


if __name__ == '__main__':
    args = parse_argv()
    dir_name = '{}/{}'.format(RESULTS_FOLDER, args.dir_name + args.dir_out_postfix)
    dir_mods = '{}/{}'.format(MODS_FOLDER, args.dir_name)
    if not os.path.exists(RESULTS_FOLDER):
        os.makedirs(RESULTS_FOLDER)
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    time_begin = perf_counter()
    for i in tqdm(range(*args.models_range)):
        filename_model = dir_mods + '/mod{}.txt'.format(i)
        filename_data = dir_mods + '/data{}.txt'.format(i)
        with open(filename_model) as f:
            mod = f.read()
        mod = Model(mod)
        data = pd.read_csv(filename_data)
        mod.load_dataset(data)
        opt = Opt(mod)
        opt.optimize(args.objective, method=args.method)
        params_to_table(opt).to_csv(dir_name + '/result{}.txt'.format(i),
                                    sep=',', index=False)
    time = perf_counter() - time_begin
    with open('{}/info.txt'.format(dir_name), 'w') as f:
        f.write(str(time) + '\n')
        f.write(str(args))